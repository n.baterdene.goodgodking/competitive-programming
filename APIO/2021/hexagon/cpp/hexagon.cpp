#include "hexagon.h"

#include <bits/stdc++.h>
using namespace std;
using ll = long long;
using PII = pair <ll, ll>;

int dx[] = {0, -1, 0, 1, 1, 0, -1};
int dy[] = {0, 0, 1, 1, 0, -1, -1};
ll n, A, B;
const int MOD = 1e9 + 7;
ll power(ll a, ll b, ll p) {
    ll res = 1;
    while(b > 0) {
        if (b & 1) res = res * a % p;
        a = a * a % p;
        b /= 2;
    }
    return res;
}
ll inv(ll a, ll p) {
    return power(a, p-2, p);
}
int draw_territory(int _n, int _A, int _B, std::vector<int> d, std::vector<int> l) {
// #define int long long
    n = _n;
    A = _A;
    B = _B;
    if (n == 3) {
        ll x = l[0]+1, y = l[0];
        ll res = (
            ( (x+1) * (x) / 2 % MOD * A % MOD) + 
            (B * 
                (
                    (y * (y+1) / 2) % MOD + 
                    y * (y + 1) % MOD * (2*y + 1) % MOD * inv(6, MOD) %MOD
                ) % MOD
            ) % MOD
        )%MOD;
        return (res % MOD + MOD) % MOD;
    }
    ll x, y, a, b, c, p;
    x = y = a = b = c = p = 0;
    for (int i = 0; i < n; i++) {
        x += dx[d[i]] * l[i];
        y += dy[d[i]] * l[i];
        a = min(a, x);
        b = min(b, y);
        c = max(c, x);
        p = max(p, y);
    }
    x = -a+1;
    y = -b+1;
    ll N = c - a + 1+5;
    ll M = p - b + 1+5;
    int dp[N][M]; memset(dp, 0, sizeof(dp));

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < l[i]; j++) {
            x += dx[d[i]];
            y += dy[d[i]];
            dp[x][y] = 1;
        }
    }
    x = 0, y = 0;
    queue <PII> bfs; bfs.push({x, y});
    dp[x][y] = -1;
    while(!bfs.empty()) {
        tie(x, y) = bfs.front();
        bfs.pop();
        for (int i = 1; i <= 6; i++) {
            int aa = x + dx[i];
            int bb = y + dy[i];
            if (aa < 0 || bb < 0 || aa >= N || bb >= M || dp[aa][bb] != 0) continue;
            dp[aa][bb] = -1;
            bfs.push({aa, bb});
        }
    }
    bfs.push({-a+1, -b+1});
    dp[-a+1][-b+1] = 2;
    ll res = 0;
    int cnt = 0;
    while(!bfs.empty()) {
        tie(x, y) = bfs.front();
        bfs.pop();
        res = (res + (dp[x][y]-2) * B % MOD + A) % MOD;
        for (int i = 1; i <= 6; i++) {
            int aa = x + dx[i];
            int bb = y + dy[i];
            if (aa < 0 || bb < 0 || aa >= N || bb >= M || (dp[aa][bb] > 1 || dp[aa][bb] < 0) ) continue;
            dp[aa][bb] = dp[x][y]+1;
            bfs.push({aa, bb});
        }
    }
    return (res%MOD+MOD)%MOD;
}
