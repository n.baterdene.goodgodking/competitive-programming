#include "roads.h"
#define pb push_back
#define ALL(x) x.begin(),x.end()
#include <bits/stdc++.h>
using namespace std;
using ll = long long;
using PII = pair <ll, ll>;
int n, m;
const int N = 1e5 + 5;
const int INF = 1e6;

vector <PII> paths[N];
vector <int> dp[N];
int solve(int pos, int lim) {
    
}
vector<ll> minimum_closure_costs(int _n, vector<int> a, vector<int> b, vector<int> c) {
    vector <ll> res;
    m = a.size();
    bool test1 = 1;
    for (int i = 0; i < m; i++) {
        a[i]++, b[i]++;
        if (a[i] > b[i]) swap(a[i], b[i]);
        test1 &= (a[i] == 0);
        paths[a[i]].pb({b[i], c[i]});
        paths[b[i]].pb({a[i], c[i]});
    }
    if (test1) {
        vector <int> edges;
        for (int i = 0; i < m; i++) edges.pb(c[i]);
        sort(ALL(edges));
        ll sum = 0; res.pb(sum);
        for (int i = 0; i < m; i++) {
            sum += edges[i];
            res.pb(sum);
        }
        reverse(ALL(res));
        // for (auto el : res) cout <<
        return res;
    }
    res = vector <ll>(n, 0);
    int mxSz = 0;
    for (int i = 1; i <= n; i++) {
        mxSz = max(mxSz, paths[i].size());
        dp[i].resize(paths[i].size()+1, -1);
    }
    for (int i = 0; i < mxSz; i++) res[i] = solve(1, i);

    return res;
}
