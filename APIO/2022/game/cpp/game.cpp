#include "game.h"
#define pb push_back
#include <bits/stdc++.h>
#define LINE "---------------------\n"
using namespace std;
using ll = long long;
using PII = pair <int, int>;
using VI = vector <int>;

const int N = 3e5 + 5;
const int M = 20;
int n, k;
vector <int> paths[N];
map <PII, int> has;
int reachMin[N];
bool vis[N];
void init(int _n, int _k) {
    n = _n;
    k = _k;
    has.clear();
    for (int i = 0; i+1 < k; i++) {
        paths[i].pb(i+1);
        has[{i, i+1}] = 1;
    }
}
void dfs(int pos) {
    vis[pos] = 1;
    reachMin[pos] = pos+1;
    for (auto el : paths[pos]) {
        if (!vis[el]) dfs(el);
        reachMin[pos] = min(reachMin[pos], reachMin[el]);
        reachMin[pos] = min(reachMin[pos], el);
    }
}
int add_teleporter(int u, int v) {
    if (has.count({u, v})) return 0;
    has[{u, v}] = 1;
    paths[u].pb(v);
    for (int i = 0; i < n; i++) vis[i] = 0;
    dfs(0);
    for (int i = 0; i < k; i++) 
        if (reachMin[i] <= i) return 1;
    return 0;
}
