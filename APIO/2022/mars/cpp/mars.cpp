
#include "mars.h"
#include <bits/stdc++.h>
using namespace std;
string process(vector <vector <string> > a, int r, int c, int k, int n) {

	int dx[] = {0, 0, 1, -1};
	int dy[] = {1, -1, 0, 0};
	int m = 2 * n + 1;
	string res = string(100, '0');
	if (k == 0) {
		for (int i = 0; i < 3; i++) 
		for (int j = 0; j < 3; j++) {
			int pos = (i+r)*m + (j + c);
			if (a[i][j][0] == '1') res[pos] = '1';
		}
	} else {
		for (int i = 0; i < 3; i++) 
		for (int j = 0; j < 3; j++) 
		for (int k = 0; k < 100; k++) {
			if (a[i][j][k] == '1') res[k] = '1';
		}
	}
	if (k == n-1) {
		string tmp = string(100, '0');
		vector < vector <bool> > vis(2 * n + 2, vector <bool>(2 * n + 2, 0));
 		function<void (int, int)> dfs = [&](int i, int j) {
 			if (vis[i][j]) return;
            vis[i][j] = 1;
            for (int d = 0; d < 4; d++) {
            	int ii = i + dx[d];
            	int jj = j + dy[d];
            	if (ii < 0 || jj < 0 || ii >= m || jj >= m || res[i*(2*n+1)+j] == '0') continue;
            	dfs(ii, jj);
            }
        };

		int ans = 0;
		for (int i = 0; i < m; i ++)
		for (int j = 0; j < m; j ++) {
			if (vis[i][j] || res[i*m+j] == '0') continue;
			dfs(i, j);
			ans++;
		}
		for (int i = 0; i < 20; i++) {
			if (ans & (1<<i)) tmp[i] = '1';
			else tmp[i] = '0';
		}
		res = tmp;
	}
	return res;
}
