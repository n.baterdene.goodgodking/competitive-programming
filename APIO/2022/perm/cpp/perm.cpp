
#include "perm.h"
#define pb push_back
#include <bits/stdc++.h>
using namespace std;
using ll = long long;
using VI = vector <int>;
const int M = 60;

void compress(VI&nums) {
	set <int> vals;
	for ( auto el : nums) vals.insert(el);
	int id = 0;
	map <int, int> comp;
	for (auto el : vals) comp[el] = id++;
	for (auto& el : nums) el = comp[el];
}

vector<int> construct_permutation(long long k) {
	VI res, bg;
	if (k & 1) {
		bg.pb(10000000);
		k--;
	}
	int pow = 1;
	while(1ll<<(pow+1) <= k) pow++;
	k -= (1ll<<pow);
	for (int i = 0; i < pow; i++) res.pb(i*100);
		pow = 1;
	for (int i = res.size()-1; i > 0; i--, pow++) {
		if (k & (1ll<<pow)) {
			bg.pb((res[i]+res[i-1]) / 2);
		}
	}
	swap(bg, res);
	for (auto el : bg) res.pb(el);
	compress(res);
	return res;
}