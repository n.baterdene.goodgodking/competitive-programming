#include "treasure.h"
#include <bits/stdc++.h>
#define pb push_back
#define ALL(x) x.begin(),x.end()
using namespace std;

vector<int> encode(vector<int> x,vector<int> y) {
    int n = x.size();
    vector <int> res;
    vector <pair <int, int> > pts;
    set <int> Y;
    for (int i = 0; i < n; i++) {
        Y.insert(y[i]);
        pts.pb({x[i], y[i]});
    }
    sort(ALL(pts));
    int p = 0;
    map <int, int> id;
    for (auto el : Y) id[el] = p++;
    int sum = 0, zero = 5e8;
    for (auto& [a, b] : pts) {
        sum += id[b];
        res.pb(a); // x
        res.pb(b+zero); // y
        res.pb(sum+zero+zero); // pos
    }
    return res;
}

vector<int> decode(vector<int> e) {
    vector <int> res;
    sort(ALL(e));
    map <int, int> x, y;
    int a, b, zero = 5e8, n = e.size() / 3; a = b = 0;
    int lol = -1;
    for (int i = 0; i < n; i++) {
        x[a++] = e[i];
        y[b] = e[i+n]-zero;
        if (y[b] != lol) {
            lol = e[i+n]-zero;
            b++;
        }
    }
    // for (int i = 0; i < n; i++) cout << x[i] << " \n"[i==n-1];
    // for (int i = 0; i < n; i++) cout << y[i] << " \n"[i==n-1];
    int last = zero+zero;
    for (int i = 2*n; i < 3*n; i++) {
        res.pb(x[i-2*n]);
        res.pb(y[e[i]-last]);
        last = e[i];
    }
    return res;
}
