#include <bits/stdc++.h>
using namespace std;
using ll = long long;
const int N = 1e6 + 5;


struct Line {
    ll a, b; // ax + b
};
ll n, A, B, C, ans;
ll dp[N], pre[N];
ll get(Line&l, ll x) {
	return l.a * x + l.b;
}
/* intersectX
	x * a.a + a.b = x * b.a + b.b
	a.b - b.b = x * (b.a - a.a);
	x = (a.b - b.b) / (b.a - a.a);

*/
long double intersectX(Line&a, Line&b) {
	// if (b.a == a.a) {
	// 	cout << "here: " << a.a << ',' << a.b << " " << b.a << ',' << b.b << "\n";
	// 	exit(0);
	// }
	return (a.b - b.b) / (b.a - a.a);
}
signed main() {
	// freopen("0.in", "r", stdin);
	// freopen("0.out", "w", stdout);
	cin >> n >> A >> B >> C;
	deque <Line> dq;
	dq.push_back({0, 0});
	for (int i = 1; i <= n; i++) {
		ll x; cin >> x;
		pre[i] = pre[i-1] + x;
		while (dq.size() > 1 && get(dq[0], pre[i]) <= get(dq[1], pre[i]))
			dq.pop_front();

		dp[i] = get(dq[0], pre[i]) + pre[i] * pre[i] * A + (pre[i] * B + C);
		ans = max(ans, dp[i]);
		Line curLine = {-2 * A * pre[i], pre[i] * pre[i] * A - pre[i] * B + dp[i]};
		// cout << curLine.a << ',' << curLine.b << ' ' << pre[i] << ": ";
		// cout << dq[0].a << "," << dq[0].b << " -> ";
		// if (dq.size() > 1)
		// 	cout << "(" << dp[i]-get(dq[0], pre[i])+get(dq[1], pre[i]) << ") ";

		while (dq.size() > 1 && intersectX(dq.back(), curLine) <= intersectX(dq[dq.size()-2], dq.back()))
			dq.pop_back();
		dq.push_back(curLine);
		// cout << dp[i] << "\n";
	}
	cout << dp[n] << '\n';
}