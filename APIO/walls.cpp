#include <bits/stdc++.h>
// #include "paint.h"
using namespace std;
int minimumInstructions(int n, int m, int k, vector <int>  colors, vector <int>  A, vector <vector <int> > B) {
    vector <vector <int> > colorWorker(k);
    vector <vector <int> > paintAble(2, vector <int> (m));
    vector <vector <int> > paintLen(2, vector <int> (m));
    for (int i = 0; i < m; i++) 
    for (auto color : B[i]) 
        colorWorker[color].push_back(i);

    int idx = 0;
    vector <int> ends(1, 0);
    for (int i = 0; i < n; i++) {
        bool ended = 0;
        for (auto worker : colorWorker[colors[i]]) {
            paintAble[idx][worker] = i;
            int pre = (worker ? worker-1 : m-1);
            if (paintAble[idx^1][pre] == i-1) paintLen[idx][worker] = paintLen[idx^1][pre]+1;
            else paintLen[idx][worker] = 1;
            if (paintLen[idx][worker] >= m) ended = 1;
        }
        if (ended) ends.push_back(i);
        idx ^= 1;
    }
    int end = -1, ans = 0;
    while(end < n-1) {
        auto it = --upper_bound(ends.begin(), ends.end(), end+m);
        if (*it == end) return -1;
        end = *it;
        ans++;
    }
    return ans;
}