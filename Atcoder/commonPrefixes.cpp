#include <bits/stdc++.h>
// #define int long long
using namespace std;
// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))

#define _upgrade ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define II <int, int>
#define Q queue
#define ff first
#define ss second
#define rs resize
#define ins insert 
#define fr front() 
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define PQ priority_queue
#define umap unordered_map
#define sz(x) (int)x.size()

typedef long long ll;
typedef double db;
typedef unsigned uint;
typedef unsigned long long ull;
typedef unordered_map<int, int> umapII;
// PQ going up <int, VI, greater<int> >
typedef vector<int> VI;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<bool> VB;
typedef vector<VB> VVB;
typedef vector<VVB> VVVB;
typedef vector<umapII> VumapII;
typedef vector<ll> VLL;
typedef vector<VLL> VVLL;
typedef vector<VVLL> VVVLL;
typedef vector<VVVLL> VVVVLL;
typedef vector<db> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<string, int> MSI;
typedef pair<ll, ll> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VPI> VVPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
typedef vector<VVVI> VVVVI;
typedef vector<VVVVI> VVVVVI;
mt19937 rng(chrono::high_resolution_clock::now().time_since_epoch().count() + reinterpret_cast<unsigned long>(new int) + *(new unsigned long));
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int bitCnt(int a) {
    bitset <64> b(a);
    return b.count();
}
int bitCnt(string a) {
    bitset <64> b(a);
    return b.count();
}
VI readVI(int n) {
    VI a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVI readVVI(int n, int m) {
    VVI a(n, VI(m));
    REP(i, n) a[i] = readVI(m);
    return a;
}
VLL readVLL(ll n) {
    VLL a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVLL readVVLL(ll n, ll m) {
    VVLL a(n, VLL(m));
    REP(i, n) a[i] = readVLL(m);
    return a;
}


int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}

void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VPI& a) {
    for (auto el : a) {
        cout << el.ff << ',' << el.ss << ' ';
    }
    cout << '\n';
}

void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VVI& a) {
    for (auto el : a) {
        print(el);
    }
}
const int MOD = 1e9 + 7;
const int MOD1 = 998244353;//119*2^23 +1;
const int INF = 2e9;
const ll INFF = INT64_MAX;
const db EPS = 1e-9;
const db PI = acos(-1.0); //M_PI;
const int moveX[] = {-1, 0, 1, 0};
const int moveY[] = {0, 1, 0, -1};
/*
|      ⣠⣶⡾⠏⠉⠙⠳⢦⡀⠀  ⠀⢠⠞⠉⠙⠲⡀
|    ⣴⠿⠏⠀⠀⠀⠀⠀⠀⢳⡀⠀  ⡏⠀⠀ ⠀⠀ ⢷
|⠀⠀⢠⣟⣋⡀⢀⣀⣀⡀⠀⣀⡀⣧⠀  ⢸⠀⠀⠀⠀   ⡇ ⠀
| ⠀⢸⣯⡭⠁⠸⣛⣟⠆⡴⣻⡲⣿   ⣸⠀⠀OK.  ⡇
|⠀⠀⣟⣿⡭⠀⠀⠀⠀⠀⢱⠀⠀⣿   ⢹⠀⠀⠀⠀⠀ ⡇
|⠀⠀⠙⢿⣯⠄⠀⠀⠀⢀⡀⠀⠀⡿⠀  ⡇⠀⠀ ⠀ ⡼
|⠀⠀⠀⠀⠹⣶⠆⠀⠀⠀⠀⠀⡴⠃ ⠀ ⠘⠤⣄⣠⠞⠀ ⠀
|⠀⠀⠀⠀⢸⣷⡦⢤⡤⢤⣞⣁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ ⠀
|⠀⢀⣤⣴⣿⣏⠁⠀⠀⠸⣏⢯⣷⣖⣦⡀⠀⠀⠀⠀⠀⠀
|⢀⣾⣽⣿⣿⣿⣿⠛⢲⣶⣾⢉⡷⣿⣿⠵⣿⠀⠀⠀⠀⠀
|⣼⣿⠍⠉⣿⡭⠉⠙⢺⣇⣼⡏⠀ ⠀⣄⢸⣿
|⣿⣿⣧⣀⣿.........⣀⣰⣏⣘⣆⣀
      
|                                                    _         _   _
|                                                   (_)       | | | |
 _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_
| '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|
| |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_
| .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|
| |               __/ |
|_|              |___/
 _   _      _ _    __        __         _     _ 
| | | | ___| | | __\ \      / /__  _ __| | __| |
| |_| |/ _ \ | |/ _ \ \ /\ / / _ \| '__| |/ _` |
|  _  |  __/ | | (_) \ V  V / (_) | |  | | (_| |
|_| |_|\___|_|_|\___/ \_/\_/ \___/|_|  |_|\__,_|                                  
 ____     __    _______    _______   __________
|     \  |  |  |  _____|  /  _____) |____  ____|
|  |\  \ |  |  | |__     (  (_____      |  |
|  | \  \|  |  |  __|     \_____  \     |  | 
|  |  \     |  | |_____    _____)  )    |  | 
|__|   \____|  |_______|  (_______/     |__| 
 
 
 


*/











ll n, M, sum;
string s;
void countSort(VI&sufArr, VI&p) {
    VI tmp(n), pos(n, 0);
    for (auto el : sufArr) 
        tmp[p[el]]++;
    for (int i = 1; i < n; i++)
        pos[i] = pos[i-1] + tmp[i-1];
    for(auto el : sufArr)
        tmp[pos[p[el]]++] = el;
    sufArr.swap(tmp);
}
const int N = 1e6+5;
VLL ans(N, 0), res(N, 0);
void add(set <PII>& bfs, ll val, int cnt = 1) {
    auto it = bfs.lower_bound({val, 0});
    PII addVal;
    sum += val * cnt;
    if (it != bfs.end() && it->ff == val) {
        bfs.erase(it);
        addVal = *it;
        addVal.ss += cnt;
    } else {
        addVal = {val, cnt};
    }
    bfs.insert(addVal);
}
void solve(VI&lcp) {
    /*
        f(i, j) = min(lcp[i...j]); f(i, j+1)
        int k = i-1;
        ans[i] = L(f(1, k) + f(2, k) + f(3, k) + ... f(k, k)) + R(f(i, i) + f(i, i+1), ... f(i, n))
    */
    sum = 0;
    set <PII> bfs;
    for (int i = 0; i < n-2; i++) {
        ll val = lcp[i];
        add(bfs, val);
        while(1) {
            auto it = (--bfs.end());
            if (it->ff <= val) break;
            PII cur = *it;
            bfs.erase(it);
            ll added = cur.ff * cur.ss;
            sum -= added;
            add(bfs, val, cur.ss);
        }
        ans[i+1] += sum;
    }  
    sum = 0;
    bfs.clear();
    for (int i = n-3; i >= 0; i--) {
        ll val = lcp[i];
        add(bfs, val);
        while(1) {
            auto it = (--bfs.end());
            if (it->ff <= val) break;
            PII cur = *it;
            bfs.erase(it);
            ll added = cur.ff * cur.ss;
            sum -= added;
            add(bfs, val, cur.ss);
        }
        ans[i] += sum;
    }  
}
VI suf;
VI genLCP() {
    s += '$';
    n = s.size();
    M = max(200ll, n);
    VI sufArr(n), p(n, 0), p1(n);
    {
        VVI arr;
        for (int i = 0; i < n; i++) 
            arr.pb({s[i], i});
        sort(ALL(arr));
        for (int i = 0; i < n; i++)
            sufArr[i] = arr[i][1];
        for (int i = 1; i < n; i++) {
            p[sufArr[i]] = p[sufArr[i-1]] + (arr[i][0] != arr[i-1][0]);
        }
    }
    for (int k = 0; (1<<k) < n; k++) {
        for (int i = 0; i < n; i++) sufArr[i] = (sufArr[i] - (1<<k) + n) % n;
        countSort(sufArr, p);
        p1[0] = 0;
        for (int i = 1; i < n; i++) {
            PII past = {p[sufArr[i-1]], p[(sufArr[i-1] + (1<<k))%n]};
            PII cur = {p[sufArr[i]], p[(sufArr[i] + (1<<k))%n]};
            p1[sufArr[i]] = p1[sufArr[i-1]] + (past != cur);
        }
        p.swap(p1);
    }
    VI LCP(n-2);  
    int equal = 0;
    for (int i = 0; i < n-1; i++) {
        int pos = p[i], pos1 = sufArr[pos-1];
        while(s[i + equal] == s[pos1 + equal]) equal++;
        if (pos - 2 >= 0)
            LCP[pos-2] = equal;
        equal = max(0, equal-1);
    }
    suf.swap(sufArr);
    return LCP;
}
void go () {
    cin >> n >> s;
    VI LCP = genLCP();
    solve(LCP); 
    for (int i = 0; i < n; i++) {
        res[suf[i]] = (i > 0 ? ans[i-1] : 0);    
    }
    for (int i = 0; i < n-1; i++) {
        ll tmp = n - 1 - i;
        cout << res[i] + tmp  << " \n";
    }
}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
#endif
    _upgrade
    int T = 1;
    while(T--) go();
    return 0;
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














