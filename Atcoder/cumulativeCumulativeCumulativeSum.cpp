#include <bits/stdc++.h>
// #pragma GCC optimize("O3, unroll-loops")
// #pragma GCC target("avx2")
// #define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define sz(x) (int)x.size()

using ll = long long;
using db = double;
using ld = long double;
using uint = unsigned;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VS = vector<string>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using VD = vector<db>;
using PII = pair<int, int>;
using PLL = pair<ll, ll>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int bitCnt(int a) {
    bitset <64> b(a);
    return b.count();
}
int bitCnt(string a) {
    bitset <64> b(a);
    return b.count();
}

int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}

void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VPI& a) {
    for (auto el : a) {
        cout << el.ff << ',' << el.ss << ' ';
    }
    cout << '\n';
}

void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VVI& a) {
    for (auto el : a) {
        print(el);
    }
}

/*

|                    _                                                     
|  __   _           | |                                                    
| |   \| | ___  ___ | |_                                                   
| | |\ | |/ _ \/ __/| __|                                                  
| | | \  |  __/\__ \| |_                                                   
| |_|  \_|\___ /___/ \__|                                                  
|                                                      _         _   _     
|                                                     (_)       | | | |    
|  _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_   
| | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|  
| | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_   
| | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|  
| | |               __/ |                                                  
| |_|              |___/                   _                               
|                                  _      (_)                              
|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                               
| | '_ ` _ \ / _` |_   / _` |/ _` | |/ _` | |                              
| | | | | | | (_|  /  / (_| | (_| | | (_| | |                              
| |_| |_| |_|\__,_|____\__,_|\__,_|_|\__,_|_|                              
*/

const db PI = acos(-1.0); //M_PI;
const ll INFF = 4e18;
const int INF = 1.07e9;
const int MOD = 998244353; //7*19*2^23 +1;
const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0, 1, 0, -1};





const int N = 2e5 + 5;
const int M = (1<<19) + 5;

int nums[N];

int n, m, k, q;
string s;
struct Node {
    ll sum;
    ll lazy1, lazy2;
};
Node node[M];
ll a[N];
int w[N], wp[N];
void propagate(int head, int l, int r) {
    if (node[head].lazy1 == 0 && node[head].lazy2 == 0) return;
    ll y = node[head].lazy1; node[head].lazy1 = 0;
    ll x = node[head].lazy2; node[head].lazy2 = 0;
    int mid = (l+r)>>1;
    // cout << "PROP " << l << ',' << r << '\n' ;
    { // lhs
        ll len = mid - l + 1;
        Node& cur = node[head*2+1];
        cur.sum = (cur.sum + len * y % MOD) % MOD;
        cur.lazy1 = (cur.lazy1 + y % MOD) % MOD;
        cur.sum = (cur.sum + len * (len + 1) / 2 % MOD * x % MOD) % MOD; // 
        cur.lazy2 = (cur.lazy2 + x) % MOD;
        y = (y + len * x % MOD) % MOD;
        // cout << "L: " << cur.sum << '\n';
    } 
    { // rhs
        ll len = r - mid;
        Node& cur = node[head*2+2];
        cur.sum = (cur.sum + len * y % MOD) % MOD;
        cur.lazy1 = (cur.lazy1 + y % MOD) % MOD;
        cur.sum = (cur.sum + len * (len + 1) / 2 % MOD * x % MOD) % MOD;
        cur.lazy2 = (cur.lazy2 + x) % MOD;
        // cout << "R: " << cur.sum << '\n';
    } 
}
void update(int l, int r, int L, int R, ll val, int head) {
    if (l > R || L > r) return;
    if (L <= l && r <= R) {
        ll st = l - L, len = r - l + 1;
        ll x = val, y = st * val%MOD;
        node[head].sum = (node[head].sum + len * y % MOD) % MOD;
        node[head].lazy1 = (node[head].lazy1 + y % MOD) % MOD;
        node[head].sum = (node[head].sum + len * (len + 1) / 2 % MOD * x % MOD) % MOD;
        node[head].lazy2 = (node[head].lazy2 + x) % MOD;
        // cout << l << "," << r << ": " << x << ',' << y << " -> " << node[head].sum << ' ' << node[head].lazy1 << " " << node[head].lazy2 << '\n';
        return;
    }
    int mid = (l+r)>>1;
    propagate(head, l, r);
    update(l, mid, L, R, val, head*2+1);
    update(mid+1, r, L, R, val, head*2+2);
    node[head].sum = (node[head*2+1].sum + node[head*2+2].sum) % MOD;
}
ll query(int l, int r, int R, int head) {
    if (l > R) return 0;
    if (r <= R) {
        // cout << l << "," << r << ": " << node[head].sum  << '\n';
        return node[head].sum; 
    }
    int mid = (l+r)>>1;
    propagate(head, l, r);
    return query(l, mid, R, head*2+1) + query(mid+1, r, R, head*2+2);
}
void go () {
    cin >> n >> m;
    for (int i = 1; i <= n; i++) w[i] = (w[i-1]+i) % MOD;
    for (int i = 1; i <= n; i++) {
        cin >> a[i];
        update(1, n, i, n, a[i], 0);
    }
    for (int j = 1; j <= n; j++) cout << query(1, n, j, 0) << " \n"[j==n];
    for (int i = 1; i <= m; i++) {
        int x, y; cin >> x;
        if (x == 1) {
            cin >> x >> y;
            update(1, n, x, n, -a[x], 0);
            a[x] = y;
            update(1, n, x, n, a[x], 0);
        } else {
            cin >> x;
            cout << query(1, n, x, 0) << '\n';
        }

    }
}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("0.in", "r", stdin);
    freopen("0.out", "w", stdout);
#endif
    _upgrade
    int T = 1;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














