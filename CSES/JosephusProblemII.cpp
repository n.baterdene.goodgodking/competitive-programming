#include <bits/stdc++.h>
#pragma GCC optimize("O3, unroll-loops")
#pragma GCC target("avx2")
// #define int long long
using namespace std;
 
#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define sz(x) (int)x.size()
 
using ll = long long;
using db = double;
using ld = long double;
using uint = unsigned;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VS = vector<string>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using VD = vector<db>;
using PII = pair<int, int>;
using PLL = pair<ll, ll>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int bitCnt(int a) {
    bitset <64> b(a);
    return b.count();
}
int bitCnt(string a) {
    bitset <64> b(a);
    return b.count();
}
 
int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}
 
void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VPI& a) {
    for (auto el : a) {
        cout << el.ff << ',' << el.ss << ' ';
    }
    cout << '\n';
}
 
void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VVI& a) {
    for (auto el : a) {
        print(el);
    }
}
 
/*
 
|                    _                                                     
|  __   _           | |                                                    
| |   \| | ___  ___ | |_                                                   
| | |\ | |/ _ \/ __/| __|                                                  
| | | \  |  __/\__ \| |_                                                   
| |_|  \_|\___ /___/ \__|                                                  
|                                                      _         _   _     
|                                                     (_)       | | | |    
|  _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_   
| | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|  
| | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_   
| | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|  
| | |               __/ |                                                  
| |_|              |___/                   _                               
|                                  _      (_)                              
|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                               
| | '_ ` _ \ / _` |_   / _` |/ _` | |/ _` | |                              
| | | | | | | (_|  /  / (_| | (_| | | (_| | |                              
| |_| |_| |_|\__,_|____\__,_|\__,_|_|\__,_|_|                              
*/
 
const db PI = acos(-1.0); //M_PI;
const ll INFF = 4e18;
const int INF = 1.07e9;
const int MOD = 1e9 + 7;
const int MOD1 = 998244353; //7*19*2^23 +1;
const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0, 1, 0, -1};
 
 
 
 
 
const int N = 4e5 + 5;
const int M = (1<<20) + 5;
 
ll pos[N], nums[N];
int node[M];
int n, m, k;

void build(int l, int r, int head) {
    if (l == r) {
        node[head] = 1;
        return;
    }
    int mid = (l+r)>>1;
    node[head] = r - l + 1;
    build(l, mid, head*2+1);
    build(mid+1, r, head*2+2);
}
bool found;
int res, st, ed;
int solve(int l, int r, int st, int len, int head) {
    if (found || r < st) return 0;
    if (st <= l && node[head] < len) return node[head];
    if (l == r) {
        if (node[head] == len) {
            res = r;
            found = 1;
        }
        return node[head];
    }
    int mid = (l+r)>>1;
    int lhs = solve(l, mid, st, len, head*2+1);
    int rhs = solve(mid+1, r, st, len-lhs, head*2+2);
    return lhs+rhs;
}
void update(int l, int r, int id, int head) {
    if (l == r) {
        node[head] = 0;
        return;
    }
    int mid = (l+r)>>1;
    if (id <= mid) update(l, mid, id, head*2+1);
    else update(mid+1, r, id, head*2+2);
    node[head] = node[head*2+1] + node[head*2+2];
}
void dfs(int l, int r, int head) {
    if (l == r) {
        cout << node[head] << " \n"[r==ed];
        return;
    }
    int mid = (l+r)>>1;
    dfs(l, mid, head*2+1);
    dfs(mid+1, r, head*2+2);
}
void go () {
    cin >> n >> k;
    st = 0, ed = 2*n-1;
    build(st, ed, 0);
    int pos = 0, left = n;
    k++;
    for (int i = 1; i <= n; i++, left--) {
        int tar = k%left;
        if (tar == 0) tar = left;
        found = 0; solve(st, ed, pos, tar, 0);
        cout << res%n+1 << " \n"[i==n];
        update(st, ed, res%n, 0);
        update(st, ed, res%n+n, 0);
        pos = res%n;
    }

}
 
 
 
signed main () {
 
#ifndef ONLINE_JUDGE
    freopen("0.in", "r", stdin);
    freopen("0.out", "w", stdout);
#endif
    _upgrade
    int T = 1;
    // cin >> T;
    while(T--) go();
    return (0-0); //<3
}
 
/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/
 
 
 
 
 





