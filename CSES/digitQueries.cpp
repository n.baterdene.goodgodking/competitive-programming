#include <bits/stdc++.h>
using namespace std;
using ll = long long;
ll n, m, k, q;
signed main() {
	ios::sync_with_stdio(0);
	cin.tie(0);
	cout.tie(0);
	// freopen("0.in", "r", stdin);
	// freopen("0.out", "w", stdout);
	cin >> q;
	while(q--) {
		cin >> n;
		ll st = 1, cur = 1, curCnt = 9, pre = 0, res;
		if (n <= curCnt) {
			cout << n << '\n';
			continue;
		}
		while((n - pre + cur - 1) / cur > curCnt) {
			pre += cur * curCnt;
			curCnt *= 10;
			cur++;
		}
		st = curCnt / 9;
		if ((n - pre)%cur == 0) {
			res = (st + (n - pre) / cur - 1)%10;
		} else {
			string s = to_string(st+(n - pre)/cur);
			int digit = (n - pre)%cur - 1;
			res = s[digit]-'0';
		}
		cout << res << '\n';
	}
}