#include <bits/stdc++.h>
// #pragma GCC optimize("unroll-loops")
// #pragma GCC target("avx2")
// #define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define sz(x) (int)x.size()

using ll = long long;
using db = double;
using ld = long double;
using uint = unsigned;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VS = vector<string>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using VD = vector<db>;
using PII = pair<int, int>;
using PLL = pair<ll, ll>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int bitCnt(int a) {
    bitset <64> b(a);
    return b.count();
}
int bitCnt(string a) {
    bitset <64> b(a);
    return b.count();
}

int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}

void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VPI& a) {
    for (auto el : a) {
        cout << el.ff << ',' << el.ss << ' ';
    }
    cout << '\n';
}

void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VVI& a) {
    for (auto el : a) {
        print(el);
    }
}

/*

|                    _                                                     
|  __   _           | |                                                    
| |   \| | ___  ___ | |_                                                   
| | |\ | |/ _ \/ __/| __|                                                  
| | | \  |  __/\__ \| |_                                                   
| |_|  \_|\___ /___/ \__|                                                  
|                                                      _         _   _     
|                                                     (_)       | | | |    
|  _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_   
| | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|  
| | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_   
| | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|  
| | |               __/ |                                                  
| |_|              |___/                   _                               
|                                  _      (_)                              
|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                               
| | '_ ` _ \ / _` |_   / _` |/ _` | |/ _` | |                              
| | | | | | | (_|  /  / (_| | (_| | | (_| | |                              
| |_| |_| |_|\__,_|____\__,_|\__,_|_|\__,_|_|                              
*/

const db PI = acos(-1.0); //M_PI;
const ll INFF = 4e18;
const int INF = 1.07e9;
const int MOD = 1e9 + 7;
const int MOD1 = 998244353; //7*19*2^23 +1;
const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0, 1, 0, -1};





const int N = 2e5 + 5;
const int M = 2e5 + 5;

int nums[N];

int n, m, k, q;
string s;

struct Request {
    pair <char, int> req[2];
    int used;
    Request(char a, int b, char c, int d) {
        used = 0;
        req[0] = {a, b};
        req[1] = {c, d};
    }
};
const char A = '+';
const char R = '-';
vector <Request> requests;
char state[N];
char rev(char c) {
    return A+R - c;
}
vector <int> children[N];
bool triedToFix[N];

stack <int> changedIds;
stack <VI> changedChildren;
vector <int> innocent;
bool fix(int id) {
    // cout << "FIX: " << id << '\n';
    if (triedToFix[id]) return 0;
    // cout << "OPEN FIX: " << id << '\n';
    triedToFix[id] = 1;
    vector <int> tmp = children[id];
    children[id].clear();
    state[id] = rev(state[id]);

    changedIds.push(id);
    changedChildren.push(tmp);

    for (int tried = 0; tried < 2; tried++)
    for (auto el : tmp) {
        Request& cur = requests[el];
        int j = cur.used^1;
        char POS = cur.req[j].ff;
        char REV = rev(POS);
        if (state[cur.req[cur.used].ss] == cur.req[cur.used].ff) continue;
        if (state[cur.req[j].ss] == REV) {
            // cout << "HERE: " << el << ' ' << cur.req[j].ss << ' ' << cur.req[j].ff << "," << state[cur.req[j].ss] << '\n';
            if (!fix(cur.req[j].ss)) return 0;
        }
        state[cur.req[j].ss] = POS;
        children[cur.req[j].ss].pb(el);
        cur.used = j;
        
    }
    for (auto el : tmp) {
    }
    innocent.pb(id);
    return 1;
}

void go () {
    cin >> n >> m;
    for (int i = 0; i < n; i++) {
        char a; int b; char c; int d;
        cin >> a >> b >> c >> d;
        requests.pb(Request(a, b, c, d));
    }
    for (int i = 1; i <= m; i++) state[i] = '.';
    bool changed = 1;
    int satisfied = 0, last = -1, curCnt = 0;
    while(changed) {
        changed = 0;
        curCnt = 0;
        // cout << "\n\nSTART\n";
        for (int i = 1; i <= m; i++) {
            triedToFix[i] = 0;
            children[i].clear();
        }
        for (int i = 0; i < n; i++) {
            // cout << LINE;
            // if (i > 0) cout << requests[i-1].req[0].ff << requests[i-1].req[0].ss << ' ' << requests[i-1].req[1].ff << requests[i-1].req[1].ss << '\n';
            // for (int j = 1; j <= m; j++) cout << state[j] << " \n"[j==m];
            bool done = 0;
            for (int j = 0; j < 2 && !done; j++) {
                Request& cur = requests[i];
                char POS = cur.req[j].ff;
                char REV = rev(POS);
                if (state[cur.req[j].ss] != REV) {
                    state[cur.req[j].ss] = POS;
                    children[cur.req[j].ss].pb(i);
                    cur.used = j;
                    done = 1;
                }
            }
            if (!done)
            for (int j = 0; j < 2 && !done; j++) {
                // cout << "TRY\n";
                Request& cur = requests[i];
                if (fix(cur.req[j].ss)) {
                    done = 1;
                    cur.used = j;
                    state[cur.req[j].ss] = cur.req[j].ff;
                    children[cur.req[j].ss].pb(i);
                } else {
                    while(!changedIds.empty()) {
                        int x = changedIds.top(); changedIds.pop();
                        VI tmp = changedChildren.top(); changedChildren.pop();
                        children[x] = tmp;
                        state[x] = rev(state[x]);
                    }
                }
                for (auto el : innocent) {triedToFix[el] = 0;/* cout << "RESET: " << el << "\n";*/} innocent.clear();
                while(!changedIds.empty()) {
                    changedIds.pop();
                    changedChildren.pop();
                }
            }
            if (!done) {
                break;
            } else {
                curCnt++;
            }
        }

        // cout << LINE;
        // int i = n-1; cout << requests[i].req[0].ff << requests[i].req[0].ss << ' ' << requests[i].req[1].ff << requests[i].req[1].ss << '\n';
        // for (int j = 1; j <= m; j++) cout << state[j] << " \n"[j==m];
        if (last < curCnt) {
            changed = 1;
            last = curCnt;
        }
        if (!changed && curCnt < n) {
            cout << "IMPOSSIBLE\n";
            return;
        }
    }
    for (int i = 1; i <= m; i++) cout << (state[i] == '.' ? '+' : state[i]) << " \n"[i==m];

}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("0.in", "r", stdin);
    freopen("0.out", "w", stdout);
#endif
    _upgrade
    int T = 1;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














