#include <bits/stdc++.h>
#define pb push_back
#define ALL(x) x.begin(),x.end()
using namespace std;
signed main() {
	ios::sync_with_stdio(0);
	cin.tie(0);
	cout.tie(0);
	// freopen("0.in", "r", stdin);
	// freopen("0.out", "w", stdout);
	int n; cin >> n;
	vector <int> ans;
	ans.pb(0);
	for (int i = 0; i < n; i++) {
		vector <int> cur;
		for (auto el : ans) cur.pb(el|(1<<i));
			reverse(ALL(cur));
			for (auto el : cur) ans.pb(el);
	}
	for (auto el : ans) {
		for (int i = n-1; i >= 0; i--) cout << (el&(1<<i) ? 1 : 0); cout << '\n';
	}
}