#include <bits/stdc++.h>
// #pragma GCC optimize("O3, unroll-loops")
// #pragma GCC target("avx2")
// #define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define sz(x) (int)x.size()

using ll = long long;
using db = double;
using ld = long double;
using uint = unsigned;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VS = vector<string>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using VD = vector<db>;
using PII = pair<int, int>;
using PLL = pair<ll, ll>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int bitCnt(int a) {
    bitset <64> b(a);
    return b.count();
}
int bitCnt(string a) {
    bitset <64> b(a);
    return b.count();
}

int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}

void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VPI& a) {
    for (auto el : a) {
        cout << el.ff << ',' << el.ss << ' ';
    }
    cout << '\n';
}

void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VVI& a) {
    for (auto el : a) {
        print(el);
    }
}

/*

|                    _                                                     
|  __   _           | |                                                    
| |   \| | ___  ___ | |_                                                   
| | |\ | |/ _ \/ __/| __|                                                  
| | | \  |  __/\__ \| |_                                                   
| |_|  \_|\___ /___/ \__|                                                  
|                                                      _         _   _     
|                                                     (_)       | | | |    
|  _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_   
| | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|  
| | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_   
| | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|  
| | |               __/ |                                                  
| |_|              |___/                   _                               
|                                  _      (_)                              
|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                               
| | '_ ` _ \ / _` |_   / _` |/ _` | |/ _` | |                              
| | | | | | | (_|  /  / (_| | (_| | | (_| | |                              
| |_| |_| |_|\__,_|____\__,_|\__,_|_|\__,_|_|                              
*/

const db PI = acos(-1.0); //M_PI;
const ll INFF = 4e18;
const int INF = 1.07e9;
const int MOD = 1e9 + 7;
const int MOD1 = 998244353; //7*19*2^23 +1;
const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0, 1, 0, -1};





const int N = 2e5 + 5;
const int M = 18;

int nums[N], nx[N], lvl[N], par[N], pos[N], cycSize[N];
int vis[N];
int jump[N][M];
int n, m, k, q;
string s;
vector <int> paths[N];
int find(int a) {
    if (a == par[a]) return a;
    return par[a] = find(par[a]);
}
void merge(int a, int b) {
    a = find(a);
    b = find(b);
    par[b] = a;
}
int t;
void assignLvl(int a, int l) {
    lvl[a] = l;
    for (auto& el : paths[a]) {
        if (cycSize[el] != 0) continue;
        assignLvl(el, l+1);
    }
}
void solve(int a) {
    int x = a;
    t++;    
    while (vis[x] == 0) {
        vis[x] = t;
        x = nx[x];
    }
    if (vis[x] != t) return; // already computed
    int st = x, p = 0;
    pos[st] = p++;
    x = nx[st];
    int curSz = 1;
    while(x != st) {
        curSz++;
        pos[x] = p++;
        x = nx[x];
    }
    cycSize[st] = curSz;
    x = nx[st];
    assignLvl(st, 0);
    while(x != st) {
        cycSize[x] = curSz;
        assignLvl(x, 0);
        x = nx[x];
    }
    return;
}
void up(int& a, int x) {
    for (int i = 0; i < M; i++) 
        if (x & (1<<i)) a = jump[a][i];
}
void go () {
    cin >> n >> q;
    for (int i = 1; i <= n; i++) {
        cin >> nx[i];
        paths[nx[i]].pb(i);
    }
    iota(par+1, par+n+1, 1);
    for (int i = 1; i <= n; i++) {
        jump[i][0] = nx[i];
        merge(i, nx[i]);
    }
    for (int j = 1; j < M; j++)
    for (int i = 1; i <= n; i++) 
        jump[i][j] = jump[jump[i][j-1]][j-1];
    
    for (int i = 1; i <= n; i++) {
        if (vis[i]) continue;
        solve(i);
    }
    for (int i = 1; i <= q; i++) {
        int a, b; cin >> a >> b;
        if (find(a) != find(b) || lvl[a] < lvl[b]) {
            cout << "-1\n";
            continue;
        }
        int cur = lvl[a] - lvl[b];
        up(a, cur);
        if (lvl[b] != 0 && a != b) {
            cout << "-1\n";
            continue;
        }
        if (cycSize[b] != 0)
            cur += (pos[b] + cycSize[b] - pos[a]) % cycSize[b];
        cout << cur << '\n';
    }

}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("0.in", "r", stdin);
    freopen("0.out", "w", stdout);
#endif
    _upgrade
    int T = 1;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














