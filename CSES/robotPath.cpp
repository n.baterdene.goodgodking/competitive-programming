#include <bits/stdc++.h>
#define int long long
using namespace std;
 
#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define sz(x) (int)x.size()
 
using ll = long long;
using db = double;
using ld = long double;
using uint = unsigned;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VS = vector<string>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using VD = vector<db>;
using PII = pair<int, int>;
using PLL = pair<ll, ll>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int bitCnt(int a) {
    bitset <64> b(a);
    return b.count();
}
int bitCnt(string a) {
    bitset <64> b(a);
    return b.count();
}
 
int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}
 
void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VPI& a) {
    for (auto el : a) {
        cout << el.ff << ',' << el.ss << ' ';
    }
    cout << '\n';
}
 
void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VVI& a) {
    for (auto el : a) {
        print(el);
    }
}
 
/*
|      ⣠⣶⡾⠏⠉⠙⠳⢦⡀⠀  ⠀⢠⠞⠉⠙⠲⡀
|    ⣴⠿⠏⠀⠀⠀⠀⠀⠀⢳⡀⠀  ⡏⠀⠀ ⠀⠀ ⢷
|⠀⠀⢠⣟⣋⡀⢀⣀⣀⡀⠀⣀⡀⣧⠀  ⢸⠀⠀⠀⠀   ⡇ ⠀
| ⠀⢸⣯⡭⠁⠸⣛⣟⠆⡴⣻⡲⣿   ⣸⠀⠀OK.  ⡇
|⠀⠀⣟⣿⡭⠀⠀⠀⠀⠀⢱⠀⠀⣿   ⢹⠀⠀⠀⠀⠀ ⡇
|⠀⠀⠙⢿⣯⠄⠀⠀⠀⢀⡀⠀⠀⡿⠀  ⡇⠀⠀ ⠀ ⡼
|⠀⠀⠀⠀⠹⣶⠆⠀⠀⠀⠀⠀⡴⠃ ⠀ ⠘⠤⣄⣠⠞⠀ ⠀
|⠀⠀⠀⠀⢸⣷⡦⢤⡤⢤⣞⣁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ ⠀
|⠀⢀⣤⣴⣿⣏⠁⠀⠀⠸⣏⢯⣷⣖⣦⡀⠀⠀⠀⠀⠀⠀
|⢀⣾⣽⣿⣿⣿⣿⠛⢲⣶⣾⢉⡷⣿⣿⠵⣿⠀⠀⠀⠀⠀
|⣼⣿⠍⠉⣿⡭⠉⠙⢺⣇⣼⡏⠀ ⠀⣄⢸⣿
|⣿⣿⣧⣀⣿.........⣀⣰⣏⣘⣆⣀
                           
 ____     __ _______ _______ __________
|     \  |  |  _____/  _____|____  ____|
|  |\  \ |  | |__  (  (_____    |  |
|  | \  \|  |  __|  \_____  \   |  | 
|  |  \     | |_____ _____)  )  |  | 
|__|   \____|_______(_______/   |__|                        
                      _                         
    __   _           | |                         
|  |   \| | ___  ___ | |_                                   
|  | |\ | |/ _ \/ __/| __|                                   
|  | | \  |  __/\__ \| |_                                   
|  |_|  \_|\___ /___/ \__|                                   
|                                                      _         _   _
|                                                      (_)       | | | |
|   _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_
|  | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|
|  | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_
|  | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|
|  | |               __/ |                                 
|  |_|              |___/                  _                                       
|                                  _      (_)                            
|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                                 
| | '_ ` _ \ / _` |_   / _` |/ _` | |/ _` | |                                  
| | | | | | | (_|  /  / (_| | (_| | | (_| | |                                 
| |_| |_| |_|\__,_|____\__,_|\__,_|_|\__,_|_|                                  
*/
 
const db PI = acos(-1.0); //M_PI;
const ll INFF = 4e18;
const ll INF = 1e12;
const int MOD = 1e9 + 7;
const int MOD1 = 998244353; //7*17*2^23 +1;
const int dx[] = {1, 0, -1, 0};
const int dy[] = {0, 1, 0, -1};
 
 
 
 
const int N = 1e5 + 5;
const int M = 4*N;
struct Seg {
    ll x1, y1, x2, y2, id, type;
    bool operator < (const Seg& a) const {
        if (mp(x1, y1) == mp(a.x1, a.y1)) return type < a.type;
        return mp(x1, y1) < mp(a.x1, a.y1);
    }
} segments[N];
struct Interval {
    ll x, y, id;
    bool operator < (const Interval& a) const {
        return mp(x, y) < mp(a.x, a.y);
    }
};
int nums[N];
bool types[N];
int n, m, k, ans, lst = -1;
ll res = INF, x, y, a, b, X, Y, pre[N], yPos[N], xPos[N];
string s;
set <ll> ys;
map <ll, int> compY;
map <ll, vector <Interval> > lines[2];
set <ll> node[M];
void build(int l, int r, int head) {
    node[head].insert(INF);
    if (l == r) return;
    int mid = (l+r)>>1;
    build(l, mid, head*2+1);
    build(mid+1, r, head*2+2);
}
void update(int l, int r, int id, int val, int head) {
    if (val > 0) node[head].insert(val);
    else node[head].erase(-val);
    if (l == r) return;
    int mid = (l+r)>>1;
    if (id <= mid) update(l, mid, id, val, head*2+1);
    else update(mid+1, r, id, val, head*2+2);
}
void query(int l, int r, int L, int R, int id, int head) {
    if (l > R || L > r) return;
    if (L <= l && r <= R) {
        auto it = node[head].ub(id);
        if (*it != INF) {
            res = min(res, pre[(*it)-1] + abs(xPos[id]-xPos[*it])+1);
        }
        if (it != node[head].begin()) {
            it--;
            res = min(res, pre[id-1] + abs(yPos[id]-yPos[*it])+1);
        }
        return;
    }
    int mid = (l+r)>>1;
    query(l, mid, L, R, id, head*2+1);
    query(mid+1, r, L, R, id, head*2+2);
}
void go () {
    cin >> n;
    ans = n;
    map <char, int> Dir;
    char dirs[] = {'U', 'R', 'D', 'L'};
    for (int i = 0; i < 4; i++) Dir[dirs[i]] = i;
    ys.insert(y);
    vector <Seg> segs;
    Seg st, ed;
    for (int i = 1; i <= n; i++) {
        char c;
        int d, dir; cin >> c >> d;
        dir = Dir[c];
        tie(a, b) = {x, y};
        if (i > 1) {
            a += dx[dir];
            b += dy[dir];
        }
        x += dx[dir] * d;
        y += dy[dir] * d;
        if (lst == (dir+2)%4) {
            res = min(res, pre[i-1]);
        }
        bool swp = 0;
        if (mp(x, y) > mp(a, b)) swp = 1;
        if (swp) swap(a, x), swap(b, y);
        if (b != y) {
            segs.pb({x, y, a, b, i, 2}); // query
            segs.pb({a, b, x, y, i, 2}); // ed
        } else {
            segs.pb({x, y, a, b, i, 0}); // st
            segs.pb({a, b, x, y, -i, 1}); // ed
        }
        if (b == y) lines[1][y].pb({x, a, i});
        if (a == x) lines[0][x].pb({y, b, i});
        if (swp) swap(a, x), swap(b, y);
        pre[i] = pre[i-1] + d;
        yPos[i] = b;
        xPos[i] = a;
        ys.insert(y);
        ys.insert(b);
        lst = dir;
        // cout << i << ": " << a << ' ' << b << ' ' << x << ' ' << y << '\n';
        segments[i] = {a, b, x, y, i, 0};
    }
    res = min(res, pre[n]);
    {
        Y = 0;
        for (auto& el : ys) {
            compY[el] = Y++;
        }
    }
    sort(ALL(segs));
    build(0, Y-1, 0);
    for (auto& seg : segs) {
        if (seg.type == 2) {
            query(0, Y-1, compY[seg.y1], compY[seg.y2], seg.id, 0);
        } else  {
            update(0, Y-1, compY[seg.y1], seg.id, 0);
        }
    }
    auto dif = [&](Seg& a, Seg& b) {
        return min(
            abs(a.x1 - b.x1) + abs(a.y1 - b.y1),
            abs(a.x1 - b.x2) + abs(a.y1 - b.y2)
        );
    };
    for (int j = 0; j < 2; j++) {
        map <ll, vector <Interval> >& Line = lines[j];
        for (auto iter = Line.begin(); iter != Line.end(); iter++) {
            Interval b;
            set <Interval> has;
            vector <Interval>& curLines = iter->second;
            for (auto& line : curLines) {
                auto it = has.lb(line);
                if (it != has.end()) {
                    b = *it;
                    if (line.x <= b.x && b.x <= line.y) {
                        res = min(res, pre[line.id-1] + dif(segments[line.id], segments[b.id])+1);
                    }
                }
                    if (it != has.begin()) {
                        b = *(--it);
                        if (b.x <= line.x && line.x <= b.y) {
                            res = min(res, pre[line.id-1] + dif(segments[line.id], segments[b.id])+1);
                    }
                }
                has.insert(line);
            }
        }
    }
    cout << res << '\n';
}
 
 
signed main () {
 
#ifndef ONLINE_JUDGE
    freopen("0.in", "r", stdin);
    freopen("0.out", "w", stdout);
#endif
    _upgrade
    int T = 1;
    while(T--) go();
    return (0-0); //<3
}
 
/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/