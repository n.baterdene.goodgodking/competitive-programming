#include <bits/stdc++.h>
// #pragma GCC optimize("O3, unroll-loops")
// #pragma GCC target("avx2")
// #define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define sz(x) (int)x.size()

using ll = long long;
using db = double;
using ld = long double;
using uint = unsigned;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VS = vector<string>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using VD = vector<db>;
using PII = pair<int, int>;
using PLL = pair<ll, ll>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int bitCnt(int a) {
    bitset <64> b(a);
    return b.count();
}
int bitCnt(string a) {
    bitset <64> b(a);
    return b.count();
}

int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}

void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VPI& a) {
    for (auto el : a) {
        cout << el.ff << ',' << el.ss << ' ';
    }
    cout << '\n';
}

void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VVI& a) {
    for (auto el : a) {
        print(el);
    }
}

/*

|                    _                                                     
|  __   _           | |                                                    
| |   \| | ___  ___ | |_                                                   
| | |\ | |/ _ \/ __/| __|                                                  
| | | \  |  __/\__ \| |_                                                   
| |_|  \_|\___ /___/ \__|                                                  
|                                                      _         _   _     
|                                                     (_)       | | | |    
|  _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_   
| | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|  
| | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_   
| | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|  
| | |               __/ |                                                  
| |_|              |___/                   _                               
|                                  _      (_)                              
|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                               
| | '_ ` _ \ / _` |_   / _` |/ _` | |/ _` | |                              
| | | | | | | (_|  /  / (_| | (_| | | (_| | |                              
| |_| |_| |_|\__,_|____\__,_|\__,_|_|\__,_|_|                              
*/

const db PI = acos(-1.0); //M_PI;
const ll INFF = 4e18;
const int INF = 1.07e9;
const int MOD = 1e9 + 7;
const int MOD1 = 998244353; //7*19*2^23 +1;
const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0, 1, 0, -1};





const int N = 2e5 + 5;
const int M = (1<<19) + 5;

int nums[N];
struct Node {
    ll sum, cnt;
};
Node node[M];
int n, m, k, q, tar;
string s;
map <int, int> comp;
ll pmoc[N];
int x;

void compress() {
    set <int> vals;
    for (int i = 1; i <= n; i++) vals.insert(nums[i]);
    for (auto el : vals) {
        pmoc[x] = el;
        comp[el] = x++;
    }
    for (int i = 1; i <= n; i++) nums[i] = comp[nums[i]];
}
Node merge(Node a, Node b) {
    return {a.sum + b.sum, a.cnt + b.cnt};
}
void update(int l, int r, int id, int val, int head) {
    if (l == r) {
        node[head].sum += val * pmoc[id];
        node[head].cnt += val;
        return;
    }
    int mid = (l+r)>>1;
    if (id <= mid) update(l, mid, id, val, head*2+1);
    else update(mid+1, r, id, val, head*2+2);
    node[head] = merge(node[head*2+1], node[head*2+2]);
    return;
}
int res;
int get(int l, int r, int tar, int head) {
    if (res != -1) return 0;
    if (node[head].cnt < tar) return node[head].cnt;
    if (l == r && node[head].cnt >= tar) {
        res = l;
        return l;
    }
    int mid = (l+r)>>1;
    int lhs = get(l, mid, tar, head*2+1);
    int rhs = get(mid+1, r, tar-lhs, head*2+2);
    return lhs+rhs;
}
Node sum(int l, int r, int L, int R, int head) {
    if (l > R || L > r) return {0, 0};
    if (L <= l && r <= R) return node[head];
    int mid = (l+r)>>1;
    return merge(
        sum(l, mid, L, R, head*2+1), 
        sum(mid+1, r, L, R, head*2+2)
    );
}
void go () {
    cin >> n >> m; tar = (m+1)/2;
    for (int i = 1; i <= n; i++) cin >> nums[i];
    compress();
    for (int i = 1; i <= m-1; i++) update(0, x, nums[i], 1, 0);
    for (int l = 1, r = m; r <= n; l++, r++) {
        update(0, x, nums[r], 1, 0);
        res = -1; get(0, x, tar, 0); 
        ll tmp = res;
        // cout << "median: " << pmoc[tmp] << '\n';
        Node a = sum(0, x, 0, tmp-1, 0);
        // cout << "LHS: " << a.cnt << ' ' << a.sum << '\n';
        ll lhs = a.cnt * pmoc[tmp] - a.sum;
        Node b = sum(0, x, tmp+1, x, 0);
        // cout << "RHS: " << b.cnt << ' ' << b.sum << '\n';
        ll rhs = b.sum - b.cnt * pmoc[tmp];
        cout << lhs+rhs << " \n"[r==n];
        update(0, x, nums[l], -1, 0);
    }
}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("0.in", "r", stdin);
    freopen("0.out", "w", stdout);
#endif
    _upgrade
    int T = 1;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














