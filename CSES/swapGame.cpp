#include <bits/stdc++.h>
// #pragma GCC optimize("O3, unroll-loops")
// #pragma GCC target("avx2")
// #define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define ub upper_bound 
#define umap unordered_map
#define sz(x) (int)x.size()
#define chmin(a,b) a=min(a,b)
#define chmax(a,b) a=max(a,b)

using db = double;
using ld = long double;
using sint = short int;
using ll = long long;
using uint = unsigned int;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using PII = pair<int, int>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using PLL = pair<ll, ll>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}
/*

|                    _                                                     
|  __   _           | |                                                    
| |   \| | ___  ___ | |_                                                   
| | |\ | |/ _ \/ __/| __|                                                  
| | | \  |  __/\__ \| |_                                                   
| |_|  \_|\___ /___/ \__|                                                  
|                                                      _         _   _     
|                                                     (_)       | | | |    
|  _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_   
| | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|  
| | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_   
| | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|  
| | |               __/ |                                                  
| |_|              |___/                   _                               
|                                  _      (_)                              
|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                               
| | '_ ` _ \ / _` |_   / _` |/ _` | |/ _` | |                              
| | | | | | | (_|  /  / (_| | (_| | | (_| | |                              
| |_| |_| |_|\__,_|____\__,_|\__,_|_|\__,_|_|                              
*/

const db PI = acos(-1.0); //M_PI;
const ll INFF = 4e18;
const int INF = 1.07e9;
const int MOD = 1e9 + 7;
const int MOD1 = 998244353; //7*19*2^23 +1;
const int dx[] = {-1, 0, 1, 0, -1, -1, 1, 1};
const int dy[] = {0, 1, 0, -1, -1, 1, -1, 1};




const int N = 10;
const int M = 2e5 + 5;

int nums[N], pw[N];
int n, m, k, q;
string s;
bool vis[381367045];
vector <PII> swaps = {
    {0, 1},
    {3, 4},
    {6, 7},
    {1, 2},
    {4, 5},
    {7, 8},
    {0, 3},
    {1, 4},
    {2, 5},
    {3, 6},
    {4, 7},
    {5, 8}
};
void print(int a) {
    string res = "";
    while(a) {
        string ch = string(1, (a%9)+'0');
        a /= 9;
        res = ch+res;
    }
    cout << res << '\n';
}
void go () {
    int lim = 0, tar = 0;
    for (int i = 8; i >= 0; i--) lim = lim * 9 + i;
    for (int i = 0; i < 9; i++) tar = tar * 9 + i;
    pw[0] = 1;
    for (int i = 1; i < N; i++) pw[i] = pw[i-1] * 9;
    int st = 0;
    for (int i = 0; i < 9; i++) {
        int x; cin >> x;
        st = st * 9 + x - 1;
    }
    queue <int> bfs; 
    vis[st] = 1; bfs.push(st);
    int ans = 0;
    if (st == tar) {
        cout << "0\n";
        return;
    }
    auto gen = [&](int a, int b, int c) {
        if (b > c) swap(b, c);
        int A = a / pw[8-b];
        int B = a / pw[8-c] - A * pw[c - b];
        int C = a % pw[8-c];
        int x = A % 9;
        int y = B % 9;
        A += y-x;
        B += x-y;
        return A * pw[8-b] + B * pw[8-c] + C;
    };
    for (int i = 1; i <= 20; i++) {
        int rep = bfs.size();
        while(rep--) {
            int x = bfs.front(); bfs.pop();
            for (auto& [a, b] : swaps) {
                int num = gen(x, a, b);
                if (vis[num]) continue;
                vis[num] = 1;
                bfs.push(num);
            }
        }
        if (vis[tar]) {
            cout << i << "\n";
            return;
        }
    }
}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("0.in", "r", stdin);
    freopen("0.out", "w", stdout);
#endif
    _upgrade
    int T = 1;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














