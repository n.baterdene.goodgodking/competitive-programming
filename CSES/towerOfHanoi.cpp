#include <bits/stdc++.h>
#define pb push_back
using namespace std;
int n, m, k, q;
vector <pair <int, int> > ans;
void solve (int a, int b, int cur) {
	// cout << a << ' ' << b << ' ' << cur << '\n';
	if (cur == 1) {
		ans.pb({a, b});
		return;
	}
	int mid = 6-a-b;
	solve(a, mid, cur-1);
	ans.pb({a, b});
	solve(mid, b, cur-1);
}
signed main() {
	ios::sync_with_stdio(0);
	cin.tie(0);
	cout.tie(0);
	// freopen("0.in", "r", stdin);
	// freopen("0.out", "w", stdout);
	cin >> n;
	solve(1, 3, n);
	cout<< ans.size() << "\n";
	for (auto [a, b] : ans) cout << a << ' ' << b << '\n';
}