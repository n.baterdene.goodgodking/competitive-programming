#include <bits/stdc++.h>
using namespace std;
int main () {
    int n;
    cin >> n;
    vector <int> numbers(n);
    for (int i = 0; i < n; i++) 
        cin >> numbers[i];
    for (int st = 0; st < n; st++) {
        bool seen = false;
        for (int end = st + 1; end < n; end++) {
            if (numbers[st] == numbers[end]) {
                seen = true;
                break;
            }
        }
        if (!seen) {
            cout << numbers[st] << '\n';
            return 0;
        }
    }
    cout << numbers.back() << '\n';  
    return 0;
}