#include <iostream>
#include <vector>
using namespace std;
struct stack2{
    vector <int> vec;
    int end1, end0;
    stack2(int size) {
        vec.assign(size, 0);
        end0 = 0;
        end1 = size - 1;
    }
    void push(int stnum, int num) {
        scale();
        if(stnum == 0) {
            vec[end0] = num;
            end0++;
        } else {
            vec[end1] = num;
            end1--;
        }
    }
    void scale() {
        if(end0 == end1) {
            int pastsize = vec.size();
            vec.resize(pastsize * 2);
            for(int i = pastsize - 1; i >= end1; i--) 
                vec[pastsize + i] = vec[i];
            end1 += pastsize;
        }
    }
    void pop(int stnum) {
        if(stnum == 0) end0--;
        else end1++;
    }
    int back(int stnum) {
        if(stnum == 0) return vec[end0 - 1];
        return vec[end1 - 1];
    }
};

int main () {
    stack2 stacks(1);
    int n, stnum, num;
    // cin >> n;
    string cmd;
    while(true) {
        cin >> cmd;
        if(cmd == "push") {
            cin >> stnum >> num;
            stacks.push(stnum, num);
        }
        if(cmd == "back") {
            cin >> stnum;
            cout << stacks.back(stnum) << '\n'; 
        }
        if(cmd == "pop") {
            cin >> stnum;
            stacks.pop(stnum);
        }
        if(cmd == "done") {
            break;
        }
    }
    cout << "Stack0 : ";
    for(int i = 0; i < stacks.end0; i++) 
        cout << stacks.vec[i] << ' ';
    cout << '\n';
    cout << "Stack1 : ";
    for(int i = stacks.vec.size() - 1; i > stacks.end1; i--) 
        cout << stacks.vec[i] << ' ';
    cout << '\n';     
    return 0;
}