#include <iostream>
#include <vector>
using namespace std;
struct stack3{
    vector <int> vec;
    int end2, end1, end0, st;
    stack3(int size) {
        vec.assign(size, 0);
        end0 = 0;
        end1 = size / 2;
        end2 = size - 1;
        st = end1;
    }
    void push(int stnum, int num) {
        scale();
        if(stnum == 0) {
            vec[end0] = num;
            end0++;
        }
        if(stnum == 1) {
            vec[end1] = num;
            end1++;
        }
        if(stnum == 2) {
            vec[end2] = num;
            end2--;
        }
    }
    void pop(int stnum) {
        if(stnum == 0) end0--;
        if(stnum == 1) end1--;
        if(stnum == 2) end2++;
    }
    void scale() {
        if(end0 == st || end1 == end2) {
            int pastsize = vec.size();
            vec.resize(pastsize * 2);
            for(int i = pastsize - 1; i >= end2; i--) 
                vec[pastsize + i] = vec[i];
            end2 += pastsize;
            int start1 = vec.size() / 2;
            int dif = start1 - st;
            for(int i = end1 - 1; i >= st; i--) 
                vec[i + dif] = vec[i];
            end1 += dif;
            st += dif;
        }
    }
};

int main () {
    stack3 stacks(6);
    int n, stnum, num;
    // cin >> n;
    string cmd;
    while(true) {
        cin >> cmd;
        if(cmd == "push") {
            cin >> stnum >> num;
            stacks.push(stnum, num);
        }
        if(cmd == "pop") {
            cin >> stnum;
            stacks.pop(stnum);
        }
        if(cmd == "done") {
            break;
        }
    }
    //output
    if(true) {
        cout << "sizes : " << stacks.end0 << ' ' << stacks.end1 << ' ' << stacks.end2 << '\n';
        cout << "Stack0 : ";
        for(int i = 0; i < stacks.end0; i++) 
            cout << stacks.vec[i] << ' ';
        cout << '\n';
        cout << "Stack1 : ";
        for(int i = stacks.st; i < stacks.end1; i++) 
            cout << stacks.vec[i] << ' ';
        cout << '\n';     
        cout << "Stack2 : ";
        for(int i = stacks.vec.size() - 1; i > stacks.end2; i--) 
            cout << stacks.vec[i] << ' ';
        cout << '\n';     
    }
    return 0;
}