#include <iostream>
#include <vector>
using namespace std;
int read4(char* buf) {

    int t = 0;

    while (true) {

        if (scanf("%c", buf + t) == EOF) {
            break;
        }
        t ++;
        if(t == 4) break;
    }
    return t;
}
class Solution {
public:
    string left;
    /**
     * @param buf destination buffer
     * @param n maximum number of characters to read
     * @return the number of characters read
     */
    int read(char *buf, int n) {
        // Write your code here
        int len = 0;
        string str = "";
        do{
            len += read4(buf);
            if(n - 4 < 0) {
                str += *buf.substr(0, n);
                left = *buf.substr(n);
                n = 0;
            }
            
        }while(len > 0 && n > 0);
        return len;
    }
};
int main () {
    freopen("text.txt", "r", stdin);
    
    while(true) {
        char * buf = new char(4);
        int res = read4(buf);
        if(res == 0) break;
        cout << res << ' ';
        cout << buf << endl;
    }


    return 0;
}