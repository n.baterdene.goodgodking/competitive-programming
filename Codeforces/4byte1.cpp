#include <iostream>
#include <vector>
using namespace std;
int read4(char* buf) {
    int i;
    for(i = 0; i < 4; i++) 
        if (scanf("%c", buf + i) == EOF) break;

    return i;
}// Forward declaration of the read4 API.
int read4(char *buf);

class Solution {
public:
    char *remain = new char(4);
    int remidx = 4;
    /**
     * @param buf destination buffer
     * @param n maximum number of characters to read
     * @return the number of characters read
     */

    int read(char *buf, int n) {
        // Write your code here
        int used = 4 - remidx;
        for(int i = 0; remidx + i < 4; i++) 
            buf[i] = remain[remidx + i];
            
        remidx = 0;
        n -= used;
        int tmp;
        bool orson = false;
        while(n > 0) {
            if(n - 4 < 0) {
                orson = true;
                remidx = n;
                
                read4(remain);
                for(int i = used; i < used + n; i++) 
                    buf[i] = remain[i - used];
                used += n;
                break;
            }
            tmp = read4(buf + used);
            cout << tmp << '\n';
            n -= tmp;
            if(tmp != 4) {
                remidx = 4;
                remain = new char(4);
                break;
            } 
            used += tmp;
            // cout << " tt ::: " <<buf << endl;
        }
        if(!orson) {
            remidx = 4;
            remain = new char(4);
        }
        

        return used;
    }
};
int main () {
    freopen("text.txt", "r", stdin);
    Solution sol;
    char *buf = new char(10);
    int res = sol.read(buf, 10);
    cout << res << endl;
    cout << buf << endl;
    buf = new char(11);
    res = sol.read(buf, 11);
    cout << res << endl;
    cout << buf << endl;
    buf = new char(26);
    res = sol.read(buf, 11);
    cout << res << endl;
    cout << buf << endl;
    return 0;
}