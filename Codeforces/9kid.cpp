#include <iostream>
#include <vector>
#include <map>
using namespace std;
vector < string > table;
void myFunc(string tmp, int suusan) {
    int size = tmp.size();
    if(size == 12) {
        tmp += tmp;
        table.push_back(tmp);
        return;
    }
    if(suusan > 3) return;
    string tmp1 = tmp + "0";
    myFunc(tmp1, suusan);
    suusan++;
    tmp += "1";
    myFunc(tmp, suusan);
}
int main () {
    map <string, int> mp;
    int ans = 0;
    string tmp = "";
    myFunc(tmp, 0);
    for(int i = 0; i < table.size(); i++){
        int ok = 0;
        string checking = "";
        for(int j = i; j < i + 5; j++){
            checking += table[i][j];
            if(table[i][j] == '1') ok++;
        }
        if(mp.find(checking) == mp.end() && ok == 3){
            mp[checking] = 1;
            ans++;
        }
    }
    cout << ans << '\n';
    return 0;
}