#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int main () {
    int n, m, k;
    cin >> n >> m >> k;
    vector <int> people(n), apartment(m);
    for (int i = 0; i < n; i++) 
        cin >> people[i];
    for (int i = 0; i < m; i++) 
        cin >> apartment[i];
    int ans = 0;
    sort (people.begin(), people.end());
    sort (apartment.begin(), apartment.end());
    while (apartment.size() > 0 && people.size() > 0) {
        if (abs(apartment.back() - people.back()) <= k) {
            ans++;
            apartment.pop_back();
            people.pop_back();
        } else {
            if(apartment.back() > people.back()) 
                apartment.pop_back();
            else 
                people.pop_back();
        }
    }
    cout << ans << '\n';
    return 0;
}