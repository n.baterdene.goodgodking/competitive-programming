#include <iostream>
#include <vector>
#define int long long
using namespace std;

int n, m, sum;
vector <int> a;

void rec(int idx, int tmpSum) {
  if (idx == n) {
    int num1 = tmpSum;
    int num2 = sum - tmpSum;
    if (num2 > num1) swap(num1, num2);
    m = min(num1 - num2, m);
    return;
  }
  rec(idx + 1, tmpSum);
  rec(idx + 1, tmpSum + a[idx]);
}

signed main() {
    cin >> n;
    a = vector <int>(n);
    for (int i = 0; i < n; i++) {
      cin >> a[i];
      sum += a[i];
    }
    m = sum;
    rec(0, 0);
    cout << m << endl;
    return 0;
}