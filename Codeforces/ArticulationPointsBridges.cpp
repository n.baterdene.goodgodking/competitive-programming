// paths[u] = pathsacent nodes of u
// ap = AP = articulation points
// p = parent
// disc[u] = discovery time of u
// low[u] = 'low' node of u

VI ap, low, disc;
int Time;
int dfsAP(int u, int p) {
  int children = 0;
  low[u] = disc[u] = ++Time;
  for (int& v : paths[u]) {
    if (v == p) continue;
    if (!disc[v]) {
      children++;
      dfsAP(v, u);
      if (disc[u] <= low[v]) ap[u] = 1;
      low[u] = min(low[u], low[v]);
    } else
      low[u] = min(low[u], disc[v]);
  }
  return children;
}

void AP() {
  ap = low = disc = vector<int>(n+1, 0);
  Time = 0;
  for (int u = 1; u <= n; u++)
    if (!disc[u])
      ap[u] = dfsAP(u, u) > 1;
}
map <PII, bool> br;
void dfsBR(int u, int p) {
  low[u] = disc[u] = ++Time;
  for (int& v : paths[u]) {
    if (v == p) continue;
    if (!disc[v]) { 
      dfsBR(v, u);
      if (disc[u] < low[v]) br[{u, v}] = br[{v, u}] = 1;
      low[u] = min(low[u], low[v]);
    } else
      low[u] = min(low[u], disc[v]);
  }
}

void BR() {
  low = disc = vector<int>(n+1);
  Time = 0;
  for (int u = 1; u <= n; u++)
    if (!disc[u])
      dfsBR(u, u);
}