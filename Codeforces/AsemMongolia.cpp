#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
#define PII pair <int, int> 
using namespace std;
int n, asemCity, m;
const int INF = 100000000;
map <pair <int, int>, int> prices;
map <int, vector < pair <int, int> > > paths;

int aToB(int st, int end) {
    if (st > end) swap(st, end);
    if (prices.find(mp(st, end)) != prices.end()) return prices[mp(st, end)];
	vector <int> fromSt(n, INF);
	vector <int> from(n, -1);
	priority_queue<PII, vector<PII>, greater<PII> > bfs;
	bfs.push(make_pair(0, st));
	fromSt[st] = 0;
	while (!bfs.empty()) {
		PII p = bfs.top();
		bfs.pop();
		int here = p.second;
		if (here == end) break;
		if (fromSt[here] != p.first) continue;
		for (vector<PII>::iterator it = paths[here].begin(); it != paths[here].end(); it++) {
			if (fromSt[here] + it->first < fromSt[it->second]) {
				fromSt[it->second] = fromSt[here] + it->first;
				from[it->second] = here;
				bfs.push(make_pair(fromSt[it->second], it->second));
			}
		}
		
	}
    return prices[mp(st, end)] = fromSt[end];
}
int main () {
    cin >> n >> asemCity >> m;
    vector <int> destinations(asemCity);
    for (int i = 0; i < asemCity; i++) {
        cin >> destinations[i];
        destinations[i]--;
    }
    int a, b, tax;
	for (int i = 0; i < m; i++) {
        cin >> a >> b >> tax;
        a--;
        b--;
		paths[a].push_back(make_pair(tax, b));
		paths[b].push_back(make_pair(tax, a));
	}
    sort (destinations.begin(), destinations.end());

    int ans = INF;
    do { 
        int curAns = aToB(0, destinations[0]) + aToB(0, destinations.back());
        for (int i = 1; i < asemCity; i++) 
            curAns += aToB(destinations[i], destinations[i - 1]);
        ans = min(ans, curAns);
    } while (next_permutation(destinations.begin(), destinations.end())); 
    cout << ans << '\n';
    return 0;
}