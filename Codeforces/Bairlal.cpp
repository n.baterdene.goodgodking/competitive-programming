#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
#define umap unordered_map
#define INF INT_MAX
#define MOD 1e9 + 7
using namespace std;
int ansNum;
int strToInt(string n) {
    stringstream a(n);
    int tmp;
    a >> tmp;
    return tmp;
}
int ANS() {
    int cur = 9, oron = 1, res = 0, tmp = 1;
    while (cur < ansNum) {
        res += cur * oron;
        cur *= 10, tmp *= 10;
        oron++;
    }
    res += (ansNum - tmp + 1) * oron;
    return res;
}
bool check1(string str1, string str2) {
    int b = strToInt(str2);
    b--;
    str2 = to_string(b);
    int n = str1.size(), dif = str2.size() - str1.size();
    for (int i = 0; i < n; i++) 
        if (str1[i] != str2[i + dif]) return false;
    
    return true;
}
bool check2(string str1, string str2) {
    int b = strToInt(str1);
    b++;
    str1 = to_string(b);
    int n = str2.size(), dif = str1.size() - str2.size();
    for (int i = 0; i < n; i++) 
        if (str2[i] != str1[i + dif]) return false;
    
    return true;
}
bool check(string str1, string str2) {
    int b = strToInt(str1);
    b++;
    str1 = to_string(b);
    int n = str2.size();
    for (int i = 0; i < n; i++) 
        if (str2[i] != str1[i]) {
            return false;
        }
    
    return true;
}
void solve(string str, int len) {
    int n = str.size();
    for (int i = 1; i <= len; i++) {
        vector <string> numbers;
        numbers.pb(str.substr(0, i));
        for (int j = i; j < n; j+=len) 
            numbers.pb(str.substr(j, len));
        int m = numbers.size();
        bool out = false;
        for (int j = 0; j < m; j++) {
            if (numbers[j][0] == '0') out = true;
        }
        if (out) {
            continue;
        }
        if (numbers.size() == 2) {
            if (check(numbers[0], numbers[1])) {
                int curAns = strToInt(numbers[0]);
                ansNum = min(ansNum, curAns);
                continue;
            }
            if (check1(numbers[0], numbers[1])) {
                int curAns = strToInt(numbers[1]) - 1;
                ansNum = min(ansNum, curAns);
                continue;
            }
        }
        if (!check1(numbers[0], numbers[1])) {
            continue;
        }
        if (!check2(numbers[m - 2], numbers[m - 1])) {
            continue;
        }
        for (int j = 1; j < m - 2; j++) {
            int a = strToInt(numbers[i]);
            int b = strToInt(numbers[i + 1]);
            if (a + 1 != b) out = true;
        }
        if (out) {
            continue;
        }
        int curAns = strToInt(numbers[1]) - 1;
        ansNum = min(curAns, ansNum);
    }
}
void solve(string str) {
    string a, b;
    for (int i = 1; i < str.size(); i++) {
        a = str.substr(0, i);
        b = str.substr(i);
        b += a;
        if (b[0] == '0') continue;
        int ans = strToInt(b);
        ansNum = min(ansNum, ans);
    }
}
int myFind(string a, string b) {
    int st = 0;
    int n = b.size();
    while(true) {
        while(a[st] != b[0]) st++;
        bool con = false;
        for (int i = 0; i < n; i++) 
            if (a[st + i] != b[i]) con = true;
        if (con) {
            st++;
            continue;
        }
        return st + 1;
    }
}
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    string n;
    cin >> n;
    ansNum = strToInt(n);
    int tmp = ansNum;
    for (int i = 1; i < n.size(); i++) 
        solve(n, i);
    solve(n);
    ansNum--;
    int ans = ANS();
    ansNum++;
    string temp = "";
    do {
        temp += to_string(ansNum++);
    } while(temp.size() <= n.size());
    ans += myFind(temp, n);
    cout << ansNum << '\n';
    cout << ans << '\n';
	return 0;
}