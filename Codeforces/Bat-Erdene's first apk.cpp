#include <iostream>
using namespace std;
int main() {
    string a;
    cout << "Welcome to my app"<< endl << endl;
    cout << "Enter your color name (english language only and use little letters)"<< endl;
    cin >> a;

    string colors[] = {
        "maroon",
        "red",
        "orange",
        "yellow",
        "olive",
        "purple",
        "pink",   
        "white",
        "lime",
        "green",
        "navy",
        "blue",
        "aqua",
        "teal",
        "black",
        "silver",
        "gray"
    };
    string color_code[] = {
        "#800000",
        "#ffa500",
        "#ffff00",
        "#808000",
        "#800080",
        "#ff00ff",
        "#ff0000",
        "#ffffff",
        "#00ff00",
        "#008000",
        "#000080",
        "#0000ff",
        "#00ffff",
        "#008080",
        "#000000",
        "#c0c0c0",
        "#808080"
    };
    int len = 17;
    bool f = false;
    for(int i = 0 ; i < len; i++){
        if( a == colors[i] ){
            cout << color_code[i];
            f = true;
        }
    }

    if( f == true ){
        cout << ' '<<" is your color's code"<< endl;
    } else{
        cout << "Unfortunately, we could not find your according code for your color.\n";
    }
    return 0;
}