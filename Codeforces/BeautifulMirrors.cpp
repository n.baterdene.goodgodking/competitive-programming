#include <bits/stdc++.h>
using namespace std;
 
#define REP(i, n) for (int i=0;i<n;++i)
#define FOR(i, a, b) FOR (int i=a;i<b;++i)
 
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define pb push_back
#define mp make_pair
#define PTT pair<T, T>
#define ff first
#define ss second
 
typedef long long ll;
typedef double d;
typedef unsigned uint;
typedef unsigned long long ull;
 
typedef vector<int> vi;
typedef vector<char> vc;
typedef vector<string> vs;
typedef vector<ll> vll;
typedef vector<d> vd;
typedef set<int> si;
typedef set<string> ss;
typedef map<int, int> mii;
typedef map<string, int> msi;
typedef pair<int, int> pii;
typedef pair<ll, ll> pll;
typedef vector<pii> vpi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
 
const ll MOD = 998244353;
const int INF = 0x3f3f3f3f;
const ll INFF = 0x3f3f3f3f3f3f3f3fLL;
const d EPS = 1e-9;
const d OO = 1e20;
const d PI = acos(-1.0); //M_PI;
const int moveX[] = {-1, 0, 1, 0};
const int moveY[] = {0, 1, 0, -1};

//                                        .....'',;;::cccllllllllllllcccc:::;;,,,''...'',,'..
//                             ..';cldkO00KXNNNNXXXinp000OOinpinpkxxxxxddoooddddddxxxxinpinpOO0XXKx:.
//                       .':ok0KXXXNXK0kxolc:;;,,,,,,,,,,,;;,,,''''''',,''..              .'lOXKd'
//                  .,lx00Oxl:,'............''''''...................    ...,;;'.             .oKXd.
//               .cinpinpc'...'',:::;,'.........'',;;::::;,'..........'',;;;,'.. .';;'.           'kNKc.
//            .:kXXk:.    ..       ..................          .............,:c:'...;:'.         .dNNx.
//           :0NKd,          .....''',,,,''..               ',...........',,,'',,::,...,,.        .dNNx.
//          .xXd.         .:;'..         ..,'             .;,.               ...,,'';;'. ...       .oNNo
//          .0K.         .;.              ;'              ';                      .'...'.           .oXX:
//         .oNO.         .                 ,.              .     ..',::ccc:;,..     ..                lXX:
//        .dNX:               ......       ;.                'cxOinp0OXWWWWWWWNX0kc.                    :KXd.
//      .l0N0;             ;d0inpinpKXK0ko:...              .l0X0xc,...lXWWWWWWWWKO0Kx'                   ,ONKo.
//    .lKNKl...'......'. .dXWN0inpk0NWWWWWN0o.            :KN0;.  .,cokXWWNNNNWNinpxONK: .,:c:.      .';;;;:lk0XXx;
//   :KN0l';ll:'.         .,:lodxxkO00KXNWWWX000k.       oXNx;:oinpX0kdl:::;'',;coxinpd, ...'. ...'''.......',:lxKO:.
//  oNNk,;c,'',.                      ...;xNNOc,.         ,d0X0xc,.     .dOd,           ..;dOKXK00000Ox:.   ..''dKO,
// 'KW0,:,.,:..,oxinpkdl;'.                'inp'              ..           .dXX0o:'....,:oOXNN0d;.'. ..,lOKd.   .. ;KXl.
// ;XNd,;  ;. l00kxoooxKXKx:..ld:         ;inp'                             .:dkO000000Okxl;.   c0;      :inp;   .  ;XXc
// 'XXdc.  :. ..    '' 'kNNNinpinp,      .,dKNO.                                   ....       .'c0NO'      :X0.  ,.  xN0.
// .kNOc'  ,.      .00. ..''...      .l0X0d;.             'dOkxo;...                    .;oinpXK0KNXx;.   .0X:  ,.  lNX'
//  ,inpdl  .c,    .dNK,            .;xXWKc.                .;:coOXO,,'.......       .,lx0XXOo;...oNWNXinp:.'KX;  '   dNX.
//   :XXkc'....  .dNWXl        .';l0NXNKl.          ,lxinpkxo' .cK0.          ..;lx0XNX0xc.     ,0Nx'.','.kXo  .,  ,KNx.
//    cXXd,,;:, .oXWNNKo'    .'..  .'.'dinp;        .cooollox;.xXXl     ..,cdOKXXX00NXc.      'oKWK'     ;k:  .l. ,0Nk.
//     cXNx.  . ,KWX0NNNXOl'.           .o0Ooldk;            .:c;.':lxOinpK0xo:,.. ;XX:   .,lOXWWXd.      . .':,.lKXd.
//      lXNo    cXWWWXooNWNXinpo;'..       .lk0x;       ...,:ldk0KXNNOo:,..       ,OWNOxO0KXXNWNO,        ....'l0Xk,
//      .dNK.   oNWWNo.cXK;;oOXNNXK0kxdolllllooooddxk00inpinp0kdoc:c0No        .'ckXWWWNXkc,;kNKl.          .,kXXk,
//       'KXc  .dNWWX;.xNk.  .kNO::lodxkOXWN0OkxdlcxNKl,..        oN0'..,:ox0XNWWNNWXo.  ,ONO'           .o0Xk;
//       .ONo    oNWWN0xXWK, .oNKc       .ONx.      ;X0.          .:XNinpNNWWWWNinpl;kNk. .cKXo.           .ON0;
//       .xNd   cNWWWWWWWWKOinpNXxl:,'...;0Xo'.....'lXK;...',:lxk0KNWWWWNNKOd:..   lXKclON0:            .xNk.
//       .dXd   ;XWWWWWWWWWWWWWWWWWWNNNNNWWNNNNNNNNNWWNNNNNNWWWWWNXKNNk;..        .dNWWXd.             cXO.
//       .xXo   .ONWNWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWNNK0ko:'..OXo          'l0NXx,              :inp,
//       .OXc    :XNk0NWXKNWWWWWWWWWWWWWWWWWWWWWNNNX00NNx:'..       lXKc.     'lONN0l.              .oXK:
//       .KX;    .dNKoON0;lXNkcld0NXo::cd0NNO:;,,'.. .0Xc            lXXo..'l0NNKd,.              .c0Nk,
//       :XK.     .xNX0NKc.cXXl  ;KXl    .dN0.       .0No            .xNXOKNXOo,.               .l0Xk;.
//      .dXk.      .lKWN0d::OWK;  lXXc    .OX:       .ONx.     . .,cdk0XNXOd;.   .'''....;c:'..;xKXx,
//      .0No         .:dOKNNNWNKOxkXWXo:,,;ONk;,,,,,;c0NXOxxkO0XXNXKOdc,.  ..;::,...;lol;..:xKXOl.
//      ,XX:             ..';cldxkOO0inpKXXXXXXXXXXinpinpK00Okxdol:;'..   .';::,..':llc,..'linpXkc.
//      :NX'    .     ''            ..................             .,;:;,',;ccc;'..'linpX0d;.
//      lNK.   .;      ,lc,.         ................        ..,,;;;;;;:::,....,linpX0d:.
//     .oN0.    .'.      .;ccc;,'....              ....'',;;;;;;;;;;'..   .;oOXX0d:.
//     .dN0.      .;;,..       ....                ..''''''''....     .:dOinpko;.
//      lNK'         ..,;::;;,'.........................           .;d0X0kc'.
//      .xXO'                                                 .;oOK0x:.
//       .cinpo.                                    .,:oxinpkxk0K0xc'.
//         .oinpkc,.                         .';cok0XNNNX0Oxoc,.
//           .;d0XX0kdlc:;,,,',,,;;:clodkO0inp0Okdl:,'..
//               .,coxO0KXXXXXXXinp0OOxdoc:,..
//                         ...
 


























int main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    
    return 0;
}
 
 