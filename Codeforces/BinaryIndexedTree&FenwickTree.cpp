#include <bits/stdc++.h>
using namespace std;
vector <int> sum, numbers;
int n, m;
void add(int num, int idx) {
    if (idx > n) return;
    sum[idx] += num;
    idx += idx & (-idx);
    return add(num, idx);
}
int findSum (int idx) {
    if (idx <= 0) return 0;
    int tmp = sum[idx];
    idx -= idx & (-idx);
    return tmp + findSum(idx);
}
int main () {
    cin >> n >> m;
    numbers.resize(n + 1);
    sum.resize(n + 1, 0);
    for (int i = 1; i <= n; i++) {
        cin >> numbers[i];
        add(numbers[i], i);
    }
    int type, st, end;
    for (int i = 0; i < m; i++) {
        cin >> type >> st >> end;
        if (type == 1) {
            add(end - numbers[st], st);
            numbers[st] = end;
        } else {
            cout << findSum(end) - findSum(st - 1) << '\n';
        }
    }
    cout << '\n';
    return 0;
}