#include<bits/stdc++.h>
 
using namespace std;
vector <int> nums(1e5 + 5);
int main() {
	int n, m;
	cin >> n >> m;
	for (int i = 0; i < n; i++) {
		cin >> nums[i];
	}
	int idx = 0;
	for (int i = n; i > 0; i /= 2) {
		while(idx + i < n && nums[idx + i] <= m) 
			idx += i;
	}
	cout << idx + 1 << '\n';
    return 0;
}
 