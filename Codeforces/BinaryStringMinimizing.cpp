#include <bits/stdc++.h>
#define int long long
using namespace std;
string cur;
signed main () {
    int N, len, canMove;
    cin >> N;
    for (int I = 0; I < N; I++) { 
        cin >> len >> canMove >> cur;
        int st = 0, curMove, count = 0;
        for (int i = 0; i < len; i++) 
            if (cur[i] == '0') count++;

        for (int i = 0; i < len && canMove > 0; i++) {
            if (st == count) canMove = 0;
            if (cur[i] == '0') {
                // cout << cur << " to ";
                curMove = min(canMove, i - st);
                canMove -= curMove;
                swap(cur[i - curMove], cur[i]);
                st++;
                // cout << cur << '\n';
            }
        }
        cout << cur << '\n';
    }
    return 0;
}