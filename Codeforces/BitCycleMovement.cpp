#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
#define umap unordered_map
#define INF INT_MAX
#define MOD 1e9 + 7
using namespace std;
vector <bool> binary;
int m;
void shift () {
    bool tmp = binary[0];
    for (int i = 0; i < m - 1; i++) 
        binary[i] = binary[i + 1];
    binary.back() = tmp;
}
void shift1 (vector <int>& conts) {

    bool tmp = conts[0];
    for (int i = 0; i < conts.size() - 1; i++) 
        conts[i] = conts[i + 1];

    conts.back() = tmp;
}
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    ll n;
    cin >> n;
    int cur = 0;
    while (n > 0) {
        binary.pb(n % 2);
        n /= 2;
    }
    reverse(binary.begin(), binary.end());
    vector <vector <bool> > answers;
    m = binary.size();
    for (int i = 0; i < m; i++) {
        answers.pb(binary);
        shift();
    }
    sort(answers.begin(), answers.end());
    binary = answers.back();
    int ans = 0;
    cur = 1;
    for (int i = m - 1; i >= 0; i--) {
        if (binary[i]) ans += cur;
        cur *= 2;
    }
    cout << ans << '\n';
	return 0;
}