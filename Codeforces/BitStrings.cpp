#include <iostream>
#include <bitset>
#define int long long
#define MAX 1000000007
using namespace std;
signed main () {
    int max = 1;
    int n;
    cin >> n;
    for(int i = 0; i < n; i++) 
        max = (max * 2) % MAX;
    cout << max << '\n';
    return 0;
}