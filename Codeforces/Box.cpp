#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
#define ll long long
#define int long long
#define ff first
#define ss second
#define PII pair <int, int>
using namespace std;
void solve () {
    int n;
    cin >> n;
    vector <int> numbers(n);
    vector <int> ans(n);
    int cur = 0;
    for (int i = 0; i < n; i++) {
        cin >> numbers[i];
        if (numbers[i] < cur) 
            cur = -1;
        cur = numbers[i];
    }
    if (cur == -1) {
        cout << -1 << "\n";
        return;
    }
    vector <bool> seen(n + 1, 0);
    int tmp = 1;
    ans[0] = numbers[0];
    seen[ans[0]] = 1;
    if (tmp == ans[0])
        tmp++;
    for (int i = 1; i < n; i++) {
        if(numbers[i] > numbers[i - 1]) {
            ans[i] = numbers[i];
            seen[ans[i]] = 1;
        } else {
            while(seen[tmp] && tmp <= numbers[i]) 
                tmp++;
            
            if (tmp > numbers[i]) {
                cout << -1 << "\n";
                return;
            }
            ans[i] = tmp;
            seen[tmp] = 1;
            tmp++;
        }
    }
    for(int i = 0; i < n; i++) 
        cout << ans[i] << ' ';
    cout << "\n";
}

signed main () {
    int t;
    cin >> t;

    while (t--) solve();

    return 0;
}