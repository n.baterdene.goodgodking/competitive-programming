#include <iostream> 
#include <vector>
#define int long long
using namespace std;
vector <int> ans;
int l = 1;
bool my_func(int n){
    if (n == 0) return true;
    if(n % 3 == 2) return false;
    if(n % 3 == 1){
        ans.push_back(l);
        l = l * 3;
        return my_func(n / 3);
    }
    l = l * 3;
    return my_func(n / 3);   
}
signed main () {
    int n;
    cin >> n;
    bool a = my_func(n);
    if(a == false){
        cout << "Miss\n";
        return 0;
    }
    cout << n  << " = ";
    cout << ans[0];
    for(int i = 1; i < ans.size(); i++){
        cout <<  " + " << ans[i];
    }
    cout << '\n';

    return 0;
}