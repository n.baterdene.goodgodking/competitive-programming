#include <bits/stdc++.h>
#define pb push_back
using namespace std;
int main () {
    int N, n;
    cin >> N;
    string a, b;
    for (int I = 0; I < N; I++) {
        cin >> n >> a >> b;
        vector <int> idx;
        for (int i = 0; i < n; i++) 
            if (a[i] != b[i]) idx.pb(i);

        if (idx.size() != 2) {
            cout << "No\n";
            continue;
        }
        cout << a[idx[0]] << ' ' << a[idx[1]] << '\n';
        cout << b[idx[0]] << ' ' << b[idx[1]] << '\n';
        if (a[idx[0]] == a[idx[0]] && b[idx[1]] == b[idx[1]]) {
            cout << "Yes\n";
            continue;
        }
        cout << "No\n";
    }
    return 0;
}