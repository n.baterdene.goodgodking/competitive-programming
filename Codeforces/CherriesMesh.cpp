#include <map>
#include <vector>
#include <iostream>

#define pb push_back
using namespace std;
int wire = 0;
int main () {
    int tests;
    cin >> tests;
    int ans = 0;
    for (int i = 0; i < tests; i++) {
        vector <vector <int> > numbers;
        int candies, black, st, end;
        map <int, int> used;
        cin >> candies >> black;
        for (int j = 0; j < black; j++) {
            cin >> st >> end;
            if (used.find(st) == used.end() 
            && used.find(end) == used.end()) {
                vector <int> tmp;
                tmp.pb(st);
                tmp.pb(end);
                used[st] = wire;
                used[end] = wire;
                numbers.pb(tmp);
                wire++;
            } else {
                if (used.find(st) == used.end()) 
                    numbers[used[end]].pb(st);

                if (used.find(end) == used.end()) 
                    numbers[used[st]].pb(end);
                
                if (used.find(end) != used.end() && used.find(st) != used.end()) {
                    if (used[end] == used[st]) {
                        ans--;
                        continue;
                    }
                    if (numbers[used[st]].size() > numbers[used[end]].size()) {
                        while (numbers[used[end]].size() > 0) {
                            numbers[used[st]].pb(numbers[used[end]].back());
                            used[numbers[used[end]].back()] = used[st];
                            numbers[used[end]].pop_back();
                        }
                    } else {
                        while (numbers[used[st]].size() > 0) {
                            numbers[used[end]].pb(numbers[used[st]].back());
                            used[numbers[used[st]].back()] = used[end];
                            numbers[used[st]].pop_back();
                        }
                    }
                }
            }
            ans += black;
            for (int idx = 0; idx < numbers.size(); idx++) 
                if(numbers[idx].size() != 0) ans++;
            cout << ans - 1<< '\n';
        }
    }

    return 0;
}