#include <iostream>
#include <vector>
#define n 8

using namespace std;

vector <bool> col(8, true);
vector <bool> lineX(20, true), lineY(20, true);
vector <vector <char> > board(n, vector <char>(n));
// line0 x, line1 bde, line2 bdo
int ans = 0;
void place(int j) {
    // cout << "---------------Opened---------------\n";
    // for(int x = 0; x < n; x++) {
    //     for(int y = 0; y < n; y++) 
    //         cout << board[x][y];
    //     cout << '\n';
    // }
    if(j == n) {
        ans++;
        return;
    }
    for(int i = 0; i < n; i++) {
        if(board[j][i] != '*' && col[i] && lineX[i + j] && lineY[i - j + 8]) {
            col[i] = false;
            lineX[i + j] = false;
            lineY[i - j + 8] = false;
            // board[j][i] = 'Q';
            place(j + 1);
            col[i] = true;
            lineX[i + j] = true;
            lineY[i - j + 8] = true;
            // board[j][i] = '.';
        }
    }
} 
int main () {
    for(int i = 0; i < n; i++) 
        for(int j = 0; j < n; j++) 
            cin >> board[i][j];

    place(0);
    cout << ans << '\n';
    return 0;
}