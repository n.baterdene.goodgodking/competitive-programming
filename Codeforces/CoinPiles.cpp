#include <iostream>
#define int long long
using namespace std;
signed main () {
    int n, first, second;
    cin >> n;
    for(int i = 0; i < n; i++) {
        cin >> first >> second;
        if(first < second) 
            swap(first, second);
        int dif = first - second;
        if(dif == 0) {
            if(first % 3 == 0) cout << "YES\n";
            else cout << "NO\n";
            continue;
        }
        if(second >= dif && (second - dif) % 3 == 0) cout << "YES\n";
        else cout << "NO\n";
    }


    return 0;
}