#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
using namespace std;
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int n, m;
    cin >> m >> n;
    int a, b;
    a = n / m;
    b = n % m;
    if (b != 0) a++;
    else b = m;
    cout << a << ' ' << b << '\n';
	return 0;
}