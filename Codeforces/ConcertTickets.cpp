#include <iostream>
#include <vector>
#include <algorithm>
#include <set>

using namespace std;

int main () {
    // freopen("out.txt", "w", stdout);
    int n, m, customer, tmp;
    cin >> n >> m;
    multiset<int> numbers;
    for (int i = 0; i < n; i++) {
        cin >> tmp;
        numbers.insert(tmp);    
    }
    for (int i = 0; i < m; i++) {
        cin >> customer;

        numbers.insert(customer);
        auto idx = numbers.find(customer);
        auto start = numbers.begin();

        if (idx == start) {
            auto next = (++start);
            start--;
            if (customer == *next) {
                numbers.erase(idx);
                numbers.erase(next);
                cout << customer << '\n';
            } else {
                numbers.erase(idx);    
                cout << -1 << '\n';
            }
        } else {
            auto next = (++idx);
            idx--;
            if (customer == *next) {
                numbers.erase(idx);
                numbers.erase(next);
                cout << customer << '\n';
            } else {
                idx--;
                cout << *idx << '\n';
                numbers.erase(idx);
                numbers.erase(customer);
            }
        }
    }
    return 0;
}