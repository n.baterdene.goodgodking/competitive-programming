#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
using namespace std;
int make(vector <int>& idx) {
    if (idx.size() == 0) return 1;
    int cur = idx.back() - 1, len = idx.back(), count = 1;
    idx.pop_back();
    for (int choose = 1; cur >= (len + 1) / 2; choose++) {
        int tmp = 1;
        for (int i = 0; i < choose; i++) 
            tmp *= (cur - i);
            
        for (int i = 1; i <= choose; i++)
            tmp /= i;
        count += tmp;
        cur--;
    }
    return count * make(idx);
}
int main () {
    string str;
    cin >> str;
    vector <int> idx;
    int n = str.size();
    for (int i = 0; i < n - 1; i++) {
        if ((str[i] == 'm' || str[i] == 'w')) {
            cout << 0 << '\n';
            return 0;
        }
    }
    for (int i = 0; i < n; i++) 
        if (str[i] == 'u' || str[i] == 'n') {
            int count = 1, j;
            for (j = i + 1; j < n; j++) {
                if (str[i] == str[j]) count++;
                else break;
            }
            i = j - 1;
            idx.pb(count);
        }

    sort (idx.begin(), idx.end());
    cout << make(idx) << '\n';
    return 0;
}