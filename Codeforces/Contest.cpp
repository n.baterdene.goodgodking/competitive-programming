#include <iostream>
#include <algorithm>
using namespace std;
int main () {
    int misha, vasya, time_misha, time_vasya;
    cin >> misha >> vasya >> time_misha >> time_vasya;
    misha = max(misha * 3 / 10, misha - (misha / 250) * time_misha);
    vasya = max(vasya * 3 / 10, vasya - (vasya / 250) * time_vasya);
    if(misha > vasya) cout << "Misha\n";
    else {
        if(misha == vasya) cout << "Tie\n";
        else cout << "Vasya\n";
    }
    return 0;
}