#include <iostream>
#include <vector>

#define pb push_back
using namespace std;
int ans = 0;
vector <int> numbers;
void myMergeSortConquer(int st, int end) {
    int size = end - st + 1;
    int mid = size / 2 + st;
    vector <int> firstHalf, secondHalf, updated;
    for (int i = st; i < mid; i++) 
        firstHalf.pb(numbers[i]);

    for (int i = mid; i <= end; i++) 
        secondHalf.pb(numbers[i]);

    int idx1 = 0, idx2 = 0;
    //merge hiij baigaa heseg
    while (idx1 != firstHalf.size() || idx2 != secondHalf.size()) {
        if (idx1 == firstHalf.size()) {
            updated.pb(secondHalf[idx2]);
            idx2++;
            continue;
        }
        if (idx2 == secondHalf.size()) {
            updated.pb(firstHalf[idx1]);
            idx1++;
            continue;
        }
        if (firstHalf[idx1] > secondHalf[idx2]) {
            //end hariugaa nemj baigaa
            ans += (firstHalf.size() - idx1);
            updated.pb(secondHalf[idx2]);
            idx2++;
        } else {
            updated.pb(firstHalf[idx1]);
            idx1++;
        }
    }
    for(int i = 0; i < updated.size(); i++) 
        numbers[i + st] = updated[i];
}
void myMergeSortDivide(int st, int end) {
    int size = end - st + 1;
    int mid = size / 2 + st;
    // duusah nohtsol
    if (size == 1) return;

    // ehnii tald ni mergesort
    if (mid != end) myMergeSortDivide(st, mid - 1);

    //suuliin tald ni mergesort
    if (mid != st) myMergeSortDivide(mid, end);
    myMergeSortConquer(st, end);
}
int main () {
    int n, tmp;
    cin >> n;
    for (int i = 0; i < n; i++) {
        cin >> tmp;
        numbers.pb(tmp);
    }
    myMergeSortDivide(0, n - 1);
    cout << ans << '\n';
    return 0;
}