#include <bits/stdc++.h>
#define mp make_pair
using namespace std;
int N, M;
vector <vector <double> > dp(300,vector <double>(2000, -1));
double ans(int n, int m) {
    // cout << n << ' ' << m << '\n';
    if (n > m || m < 0) return 0;
    if (n > 0 && m == 0) return 0;
    if (n == 0 && m == 0) return 1;
    
    if (dp[n][m] != -1) return dp[n][m];
    double res = 0;
    for (int i = 1; i <= 6; i++) 
        res += ans(n - 1, m - i) / 6;
                
    return dp[n][m] = res;
}
int main () {
    int n, m;
    cin >> n >> m;
    N = n, M = m;
    cout << fixed << setprecision(3) << ans(n, m) << '\n';
    return 0;
}