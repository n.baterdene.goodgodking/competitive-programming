#include <bits/stdc++.h>
#define mp make_pair
#define int double
using namespace std;
int N, M;
map <pair <int, int> , int> dp;
int ans(int n, int m, int cur) {
    if (m < 0) return 0;
    if (n > 0 && m == 0) return 0;
    if (n == 0 && m == 0) return 1;
    if (n == 1 && m <= 6) return 1;

    if (dp.find(mp(n, m)) != dp.end()) return dp[mp(n, m)];
    // if (dp.find(mp(N - n, M - m)) != dp.end()) return dp[mp(N - n, M - m)];
    int res = 0;
    for (int i = 1; i <= 6; i++) 
        res += ans(n - 1, m - i, cur + 1) / 6;
    
    return dp[mp(n, m)] = res;
}
signed main () {
    int n, m;
    cin >> n >> m;
    N = n, M = m;
    cout << fixed << setprecision(3) << ans(n, m, 0) << '\n';
    return 0;
}