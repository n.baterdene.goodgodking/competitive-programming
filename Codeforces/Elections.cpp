#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
#define umap unordered_map
#define INF INT_MAX
#define MOD 1e9 + 7
using namespace std;
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int n, m;
    cin >> n >> m;
    vector <vector <int> > polls;
    for (int i = 0; i < m; i++) {
        vector <int> poll(n);
        for (int j = 0; j < n; j++) {
            cin >> poll[j];
        }
        polls.pb(poll);
    }
    int ans = m;
    vector <vector <PII> > dif(n - 1);

    for (int i = 0; i < n - 1; i++) {
        for (int j = 0; j < m ; j++) 
            dif[i].pb(mp(polls[j][i] - polls[j][n - 1], j));
        sort(dif[i].begin(), dif[i].end());
        reverse(dif[i].begin(), dif[i].end());
    }

    vector <bool> res(m, true);
    for (int i = 0; i < n - 1; i++) {
        int choose = m, sum = 0; 
        vector <bool> choosed(m, true);
        for (int j = 0; j < m; j++) {
            sum += dif[i][j].ff;
            if (sum < 0) break;
            choosed[dif[i][j].ss] = false;
            choose--;
        }
        if (choose < ans) {

            res = choosed;
            ans = choose;
        }
    }
    cout << ans << '\n';
    for (int i = 0; i < m; i++) 
        if (res[i]) cout << i + 1 << ' ';
    cout << '\n';
	return 0;
}