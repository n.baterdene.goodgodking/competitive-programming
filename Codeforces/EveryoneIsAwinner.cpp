#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
#define umap unordered_map
using namespace std;
void go() {
    ll num;
    cin >> num;
    ll limit = sqrt(num);
    set <ll> ans;
    for (int i = 0; i <= limit; i++) {
        ans.insert(i);
        if (i != 0) ans.insert(num / i);
    }
    cout << ans.size() << '\n';
    for (auto el: ans) 
        cout << el << ' ';
    
    cout << '\n';
}
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int T;
    cin >> T;
    while(T--) go();
	return 0;
}