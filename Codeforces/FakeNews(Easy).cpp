#include <bits/stdc++.h>
#define pb push_back
using namespace std;
int main () {
    int n;
    vector <char> ans; 
    ans.pb('i'); 
    ans.pb('d'); 
    ans.pb('i'); 
    ans.pb('e'); 
    ans.pb('h');
    char cur;
    while (cin >> cur) 
        if (cur == ans.back()) ans.pop_back();

    if (ans.size() == 0) cout  << "YES\n";
    else cout << "NO\n";
    return 0;
}