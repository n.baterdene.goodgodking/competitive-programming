#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main () {
    int n, size;
    cin >> n >> size;
    vector <int> kids(n);
    for (int i = 0; i < n; i++) 
        cin >> kids[i];

    sort(kids.begin(), kids.end());
    int idx1 = 0, idx2 = n - 1, ans = 0;
    while(idx1 <= idx2) {
        ans++;
        if(kids[idx1] + kids[idx2] <= size) 
            idx1++;
        idx2--;
    }
    cout << ans << '\n';
    return 0;
}