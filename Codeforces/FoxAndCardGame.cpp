#include <bits/stdc++.h>
#define pb push_back
using namespace std;
int turn = 0;
struct cardLine {
    vector <int> cards, prefix;
    int st, end;
    void findPrefix () {
        prefix.pb(cards[0]);
        for (int i = 1; i < cards.size(); i++) 
            prefix.pb(cards[i] + prefix.back());
    }
    int curMax() {
        if (st > end) return INT_MIN;
        int size = end - st;
        if (turn == 0) size++;
        int mid = st + size / 2;
        int firstHalf = findSum(mid, st - 1);
        int secondHalf = findSum(end, mid);
        return firstHalf - secondHalf;
    }
    int findSum(int end, int st) {
        if (st == -1) return prefix[end];
        return prefix[end] - prefix[st];
    } 
};
int main () {
    int n, times, num, allCard = 0;
    cin >> n;
    vector <cardLine> cards;
    for (int i = 0; i < n; i++) {
        cin >> times;
        allCard += times;
        cardLine tmp;
        for (int j = 0; j < times; j++) {
            cin >> num;
            tmp.cards.pb(num);
        }
        tmp.st = 0;
        tmp.end = times - 1;
        tmp.findPrefix();
        cards.pb(tmp);
    }
    int curChoosed, dif = 0;
    vector <int> ans(2, 0);
    int turns[] = {1, -1};
    for (int I = 0; I < allCard; I++) {
        int dif = INT_MIN;
        for (int i = 0; i < n; i++)  {
            if (cards[i].curMax() > dif) {
                curChoosed = i;
                dif = cards[i].curMax() ;
            }
        }
        if (turn == 0) {
            ans[turn] += cards[curChoosed].cards[cards[curChoosed].st];   
            cards[curChoosed].st++;
        } else {
            ans[turn] += cards[curChoosed].cards[cards[curChoosed].end];   
            cards[curChoosed].end--;
        }
        turn = (turn + 1) % 2;
    }
    cout << ans[0] << ' ' << ans[1] << '\n';
    return 0;
}