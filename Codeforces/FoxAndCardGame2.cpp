#include <bits/stdc++.h>
#define pb push_back
using namespace std;
int main () {
    int n, m, sum = 0, num, ans = 0;
    cin >> n;
    vector <int> mids;
    for (int i = 0; i < n; i++) {
        cin >> m;
        for (int j = 0; j < m; j++) {
            cin >> num;
            sum += num;
            if (m & 1 && j == m / 2) mids.pb(num);
            if (j < m / 2) ans += num;
        }
    }
    sort (mids.begin(), mids.end());
    for (int i = mids.size() - 1; i >= 0; i--) 
        if ((mids.size() - i) % 2 == 1) ans += mids[i];

    cout << ans << ' ' << sum - ans << '\n';
    return 0;
}