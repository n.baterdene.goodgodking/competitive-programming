#include <bits/stdc++.h>
using namespace std;
int main() {
    vector <vector <bool> > ans(1005, vector <bool>(1005, false));
    vector <bool> binary(50, false);
    int n;
    cin >> n;
    int len = 0;
    while (n > 0) {
        if (n & 1) binary[len] = true;
        len++;
        n /= 2;
    }
    int cur = 3;
    ans[1][3] = true;
    for (int i = 0; i < len - 1; i++, cur += 3) {
        ans[cur][cur+1] = true;
        ans[cur][cur+2] = true;
        ans[cur+1][cur+3] = true;
        ans[cur+2][cur+3] = true;
    }
    int tmp = cur + 1;
    ans[cur][2] = true;
    for (int i = 0; i < (len - 2) * 2; i++, tmp++) 
        ans[tmp][tmp + 1] = true;
    
    ans[tmp][cur] = true;
    int pos = cur + 1;
    cur = tmp;
    tmp = 1;
    for (int i = 0;i < len; i++) {
        if (binary[i]) ans[tmp * 3][pos] = true;
        tmp++;
        pos += 2;
    }
    cout << cur << '\n';
    for (int i = 1; i <= cur; i++) {
        for (int j = 1; j <= cur; j++) {
            if (ans[i][j] || ans[j][i])  cout << 'Y';
            else cout << 'N';
        }
        cout << '\n';
    }
    return 0;
}