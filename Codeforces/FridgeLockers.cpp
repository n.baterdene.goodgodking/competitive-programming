#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
using namespace std;
int n, m, tmp, mySum;
void go() {
    cin >> n >> m;
    tmp = m;
    vector <PII> a(n);
    mySum = 0;
    vector <PII> ans;
    for (int i = 0; i < n - 1; i++) {
        cin >> a[i].ff;
        a[i].ss = i + 1;
        mySum += a[i].ff;
        m--;
        ans.pb(mp(i + 1, (i + 2)));
    }
    cin >> a[n - 1].ff;
    a[n - 1].ss = n;
    if(n > tmp || n == 2) {
        cout << -1 << "\n";
        return;
    }
    ans.pb(mp(n , 1));
    mySum += a[n - 1].ff;
    m--;
    mySum *= 2;
    sort(a.begin(), a.end());
    for (int i = 0; i < m; i++) {
        mySum += ((a[0].ff + a[1].ff));
        ans.pb(mp(a[0].ss, a[1].ss));
    }
    cout << mySum << "\n";
    for (int i = 0; i < ans.size(); i++) 
        cout << ans[i].ff << " " << ans[i].ss << "\n";
    
}
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int T;
    cin >> T;
    while (T--) go();
	return 0;
}