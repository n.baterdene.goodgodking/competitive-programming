#include <bits/stdc++.h>
using namespace std;
int limit[] = {10, 100, 1000, 10000};
int life[] = {4, 7, 10, 12};
void start() {
    cout << "Please choose your level.\n";
    for (int i = 0; i < 4; i++) 
        cout << "lvl " << i + 1 << " : 1 - " << limit[i] << ' ' << "life: " << life[i] << '\n';
    int level, num;
    cin >> level;
    level--;
    int curLimit = limit[level], curLife = life[level];
    srand(time(0));
    int target = rand() % curLimit + 1;
    while (curLife--) {
        cout << "please enter your number :";
        cin >> num;
        if (num == target) {
            cout << "you win!!\n";
            break;
        }
        if (num > target) {
            cout << "it's big\n";
        } else {
            cout << "it's smaller\n";
        }
    }
    cout << "Game Over!!\n";
    cout << "if you want to play again insert 1 otherwise other number\n";
    int status;
    cin >> status;
    if (status == 1) start();
    return;
}
int main () {
    start();
    return 0;
}