#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
#define umap unordered_map
#define inf INT_MAX
#define Ninf INT_MIN
using namespace std;

int n, len, bombs, timeLimit;
vector <int> pos, defuse, boom, a;

bool can(int x) {
	int low = inf;
	for (int i = 0; i < x; i++)
		low = min(low, a[i]);

	vector <PII> curBombs;
	for (int i = 0; i < bombs; i++)
		if (boom[i] > low) curBombs.pb(mp(pos[i], defuse[i]));

	int curTime = 0;
	sort(curBombs.begin(), curBombs.end());
	int last = 0;
	for (int i = 0; i < curBombs.size(); i++) {
        PII s = curBombs[i];
		if (s.ff <= last) {
			curTime += max(0, s.ss - last);
			last = max(s.ss, last);
		} else {
			curTime += s.ss - s.ff + 1;
			last = s.ss;
		}
	}
	curTime = 2 * curTime + len + 1;
	return curTime <= timeLimit;
}

int main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    cin >> n >> len >> bombs >> timeLimit;
	a.resize(n);
	for (int i = 0; i < n; i++)
        cin >> a[i];
	sort(a.begin(), a.end());
	reverse(a.begin(), a.end());
	pos.resize(bombs);
	defuse.resize(bombs);
	boom.resize(bombs);
	for (int i = 0; i < bombs; i++)
        cin >> pos[i] >> defuse[i] >> boom[i];

	int left = 0;
	int right = n + 1;
	while (right - left > 1) {
		int mid = (left + right) / 2;
		if (can(mid)) left = mid;
		else right = mid;
	}
    cout << left << '\n';
	return 0;
}