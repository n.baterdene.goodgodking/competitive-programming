#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
using namespace std;
bool gcd(int a, int b) {
    if (a < b) swap(a, b);
    if (a == b || a % b == 0) {
        if (b == 1) return false;
        return true;
    }
    return gcd(a % b, b);
}
int main () {
    int a, b, n;
    cin >> n;
    for (int i = 0; i < n; i++) {
        cin >> a >> b;
        if (min(a, b) != 1 && gcd(a, b)) cout << "Infinite\n";
        else cout << "Finite\n";
    }
    return 0;
}