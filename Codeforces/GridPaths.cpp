#include <iostream>
#include <vector>
#include <map>
#define mp make_pair

using namespace std;

int ans = 0;
pair <int, int> pos, togsgol;
string path;
map < pair <int, int>, bool > colored;
char moves[4] = {'D', 'U', 'L', 'R'};
int moveX[4] = {0, 0, -1, 1};
int moveY[4] = {1, -1, 0, 0};
void start(int idx);
void moveMe(char move, int idx) {
    // cout << "moving\n";
    for (int i = 0; i < 4; i++) {
        if (move == moves[i]) {
            pos.second += moveX[i];
            pos.first += moveY[i];
            if (pos.second > 6 || pos.first > 6 || pos.second < 0 || pos.first < 0) {
                pos.second -= moveX[i];
                pos.first -= moveY[i];
                cout << endl;
                return;
            }
            if (colored[pos]) {
                pos.second -= moveX[i];
                pos.first -= moveY[i];
                cout << endl;
                return;
            }
            colored[pos] = true;
            start(idx + 1);
            pos.second -= moveX[i];
            pos.first -= moveY[i];
            colored[pos] = false;
            break;
        }
    }
}

void start(int idx) {
    cout << idx << ". pos : " << pos.first << ' ' << pos.second << '\n';
    if (pos == togsgol && idx != 48) return;
    if (idx == 48) {
        if (pos == togsgol) ans++;
        return;
    } 
    if (path[idx] == '?') 
        for (int i = 0; i < 4; i++) 
            moveMe(moves[i], idx);
    else 
        moveMe(path[idx], idx);
}
int main () {
    pair <int, int> pos;
    cin >> path;
    pos = mp(0, 0);
    togsgol = mp(6, 0);
    for (int i = 0; i < 7; i++) 
        for (int j = 0; j < 7; j++) 
            colored[mp(i, j)] = false;
    start(0);
    cout << ans << '\n';
    return 0;
}