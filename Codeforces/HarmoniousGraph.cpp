#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
#define MAX_NODE 200001
using namespace std;

vector <int> parents(MAX_NODE);

int myFind(int i) {
    if (parents[i] == i) return i;
    return parents[i] = myFind(parents[i]);
}
void merge(int b, int a) {
    int from = myFind(a);
    int to = myFind(b);
    parents[from] = parents[to];
}
int main () {
    // freopen("in.txt", "r", stdin);   
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int n, m;
    cin >> n >> m;
    // n = MAX_NODE;
    for (int i = 0; i < n; i++) 
        parents[i] = i;

    int a, b;
    for (int i = 0; i < m; i++) {
        cin >> a >> b;
        a--, b--;
        if (a > b) swap(a, b);
        if (myFind(a) == myFind(b)) continue;
        merge(a, b);
    }
    
    vector <vector <int> > samePar(n);
    for (int i = 0; i < n; i++) 
        samePar[myFind(i)].pb(i);
    
    int ans = 0;
    for (int i = 0; i < n; i++) {
        if (samePar[i].size() == 0) continue;
        int limit = samePar[i].back();
        int st = samePar[i][0];
        for (int j = st + 1; j <= limit; j++) {
            //st->j
            if (myFind(st) != myFind(j)) {
                int tmp = myFind(j);
                merge(st, tmp);
                if (samePar[tmp].size() == 0) limit = max(limit, tmp);
                else limit = max(limit, samePar[tmp].back());
                ans++;
            }
        }
        i = limit;
    }
    cout << ans << '\n';
	return 0;
}