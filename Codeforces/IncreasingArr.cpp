#include <iostream>
#include <vector>
#define int long long
using namespace std;

signed main () {
    int n;
    cin >> n;
    vector <int> numbers(n);
    for (int i = 0; i < n; i++) 
        cin >> numbers[i];
    int ans = 0;
    for (int i = 1; i < n; i++)
        if (numbers[i] < numbers[i - 1]) {
            ans += (numbers[i - 1] - numbers[i]);
            numbers[i] = numbers[i - 1];
        }
    cout << ans << '\n';
    return 0;
}