#include <iostream>
#include <vector>
using namespace std;
int main () {
    vector <int> children;
    int n, power, tmp;
    cin >> n >> power;
    for(int i = 0; i < n; i++){
        cin >> tmp;
        children.push_back(tmp);
    }
    int idx = 0, done = n;
    while(done != 0){
        if(children[idx] > 0) {
            children[idx] -= power;
            if(children[idx] <= 0) done--;
        }
        idx = (idx + 1) % n;
    }
    if(idx == 0) cout << n << '\n';
    else cout << idx << '\n';
    return 0;
}