#include <bits/stdc++.h>
#define ff first
#define ss second
using namespace std;

int lis(vector<int> const& a) {
    int n = a.size();
    const int INF = 1e9;
    vector <pair <int, int> > dp(n+5, {INF, 0}); // val, pos
    vector <int> anc(n+5, -1);
    dp[0] = {-INF, INF};
    for (int i = 0; i < n; i++) {
        int num = a[i];
        pair <int, int> cur = {num, -2};
        int j = (upper_bound(dp.begin(), dp.end(), cur) - dp.begin());
        if (dp[j-1].ff < num && num < dp[j].ff) {
            dp[j] = {num, i};
            anc[i] = dp[j-1].ss;
        }
    }
    int ans = 0, idx = INF;
    for (int i = 1; i <= n; i++) {
        if (dp[i].ff != INF) ans = i, idx = dp[i].ss;
    }
    vector <int> vals;
    while(idx != INF) {
        vals.push_back(a[idx]);
        idx = anc[idx];
    }
    reverse(vals.begin(), vals.end());
    for (auto &el : vals) cout << el << ' ';
    cout << '\n';   
    return ans;
}

int main() {
    vector <int> nums = {0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15};
    cout << lis(nums) << '\n';
    cout << '\n';   
}