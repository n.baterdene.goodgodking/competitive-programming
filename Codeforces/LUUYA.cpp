#include <bits/stdc++.h>
using namespace std;
int main () {
    int n;
    //  = 100000;
    cin >> n;
    queue <int> bfs;
     bfs.push(0);

    // cout << "here\n";
    while(!bfs.empty()) {
        int cur = bfs.front();
        bfs.pop();
        if (cur >= n) break;
        cout << cur << '\n';
        for (int j = 0; j < 10; j++) 
            bfs.push(cur * 10 + j);
    }
    return 0;
}