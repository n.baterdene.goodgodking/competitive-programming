#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
#define MAX 100005
using namespace std;
vector <vector <int> > seen(MAX);
vector <vector <int> > numbers;
int findPer(int pastPer, PII past) {
    vector <int> pos, per;
    int tmp = past.ff;
    for (int j = 0; j < 2; j++) {
        for (int i = 0; i < seen[tmp].size(); i++) 
            pos.pb(seen[tmp][i]), per.pb(i);
        tmp = past.ss;
    }
    int n = pos.size();
    for (int i = 0; i < n; i++) {
        int tmp = pos[i], count = 1;
        for (int j = i + 1; j < n; j++) 
            if (pos[j] == tmp && tmp != pastPer) return pos[j];
    }
}
int findEl(int pastPer, PII past) {
    vector <int> pos = numbers[pastPer];
    for (int i = 0; i < 3; i++) 
        if (pos[i] == past.ff || pos[i] == past.ss) pos[i] = -1;

    for (int i = 0; i < 3; i++) 
        if (pos[i] != -1) return pos[i];
}
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    // freopen("in.txt", "r", stdin);
    // freopen("out.txt", "w", stdout);
    int n, m = 3, num;
    cin >> n;
    for (int i = 0; i < n - 2; i++) {
        vector <int> tmp;
        for (int j = 0; j < m; j++) {
            cin >> num;
            seen[num].pb(i);
            tmp.pb(num);
        }
        numbers.pb(tmp);
    }
    vector <int> st, mid;
    for (int i = 1; i < MAX; i++) {
        if (seen[i].size() == 1) st.pb(i);
        if (seen[i].size() == 2) mid.pb(i);
    }
    int pastPer;
    vector <int> ans(n);
    ans[0] = st[0];
    ans[n - 1] = st[1];
    pastPer = seen[ans[0]][0];
    if (seen[ans[0]][0] == seen[mid[0]][0] || seen[ans[0]][0] == seen[mid[0]][1]) {
        ans[1] = mid[0];
        ans[n - 2] = mid[1];
    } else {
        ans[1] = mid[1];
        ans[n - 2] = mid[0]; 
    }
    
    PII past = mp(ans[0], ans[1]);
    for (int i = 2; i < n - 2; i++) {
        ans[i] = findEl(pastPer, past);
        past = mp(ans[i - 1], ans[i]);
        pastPer = findPer(pastPer, past);
    }
    for (int i = 0; i < n; i++) 
        cout << ans[i] << ' ';
    cout << '\n';
	return 0;
}