#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
#define umap unordered_map
#define INF INT_MAX
#define MOD 1e9 + 7
using namespace std;
int st, last, used = 0;
string word;
vector <string> ans;
int start() {
    used++;
    st++;
    return st - 1;
}
int lasting() {
    used++;
    last--;
    return last + 1;
}
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int n, m, pos;
    cin >> n >> m >> pos;
    int len = n * m;
    last = len - 1;
    word.resize(len);
    ans.resize(n);
    cin >> word;
    sort (word.begin(), word.end());
    for (int i = 0; i < pos; i++)
        ans[i].pb(word[start()]);

    while (used < pos * m) {
        int a = pos - 1;
        for (int i = pos - 2; i >= 0; i--) {
            if (ans[i] == ans[pos - 1]) a = i;
            else break;
        }
        for (int i = a; i < pos; i++) {
            ans[i].pb(word[start()]);
        }
        
        for (int i = a - 1; i >= 0; i--) {
            ans[i].pb(word[lasting()]);
        }
    }
    for (int i = pos; i < n; i++) 
    for (int j = 0; j < m; j++) 
            ans[i].pb(word[start()]);
    
    for (int i = 0; i < n; i++) {
        cout << ans[i] << '\n';
    }
	return 0;
}