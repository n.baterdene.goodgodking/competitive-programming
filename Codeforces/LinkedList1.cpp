#include <iostream>
using namespace std;
    
class MyLinkedList {
    struct Node {   
        int val;
        Node * next;
    };
    Node * head, * pointer;
    
    public:
    
    MyLinkedList() {
        head = NULL;
    }
    int get(int index) {
        if(head == NULL) return -1;
        pointer = head;
        for(int i = 0; i < index; i++) {
            if(pointer->next == NULL) return -1;
            pointer = pointer->next;
        }
        if(pointer == NULL) return -1;
        return pointer->val;
    }
    void addAtHead(int val) {
        if(head == NULL) {
            head = new Node();
            head->val = val;
            head->next = NULL;
            return;
        } 
        Node * node = new Node();
        node->next = head;
        head = node;
        head->val = val;
    }
    void addAtTail(int val) {
        cout << "AddatTail\n";
        if(head == NULL) {
            head = new Node();
            head->val = val;
            head->next = NULL;
            return;
        } 
        pointer = head;
        for(; true; ) {
            if(pointer->next == NULL) break;
            pointer = pointer->next;
        }
        cout << "here\n";
        Node * node = new Node();
        node->val = val;
        pointer->next = node;
        node->next = NULL;
    }
    void addAtIndex(int index, int val) {
        cout << "AddAtIndex\n";
        if(head == NULL && index != 0) return;
        if(head == NULL) {
            head = new Node();
            head->val = val;
            return;
        }
        pointer = head;
        for(int i = 0; i < index - 1; i++) {
            if(pointer->next == NULL) return;
            pointer = pointer->next;
        }
        Node * node = new Node();
        if(pointer->next == NULL) {
            pointer->next = node;
            node->next = NULL;
            node->val = val;
            return;
        }
        Node * next = pointer->next;
        cout << pointer->val << ' ' << next->val << '\n';
        pointer->next = node;
        node->next = next;
        node->val = val;
    }
    void deleteAtIndex(int index) {
        cout << "DeleteAtHead\n";
        if(head == NULL) return;
        pointer = head;
        for(int i = 0; i < index - 1; i++) {
            if(pointer->next == NULL) return;
            pointer = pointer->next;
        }
        Node * next = pointer->next;
        pointer->next = next;
    }
};
/**
 * Your MyLinkedList object will be instantiated and called as such:
 * MyLinkedList* obj = new MyLinkedList();
 * int param_1 = obj->get(index);
 * obj->addAtHead(val);
 * obj->addAtTail(val);
 * obj->addAtIndex(index,val);
 * obj->deleteAtIndex(index);
 */
int main () {
    MyLinkedList a;
    // cout << a.get(0) << '\n';
    // cout << a.get(0) << '\n';
    // cout << a.get(1) << '\n';
    // a.addAtIndex(0, 1);
    // cout << a.get(0) << '\n';
    // cout << a.get(1) << '\n';
    a.addAtHead(38);
    a.addAtHead(45);
    // cout << "Numbers: ";
    // pointer = head;
    // while(true) {
    //     cout << pointer->val << ' '; 
    //     if(pointer->next == NULL) break;
    //     pointer = pointer->next;
    // }
    // cout << '\n';
    a.deleteAtIndex(2);
    a.addAtIndex(1, 24);
    a.addAtTail(36);
    // a.deleteAtIndex(0);
    // a.addAtTail(3);


    return 0;
}