#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
bool mySort(pair <int, int>& pa1, pair <int, int>& pa2){
    return (pa1.first - pa1.second) > (pa2.first - pa2.second);
}

int main () {
    int res, time;
    cin >> res >> time;
    vector < pair <int, int> > cost;
    int une, tsag;
    for(int i = 0; i < res; i++) {
        cin >> une >> tsag;
        cost.push_back(make_pair(une, tsag));
    }
    sort(cost.begin(), cost.end(), mySort);
    cout << '\n';
    for(int i = 0; i < res; i++){
        cout << cost[i].first << ' ' << cost[i].second << '\n';
    }
    cout << '\n';
    int ans = 0;
    for(int i = 0; i < res; i++) {
        if(cost[i].first >= cost[i].second) {
            ans += cost[i].first;
            time -= cost[i].second;
        } else {
            if(time > 0) {
                time -= cost[i].second;
                ans += cost[i].first;
            } else {
                cout << time << '\n';
                cout << "last idx --> " << i << '\n';
                break;
            }
        }
    }
    if(time <= 0) {
        ans += time;
    }
    cout << ans << '\n';
    return 0;
}