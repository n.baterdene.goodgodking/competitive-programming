#include <iostream>
using namespace std;
int main () {
    int res, time, max_score = -999999999;
    cin >> res >> time;
    for(int i = 0; i < res; i++){
        int tmp_ans, wait;
        cin >> tmp_ans >> wait;
        if(wait > time){
            tmp_ans = tmp_ans - wait + time;
        }
        if(max_score < tmp_ans){
            max_score = tmp_ans;
        }
    }
    cout << max_score << '\n';

    return 0;
}