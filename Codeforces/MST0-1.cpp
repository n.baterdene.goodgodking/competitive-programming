#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
using namespace std;
vector <int> notVisited;
vector <set <int> > paths;
set <int> points;
void discover(int pos) {
    queue <int> bfs;
    bfs.push(pos);
    points.erase(pos);
    while (bfs.size() > 0) {
        int cur = bfs.front();
        bfs.pop();
        if (!notVisited[cur]) continue;
        notVisited[cur] = false;

        vector <int> deleting;
        for (auto el : points) 
            if (paths[el].find(cur) == paths[el].end()) {
                bfs.push(el);
                deleting.pb(el);
            }

        for (int i = 0; i < deleting.size(); i++) 
            if (points.find(deleting[i]) != points.end()) 
                points.erase(deleting[i]);
    }
}
int main () {
    int n, m, a, b;
    cin >> n >> m;
    notVisited.resize(n, true);
    paths.resize(n);
    for (int i = 0; i < n; i++) 
        points.insert(i);

    for (int i = 0; i < m; i++) {
        cin >> a >> b;
        a--, b--;
        paths[a].insert(b);
        paths[b].insert(a);
    }
    int ans = 0;
    for (int i = 0; i < n; i++) 
        if (notVisited[i]) {
            ans++;
            discover(i);
        }
        
    cout << ans - 1 << '\n';
    return 0; 
}