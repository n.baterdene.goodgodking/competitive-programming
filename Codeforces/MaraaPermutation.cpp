#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
#define limit numbers.size()
using namespace std;
vector <vector <int> > answers;
void permutation(int idx, vector <int>&numbers) {
    if (idx == limit) return answers.pb(numbers);
    for (int i = idx; i < limit; i++) {
        swap(numbers[i], numbers[idx]);
        permutation(idx + 1, numbers);
        swap(numbers[i], numbers[idx]);
    }
}
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int n;
    cin >> n;
    vector <int> numbers(n);
    for (int i = 0; i < n; i++) 
        cin >> numbers[i];

    permutation(0, numbers);
    cout << answers.size() << '\n';
    for (int i = 0; i < answers.size(); i++) {
        for (int j = 0; j < n; j++) {
            cout << answers[i][j] << ' ' ;
        }
        cout << '\n';
    }
	return 0;
}