#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
#define int long long
#define ff first
#define ss second
#define PII pair <int, int>
using namespace std;
bool mySort(PII&a, PII&b) {
    return a.ss < b.ss;
}
void go() {
    int n, a, b;
    cin >> n;
    vector <PII> len;
    for (int i = 0; i < n; i++) {
        cin >> a >> b;
        len.pb(mp(a, b));
    }
    if (n == 1) {
        cout << 0 << '\n';
        return;
    }
    sort(len.begin(), len.end());
    a = len.back().ff;
    sort(len.begin(), len.end(), mySort);
    b = len[0].ss;
    if (a - b < 0) cout << 0 << "\n";
    else cout << a - b << '\n';
    // cout << abs(b - a) << '\n';
}
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int T;
    cin >> T;
    while(T--) go();
	return 0;
}