#include <iostream>
#include <vector>
#include <math.h>
#define int long long 
using namespace std;
signed main () {
    int n, m;
    cin >> n >> m;
    if (m == 1) {
        cout << n << "\n";
        return 0;
    }
    if (n / m < (m + 1) / 2 || n / (m + 1) < m / 2) {
        cout << -1 << "\n";
        return 0;
    }
    vector <int> divisors;
    vector <int> cur;
    for (int i = 1; i <= sqrt(n); i++) 
        if (n % i == 0) {
            if (n / i == i) divisors.push_back(i);
            else cur.push_back(i), divisors.push_back(n / i);
        }

    for (int i = cur.size() - 1; i >= 0; --i)
        divisors.push_back(cur[i]);
        
    int idx, tmp = n / divisors[0];
    for (idx = 1; idx < divisors.size(); idx++) {
        tmp = n / divisors[idx];
        if (tmp >= m * (m + 1) / 2) break;
    }
    for (int j = 1; j < m; j++) {
        cout << divisors[idx] * j << " ";
        n -= divisors[idx] * j;
    }
    cout << n << "\n"; 
    return 0;
}