#include <algorithm>
#include <iostream>
#include <vector>
 
using namespace std;
 
int main() {
    int T;
    cin >> T;

    while (T > 0) {
        int n;
        cin >> n;

        vector<int> a(n);
        for (int i = 0; i < n; i++)
            cin >> a[i];

        sort(a.begin(), a.end());

        int result = 0;
        int l = n;
        do {
            l--;
            if (n - l > a[l])
            break;
            result = n - l;
        } while (l >= 0);
        if (result > n)
            result = n;

        cout << result << endl;
        T--;
    }
  return 0;
}