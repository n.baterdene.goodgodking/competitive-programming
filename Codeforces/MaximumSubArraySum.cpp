#include <iostream>
#include <vector>
#define int long long
using namespace std;

signed main () {
    int n, ans, tmp;
    cin >> n;
    vector <int> arr(n);
    cin >> arr[0];
    ans = arr[0];
    for(int i = 1; i < n; i++) {
        cin >> arr[i];
        if (arr[i - 1] > 0) 
            arr[i] += arr[i - 1];
        ans = max(ans, arr[i]);
    }
    cout << ans << '\n';


    return 0;
}