#include <iostream>
#include <vector>
#include <algorithm>
#include <stdio.h>

using namespace std;
struct Node {
	int val;
	Node *next;
};

struct LinkedList{
	
	Node *root;

	LinkedList() {
		root = NULL;
	}
	
	void insert(Node *&head, int number) {
		if (head == NULL) {
			head = new Node();
			head->val = number;
			head->next = NULL;
			return;
		}
		Node *tail = head;
		while (tail->next != NULL) 
			tail = tail->next;

		Node *node = new Node();
		node->val = number;
		tail->next = node;
		node->next = NULL;
	}

	void build(vector <int>& numbers, Node *&head) {
		int n = numbers.size();
		
		for (int i = 0; i < n; i++) 
			insert(head, numbers[i]);

	}

	pair <Node*, Node*> split(Node *head) {
		Node *slow = head, *fast = head;

		while(fast->next != NULL && fast->next->next != NULL){
			fast = fast ->next->next;
			slow = slow->next;
		}
		Node* mid = slow->next;
		slow->next = NULL;

		return make_pair(head, mid);
	}
	
	Node* conquer(Node* pointer1, Node* pointer2) {
		Node* ans = new Node();
		Node* it = ans;
		
		while (pointer1 != NULL && pointer2 != NULL) {
			if (pointer1->val < pointer2->val){
				it->next = pointer1;
				pointer1 = pointer1->next;
			}else{
				it->next = pointer2;
				pointer2 = pointer2->next;
			}
			it = it->next;
		}
		if (pointer1 == NULL)
			it->next = pointer2;
		else
			it->next = pointer1;
		
		return ans->next;
	}

	Node* merge_sort(Node *head) {
		if(head->next == NULL) 
			return head;

		pair <Node*, Node*> ans = split(head);

		head = conquer(merge_sort(ans.first),
					   merge_sort(ans.second));

		return head;
	}
};



int main () {
	// freopen("out.txt", "w", stdout);
	int n;
	cin >> n;
	vector <int> numbers(n, 0);
	for (int i = 0; i < n; i++)
		cin >> numbers[i];

	LinkedList mysort;
	mysort.build(numbers, mysort.root);
	Node *ans = mysort.merge_sort(mysort.root);
	cout << "Numbers: ";
	for (int i = 0; i < n; i++) {
		cout << ans->val << ' ';
		ans = ans->next;
	}
	cout << "\n";
	return 0;
}