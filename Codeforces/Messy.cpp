 #include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
using namespace std;
void mySwap(string& word, int i, int j) {
    int len = j - i + 1;
    for (int st = 0; st < len / 2; st++) 
        swap(word[st + i], word[j - st]);
}
void go() {
    int n, m;
    cin >> n >> m;
    m--;
    string word;
    cin >> word;
    vector <PII> ans;
    int tmp = 0;
    for (int i = 0; i < n / 2; i++) 
        if (word[i] == ')') 
            for (int j = i + 1; j < n; j++) 
                if (word[j] == '(') {
                    mySwap(word, i, j);
                    tmp++;
                    ans.pb(mp(i + 1, j + 1));
                    break;
                }
            
    int st = 1, end = n / 2 + 1;
    while (m--) {
        tmp++;
        ans.pb(mp(st + 1, end));
        st += 2, end ++;
    }
    cout << ans.size() << '/n';
    for (int i = 0; i < ans.size(); i++) {
        cout << ans[i].ff << ' ' << ans[i].ss << '/n';
    }
    return;
}
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int T;
    cin >> T;
    while(T--) go();
	return 0;
}