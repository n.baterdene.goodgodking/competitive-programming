#include <bits/stdc++.h>
#define pb push_back
using namespace std;
int main () {
    int n, m;
    cin >> n >> m;
    vector <int> tmp(n), numbers;
    for (int i = 0; i < n; i++) 
        cin >> tmp[i];
    
    int choose = n - m;
    sort(tmp.begin(), tmp.end());
    for (int i = 0; i < n - 1; i++) {
        if (tmp[i] == tmp[i + 1]) {
            i += 2;
            choose--;
        }
        else numbers.pb(tmp[i]);
    }
    int used = 0;
    if (m * 2 > n) choose -= (m * 2 - n);

    return 0;
}