#include <bits/stdc++.h>
#define pb push_back
using namespace std;

vector<int> myFunc (int n, vector<int> arr, vector<int> elements) {
    vector<bool> check (n, 0);
    for (int i = 0; i < n; i++) 
        for (int j = arr[i]; j > 0; j--) {
            if(!check[j - 1] && elements[j] < elements[j - 1]) {
                swap(arr[i], arr[elements[j - 1] - 1]);
                swap(elements[j], elements[j - 1]);
                check[j - 1] = 1;
            } else break;
        }

    return elements;
}

int main () {
    int q, n;
    cin >> q;
    vector<vector<int> > results;
    for (int i = 0; i < q; i++) {
        cin >> n;
        vector<int> arr(n);
        vector<int> elements(n);
        for (int j = 0; j < n; j++) {
            cin >> elements[j];
            arr[elements[j] - 1] = j;
        }
        results.pb(myFunc(n, arr, elements));
    }
    for (int i = 0; i < q; i++) {
        for(int j = 0; j < results[i].size() - 1; j++) 
            cout << results[i][j] << " ";
        cout << results[i][results[i].size() - 1] << endl;
    }
    return 0;
}
