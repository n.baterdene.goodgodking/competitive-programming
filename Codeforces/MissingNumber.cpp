#include <iostream>
using namespace std;
int main () {
    long long ans;
    cin >> ans ;
    int n = ans, tmp;
    ans *= (ans + 1);
    ans /= 2;
    for(int i = 0; i < n - 1; i++) {
        cin >> tmp;
        ans -= tmp;
    }
    cout << ans << '\n';
    return 0;
}