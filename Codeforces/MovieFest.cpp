#include <iostream>
#include <vector>
#include <algorithm>
#define mp make_pair

using namespace std;

bool sortbysec(const pair<int, int> &a, const pair<int, int> &b) { 
    return (a.second < b.second); 
} 
  
int main () {
    int n;
    cin >> n;
    vector <pair <int, int> > movies(n);
    int st, end;
    for(int i = 0; i < n; i++) {
        cin >> st >> end;
        movies[i] = mp(st, end);
    }
    sort (movies.begin(), movies.end(), sortbysec);
    int time = 0, ans = 0;
    for(int i = 0; i < n; i++) 
        if(movies[i].first >= time) {
            time = movies[i].second;
            ans++;
        }

    cout << ans << '\n';
    return 0;
}