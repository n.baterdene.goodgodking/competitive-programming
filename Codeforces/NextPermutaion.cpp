#include <iostream>
#include <vector>
using namespace std;
int main (){
    int n;
    cin >> n;
    vector<int> vec;
    int a;
    for(int i = 0; i < n; i++){
        cin >> a;
        vec.push_back(a);
    }
    int cnt = 1;
    do{
        // cout << cnt++ << ": ";
        for(int i = 0; i < n; i++){
            cout << vec[i] << ' ';
        }
        cout << '\n';
    } while(next_permutation(vec.begin(), vec.end()));
}