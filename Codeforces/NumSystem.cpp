#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
#define umap unordered_map
using namespace std;
char chars[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
umap <char, int> pos;
int returnBase(int n, string num) {
    int tmp = 1, ans = 0;
    for (int i = num.size() - 1; i >= 0; i--) {
        ans += pos[num[i]] * tmp;
        tmp *= n;
    }
    return ans;
}
string AtoBaseB(int num, int n) {
    string ans = "";
    while (num > 0) {
        ans = chars[num % n] + ans;
        num /= n;
    }
    return ans;
}
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int n, m;
    string num;
    for (int i = 0; i < 35; i++) 
        pos[chars[i]] = i;
    // cout << AtoBaseB(6, 2) << '\n';
    // cout << returnBase(2, "111") << '\n';
    cin >> n >> m >> num;
    cout << AtoBaseB(returnBase(n, num), m) << '\n';
	return 0;
}