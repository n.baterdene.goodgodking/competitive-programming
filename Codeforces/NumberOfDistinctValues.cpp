#include <iostream>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>
#include <vector>
using namespace std;
int main () {
    // freopen("in.txt" , "r", stdin);
    int n;
    cin >> n;
    vector <int> numbers(n);
    for(int i = 0; i < n; i++) 
        cin >> numbers[i];
    sort(numbers.begin(), numbers.end());
    int ans = 0;
    for(int i = 1; i < n; i++) 
        if(numbers[i] != numbers[i - 1]) ans++;
    cout << ans + 1 << '\n';
    return 0;
}