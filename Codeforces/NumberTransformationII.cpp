#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
#define umap unordered_map
#define INF INT_MAX
#define MOD 1e9 + 7
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MIN(a, b) ((a) > (b) ? (a) : (b))
using namespace std;
ll n, a, b, ans = 0;
map <vector< pair <ll, ll> >, int> seen;
vector <pair <ll, ll> > numbers(100005);
void subtract() {
    ans++;
    int best = 1, dif = a - b;
    for (int i = 0; i < n; i++) {
        if (numbers[i].ss > dif) continue;
        best = MAX(numbers[i].ss, best);
    }
    for (int i = 0; i < n; i++) {
        numbers[i].ss -= best;
        if (numbers[i].ss < 0) 
            numbers[i].ss = (numbers[i].ff + (numbers[i].ss % numbers[i].ff)) % numbers[i].ff;
    }
    a -= best;
    return;
}
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    cin >> n;
    for (int i = 0; i < n; i++) 
        cin >> numbers[i].ff;
    
    cin >> a >> b;
    for (int i = 0; i < n; i++) 
        numbers[i].ss = (a % numbers[i].ff);

    while (a > b) subtract();
    cout << ans << '\n';
	return 0;
}
