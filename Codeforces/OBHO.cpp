#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

vector <int> dp, numbers, ans;
void myFunc(int i) {
    // cout << "here\n";
    // cout << i << ' ';
    if(i == 0) return;
    if(i > 0 && numbers[i] > numbers[i - 1]) 
        dp[i] = i - 1;
    int idx = i - 1;
    while(idx >= 0 && numbers[idx] >= numbers[i]) {
        myFunc(idx);
        idx = dp[idx];
    }
    if(idx >= 0) dp[i] = idx;

}
int main () {
    int n, tmp;
    cin >> n;
    for(int i = 0; i < n; i++) 
        dp.push_back(-1);
    for(int i = 0; i < n; i++) {
        cin >> tmp;
        numbers.push_back(tmp);
    }
    int i = n - 1;
    while(i > 0) {
        myFunc(i);
        i = dp[i];
    }
    // for(int i = n - 1; i >= 0; i--) 
    //     if(oroogui[i]) myFunc(i);

    for(int i = 0; i < n; i++) {
        cout << dp[i] + 1 << ' ';
    }
    cout << '\n';
    return 0;
}