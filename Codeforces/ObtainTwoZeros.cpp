#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define int long long
#define ff first
#define ss second
#define umap unordered_map
using namespace std;
void go () {
    int num1, num2;
    cin >> num1 >> num2;
    if (num1 < num2) {
        swap(num1, num2);
    }
    if (num1 > 2 * num2) {
        cout << "no\n";
        return;
    }
    if ((num1 + num2) % 3 == 0) 
        cout << "yes\n";
    else cout << "no\n";
}

signed main () {
    int t;
    cin >> t;
    while(t--) go();
    return 0;
}