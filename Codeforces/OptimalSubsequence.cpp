#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
#define PII pair <int, int>
#define forn(a, b) for (int a = 0; a < b; a++)
using namespace std;

void runQuery(set <PII > myNums, int n, int k, int pos) {
	set <PII > mySeq;
	auto it = prev(myNums.end());
	forn(i, k) {
		auto tmp = *it;
		if (it != myNums.begin()) it--;
		mySeq.emplace(n - tmp.second, tmp.first);
	}

	cout << next(mySeq.begin(), pos - 1)->second << endl;
}

int main() {
	int n;
	cin >> n;

	int t;
	set <PII > myNums;
	forn(i, n) {
		cin >> t;
		myNums.emplace(t, n - i);
	}

	int m;
	cin >> m;
	vector <PII > myQueue(m);
	forn(i, m) 
        cin >> myQueue[i].first >> myQueue[i].second;
	forn(i, m)
         runQuery(myNums, n, myQueue[i].first, myQueue[i].second);
	return 0;
}
