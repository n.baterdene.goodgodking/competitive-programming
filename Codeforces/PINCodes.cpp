#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
#define umap unordered_map
using namespace std;
string tmp;
void go() {
    int n, count = 0;
    cin >> n;
    vector <string> ping, pong;
    vector <vector <bool> > seen(4, vector <bool> (10, 0));
    umap <string, int> map;
    vector <bool> temp(n, 0);
    for (int i = 0; i < n; i++) {
        cin >> tmp;
        ping.pb(tmp);
        if (map.find(tmp) == map.end()) {
            map[tmp] = 1;
            temp[i] = 1;
            for (int d = 0; d < 4; d++)
                seen[d][int(tmp[d] - '0')] = 1;
        }
    }

    for (int i = 0; i < ping.size(); i++) {
        if (temp[i]) continue;
        count++;
        for (int w = 0; w < 4; w++) {
            for (int h = 0; h < 10; h++){
                if (!seen[w][h]) {
                    ping[i][w] = char(h + int('0'));
                    seen[w][h] = 1;
                    w = 3;
                    break;
                }
            }
        }
    }
    cout << count << "\n";
    for (int i = 0; i < n; i++) 
        cout << ping[i] << "\n";
    return;
}
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int T;
    cin >> T;
    while(T--) go();
	return 0;
}