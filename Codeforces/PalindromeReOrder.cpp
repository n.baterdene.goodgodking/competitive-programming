#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;
int main () {
    string str;
    cin >> str;
    unordered_map <char, int> seen;
    int n = str.size();
    for (int i = 0; i < n; i++) {
        if (seen.find(str[i]) == seen.end()) 
            seen[str[i]] = 1;
        else 
            seen[str[i]]++;
    }
    unordered_map <char, int> :: iterator it;
    vector <pair <char, int> > ans;
    bool odd = false;
    char oddchar;
    for (it = seen.begin(); it != seen.end(); it++) {
        if (it->second % 2 == 0) 
            ans.push_back(make_pair(it->first, it->second));
        else {
            if(!odd) {
                odd = true;
                oddchar = it->first;
                ans.push_back(make_pair(it->first, it->second - 1));
            } else {
                cout << "NO SOLUTION\n";
                return 0;
            }
        }
    }
    for(int i = 0; i < ans.size(); i++) 
        for(int times = 0; times < ans[i].second / 2; times++) 
            cout << ans[i].first;
    if (odd) cout << oddchar;

    for(int i = ans.size() - 1; i >= 0; i--) 
        for(int times = 0; times < ans[i].second / 2; times++) 
            cout << ans[i].first;
    cout << '\n';
    return 0;
}