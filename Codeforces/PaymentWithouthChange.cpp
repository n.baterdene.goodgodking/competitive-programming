#include <bits/stdc++.h>
#define int long long
using namespace std;
signed main () {
    int N;
    int a, b, s, n;
    cin >> N;
    for (int i = 0; i < N; i++) {
        cin >> a >> b >> n >> s;
        int times = min (a, s / n);
        if (s - n * times <= b) {
            cout << "YES\n";
            continue;
        }
        cout << "NO\n";

    }
    return 0;
}