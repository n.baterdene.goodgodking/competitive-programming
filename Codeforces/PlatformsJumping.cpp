#include <bits/stdc++.h>
using namespace std;
int main () {
    cout << INT_MAX << '\n';
    int len, n, jump, sum = 0;
    cin >> len >> n >> jump;
    vector <int> bridge(n);
    for (int i = 0; i < n; i++) {
        cin >> bridge[i];
        sum += bridge[i];
    }
    //hamgiin holdoo haana hurj boloh ve?
    int left = (jump - 1) * (n + 1) + sum - len;
    if (left < 0 || sum > len) {
        cout << "NO\n";
        return 0;
    }
    //gold her urttai gazar us baih ve?b
    left = len - sum;
    int cur = 0;
    vector <int> ans(len, 0);
    for (int curBridge = 0; curBridge < n; curBridge++) {
        if (jump - 1 > left) {
            jump = 0;
            cur += left;
        } else {
            cur += (jump - 1);
            left -= (jump - 1);
        }
        for (int i = 0; i < bridge[curBridge]; i++) {
            ans[cur] = curBridge + 1;
            cur++;
        }
    }

    cout << "YES\n";
    for (int i = 0; i < len; i++) 
        cout << ans[i] << ' ';
    cout << '\n';
    return 0;
}