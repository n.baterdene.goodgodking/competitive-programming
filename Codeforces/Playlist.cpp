#include <iostream>
#include <vector>
#include <map>
#include <queue>
using namespace std;
int main () {
    int n;
    cin >> n;
    queue <int> numbers;
    map <int, int> heard;
    vector <int> songs(n);
    int cur = 0, maximum = 1;
    for (int i = 0; i < n; i++) 
        cin >> songs[i];
 
    for (int i = 0; i < n; i++) { 
        maximum = max(maximum, cur);
        int song = songs[i];
        if (i < n && songs[i + 1] == song) {
            while (numbers.size() > 0) 
                numbers.pop();
            cur = 0;
            heard.clear();
            continue;
        }
        if (heard.find(song) != heard.end() && heard[song] != -1) {
            cur = i - heard[song];
            while (true) {
                int popped = numbers.front();
                numbers.pop();
                heard[popped] = -1;
                if (popped == song) break;
            }
            numbers.push(song);
            heard[song] = i;
            continue;
        }
        heard[song] = i;
        cur++;
        numbers.push(song);
    }
    maximum = max(maximum, cur);
    cout << maximum << '\n';
    return 0;
}