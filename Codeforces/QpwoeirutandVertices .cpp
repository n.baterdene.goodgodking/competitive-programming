#include <bits/stdc++.h>
// #pragma GCC optimize("O3, unroll-loops")
// #pragma GCC target("avx2")
// #define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define ub upper_bound 
#define sz(x) (int)x.size()
#define chmin(a,b) a=min(a,b)
#define chmax(a,b) a=max(a,b)

using ll = long long;
using db = double;
using ld = long double;
using uint = unsigned;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using PII = pair<int, int>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using PLL = pair<ll, ll>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}
/*

|                    _                                                     
|  __   _           | |                                                    
| |   \| | ___  ___ | |_                                                   
| | |\ | |/ _ \/ __/| __|                                                  
| | | \  |  __/\__ \| |_                                                   
| |_|  \_|\___ /___/ \__|                                                  
|                                                      _         _   _     
|                                                     (_)       | | | |    
|  _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_   
| | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|  
| | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_   
| | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|  
| | |               __/ |                                                  
| |_|              |___/                   _                               
|                                  _      (_)                              
|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                               
| | '_ ` _ \ / _` |_   / _` |/ _` | |/ _` | |                              
| | | | | | | (_|  /  / (_| | (_| | | (_| | |                              
| |_| |_| |_|\__,_|____\__,_|\__,_|_|\__,_|_|                              
*/

const db PI = acos(-1.0); //M_PI;
const ll INFF = 4e18;
const int INF = 1.07e9;
const int MOD = 1e9 + 7;
const int MOD1 = 998244353; //7*19*2^23 +1;
const int dx[] = {-1, 0, 1, 0, -1, -1, 1, 1};
const int dy[] = {0, 1, 0, -1, -1, 1, -1, 1};




const int N = 1e5 + 5;
const int M = 3*N;

int n, m, k, q, i;
int dp[N], par[N]; // minimumChild, isBridge
vector <int> children[N];
int find(int a) {
    if (par[a] == a) return a;
    return par[a] = find(par[a]);
}
void merge(int a, int b) {
    a = find(a);
    b = find(b);
    if (a == b) return;
    if (sz(children[a]) < sz(children[b])) swap(a, b);
    par[b] = a;
    for (auto el : children[b]) {
        children[a].pb(el);
        if (dp[el-1] == -1 && find(el-1) == find(el)) dp[el-1] = i;
        if (dp[el] == -1 && find(el+1) == find(el)) dp[el] = i;
    }
    // cout << a << ' ' << b <<  " merged:\n";
    // for (auto el : children[a]) cout << el << ' '; cout << '\n';
    // for (auto el : children[b]) cout << el << ' '; cout << '\n';
}
int node[M];
void build(int l, int r, int head) {
    if (l == r) {
        node[head] = dp[l];
        return;
    }
    int mid = (l+r)>>1;
    build(l, mid, head*2+1);
    build(mid+1, r, head*2+2);
    node[head] = max(node[head*2+1], node[head*2+2]);
}
int query(int l, int r, int L, int R, int head) {
    if (l > R || L > r || L > R) return 0;
    if (L <= l && r <= R) return node[head];
    int mid = (l+r)>>1;
    return max(
        query(l, mid, L, R, head*2+1),
        query(mid+1, r, L, R, head*2+2)
    );
}
void go () {
    cin >> n >> m >> q;
    for (i = 1; i <= n; i++) {
        dp[i] = -1;
        par[i] = i;
        children[i] = {i};
    }
    // cout << LINE;
    for (i = 1; i <= m; i++) {
        int a, b; cin >> a >> b;
        merge(a, b);
        // for (int i = 1; i <= n; i++) cout << find(i) <<' '; cout << '\n';
    }
    // for (int i = 1; i < n; i++) cout << dp[i] << " "; cout << '\n';
    build(1, n-1, 0);
    for (i = 1; i <= q; i++) {
        int a, b; cin >> a >> b;
        cout << query(1, n-1, a, b-1, 0) << " ";
    }
    cout << '\n';
}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("0.in", "r", stdin);
    freopen("0.out", "w", stdout);
#endif
    _upgrade
    int T = 1;
    cin >> T;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














