#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
using namespace std;
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int a, b, x, y;
    cin >> a >> b >> x >> y;
    if (a == x || b == y || a + b == x + y || a - b == x - y) 
        cout << "Yes\n";
    else cout << "No\n";

	return 0;
}
