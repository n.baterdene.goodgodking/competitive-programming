#include <bits/stdc++.h>
#define mp make_pair
#define PII pair <int, int>
using namespace std;
struct segmentTree {
    PII val;
    segmentTree *left, *right;
    segmentTree() {
        left = NULL;
        right = NULL;
        val = mp(0, 0);
    }
};
segmentTree *root = new segmentTree();
void build (int l, int r, segmentTree *head) {
    // cout << l << ' ' << r << '\n';
    if (l == r) return;
    int mid = l + (r - l) / 2;
    segmentTree *tmp1 = new segmentTree();
    segmentTree *tmp2 = new segmentTree();
    head->left = tmp1;
    head->right = tmp2;
    // cout << "hahaha1\n";
    build(l, mid, head->left);
    build(mid + 1, r, head->right);
}
void update (int l, int r, int st, int end, segmentTree* head) {
    // cout << l << ' ' << r << ' ' << st << ' ' << end << '\n';
    if (r < st || l > end) return;
    if (l == r) {
        head->val.first++;
        head->val.second++;
        return;
    }
    int mid = (l + r) / 2;
    if (mid >= st) update(l, mid, st, end, head->left);
    if (mid <= end) update(mid + 1, r, st, end, head->right);
    head->val.first = max(head->left->val.first, head->right->val.first);
    head->val.second = min(head->left->val.second, head->right->val.second);
}
int maxQuery(int l, int r, int st, int end, segmentTree* head) {
    // cout << "testing " << l << ' ' << r << ' ' << st << ' ' << end << '\n';
    if (st <= l && r <= end) {
        // cout << "here " << l << ' ' << r << '\n';
        return head->val.first;
    }
    if (l == r) return head->val.first;
    int mid = (l + r) / 2;
    int ans = INT_MIN;
    if (mid >= st) ans = max(ans, maxQuery(l, mid, st, end, head->left));
    if (mid <= end) ans = max(ans, maxQuery(mid + 1, r, st, end, head->right));
    ans = max(head->val.second, ans);
    return ans;
}
int minQuery(int l, int r, int st, int end, segmentTree* head) {
    if (st <= l && r <= end) return head->val.second;
    if (l == r) return head->val.second;
    int mid = (l + r) / 2;
    int ans = INT_MAX;
    if (mid >= st) ans = min(ans, minQuery(l, mid, st, end, head->left));
    if (mid <= end) ans = min(ans, minQuery(mid + 1, r, st, end, head->right));
    ans = min(head->val.first, ans);
    return ans;
}
int main () {
    int n, m, a, b;
    cin >> n;
    n--;
    build(0, n, root);
    cin >> m;
    string tmp;
    for (int i = 0; i < m; i++) {
        cin >> tmp >> a >> b;
        if (tmp == "FIND") {
            int y = minQuery(0, n, a, b, root);
            if (y == INT_MAX) y = 0;
            int x = maxQuery(0, n, a, b, root);
            if (x == INT_MIN) x = 0;

            cout << "ans: " << x << ' ' << y << '\n';
        } else {
            update(0, n, a, b, root);
        }
    }
    cout << 0 << ' ' << 3 << ' ' << root->val.first << ' ' << root->val.second << '\n';
    cout << 0 << ' ' << 1 << ' ' << root->left->val.first << ' ' << root->left->val.second << '\n';
    cout << 2 << ' ' << 3 << ' ' << root->right->val.first << ' ' << root->right->val.second << '\n';
    cout << 0 << ' ' << 0 << ' ' << root->left->left->val.first << ' ' << root->left->left->val.second << '\n';
    cout << 1 << ' ' << 1 << ' ' << root->left->right->val.first << ' ' << root->left->right->val.second << '\n';
    cout << 2 << ' ' << 2 << ' ' << root->right->left->val.first << ' ' << root->right->left->val.second << '\n';
    cout << 3 << ' ' << 3 << ' ' << root->right->right->val.first << ' ' << root->right->right->val.second << '\n';
    return 0;
}