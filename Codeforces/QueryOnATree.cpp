#include <bits/stdc++.h>
using namespace std;
#define _upgrade ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define REP(i, a) for (int i = 0; i < a; i++)
#define ALL(A) A.begin(), A.end()
#define pb push_back

typedef vector<int> VI;
typedef vector<bool> VB;
typedef pair<int, int> PII;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI; 
struct Node {
    int val;
    Node *l, *r;
    void addSide() {
        l = new Node();
        r = new Node();
    }
    Node() {
        l = r = NULL;
    }
};

VVVI paths;
VVI change, allBranch, allBranchCosts;
VI emptyVector, colors, parBranch, startIdx, elementIdx, lvl, numbers;
Node *root;
map <PII, int> edgePos;
VB vis, visLvl;
int color, n;
void build (Node *head, int l, int r) {
    if (l == r) {
        head->val = numbers[l];
        return;
    }
    head->addSide();
    int mid = (l + r)>>1;
    build(head->l, l, mid);
    build(head->r, mid + 1, r);
    head->val = max(head->l->val, head->r->val);
}
int query(int st, int end, int l, int r, Node *head) {
    if (l > r || r < st || end < l) return 0;
    if (st <= l && r <= end) return head->val;
    int mid = (l + r)>>1;
    return max(
        query(st, end, l, mid, head->l),
        query(st, end, mid + 1, r, head->r)
    );
}
void update(int l, int r, int idx, int num, Node *head) {
    if (l == r) {
        numbers[idx] = num;
        head->val = num;
        return;
    }
    int mid = (l + r)>>1;
    if (mid > idx) update(l, mid - 1, idx, num, head->l);
    else if (mid < idx) update(mid + 1, r, idx, num, head->r);
    else update(l, mid, idx, num, head->r);
    head->val = max(
        head->l->val, 
        head->r->val
    );
}
int buildHLD(int pos, int cost) {
    vis[pos] = 1;
    if (paths[pos].size() == 1 && vis[paths[pos][0][0]]) {
        allBranch.pb(emptyVector);
        allBranchCosts.pb(emptyVector);
        colors[pos] = color;
        allBranch[color].pb(pos);
        allBranchCosts[color++].pb(cost);
        return 1;
    }
    int maxSize = 0, child = paths[pos][0][0];
    for (auto el : paths[pos]) {
        if (vis[el[0]]) continue;
        int curSize = buildHLD(el[0], el[1]);
        if (curSize > maxSize) {
            maxSize = curSize;
            child = el[0];
        }
    }
    colors[pos] = colors[child];
    allBranch[colors[pos]].pb(pos);
    allBranchCosts[colors[pos]].pb(cost);
    return allBranch[colors[pos]].size();
}
void init() {
    cin >> n;
    color = 0;
    lvl.clear();
    vis.clear();
    visLvl.clear();
    paths.clear();
    change.clear();
    colors.clear();
    startIdx.clear();
    elementIdx.clear();
    parBranch.clear();
    lvl.resize(n + 5, -1);
    paths.resize(n + 5);
    colors.resize(n + 5);
    change.resize(n + 5);
    vis.resize(n + 5, 0);
    visLvl.resize(n + 5, 0);
    startIdx.resize(n + 5);
    elementIdx.resize(n + 5);
    parBranch.resize(n + 5, 0);
    REP(i, n - 1) {
        int a, b, c;
        cin >> a >> b >> c;
        change[i + 1] = {a, b, (int)paths[a].size(), (int)paths[b].size()};
        paths[a].pb({b, c});
        paths[b].pb({a, c});
    }   
}
void findLvl(int pos, int curLvl) {
    for (auto el : paths[pos]) {
        if (visLvl[el[0]]) continue;
        visLvl[el[0]] = 1;
        if (colors[pos] != colors[el[0]]) {
            lvl[colors[el[0]]] = curLvl + 1;
            parBranch[colors[el[0]]] = pos;
            findLvl(el[0], curLvl + 1);
        } else {
            lvl[colors[el[0]]] = curLvl;
            findLvl(el[0], curLvl);
        }
    }
}
int goUp(int& pos) {
    int curMax, curBranch = colors[pos], curIdx = elementIdx[pos];
    curMax = query(startIdx[curBranch], startIdx[curBranch] + curIdx - 1, 0, numbers.size() - 1, root);
    pos = parBranch[colors[pos]];
    return curMax;
}
int findLCA(int a, int b) {
    int ans = 0;
    if (lvl[colors[a]] > lvl[colors[b]]) swap(a, b);

    while(lvl[colors[a]] < lvl[colors[b]]) {
        int tmp = goUp(b);
        ans = max(ans, tmp);
    }
    while(colors[a] != colors[b]) {
        int tmp = goUp(a);
        ans = max(ans, tmp);
        tmp = goUp(b);
        ans = max(ans, tmp);
    }
    if (elementIdx[a] > elementIdx[b]) swap(a, b);
    ans = max(
        ans, 
        query(startIdx[colors[a]] + elementIdx[a], startIdx[colors[a]] + elementIdx[b] - 1, 0, numbers.size() - 1, root)
    );
    return ans;
}
void go () {
    init();
    int size = buildHLD(1, 0);
    visLvl[1] = 1;
    findLvl(1, 0);
    int cnt = 0;
    REP(i, color) {
        reverse(ALL(allBranch[i]));
        reverse(ALL(allBranchCosts[i]));
        startIdx[i] = numbers.size();
        REP(j, allBranchCosts[i].size()) {
            if (j > 0) {
                edgePos[{allBranch[i][j], allBranch[i][j - 1]}] = numbers.size();
                edgePos[{allBranch[i][j - 1], allBranch[i][j]}] = numbers.size();
            } else {
                if (parBranch[i] != 0) {
                    edgePos[{allBranch[i][j], parBranch[i]}] = numbers.size();
                    edgePos[{parBranch[i], allBranch[i][j]}] = numbers.size();
                }
            }
            if (allBranchCosts[i][j] != 0) numbers.pb(allBranchCosts[i][j]);
        }
    }
    REP(i, color) 
    REP(j, allBranch[i].size()) 
        elementIdx[allBranch[i][j]] = j;

    root = new Node();
    build(root, 0, numbers.size() - 1);
    string curQ;
    while(cin >> curQ) {
        if (curQ == "DONE") break;
        int a, b;
        cin >> a >> b;
        if (curQ[0] == 'Q') {
            cout << findLCA(a, b) << '\n';
        } else {
            update(0, n - 1, edgePos[{change[a][0], change[a][1]}], b, root);
        }
    }

}



signed main () {

#ifdef ONLINE_JUDGE
#else
    freopen("in.txt", "r", stdin);
#endif
    _upgrade
    int T = 1;
    cin >> T;
    while(T--) go();
    return 0;
}
