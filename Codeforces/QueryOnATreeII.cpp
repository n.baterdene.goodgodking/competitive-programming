#include <bits/stdc++.h>
#define int long long
using namespace std;
// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))
 
#define _upgrade ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define II <int, int>
#define Q queue
#define ff first
#define bk back()
#define ss second
#define rs resize
#define ins insert 
#define fr front() 
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define PQ priority_queue
#define umap unordered_map
#define sz(x) (int)x.size()
 
typedef long long ll;
typedef double db;
typedef unsigned uint;
typedef unsigned long long ull;
typedef unordered_map<int, int> umapII;
// PQ going up <int, VI, greater<int> >
typedef vector<int> VI;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<bool> VB;
typedef vector<VB> VVB;
typedef vector<VVB> VVVB;
typedef vector<umapII> VumapII;
typedef vector<ll> VLL;
typedef vector<VLL> VVLL;
typedef vector<VVLL> VVVLL;
typedef vector<VVVLL> VVVVLL;
typedef vector<db> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VPI> VVPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
typedef vector<VVVI> VVVVI;
typedef vector<VVVVI> VVVVVI;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
 
 
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
void readI(int& a) {
    cin >> a;
}
void readI(int& a, int&b) {
    cin >> a >> b;
}
void readI(int& a, int&b, int&c) {
    cin >> a >> b >> c;
}
VI readVI(int n) {
    VI a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVI readVVI(int n, int m) {
    VVI a(n, VI(m));
    REP(i, n) a[i] = readVI(m);
    return a;
}
VLL readVLL(ll n) {
    VLL a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVLL readVVLL(ll n, ll m) {
    VVLL a(n, VLL(m));
    REP(i, n) a[i] = readVLL(m);
    return a;
}
 
void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}
 
void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
// const int MOD = int(1e9) + 7;
const int INF = INT_MAX;
const ll INFF = INT64_MAX;
const db EPS = 1e-9;
const db PI = acos(-1.0); //M_PI;
const int moveX[] = {-1, 0, 1, 0};
const int moveY[] = {0, 1, 0, -1};
 
//                                        .....'',;;::cccllllllllllllcccc:::;;,,,''...'',,'..
//                             ..';cldkO00KXNNNNXXXinp000OOinpinpkxxxxxddoooddddddxxxxinpinpOO0XXKx:.
//                       .':ok0KXXXNXK0kxolc:;;,,,,,,,,,,,;;,,,''''''',,''..              .'lOXKd'
//                  .,lx00Oxl:,'............''''''...................    ...,;;'.             .oKXd.
//               .cinpinpc'...'',:::;,'.........'',;;::::;,'..........'',;;;,'.. .';;'.           'kNKc.
//            .:kXXk:.    ..       ..................          .............,:c:'...;:'.         .dNNx.
//           :0NKd,          .....''',,,,''..               ',...........',,,'',,::,...,,.        .dNNx.
//          .xXd.         .:;'..         ..,'             .;,.               ...,,'';;'. ...       .oNNo
//          .0K.         .;.              ;'              ';                      .'...'.           .oXX:
//         .oNO.         .                 ,.              .     ..',::ccc:;,..     ..                lXX:
//        .dNX:               ......       ;.                'cxOinp0OXWWWWWWWNX0kc.                    :KXd.
//      .l0N0;             ;d0inpinpKXK0ko:...              .l0X0xc,...lXWWWWWWWWKO0Kx'                   ,ONKo.
//    .lKNKl...'......'. .dXWN0inpk0NWWWWWN0o.            :KN0;.  .,cokXWWNNNNWNinpxONK: .,:c:.      .';;;;:lk0XXx;
//   :KN0l';ll:'.         .,:lodxxkO00KXNWWWX000k.       oXNx;:oinpX0kdl:::;'',;coxinpd, ...'. ...'''.......',:lxKO:.
//  oNNk,;c,'',.                      ...;xNNOc,.         ,d0X0xc,.     .dOd,           ..;dOKXK00000Ox:.   ..''dKO,
// 'KW0,:,.,:..,oxinpkdl;'.                'inp'              ..           .dXX0o:'....,:oOXNN0d;.'. ..,lOKd.   .. ;KXl.
// ;XNd,;  ;. l00kxoooxKXKx:..ld:         ;inp'                             .:dkO000000Okxl;.   c0;      :inp;   .  ;XXc
// 'XXdc.  :. ..    '' 'kNNNinpinp,      .,dKNO.                                   ....       .'c0NO'      :X0.  ,.  xN0.
// .kNOc'  ,.      .00. ..''...      .l0X0d;.             'dOkxo;...                    .;oinpXK0KNXx;.   .0X:  ,.  lNX'
//  ,inpdl  .c,    .dNK,            .;xXWKc.                .;:coOXO,,'.......       .,lx0XXOo;...oNWNXinp:.'KX;  '   dNX.
//   :XXkc'....  .dNWXl        .';l0NXNKl.          ,lxinpkxo' .cK0.          ..;lx0XNX0xc.     ,0Nx'.','.kXo  .,  ,KNx.
//    cXXd,,;:, .oXWNNKo'    .'..  .'.'dinp;        .cooollox;.xXXl     ..,cdOKXXX00NXc.      'oKWK'     ;k:  .l. ,0Nk.
//     cXNx.  . ,KWX0NNNXOl'.           .o0Ooldk;            .:c;.':lxOinpK0xo:,.. ;XX:   .,lOXWWXd.      . .':,.lKXd.
//      lXNo    cXWWWXooNWNXinpo;'..       .lk0x;       ...,:ldk0KXNNOo:,..       ,OWNOxO0KXXNWNO,        ....'l0Xk,
//      .dNK.   oNWWNo.cXK;;oOXNNXK0kxdolllllooooddxk00inpinp0kdoc:c0No        .'ckXWWWNXkc,;kNKl.          .,kXXk,
//       'KXc  .dNWWX;.xNk.  .kNO::lodxkOXWN0OkxdlcxNKl,..        oN0'..,:ox0XNWWNNWXo.  ,ONO'           .o0Xk;
//       .ONo    oNWWN0xXWK, .oNKc       .ONx.      ;X0.          .:XNinpNNWWWWNinpl;kNk. .cKXo.           .ON0;
//       .xNd   cNWWWWWWWWKOinpNXxl:,'...;0Xo'.....'lXK;...',:lxk0KNWWWWNNKOd:..   lXKclON0:            .xNk.
//       .dXd   ;XWWWWWWWWWWWWWWWWWWNNNNNWWNNNNNNNNNWWNNNNNNWWWWWNXKNNk;..        .dNWWXd.             cXO.
//       .xXo   .ONWNWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWNNK0ko:'..OXo          'l0NXx,              :inp,
//       .OXc    :XNk0NWXKNWWWWWWWWWWWWWWWWWWWWWNNNX00NNx:'..       lXKc.     'lONN0l.              .oXK:
//       .KX;    .dNKoON0;lXNkcld0NXo::cd0NNO:;,,'.. .0Xc            lXXo..'l0NNKd,.              .c0Nk,
//       :XK.     .xNX0NKc.cXXl  ;KXl    .dN0.       .0No            .xNXOKNXOo,.               .l0Xk;.
//      .dXk.      .lKWN0d::OWK;  lXXc    .OX:       .ONx.     . .,cdk0XNXOd;.   .'''....;c:'..;xKXx,
//      .0No         .:dOKNNNWNKOxkXWXo:,,;ONk;,,,,,;c0NXOxxkO0XXNXKOdc,.  ..;::,...;lol;..:xKXOl.
//      ,XX:             ..';cldxkOO0inpKXXXXXXXXXXinpinpK00Okxdol:;'..   .';::,..':llc,..'linpXkc.
//      :NX'    .     ''            ..................             .,;:;,',;ccc;'..'linpX0d;.
//      lNK.   .;      ,lc,.         ................        ..,,;;;;;;:::,....,linpX0d:.
//     .oN0.    .'.      .;ccc;,'....              ....'',;;;;;;;;;;'..   .;oOXX0d:.
//     .dN0.      .;;,..       ....                ..''''''''....     .:dOinpko;.
//      lNK'         ..,;::;;,'.........................           .;d0X0kc'.
//      .xXO'                                                 .;oOK0x:.
//       .cinpo.                                    .,:oxinpkxk0K0xc'.
//         .oinpkc,.                         .';cok0XNNNX0Oxoc,.
//           .;d0XX0kdlc:;,,,',,,;;:clodkO0inp0Okdl:,'..
//               .,coxO0KXXXXXXXinp0OOxdoc:,..
//                         ...
//       â£ â£¶â¡¾â �â �â �â ³â¢¦â¡�â �  â �â¢ â �â �â �â ²â¡�
//   â �â �â£´â ¿â �â �â �â �â �â �â �â¢³â¡�â �  â¡�â �â � â �â � â¢·
// â �â �â¢ â£�â£�â¡�â¢�â£�â£�â¡�â �â£�â¡�â£§â �  â¢¸â �â �â �â �   â¡� â �
//  â �â¢¸â£¯â¡­â �â ¸â£�â£�â �â¡´â£»â¡²â£¿   â£¸â �â �OK.  â¡�
// â �â �â£�â£¿â¡­â �â �â �â �â �â¢±â �â �â£¿   â¢¹â �â �â �â �â � â¡�
// â �â �â �â¢¿â£¯â �â �â �â �â¢�â¡�â �â �â¡¿â �  â¡�â �â � â � â¡¼
// â �â �â �â �â ¹â£¶â �â �â �â �â �â �â¡´â � â � â �â ¤â£�â£ â �â � â �
// â �â �â �â �â¢¸â£·â¡¦â¢¤â¡¤â¢¤â£�â£�â �â �â �â �â �â �â �â �â �â � â �
// â �â¢�â£¤â£´â£¿â£�â �â �â �â ¸â£�â¢¯â£·â£�â£¦â¡�â �â �â �â �â �â �
// â¢�â£¾â£½â£¿â£¿â£¿â£¿â �â¢²â£¶â£¾â¢�â¡·â£¿â£¿â µâ£¿â �â �â �â �â �
// â£¼â£¿â �â �â£¿â¡­â �â �â¢ºâ£�â£¼â¡�â � â �â£�â¢¸â£¿
// â£¿â£¿â£§â£�â£¿.........â£�â£°â£�â£�â£�â£�
      
//  _   _      _ _    __        __         _     _ 
// | | | | ___| | | __\ \      / /__  _ __| | __| |
// | |_| |/ _ \ | |/ _ \ \ /\ / / _ \| '__| |/ _` |
// |  _  |  __/ | | (_) \ V  V / (_) | |  | | (_| |
// |_| |_|\___|_|_|\___/ \_/\_/ \___/|_|  |_|\__,_|                                  
 
 
 
 
 
 
 
 
 
 
 
VI lvl;
VVI parentDP, sumDP;
VVPI paths;
int parFind(int b, int dif) {
    REPB(i, 15) {
        if (dif < (1<<i)) continue;
        dif -= (1<<i);
        b = parentDP[b][i];
    }
    return b;
}
int sumFind(int b, int dif) {
    int sum = 0;
    REPB(i, 15) {
        if (dif < (1<<i)) continue;
        dif -= (1<<i);
        sum += sumDP[b][i];
        b = parentDP[b][i];
    }
    return sum;
}
int LCA(int a, int b) {
    if (lvl[a] > lvl[b]) swap(a, b);
    int dif = lvl[b] - lvl[a];
    b = parFind(b, dif);
    if (a == b) return a;
    dif = lvl[a];
    while(dif--) {
        if (parentDP[a][dif] == parentDP[b][dif]) continue;
        a = parentDP[a][dif];
        b = parentDP[b][dif];
    }
    return parentDP[a][0];
}
int n, a, b, c, lca;
 
int dist() {
    cin >> a >> b;
    lca = LCA(a, b);
    return sumFind(a, lvl[a] - lvl[lca]) + 
           sumFind(b, lvl[b] - lvl[lca]);
}
int Kth() {
    cin >> a >> b >> c;
    lca = LCA(a, b);
    c--;
    if (lvl[a] - lvl[lca] >= c) return parFind(a, c);
    c = lvl[b] + lvl[a] - 2 * lvl[lca] - c;
    return parFind(b, c);
}
void dfs(int pos, int par, int edge, int curLvl) {
    lvl[pos] = curLvl;
    parentDP[pos][0] = par;
    sumDP[pos][0] = edge;
    for (auto el : paths[pos])  
        if (el.ff != par) 
            dfs(el.ff, pos, el.ss, curLvl + 1);
}
void init() {
    cin >> n;
    paths.resize(n + 1);
    lvl.resize(n + 1);
    parentDP.resize(n + 1, VI(15, 0)); 
    sumDP.resize(n + 1, VI(15, 0)); 
    REP(i, n - 1) {
        cin >> a >> b >> c;
        paths[a].pb({b, c});
        paths[b].pb({a, c});
    }
    dfs(1, 0, 0, 0);
 
    REP1(i, 15) 
    REP(j, n + 1) {
        if (1<<i >= n) break;
        sumDP[j][i] = sumDP[j][i - 1] + sumDP[parentDP[j][i - 1]][i - 1];
        parentDP[j][i] = parentDP[parentDP[j][i - 1]][i - 1];
    }
    return;
}
void go () {
    init();
    string str;
    while(cin >> str) {
        if (str == "DONE") break;
        if (str == "KTH") cout << Kth() << '\n';
        else cout << dist() << '\n';
    }
    paths.clear();
    lvl.clear();
    sumDP.clear();
    parentDP.clear();
}
 
 
 
signed main () {
 
#ifdef ONLINE_JUDGE
#else
    freopen("in.txt", "r", stdin);
    // freopen("out.txt", "w", stdout);
#endif
    _upgrade
    int T = 1;
    cin >> T;
    while(T--) go();
    return 0;
}
 
/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
*/ 