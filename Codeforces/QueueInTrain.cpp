#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
#define int long long
using namespace std;
vector <int> people;
struct Node {
    int val;
    Node *left, *right;
    Node() {
        left = NULL;
        right = NULL;
    }
};
int n, cost, tmp, curTime = 0, maxNumber = INT64_MAX;
Node *root = new Node();
void build(int l, int r, Node *node) {
    Node *a = new Node(), *b = new Node();
    node->left = a;
    node->right = b;
    if (l == r) {
        node->val = people[l];
        return;
    }
    int mid = (l + r) / 2;
    build(l, mid, node->left);
    build(mid + 1, r, node->right);
    node->val = min(node->left->val, node->right->val);
}
void update(int l, int r, Node *node, int idx) {
    if (l == r && l == idx) {
        // people[l - 1] = maxNumber + 1;
        node->val = maxNumber;
        return;
    }
    int mid = (l + r) / 2;
    if (mid < idx) update(mid + 1, r, node->right, idx);
    else update(l, mid, node->left, idx);
 
    node->val = min(node->left->val, node->right->val);
    
}
int query(int l, int r, Node *node) {
    Node *a = node->left, *b = node->right;
    if (l == r) return l;
    int mid = (l + r) / 2;
    if (a->val <= curTime || a->val <= b->val) 
        return query(l, mid, a);
    return query(mid + 1, r, b);
}
signed main () {
    cin >> n >> cost;
    people.resize(n);
    for (int i = 0; i < n; i++) 
        cin >> people[i];
 
    build(0, n - 1, root);
    vector <int> ans(n);
    for (int i = 0; i < n; i++) {
        int idx = query(1, n, root);
        update(1, n, root, idx);
        // cout << idx - 1 << ' ' << people[idx - 1] << ' ' << curTime << '\n';
        if (people[idx - 1] <= curTime) curTime += cost;
        else curTime = people[idx - 1] + cost;
        ans[idx - 1] = curTime;
        // cout << idx - 1 << ' ' << people[idx - 1] << ' ' << curTime << '\n';
        // cout << "------------\n";
    }
    for (int i = 0; i < n; i++) 
        cout << ans[i] << ' ';
 
    cout << '\n';
    return 0;
}