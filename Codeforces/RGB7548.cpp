#include <bits/stdc++.h>
using namespace std;

const int moveX[] = {1, 0, -1, 0};
const int moveY[] = {0, 1, 0, -1};
int main () {
    int n;
    cin >> n;
    vector <vector <int> > nums(n + 2, vector <int> (n+2, 0));
    for (int i = 0; i < n + 2; i++) {
        nums[i][0] = nums[0][i] = -1;
        nums[i][n+1] = nums[n+1][i] = -1;
    }
    
    int y = 1, x = 1, dir = 0, cur = 1;
    while(cur <= n * n) {
        int yy = y + moveY[dir];
        int xx = x + moveX[dir];
        if (nums[yy][xx] != 0 && cur < n * n) {
            dir = (dir + 1) % 4;
            continue;
        }
        nums[y][x] = cur++;
        y = yy, x = xx;
    }
    for (int i = 1; i < n + 1; i++) {
        for (int j = 1; j < n + 1; j++) 
            cout << nums[i][j] << ' ';
        cout << '\n';
    }
    return 0;
}
