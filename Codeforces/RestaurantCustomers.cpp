#include <iostream>
#include <set>
#include <vector>
#include <algorithm>

using namespace std;

int main () {
    vector <int> enter, leave;
    int n;
    cin >> n;
    int maxAns = 0, cur = 0, in, out;
    for (int i = 0; i < n; i++) {
        cin >> in >> out;
        enter.push_back(in);
        leave.push_back(out);
    }
    sort(enter.begin(), enter.end(), greater<int>());
    sort(leave.begin(), leave.end(), greater<int>());

    while (enter.size() != 0) {
        if(enter.back() < leave.back()) {
            enter.pop_back();
            cur++;
        } else {
            leave.pop_back();
            cur--;
        }
        maxAns = max(maxAns, cur);
    }
    cout << maxAns << '\n';
    return 0;
}