#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
using namespace std;
int n, points, times;
vector <int> rps(3);
string cur, played;
unordered_map <char, int> key;
unordered_map <int, char> val;
bool play() {
    vector <int> enemy(3, 0);
    for (int i = 0; i < times; i++) 
        enemy[key[cur[i]]]++;
    
    points = 0;
    for (int i = 0; i < 3; i++) 
        points += min(enemy[i], rps[(i + 1) % 3]);

    if (points * 2 < times) return false;
    vector <char> inst(times, ' ');
    for (int i = 0; i < times; i++) 
        if (rps[(key[cur[i]] + 1) % 3] > 0) {
            inst[i] = val[(key[cur[i]] + 1) % 3];
            rps[(key[cur[i]] + 1) % 3]--;
        }
    
    vector <char> leftover;
    for (int j = 0; j < 3; j++) 
        while (rps[j] > 0) {
            leftover.pb(val[j]);
            rps[j]--;
        }
    played = "";
    int idx = 0;
    for (int i = 0; i < times; i++) {
        if (inst[i] == ' ') {
            played += leftover[idx];
            idx++;
        } else {
            played += inst[i];
        }
    }

    return true;
}
int main () {
    // freopen("out.txt", "w", stdout);
    cin >> n;
    if (true) {
        key['R'] = 0;
        key['P'] = 1;
        key['S'] = 2;
        val[0] = 'R';
        val[1] = 'P';
        val[2] = 'S';
    }

    for (int i = 0; i < n; i++) {
        cin >> times;
        for (int j = 0; j < 3; j++) 
            cin >> rps[j];

        cin >> cur;
        if (play()) cout << "YES\n" << played << '\n';
        else cout << "NO\n";
    }

    return 0;
}