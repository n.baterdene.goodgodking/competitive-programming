#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
#define umap unordered_map
#define INF INT_MAX
#define MOD 1e9 + 7
using namespace std;
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int n, m, limit = 205 * 26;
    cin >> n >> m;
    vector <vector <int> > dp(205, vector <int>(limit, -INF));
    dp[0][0] = 0;
    ll tmp;
    while (n--) {
        int count2 = 0, count5 = 0;
        cin >> tmp;
        while (tmp % 5 == 0) tmp /= 5, count5++;
        while (tmp % 2 == 0) tmp /= 2, count2++;
        for (ll used = m; used > 0; used--)
        for (ll power = count5; power < limit; power++) 
            dp[used][power] = max(dp[used][power], dp[used - 1][power - count5] + count2);
    }
    int ans = 0;
    for (int i = 1; i < limit; i++) 
        ans = max(ans, min(i, dp[m][i]));
    
    cout << ans << '\n';
	return 0;
}