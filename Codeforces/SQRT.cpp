#include <bits/stdc++.h>
using namespace std;
double e = 1, k = 1, n;
int m;
double SQRT(double st, double end, double n) {
    if (st == end) return st;
    double mid = (st + end) / 2;
    if (end - st < k) {
        double a = abs(n - (st * st));
        double b = abs(n - (end * end));
        if (a < b) return st;
        else return end;
    }
    if (mid * mid < n) st = mid;
    else end = mid;
    return SQRT(st, end, n);
}
signed main () {
    cin >> n >> m;
    for (int i = 0; i < m; i++) {
        e /= 10;
        k /= 100;
    }
    double ans = SQRT(0, n + e, n);
    cout << fixed << setprecision(m) << ans << '\n';
    return 0;
}