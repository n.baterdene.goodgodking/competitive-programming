#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define mp make_pair
#define ff first
#define ss second
#define umap unordered_map
#define INF INT_MAX
#define MOD 1e9 + 7
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MIN(a, b) ((a) > (b) ? (a) : (b))
#define lb lower_bound
#define ub upper_bound
#define resz resize
#define all(x) (x).begin(), (x).end()
#define FOR(i, a, b) for (int i = (a); i < (b); i++)
#define F0R(i, a) for (int i = 0; i < (a); i++)
#define FORd(i, a, b) for (int i = (b)-1; i >= (a); i--)
#define F0Rd(i, a) for (int i = (a)-1; i >= 0; i--)
#define trav(a, x) for (auto& a : x)
#define ll = long long;
#define vi = vector<int>;
#define vvi = vector<vi>;
#define vll = vector<ll>;
#define vvll = vector<vll>;
#define vb = vector<bool>;
#define vd = vector<double>;
#define vs = vector<string>;
#define pii = pair<int, int>;
#define pll = pair<ll, ll>;
#define pdd = pair<double, double>;
#define vpii = vector<pii>;
#define vvpii = vector<vpii>;
#define vpll = vector<pll>;
#define vvpll = vector<vpll>;
#define vpdd = vector<pdd>;
#define vvpdd = vector<vpdd>;

 
 
//                                        .....'',;;::cccllllllllllllcccc:::;;,,,''...'',,'..
//                             ..';cldkO00KXNNNNXXXinp000OOinpinpkxxxxxddoooddddddxxxxinpinpOO0XXKx:.
//                       .':ok0KXXXNXK0kxolc:;;,,,,,,,,,,,;;,,,''''''',,''..              .'lOXKd'
//                  .,lx00Oxl:,'............''''''...................    ...,;;'.             .oKXd.
//               .cinpinpc'...'',:::;,'.........'',;;::::;,'..........'',;;;,'.. .';;'.           'kNKc.
//            .:kXXk:.    ..       ..................          .............,:c:'...;:'.         .dNNx.
//           :0NKd,          .....''',,,,''..               ',...........',,,'',,::,...,,.        .dNNx.
//          .xXd.         .:;'..         ..,'             .;,.               ...,,'';;'. ...       .oNNo
//          .0K.         .;.              ;'              ';                      .'...'.           .oXX:
//         .oNO.         .                 ,.              .     ..',::ccc:;,..     ..                lXX:
//        .dNX:               ......       ;.                'cxOinp0OXWWWWWWWNX0kc.                    :KXd.
//      .l0N0;             ;d0inpinpKXK0ko:...              .l0X0xc,...lXWWWWWWWWKO0Kx'                   ,ONKo.
//    .lKNKl...'......'. .dXWN0inpk0NWWWWWN0o.            :KN0;.  .,cokXWWNNNNWNinpxONK: .,:c:.      .';;;;:lk0XXx;
//   :KN0l';ll:'.         .,:lodxxkO00KXNWWWX000k.       oXNx;:oinpX0kdl:::;'',;coxinpd, ...'. ...'''.......',:lxKO:.
//  oNNk,;c,'',.                      ...;xNNOc,.         ,d0X0xc,.     .dOd,           ..;dOKXK00000Ox:.   ..''dKO,
// 'KW0,:,.,:..,oxinpkdl;'.                'inp'              ..           .dXX0o:'....,:oOXNN0d;.'. ..,lOKd.   .. ;KXl.
// ;XNd,;  ;. l00kxoooxKXKx:..ld:         ;inp'                             .:dkO000000Okxl;.   c0;      :inp;   .  ;XXc
// 'XXdc.  :. ..    '' 'kNNNinpinp,      .,dKNO.                                   ....       .'c0NO'      :X0.  ,.  xN0.
// .kNOc'  ,.      .00. ..''...      .l0X0d;.             'dOkxo;...                    .;oinpXK0KNXx;.   .0X:  ,.  lNX'
//  ,inpdl  .c,    .dNK,            .;xXWKc.                .;:coOXO,,'.......       .,lx0XXOo;...oNWNXinp:.'KX;  '   dNX.
//   :XXkc'....  .dNWXl        .';l0NXNKl.          ,lxinpkxo' .cK0.          ..;lx0XNX0xc.     ,0Nx'.','.kXo  .,  ,KNx.
//    cXXd,,;:, .oXWNNKo'    .'..  .'.'dinp;        .cooollox;.xXXl     ..,cdOKXXX00NXc.      'oKWK'     ;k:  .l. ,0Nk.
//     cXNx.  . ,KWX0NNNXOl'.           .o0Ooldk;            .:c;.':lxOinpK0xo:,.. ;XX:   .,lOXWWXd.      . .':,.lKXd.
//      lXNo    cXWWWXooNWNXinpo;'..       .lk0x;       ...,:ldk0KXNNOo:,..       ,OWNOxO0KXXNWNO,        ....'l0Xk,
//      .dNK.   oNWWNo.cXK;;oOXNNXK0kxdolllllooooddxk00inpinp0kdoc:c0No        .'ckXWWWNXkc,;kNKl.          .,kXXk,
//       'KXc  .dNWWX;.xNk.  .kNO::lodxkOXWN0OkxdlcxNKl,..        oN0'..,:ox0XNWWNNWXo.  ,ONO'           .o0Xk;
//       .ONo    oNWWN0xXWK, .oNKc       .ONx.      ;X0.          .:XNinpNNWWWWNinpl;kNk. .cKXo.           .ON0;
//       .xNd   cNWWWWWWWWKOinpNXxl:,'...;0Xo'.....'lXK;...',:lxk0KNWWWWNNKOd:..   lXKclON0:            .xNk.
//       .dXd   ;XWWWWWWWWWWWWWWWWWWNNNNNWWNNNNNNNNNWWNNNNNNWWWWWNXKNNk;..        .dNWWXd.             cXO.
//       .xXo   .ONWNWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWNNK0ko:'..OXo          'l0NXx,              :inp,
//       .OXc    :XNk0NWXKNWWWWWWWWWWWWWWWWWWWWWNNNX00NNx:'..       lXKc.     'lONN0l.              .oXK:
//       .KX;    .dNKoON0;lXNkcld0NXo::cd0NNO:;,,'.. .0Xc            lXXo..'l0NNKd,.              .c0Nk,
//       :XK.     .xNX0NKc.cXXl  ;KXl    .dN0.       .0No            .xNXOKNXOo,.               .l0Xk;.
//      .dXk.      .lKWN0d::OWK;  lXXc    .OX:       .ONx.     . .,cdk0XNXOd;.   .'''....;c:'..;xKXx,
//      .0No         .:dOKNNNWNKOxkXWXo:,,;ONk;,,,,,;c0NXOxxkO0XXNXKOdc,.  ..;::,...;lol;..:xKXOl.
//      ,XX:             ..';cldxkOO0inpKXXXXXXXXXXinpinpK00Okxdol:;'..   .';::,..':llc,..'linpXkc.
//      :NX'    .     ''            ..................             .,;:;,',;ccc;'..'linpX0d;.
//      lNK.   .;      ,lc,.         ................        ..,,;;;;;;:::,....,linpX0d:.
//     .oN0.    .'.      .;ccc;,'....              ....'',;;;;;;;;;;'..   .;oOXX0d:.
//     .dN0.      .;;,..       ....                ..''''''''....     .:dOinpko;.
//      lNK'         ..,;::;;,'.........................           .;d0X0kc'.
//      .xXO'                                                 .;oOK0x:.
//       .cinpo.                                    .,:oxinpkxk0K0xc'.
//         .oinpkc,.                         .';cok0XNNNX0Oxoc,.
//           .;d0XX0kdlc:;,,,',,,;;:clodkO0inp0Okdl:,'..
//               .,coxO0KXXXXXXXinp0OOxdoc:,..
//                         ...
 
 
int n, m;
map <vector<int>, int> seen;
int query (int x1, int y1, int x2, int y2) {
    vector <int> cur;
    cur.pb(x1);
    cur.pb(y1);
    cur.pb(x2);
    cur.pb(y2);
    if (seen.find(cur) != seen.end()) return seen[cur];
    cout << "? " << x1 << ' ' << y1 << ' ' << x2 << ' ' << y2 << '\n';
    cin >> m;
    return seen[cur] = m;
}
signed main () {
    // ios_base::sync_with_stdio(0);
    // cin.tie(0);
    // cout.tie(0);
    cin >> n;
    set <vector <int> > ans;
    
    // rect1
    if (true) {
        vector <int> tmp;
        int x1 = 1, x2 = n, y1 = 1, y2 = n;
        int st = 1, end = n;
        // cout << "RIGHT\n";
        while (true) {
            if (st == end) break;
            int mid = (st + end) / 2;
            m = query(x1, y1, mid, y2);
            // cout << "? " << x1 << ' ' << y1 << ' ' << mid << ' ' << y2 << '\n';
            // cin >> m;
            if (mid == st && m == 1) break;
            if (m == 0) {
                if (st == mid) st++;
                else st = mid;
            } else {    
                end = mid;
            }
        }
        x2 = st;
        st = 1, end = x2;
        // cout << "LEFT\n";
        while (true) {
            if (st == end) break;
            int mid = (st + end + 1) / 2;
            m = query(mid, y1, x2, y2);
            // cout << "? " << mid << ' ' << y1 << ' ' << x2 << ' ' << y2 << '\n';
            // cin >> m;
            if (mid == end && m == 1) break;
            if (m == 0) {
                if (mid == end) end--;
                else end = mid;
            } else {    
                st = mid;
            }
        }
        x1 = end;
        st = 1, end = n;
        // cout << "TOP\n";
        while (true) {
            if (st == end) break;
            int mid = (st + end) / 2;
            m = query(x1, y1, x2, mid);
            // cout << "? " << x1 << ' ' << y1 << ' ' << x2 << ' ' << mid << '\n';
            // cin >> m;
            if (mid == st && m == 1) break;
            if (m == 0) {
                if (st == mid) st++;
                else st = mid;
            } else {    
                end = mid;
            }
        }
        y2 = st;
        st = 1, end = y2;
        // cout << "BOT\n";
        while (true) {
            if (st == end) break;
            int mid = (st + end + 1) / 2;
            m = query(x1, mid, x2, y2);
            // cout << "? " << x1 << ' ' << mid << ' ' << x2 << ' ' << y2 << '\n';
            // cin >> m;
            if (mid == end && m == 1) break;
            if (m == 0) {
                if (mid == end) end--;
                else end = mid;
            } else {    
                st = mid;
            }
        }
        y1 = end;
        tmp.pb(x1);
        tmp.pb(y1);
        tmp.pb(x2);
        tmp.pb(y2);
        ans.insert(tmp);
    }
    // rect2
    if (true) {
        vector <int> tmp;
        int x1 = 1, x2 = n, y1 = 1, y2 = n;
        int st = 1, end = n;
        // cout << "LEFT\n";
        while (true) {
            if (st == end) break;
            int mid = (st + end + 1) / 2;
            m = query(mid, y1, x2, y2);
            if (mid == end && m >= 1) break;
            if (m == 0) {
                if (mid == end) end--;
                else end = mid;
            } else {    
                st = mid;
            }
        }
        x1 = end;
        st = x1, end = n;
        // cout << "RIGHT\n";
        while (true) {
            if (st == end) break;
            int mid = (st + end) / 2;
            m = query(x1, y1, mid, y2);
            if (mid == st && m >= 1) break;
            if (m == 0) {
                if (st == mid) st++;
                else st = mid;
            } else {    
                end = mid;
            }
        }
        x2 = st;
        st = 1, end = n;
        // cout << "BOT\n";
        while (true) {
            if (st == end) break;
            int mid = (st + end + 1) / 2;
            m = query(x1, mid, x2, y2);
            if (mid == end && m >= 1) break;
            if (m == 0) {
                if (mid == end) end--;
                else end = mid;
            } else {    
                st = mid;
            }
        }
        y1 = end;
        st = y1, end = n;
        // cout << "TOP\n";
        while (true) {
            if (st == end) break;
            int mid = (st + end) / 2;
            m = query(x1, y1, x2, mid);
            if (mid == st && m >= 1) break;
            if (m == 0) {
                if (st == mid) st++;
                else st = mid;
            } else {    
                end = mid;
            }
        }
        y2 = st;
        tmp.pb(x1);
        tmp.pb(y1);
        tmp.pb(x2);
        tmp.pb(y2);
        ans.insert(tmp);
    }
    // rect4
    if (ans.size() == 1) {
        vector <int> tmp;
        int x1 = 1, x2 = n, y1 = 1, y2 = n;
        int st = 1, end = n;
        // cout << "TOP\n";
        while (true) {
            if (st == end) break;
            int mid = (st + end) / 2;
            m = query(x1, y1, x2, mid);
            if (mid == st && m == 1) break;
            if (m == 0) {
                if (st == mid) st++;
                else st = mid;
            } else {    
                end = mid;
            }
        }
        y2 = st;
        st = 1, end = y2;
        // cout << "BOT\n";
        while (true) {
            if (st == end) break;
            int mid = (st + end + 1) / 2;
            m = query(x1, mid, x2, y2);
            if (mid == end && m == 1) break;
            if (m == 0) {
                if (mid == end) end--;
                else end = mid;
            } else {    
                st = mid;
            }
        }
        y1 = end;
        st = 1, end = n;
        // cout << "RIGHT\n";
        while (true) {
            if (st == end) break;
            int mid = (st + end) / 2;
            m = query(x1, y1, mid, y2);
            if (mid == st && m == 1) break;
            if (m == 0) {
                if (st == mid) st++;
                else st = mid;
            } else {    
                end = mid;
            }
        }
        x2 = st;
        st = 1, end = x2;
        // cout << "LEFT\n";
        while (true) {
            if (st == end) break;
            int mid = (st + end + 1) / 2;
            m = query(mid, y1, x2, y2);
            if (mid == end && m == 1) break;
            if (m == 0) {
                if (mid == end) end--;
                else end = mid;
            } else {    
                st = mid;
            }
        }
        x1 = end;
        tmp.pb(x1);
        tmp.pb(y1);
        tmp.pb(x2);
        tmp.pb(y2);
        ans.insert(tmp);
    }
    // rect3
    if (ans.size() == 1) {
        vector <int> tmp;
        int x1 = 1, x2 = n, y1 = 1, y2 = n;
        int st = 1, end = n;
        // cout << "BOT\n";
        while (true) {
            if (st == end) break;
            int mid = (st + end + 1) / 2;
            m = query(x1, mid, x2, y2);
            // cout << "? " << x1 << ' ' << mid << ' ' << x2 << ' ' << y2 << '\n';
            // cin >> m;
            if (mid == end && m == 1) break;
            if (m == 0) {
                if (mid == end) end--;
                else end = mid;
            } else {    
                st = mid;
            }
        }
        y1 = end;
        st = y1, end = n;
        // cout << "TOP\n";
        while (true) {
            if (st == end) break;
            int mid = (st + end) / 2;
            m = query(x1, y1, x2, mid);
            // cout << "? " << x1 << ' ' << y1 << ' ' << x2 << ' ' << mid << '\n';
            // cin >> m;
            if (mid == st && m == 1) break;
            if (m == 0) {
                if (st == mid) st++;
                else st = mid;
            } else {    
                end = mid;
            }
        }
        y2 = st;
        st = 1, end = n;
        // cout << "LEFT\n";
        while (true) {
            if (st == end) break;
            int mid = (st + end + 1) / 2;
            m = query(mid, y1, x2, y2);
            // cout << "? " << mid << ' ' << y1 << ' ' << x2 << ' ' << y2 << '\n';
            // cin >> m;
            if (mid == end && m == 1) break;
            if (m == 0) {
                if (mid == end) end--;
                else end = mid;
            } else {    
                st = mid;
            }
        }
        x1 = end;
        st = x1, end = n;
        // cout << "RIGHT\n";
        while (true) {
            if (st == end) break;
            int mid = (st + end) / 2;
            m = query(x1, y1, mid, y2);
            // cout << "? " << x1 << ' ' << y1 << ' ' << mid << ' ' << y2 << '\n';
            // cin >> m;
            if (mid == st && m == 1) break;
            if (m == 0) {
                if (st == mid) st++;
                else st = mid;
            } else {    
                end = mid;
            }
        }
        x2 = st;
        tmp.pb(x1);
        tmp.pb(y1);
        tmp.pb(x2);
        tmp.pb(y2);
        ans.insert(tmp);
    }
    cout << "! ";
    for (auto el: ans) 
    for (auto num: el) 
        cout << num << ' ';
    
    cout << '\n';
	return 0;
}