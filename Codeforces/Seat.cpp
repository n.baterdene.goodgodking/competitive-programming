#include <bits/stdc++.h>
using namespace std;
void solve(string a) {
    int n = a.size();
    int num;
    char seat = a[n - 1];
    stringstream tmp(a.substr(0, n - 1));
    tmp >> num;
    int suudal = seat - 'A';
    if (num < 3) {
        // a,| b, c,| d
        if (seat % 4 == 0 || seat % 4 == 3) cout << "window\n";
        else cout << "edge\n";
        return;
    }
    if (num < 21) {
        // a, b, c, d, e, f
        // 0, 1, | 2, 3, | 4, 5
        if (suudal % 6 == 0 || suudal % 6 == 5) cout << "window\n";
        else cout << "edge\n";
        return;
    }
    // a, b, c, d, e, f, g, h, j, k
    // 0, 1, 2, | 3, 4, 5, 6, | 7, 8, 9
    if (suudal > 'I' - 'A') suudal--;
    if (suudal % 10 == 0 || suudal % 10 == 9) {
        cout << "window\n";
        return;
    }
    if (suudal % 10 == 2 || suudal % 10 == 3 || suudal % 10 == 6 || suudal % 10 == 7) 
        cout << "edge\n";
    else cout << "mid\n";

} 
int main () {
    string a;       
    while (cin >> a) {
        solve(a);
    }

    return 0;
}