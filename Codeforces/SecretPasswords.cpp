#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
#define umap unordered_map
using namespace std;
vector <int> parents;
int myFind(int i) {
    if (parents[i] == i) return i;
    return parents[i] = myFind(parents[i]);
}
void merge(int b, int a) {
    int from = myFind(a);
    int to = myFind(b);
    parents[from] = parents[to];
}
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int n;
    cin >> n;
    vector <bool> used(26, false);
    for (int i = 0; i < 26; i++) 
        parents.pb(i);

    string str;
    for (int i = 0; i < n; i++) {
        cin >> str;
        for (int j = 0; j < str.size(); j++) {
            if (!used[str[j] - 'a']) used[str[j] - 'a'] = true;
            for (int idx = j + 1; idx < str.size(); idx++) 
                merge(myFind(str[j] - 'a'), myFind(str[idx] - 'a'));
        }
    }
    int ans = 0;
    for (int i = 0; i < 26; i++) {
        // cout << char(i + 'a') << ' ' << char(myFind(i) + 'a') << '\n';
        if (!used[i]) continue;
        if (myFind(i) == i) ans++;
    }
    cout << ans << '\n';
	return 0;
}