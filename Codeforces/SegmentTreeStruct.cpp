#include <bits/stdc++.h>
using namespace std;
struct Node {
    int val;
    Node *left, *right;
    Node() {
        left = NULL;
        right = NULL;
    }
};
vector <int> numbers;
Node *root = new Node();
void build(int st, int end, Node *head) {
    if (st == end) {
        head->val = numbers[st];
        return;
    }
    head->left = new Node();
    head->right = new Node();
    int mid = (st + end) >> 1;
    build(st, mid, head->left);
    build(mid + 1, end, head->right);
    head->val = head->left->val + head->right->val;
}
int query(int st, int end, int l, int r, Node *head) {
    if (end < l || st > r) return 0;
    if (l <= st && end <= r) return head->val;
    int mid = (st + end) >> 1;
    return query(st, mid, l, r, head->left) + 
           query(mid + 1, end, l, r, head->right);
}
void update(int st, int end, int idx, int num, Node *head) {
    if (st == end) {
        head->val = num;
        return;
    }
    int mid = (st + end) >> 1;
    if (mid >= idx) update(st, mid, idx, num, head->left);
    else update(mid + 1, end, idx, num, head->right);
    head->val = head->left->val + head->right->val;
}
int main () {
    int n, m;
    cin >> n >> m;
    numbers.resize(n);
    for (int i = 1; i <= n; i++) 
        cin >> numbers[i];

    if (n != 0) build(1, n, root);
    int type, st, end;
    for (int i = 0; i < m; i++) {
        cin >> type >> st >> end;
        if (type == 1) {
            numbers[st] = end;
            cout << "update: " << 1 << ' ' << n << ' ' << st - 1 << ' ' << end << '\n';
            update(1, n, st - 1, end, root);
        } else {
            cout << "sumrange: " << 1 << ' ' << n << ' ' << st << ' ' << end << '\n';
            cout << "here " << query(1, n, st, end, root) << '\n';
        }
    }    
    return 0;
}