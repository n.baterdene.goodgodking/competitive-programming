#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define int long long
#define ff first
#define ss second
#define MAX 100005
using namespace std;
int ans = INT_MAX;
vector <int> numbers(MAX);
int merge(int st, int end, int sum) {
    int curAns = 0;
    int size = end - st + 1;
    vector <int> right(size), left(size);
    int curCount = numbers[st];
    left[0] = 0;
    for (int i = 1; i < size; i++) {
        left[i] = left[i - 1] + curCount;
        curCount += numbers[i + st];
    }
    curCount = numbers[end];
    for (int i = end - 1 - st; i >= 0; i--) {
        right[i] = right[i + 1] + curCount;
        curCount += numbers[i];
    }
    curAns = left[0] + right[0];
    for (int i = 1; i < size; i++) 
        curAns = min(curAns, left[i] + right[i]);
    return curAns;
}
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    int n, sum = 0;
    cin >> n;
    for (int i = 0; i < n; i++) {
        cin >> numbers[i];
        sum += numbers[i];
    }
    if (sum == 1) {
        cout << -1 << '\n';
        return 0;
    }
    vector <int> factors;
    for (int i = 2; i <= sqrt(n); i++) 
        if (sum % i == 0) factors.pb(i), factors.pb(sum / i);


    if (factors.size() == 0) {
        cout << merge(0, n - 1, sum) << '\n';
        return 0;
    }
    for (int j = 0; j < factors.size(); j++) {
        int st = 0;
        int cur = factors[j];
        int curAns = 0, times = sum / cur;
        if (cur == 1) continue;
        for (int i = 0; i < n && times; i++) {
            st = i;
            int curCount = 0;
            while (true) {
                curCount += numbers[i];
                if (cur == curCount) break;
                i++;
            }
            int tmp = merge(st, i, cur);
            // cout << cur << ' ' << st << ' ' << i << ' ' << tmp << '\n';
            curAns += tmp;
            times--;
        }
        ans = min(ans, curAns);
    }
    cout << ans << '\n';
	return 0;
}