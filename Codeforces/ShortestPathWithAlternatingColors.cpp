#include <iostream>
#include <vector>
#include <map>
using namespace std;
class Solution {
public:
    vector <int> ans;
    vector <vector <vector <int> > > moves;
    bool oroogui = true;
    void solve(int pos, int colour, int len, int end, map<pair <int, int>, bool> reached) {
        cout << "open\n";
        cout << pos << ' ' << colour << ' ' << len << ' ' << end;
        if(pos == end) {
            if(ans[end] == -1) ans[end] = len;
            else ans[end] = min(len, ans[end]);
            return;
        }
        pair <int, int > tmp;
        tmp = make_pair(pos, colour);
        if(reached.find(tmp) != reached.end())
            if(reached[make_pair(pos, colour)] == true) return;
        for(int i = 0; i < moves[colour][pos].size(); i++) {
            reached[make_pair(moves[colour][pos][i], colour)] = true;
            cout << "here1\n";
            solve(moves[colour][pos][i], (colour + 1) % 2, len + 1, end, reached);
            cout << "here2\n";
            reached[make_pair(moves[colour][pos][i], colour)] = false;
        }
        
        cout << "end\n";
    }
    
    vector<int> shortestAlternatingPaths(int n, vector<vector<int> >& red, vector<vector<int> >& blue) {
        if(true) {
            vector <vector <int> > temp;
            vector <int> temp1;
            
            for(int j = 0; j < 100; j++) 
                temp.push_back(temp1);
            
            for(int i = 0; i < 2; i++) 
                moves.push_back(temp);
            
            for(int i = 0; i < n; i++) 
                ans.push_back(-1);
        }
        
        vector <vector <int> > tmp;
        vector <int> tmp1;
        map <pair <int, int>, bool> reached;
        
        // red 0     blue 1
        
        for(int i = 0; i < red.size(); i++) 
            moves[0][red[i][0]].push_back(red[i][1]);
        
        for(int i = 0; i < blue.size(); i++) 
            moves[1][blue[i][0]].push_back(blue[i][1]);
        
        ans[0] = 0;
        
        for(int colour = 0; colour < 2; colour++) {
            reached[make_pair(0, colour)] = true;
            for(int end = 0; end < n; end++) 
                for(int i = 0; i < moves[colour][0].size(); i++) {
                    cout << "new\n";
                    reached[make_pair(moves[colour][0][i], colour)] = true;
                    solve(moves[colour][0][i], (colour + 1) % 2, 1, end, reached);
                    reached[make_pair(moves[colour][0][i], colour)] = false;
                }
            reached[make_pair(0, colour)] = false;
        }
        return ans;
    }
};
int main () {
    freopen("in.txt","r", stdin);
    vector <int> ans;
    int n;
    cin >> n;
    vector <vector <int> > red, blue;
    int num1, num2, rednum, bluenum;
    cin >> rednum;
    for(int i = 0; i < rednum; i++) {
        vector <int> a;
        cin >> num1 >> num2;
        a.push_back(num1);
        a.push_back(num2);
        red.push_back(a);
    }
    cin >> bluenum;
    for(int i = 0; i < bluenum; i++) {
        vector <int> a;
        cin >> num1 >> num2;
        a.push_back(num1);
        a.push_back(num2);
        blue.push_back(a);
    }
    Solution a;
    ans = a.shortestAlternatingPaths(n, red, blue);
    for(int i = 0; i < n; i++) {
        cout << ans[i] << ' ';
    }
    cout << '\n';
    return 0;
}