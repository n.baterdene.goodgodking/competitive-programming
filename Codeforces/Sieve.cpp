#include <iostream>
#include <vector>
using namespace std;
int main () {
    int n, tmp, tmp1;
    cin >> n;
    vector <bool> prime(n + 1, true);
    freopen("out.txt", "w", stdout);
    prime[0] = false;
    prime[1] = false;
    for(int i = 0; i <= n; i++) {
        if(prime[i]) {
            cout << i << " ";
            tmp = i;
            while(tmp + i <= n){
                tmp += i;
                prime[tmp] = false;
            }
        }
    }
    return 0;
}