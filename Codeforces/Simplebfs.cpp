#include <iostream>
#include <unordered_map>
#include <vector>
using namespace std;
vector <vector <int> > path(10, vector<int>());
vector <vector <int> > points;
vector <int> ans;
int main () {
    unordered_map<int, int> pointed;
    int n, st, end;
    cin >> n;
    for(int i = 0; i < n; i++) {
        cin >> st >> end;
        path[st].push_back(end);
        path[end].push_back(st);
    }
    vector <int> tmp;
    tmp.push_back(0);
    ans[0] = 0;
    points.push_back(tmp);
    for(int idx = 0; points[idx].size() > 0; idx++) {
        for(int i = 0; i < points[idx].size(); i++) {
            for(int j = 0; j < path[points[idx][i]].size(); j++) {
                if(pointed.find(path[points[idx][i]][j]) == pointed.end()){
                    points[idx + 1].push_back(path[points[idx][i]][j]);
                    ans[path[points[idx][i]][j]] = idx + 1;
                }
            }
        }
    }
    for(int i = 0; i < ans.size(); i++) 
        cout << i << ' ' << ans[i] << '\n';
    return 0;
}