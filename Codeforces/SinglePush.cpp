#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
using namespace std;
void go() {
    int n;
    cin >> n;
    vector <int> numbers1(n), numbers2(n);
    for (int i = 0; i < n; i++)
        cin >> numbers1[i];

    for (int i = 0; i < n; i++)
        cin >> numbers2[i];
    
    int i;
    for (i = 0; i < n; i++) {
        if (numbers1[i] != numbers2[i]) break;
    }
    int dif = numbers1[i] - numbers2[i];
    // cout << dif << '\n';
    if (dif > 0) {
        cout << "NO\n";
        return;
    }
    for (; i < n; i++) {
        if (numbers1[i] - numbers2[i] != dif) break;
    }
    for (; i < n; i++) {
        if (numbers1[i] != numbers2[i]) {
            // cout << i << '\n';
            cout << "NO\n";
            return;
        }
    }

    cout << "YES\n";
    return;
}
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int T;
    cin >> T;
    while (T--) go();
	return 0;
}