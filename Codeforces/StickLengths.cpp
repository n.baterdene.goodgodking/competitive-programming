#include <iostream>
#include <vector>
#include <algorithm>
#define int long long
using namespace std;

signed  main () {
    int n;
    cin >> n;
    vector <int> numbers(n);
    for(int i = 0; i < n; i++) 
        cin >> numbers[i];
    sort(numbers.begin(), numbers.end());
    int mid = n / 2;
    int target = numbers[mid], ans = 0;
    for(int i = 0; i < n; i++) 
        ans += abs(target - numbers[i]);

    cout << ans << '\n';
    return 0;
}