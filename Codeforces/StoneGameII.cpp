#include <iostream>
#include <vector>
#include <algorithm>
#include <map>

#define mp make_pair
using namespace std;

class Solution {
public:
    map<pair<int, int>, int > chance;

    int play(int st, int m, vector<int> &backSum, vector<int> &piles) {

        if (st == piles.size()) 
            return 0;

        if (2 * m >= piles.size() - st) 
            return backSum[st];

        pair<int, int> key = mp(st, m);
        if (chance.find(key) != chance.end()) 
            return chance[key];

        int ans = backSum[0];
        for (int i = 1; i <= 2 * m; i++) 
            ans = min(ans, play(st + i, max(m, i), backSum, piles));

        chance[key] = backSum[st] - ans;
        return chance[key];
    }
    int stoneGameII(vector<int>& piles) {
        int n = piles.size();
        vector<int> tmp(n, 0), backSum = piles;

        for (int i = n - 2; i >= 0; i--) 
            backSum[i] += backSum[i + 1];

        return play(0, 1, backSum, piles);
    }  
};
int main () {
    int n;
    cin >> n;
    vector <int> piles(n);
    for(int i = 0; i < n; i++) 
        cin >> piles[i];
    
    Solution sol;
    cout << sol.stoneGameII(piles) << '\n';

    return 0;
}