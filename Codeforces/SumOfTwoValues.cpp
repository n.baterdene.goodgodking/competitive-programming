#include <iostream>
#include <vector>
#include <map>
using namespace std;
int main () {
    int n, target, num;
    cin >> n >> target;
    map <int, int> found;
    for(int i = 0; i < n; i++) {
        cin >> num;
        if(found.find(target - num) != found.end()) {
            cout << i + 1 << ' ' << found[target - num] + 1 << '\n';
            return 0;
        }
        found[num] = i;
    }
    cout << "IMPOSSIBLE\n";
    return 0;
}