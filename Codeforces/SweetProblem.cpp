#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
#define umap unordered_map
using namespace std;
void go() {
    int sum = 0;
    vector <int> numbers(3);
    for (int i = 0; i < 3; i++) {
        cin >> numbers[i];
        sum += numbers[i];
    }
    sort(numbers.begin(), numbers.end());
    if (numbers.back() >= numbers[0] + numbers[1]) {
        cout << (numbers[0] + numbers[1]) << '\n';
        return;
    }   
    if (sum & 1) cout << (sum - 1) / 2 << '\n';
    else cout << sum / 2 << '\n';
    return;
}
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int T;
    cin >> T;
    while(T--) go();
	return 0;
}