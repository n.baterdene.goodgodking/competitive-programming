#include <bits/stdc++.h>
using namespace std;
vector<vector<int> > paths, stronglyConnected;
vector<int> lowLink, idx, s;
vector<bool> vis, currentlyUsing, selfLoop;
int cnt = 0;
// void print(vector <int>& a) {
//     for (auto el : a) cout << el << ' ';
//     cout << '\n';
// }
void dfs(int pos) {
    // cout << pos << " : ";
    // print(paths[pos]);
    s.push_back(pos);
    currentlyUsing[pos] = vis[pos] = 1;
    lowLink[pos] = idx[pos] = ++cnt;
    for (auto el: paths[pos]) {
        if (!vis[el]) {
            dfs(el);
            lowLink[pos] = min(lowLink[pos], lowLink[el]);
        } else if(currentlyUsing[el]) {
            lowLink[pos] = min(lowLink[pos], idx[el]);
        }
    }
    // return;

    if (lowLink[pos] == idx[pos]) {
        vector <int> curAns;
        while(s.back() != pos) {
            curAns.push_back(s.back());
            currentlyUsing[s.back()] = 0;
            s.pop_back();
        }
        currentlyUsing[pos] = 0;
        curAns.push_back(pos);
        s.pop_back();
        if (curAns.size() > 1)
            stronglyConnected.push_back(curAns);
        else if (selfLoop[pos]) stronglyConnected.push_back(curAns);
    }
}
int main () {
    freopen("inTarjan.txt", "r", stdin);
    int n, m;
    cin >> n >> m;
    vis.resize(n + 5);
    idx.resize(n + 5);
    paths.resize(n + 5);
    lowLink.resize(n + 5);
    selfLoop.resize(n + 5);
    currentlyUsing.resize(n + 5);
    for (int i = 0; i < m; i++) {
        int a, b;
        cin >> a >> b;
        if (a == b) selfLoop[a] = 1;
        paths[a].push_back(b);
        // paths[b].push_back(a);
    }
    // return 0;
    for (int i = 1; i <= n; i++) 
        if (!vis[i]) {
            dfs(i);
            // cout << '\n';
        }

    for (auto el : stronglyConnected) {
        for (auto el1: el) cout << el1 << ' ';
        cout << '\n';
    }
}