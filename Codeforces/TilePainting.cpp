#include <bits/stdc++.h>
#define pb push_back
#define int long long
using namespace std;
signed main () {
    int n;
    cin >> n;
    int num = -1;
    for (int i = 2; i <= sqrt(n); i++) 
        if (n % i == 0) {
            num = i;
            break;
        }

    if (num == -1) {
        cout << n << '\n';
        return 0;
    }
    while (n % num == 0) {
        n /= num;
    }
    if (n == 1) cout << num << '\n';
    else cout << 1 << '\n';
    return 0;
}