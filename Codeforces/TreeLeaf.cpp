#include <iostream>
#include <set>
#include <unordered_map>
using namespace std;
struct TreeNode {
    int val;
    TreeNode *left, *right;
    TreeNode(int num) {
        val = num;
        left = NULL;
        right = NULL;
    }
};
int power(TreeNode *head, unordered_map<int, int>& nodes, int ans) {
    if (head == NULL) 
        return ans;
    nodes[head->val]++;
    if(nodes[head->val] == 1) ans++;
    int returningAns = max(power(head->left, nodes, ans), power(head->right, nodes, ans));
    nodes[head->val]--;
    if(nodes[head->val] == 0) nodes.erase(head->val);
    return returningAns;
}
int main () {
    TreeNode *root = new TreeNode(4);
    if (true) {
        TreeNode *lvl0l = new TreeNode(3);
        TreeNode *lvl0r = new TreeNode(3);
        TreeNode *lvl1ll = new TreeNode(2);
        TreeNode *lvl1rl = new TreeNode(2);
        TreeNode *lvl1rr = new TreeNode(2);
        TreeNode *lvl2lll = new TreeNode(1);
        TreeNode *lvl2rll = new TreeNode(1);
        TreeNode *lvl2rlr = new TreeNode(1);
        TreeNode *lvl3llll = new TreeNode(0);
        TreeNode *lvl3lllr = new TreeNode(0);
        TreeNode *lvl3rlll = new TreeNode(0);
        TreeNode *lvl3rllr = new TreeNode(0);
        root->left = lvl0l;
        root->right = lvl0r;
        lvl0l->left = lvl1ll;
        lvl0r->left = lvl1rl;
        lvl0r->right = lvl1rr;
        lvl1ll->left = lvl2lll;
        lvl1rl->left = lvl2rll;
        lvl1rl->right = lvl2rlr;
        lvl2lll->left = lvl3llll;
        lvl2lll->right = lvl3lllr;
        lvl2rll->left = lvl3rlll;
        lvl2rll->right = lvl3rllr;
    }
    unordered_map<int, int> nodes;
    cout << power(root, nodes, 0) << '\n';
    return 0;
}