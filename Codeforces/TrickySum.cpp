#include <iostream>
#define int long long
using namespace std;
signed main () {
    int n;
    cin >> n;
    for(int i = 0; i < n; i++){
        int num;
        cin >> num;
        int ans = num * (num + 1) / 2;
        for(int j = 1; j <= num; j *= 2)
            ans -= (j * 2);
        cout << ans << '\n';
    }
    return 0;
}