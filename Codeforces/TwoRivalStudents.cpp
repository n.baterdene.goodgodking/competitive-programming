#include <bits/stdc++.h>
using namespace std;
int main () {
    int N;
    cin >> N;
    for (int I = 0; I < N; I++) {
        int n, a, b, m;
        cin >> n >> m >> a >> b;
        if (a > b) swap(a, b);
        int x = a - 1; 
        int y = n - b;
        int ans = b - a + min(x + y, m);
        cout << ans << '\n';
    }   
    return 0;
}