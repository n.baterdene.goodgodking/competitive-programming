#include <iostream>

using namespace std;

int main () {
    int n;
    cin >> n;
    if(n % 4 != 0 && (n + 1) % 4 != 0) {
        cout << "NO\n";
        return 0;
    }
    cout << "YES\n";
    if (n % 4 == 0) {
        cout << n / 2 << '\n';

        for(int i = 1; i <= n / 4; i++) 
            cout << i << ' ' << n - i + 1<< ' ';

        cout << '\n';
        cout << n / 2 << '\n';

        for(int i = n / 4 + 1; i <= n - n / 4; i++) 
            cout << i << ' ';

    } else {
        cout << n / 2 << '\n';
        cout << n << ' ';
        for(int i = 1; i <= n / 4; i++) 
            cout << i << ' ' << n - i << ' ';

        cout << '\n';
        cout << (n + 1) / 2 << '\n';
        
        for(int i = n / 4 + 1; i < n - n / 4; i++) 
            cout << i << ' ';

    }
    cout << '\n';
    return 0;
}