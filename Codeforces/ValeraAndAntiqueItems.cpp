#include <iostream>
#include <vector>
using namespace std;
int main () {
    int n, money, tmp, ans = 0;
    cin >> n >> money;
    int items;
    vector <int> answer;
    vector < vector <int> > stocks;
    for(int i = 0; i < n; i++) {
        cin >> items;
        vector <int> tmpvec;
        for(int j = 0; j < items; j++) {
            cin >> tmp;
            tmpvec.push_back(tmp);
        }
        stocks.push_back(tmpvec);
    }
    for(int i = 0; i < stocks.size(); i++) {
        for(int j = 0; j < stocks[i].size(); j++) {
            if(stocks[i][j] < money) {
                answer.push_back(i + 1);
                ans++;
                break;
            }
        }
    }
    cout << ans << '\n';
    for(int i = 0; i < answer.size(); i++)
        cout << answer[i] << ' ';
    return 0;
}