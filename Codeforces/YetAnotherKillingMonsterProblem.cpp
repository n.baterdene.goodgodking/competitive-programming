#include <bits/stdc++.h>
using namespace std;
#define ll long long
#define ff first
#define ss second
#define pb push_back
#define mp make_pair
#define N 200005
 
vector<pair<int, int> > hereos, tmp;
 
int monsters[N];
int chooseOne(int power, int st, int end) {
	int mid = (end + st) / 2;
	if (hereos[mid].ff == power || st == end) return mid;
	if (hereos[mid].ff < power ) st = mid + 1;
	else end = mid;
	return chooseOne(power, st, end);
}
void go(){
	int n;
	cin >> n;
	int maxMonster = 0, maxHero = 0;
	for (int i = 1; i <= n; i++){
		cin >> monsters[i];
		maxMonster = max(maxMonster, monsters[i]);
	}
	int m;
	cin >> m;
	hereos.clear();
	tmp.clear();
	for (int power, hp, i = 1; i <= m; i++){
		cin >> power >> hp;
		hereos.pb(mp(power, hp));
		maxHero = max(maxHero, power);
	}
	if (maxHero < maxMonster){
		cout << "-1\n";
		return;
	}
	sort(hereos.begin(), hereos.end());
	for (auto x : hereos){
		while (!tmp.empty() && tmp.back().ss <= x.ss)
			tmp.pop_back();
		tmp.push_back(x);
	}
	swap(hereos, tmp);
	int ans = 1;
	int past = 1, curMonster = 0;
	for (int i = 1; i <= n; i++){
		curMonster = max(monsters[i], curMonster);
		int chosenOne = chooseOne(curMonster, 0, hereos.size() - 1);
		if (i - past + 1 > hereos[chosenOne].ss){
			ans++;
			past = i;
			curMonster = monsters[i];
		}
	}
	cout << ans << '\n';
}
 
int main(){
    int t;
    cin >> t;
    while (t--) go();
}