// #include<ext/pb_ds/assoc_container.hpp>
#include<bits/stdc++.h>
 
// using namespace __gnu_pbds;
using namespace std;
 
#ifdef ONLINE_JUDGE
#define cerr if (false) cerr
#endif
 
#define rost(v) sort(v.rbegin(), v.rend())
#define sort(v) sort(v.begin(), v.end())
#define trav(i, v) for(auto &i : v)
#define debug(x) cerr <<__LINE__ <<' ' <<#x << ' ' <<(x)
#define mp make_pair
#define pb push_back
#define ff first
#define ss second
#define lb lower_bound
#define ub upper_bound
 
typedef long long ll;
typedef pair<int, int> pi;
typedef vector<int> vi;
// typedef tree<int, null_type, less_equal<int>, rb_tree_tag, tree_order_statistics_node_update> ordered_set;
//  
const int MOD = 1e9+7;
const int MX = 1e6+5;
const ll INF = 1e18; 
const long double PI = 4*atan((long double)1); 
 
mt19937 gen(static_cast<std::mt19937::result_type>(std::time(nullptr)));
 
ll powmod(ll a, ll b, ll mod) {ll res=1;a%=mod; assert(b>=0); for(;b;b>>=1){if(b&1)res=res*a%mod;a=a*a%mod;}return res;}
ll gcd(ll a, ll b) { return b?gcd(b,a%b):a;}
 
class BigInt{
    public: 
        int num[10005];
        int len;
        BigInt() {num[0] = 1; len = 0;}
        void print();
        void mult(int k);
        void reduce(BigInt b);
};
 
void BigInt::print() {
    for(int i = len; i >= 0; i--) 
        cout<<num[i];
    cout<<"\n";
}
 
void BigInt::mult(int k) {
    for(int i = 0; i <= len; i++) 
        num[i] *= k;
    for(int i = 0;i<len+10;i++)
	{
		num[i+1] += num[i]/10;
		num[i] %= 10;
	}
	int i = len+10;
	while(num[i] == 0)
	{
		i--;
	}
	len = i;
}
void BigInt::reduce(BigInt b) {
    for(int i = 0; i <= len; i++) {
        if(num[i] >= b.num[i]) 
            num[i] -= b.num[i];
        else {
            num[i+1]--;
            num[i] = num[i] + 10 - b.num[i];
        }
    }
    while(!num[len]) 
        len--;
}
int comp(BigInt a, BigInt b) {
    if(a.len > b.len) 
        return 1;
    if(a.len < b.len) 
        return 2;
 
    for(int i = a.len; i >= 0; i--) 
        if(a.num[i] > b.num[i]) 
            return 1;
        else if(a.num[i] < b.num[i])
            return 2;
 
    return 0;
}
 
BigInt a, b;
 
void lol() {
    int aint, bint;
    cin >>aint >>bint;
    for(int i = 1; i <= bint; i++) 
        a.mult(aint);
    for(int i = 1; i <= aint; i++) 
        b.mult(bint);
    int o = comp(a, b);
    if(o == 0) {
        cout << "0\n";
        return;
    }
    // cout << "correct solution: " << aint << ' ' << bint << " : ";
    if( o == 1 ) {
        a.reduce(b);
        a.print();
    }
    else {
        b.reduce(a);
        cout<<"-";
        b.print();
    }
}
 
int main() {
    freopen("in.txt", "r", stdin);
    // freopen("out1.txt", "w", stdout);

	int t = 1;
    // cin >>t;
    while(t--)
    	lol();
}