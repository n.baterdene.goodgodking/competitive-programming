#include <iostream>
#include <vector>

using namespace std;
int main () {
    int n, ans = 0;
    cin >> n;
    vector <int> numbers(n);
    for (int i = 0; i < n; i++) 
        cin >> numbers[i];
    vector <vector <int> > maximums(n, vector <int> (n));
    for (int i = 0; i < n; i++) {
        int maxNum = numbers[i];
        for (int j = i; j < n; j++) {
            maxNum = max(maxNum, numbers[j]);
            maximums[i][j] = maxNum;
        }
    }
    vector <int> front(n), back(n);
    int choose;
    int mini, len, half, must;
    for (int st = 1; st < n - 1; st++) {
        choose = n - 1;
        must = st;
        mini = numbers[st];
        while(must <= choose) {
            len = choose - st;
            half = len / 2;
            if (half == 0)
                break;
            mini = min(mini, numbers[choose]);
            if (maximums[st][choose] >= mini) {
                choose -= half;
            } else {
                must = choose;
                choose += half;
            }
        }
        front[st] = choose;
    }
    for(int end = n - 2; end > 0; end--) {
        choose = 0; 
        must = end;
        mini = numbers[end];
        while(must >= choose) {
            len = end - choose;
            half = len / 2;
            if(half == 0) 
                break;
            mini = min(mini, numbers[choose]);
            if (maximums[choose][end] >= mini) {
                choose += half;
            } else {
                must = choose;
                choose -= half;
            }
        }
        back[end] = choose - 1;
    }
    cout << "front : ";
    for(int i = 0; i < n; i++) 
        cout << front[i] << ' ';
    cout << '\n';
    cout << "back : ";
    for(int i = 0; i < n; i++) 
        cout << back[i] << ' ';
    cout << '\n';
    for(int i = 0; i < n; i++) {
        if (back[front[i]] <= i) {
            cout << "Numbers: ";
            for(int j = i; j <= front[i]; j++) 
                cout << numbers[i] << ' ';
            cout << '\n';
        }
    }
    return 0;
}