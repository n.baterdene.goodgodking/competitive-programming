#include <bits/stdc++.h>

using namespace std;

int V, E;
vector<int> adj[100001];
int vis[100001], maxVis[100001];
int cnt = 0;

int dfs(int u, int p, int t) {
    vis[u] = t;
    int ret = INT_MAX;
    maxVis[u] = 0;
    for (int i = 0; i < adj[u].size(); i++) {
        int v = adj[u][i];
        if (v == p) continue;
        int minVisTime;
        if (vis[v] == 0) {
            minVisTime = dfs(v, u, t + 1);
            if (u == p) cnt++;
        } else {
            minVisTime = vis[v];
        }
        maxVis[u] = max(maxVis[u], minVisTime);
        ret = min(ret, minVisTime);
    }
    return ret;
}

void init() {
    int u, v;
    cin >> V >> E;
    for (int i = 0; i < E; i++) {
        cin >> u >> v;
        adj[v].push_back(u);
        adj[u].push_back(v);
    }
}

int main() {
    init();
    dfs(1, 1, 1);
    if (cnt > 1) {
        cout << 1 << " ";
    }

    for (int i = 2; i <= V; i++) {
        if (vis[i] <= maxVis[i])
            cout << i << " ";
    }
    cout << '\n';
    return 0;
}