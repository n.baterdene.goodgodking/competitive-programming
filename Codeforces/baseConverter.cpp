#include <bits/stdc++.h>
using namespace std;
struct Number {
    int a, base;
    string A, digits;
    Number(int a1, int k = 10) {
        a = a1;
        base = k;
        digits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        genInBase();
    }
    void changeBase(int k) {
        base = k;
        genInBase();
    }
    void genInBase() {
        A = "";
        int a1 = a;
        while(a1 > 0) {
            A = digits[a1%base]+A;
            a1 /= base;
        }
        cout << A << '\n';
    }
    void changeVal(string A1, int k = -1) {
        A = A1;
        if (k != -1) base = k;
        genInt();
    }
    void genInt() {
        int val = 1;
        a = 0;
        for (int i = 0; i < A.size(); i++) {
            int j = A.size()-i-1;
            a += (A[j]-'0')*val;
            val *= base;
        }
        cout << a << '\n';
    }
};
int main() {
    Number a(2147483647);
    a.changeBase(2);
    // a.changeVal("1110", 2);
}