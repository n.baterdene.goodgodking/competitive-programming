#include <iostream>
#include <vector>
using namespace std;
int n;
bool row[9], col[9], diagx[25], diagy[25];
vector <int> tmp;
int couted_num = 0;
void my_func(int i){
    for(int j = 1; j <= n; j++){
        if(col[j] == false && row[i] == false && diagx[i + j] == false && diagy[i - j + 8] == false){
            row[i] = true;
            col[j] = true;
            diagx[i + j] = true;
            diagy[i - j + 8] = true;
            tmp.push_back(j);
            if(i == n){
                if(couted_num < 3){
                    for(int a = 0; a < tmp.size(); a++){
                        cout << tmp[a] << ' ';
                    }
                    cout << '\n';
                }
                couted_num++;
            }
            my_func(i + 1);
            row[i] = false;
            col[j] = false;
            diagx[i + j] = false;
            diagy[i - j + 8] = false;
            tmp.pop_back();
        }
    }
}
int main () {
    cin >> n;
    my_func(1);
    cout << couted_num << '\n';
    return 0;
}