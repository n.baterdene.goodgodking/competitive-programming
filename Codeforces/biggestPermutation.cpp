
#include<bits/stdc++.h> 
using namespace std; 
void myFunc(vector <int>& nums, int n, int m) { 
    vector <int> pos(n + 1);
    for (int i = 0; i < n; ++i) 
        pos[nums[i]] = i; 
  
    for (int i = 0; i < n && m > 0; ++i) { 
        if (nums[i] == n - i)  continue; 
        int temp = pos[n - i]; 
        pos[nums[i]] = pos[n - i]; 
        pos[n - i] = i; 
        swap(nums[temp], nums[i]); 
        m--;
    } 
    return;
} 
  
int main()  { 
    int n, m;
    cin >> n >> m;
    vector <int> nums(n);
    for (int i = 0; i < n; i++) 
        cin >> nums[i];

    myFunc(nums, n, m); 
    for (int i = 0; i < n; i++) 
        cout << nums[i] << ' ';
    cout << '\n';
    return 0; 
} 