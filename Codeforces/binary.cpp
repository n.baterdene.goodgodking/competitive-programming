#include <iostream>
using namespace std;
int main () {
    long long bina, num, rem, rev = 2;
    cin >> num;
    // num = 2147483647;
    while(num != 1){
        rem = num % 2;
        rev = rev * 10 + rem;
        num = num / 2;
    }
    rev = rev * 10 + 1;
    num = rev;
    rev = 0;
    while(num > 0){
        rem = num % 10;
        rev = rev * 10 + rem;
        num /= 10; 
    }
    rev /= 10;
    cout << rev << "\n";

    return 0;
}