#include <bits/stdc++.h>
using namespace std;
// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))

#define _upgrade ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define FOR(i, a, b) for (int i = a; i < b; i++)
#define FORB(i, a, b) for (int i = a - 1; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define umap unordered_map
#define pb push_back
#define mp make_pair
#define ff first
#define ss second
#define PQ priority_queue
#define Q queue

typedef long long ll;
typedef double d;
typedef unsigned uint;
typedef unsigned long long ull;
typedef unordered_map<int, int> umapII;
// PQ going up <int, VI, greater<int> >
typedef vector<int> VI;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<bool> VB;
typedef vector<umapII> VumapII;
typedef vector<ll> VLL;
typedef vector<VLL> VVLL;
typedef vector<d> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VPI> VVPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
 

VI readVI(int n) {
    VI a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVI readVVI(int n, int m) {
    VVI a(n, VI(m));
    REP(i, n) a[i] = readVI(m);
    return a;
}
VLL readVLL(ll n) {
    VLL a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVLL readVVLL(ll n, ll m) {
    VVLL a(n, VLL(m));
    REP(i, n) a[i] = readVLL(m);
    return a;
}


const int MOD = int(1e9) + 7;
const int INF = INT_MAX;
const ll INFF = INT64_MAX;
const d EPS = 1e-9;
const d PI = acos(-1.0); //M_PI;
const int moveX[] = {-1, 0, 1, 0};
const int moveY[] = {0, 1, 0, -1};

//                                        .....'',;;::cccllllllllllllcccc:::;;,,,''...'',,'..
//                             ..';cldkO00KXNNNNXXXinp000OOinpinpkxxxxxddoooddddddxxxxinpinpOO0XXKx:.
//                       .':ok0KXXXNXK0kxolc:;;,,,,,,,,,,,;;,,,''''''',,''..              .'lOXKd'
//                  .,lx00Oxl:,'............''''''...................    ...,;;'.             .oKXd.
//               .cinpinpc'...'',:::;,'.........'',;;::::;,'..........'',;;;,'.. .';;'.           'kNKc.
//            .:kXXk:.    ..       ..................          .............,:c:'...;:'.         .dNNx.
//           :0NKd,          .....''',,,,''..               ',...........',,,'',,::,...,,.        .dNNx.
//          .xXd.         .:;'..         ..,'             .;,.               ...,,'';;'. ...       .oNNo
//          .0K.         .;.              ;'              ';                      .'...'.           .oXX:
//         .oNO.         .                 ,.              .     ..',::ccc:;,..     ..                lXX:
//        .dNX:               ......       ;.                'cxOinp0OXWWWWWWWNX0kc.                    :KXd.
//      .l0N0;             ;d0inpinpKXK0ko:...              .l0X0xc,...lXWWWWWWWWKO0Kx'                   ,ONKo.
//    .lKNKl...'......'. .dXWN0inpk0NWWWWWN0o.            :KN0;.  .,cokXWWNNNNWNinpxONK: .,:c:.      .';;;;:lk0XXx;
//   :KN0l';ll:'.         .,:lodxxkO00KXNWWWX000k.       oXNx;:oinpX0kdl:::;'',;coxinpd, ...'. ...'''.......',:lxKO:.
//  oNNk,;c,'',.                      ...;xNNOc,.         ,d0X0xc,.     .dOd,           ..;dOKXK00000Ox:.   ..''dKO,
// 'KW0,:,.,:..,oxinpkdl;'.                'inp'              ..           .dXX0o:'....,:oOXNN0d;.'. ..,lOKd.   .. ;KXl.
// ;XNd,;  ;. l00kxoooxKXKx:..ld:         ;inp'                             .:dkO000000Okxl;.   c0;      :inp;   .  ;XXc
// 'XXdc.  :. ..    '' 'kNNNinpinp,      .,dKNO.                                   ....       .'c0NO'      :X0.  ,.  xN0.
// .kNOc'  ,.      .00. ..''...      .l0X0d;.             'dOkxo;...                    .;oinpXK0KNXx;.   .0X:  ,.  lNX'
//  ,inpdl  .c,    .dNK,            .;xXWKc.                .;:coOXO,,'.......       .,lx0XXOo;...oNWNXinp:.'KX;  '   dNX.
//   :XXkc'....  .dNWXl        .';l0NXNKl.          ,lxinpkxo' .cK0.          ..;lx0XNX0xc.     ,0Nx'.','.kXo  .,  ,KNx.
//    cXXd,,;:, .oXWNNKo'    .'..  .'.'dinp;        .cooollox;.xXXl     ..,cdOKXXX00NXc.      'oKWK'     ;k:  .l. ,0Nk.
//     cXNx.  . ,KWX0NNNXOl'.           .o0Ooldk;            .:c;.':lxOinpK0xo:,.. ;XX:   .,lOXWWXd.      . .':,.lKXd.
//      lXNo    cXWWWXooNWNXinpo;'..       .lk0x;       ...,:ldk0KXNNOo:,..       ,OWNOxO0KXXNWNO,        ....'l0Xk,
//      .dNK.   oNWWNo.cXK;;oOXNNXK0kxdolllllooooddxk00inpinp0kdoc:c0No        .'ckXWWWNXkc,;kNKl.          .,kXXk,
//       'KXc  .dNWWX;.xNk.  .kNO::lodxkOXWN0OkxdlcxNKl,..        oN0'..,:ox0XNWWNNWXo.  ,ONO'           .o0Xk;
//       .ONo    oNWWN0xXWK, .oNKc       .ONx.      ;X0.          .:XNinpNNWWWWNinpl;kNk. .cKXo.           .ON0;
//       .xNd   cNWWWWWWWWKOinpNXxl:,'...;0Xo'.....'lXK;...',:lxk0KNWWWWNNKOd:..   lXKclON0:            .xNk.
//       .dXd   ;XWWWWWWWWWWWWWWWWWWNNNNNWWNNNNNNNNNWWNNNNNNWWWWWNXKNNk;..        .dNWWXd.             cXO.
//       .xXo   .ONWNWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWNNK0ko:'..OXo          'l0NXx,              :inp,
//       .OXc    :XNk0NWXKNWWWWWWWWWWWWWWWWWWWWWNNNX00NNx:'..       lXKc.     'lONN0l.              .oXK:
//       .KX;    .dNKoON0;lXNkcld0NXo::cd0NNO:;,,'.. .0Xc            lXXo..'l0NNKd,.              .c0Nk,
//       :XK.     .xNX0NKc.cXXl  ;KXl    .dN0.       .0No            .xNXOKNXOo,.               .l0Xk;.
//      .dXk.      .lKWN0d::OWK;  lXXc    .OX:       .ONx.     . .,cdk0XNXOd;.   .'''....;c:'..;xKXx,
//      .0No         .:dOKNNNWNKOxkXWXo:,,;ONk;,,,,,;c0NXOxxkO0XXNXKOdc,.  ..;::,...;lol;..:xKXOl.
//      ,XX:             ..';cldxkOO0inpKXXXXXXXXXXinpinpK00Okxdol:;'..   .';::,..':llc,..'linpXkc.
//      :NX'    .     ''            ..................             .,;:;,',;ccc;'..'linpX0d;.
//      lNK.   .;      ,lc,.         ................        ..,,;;;;;;:::,....,linpX0d:.
//     .oN0.    .'.      .;ccc;,'....              ....'',;;;;;;;;;;'..   .;oOXX0d:.
//     .dN0.      .;;,..       ....                ..''''''''....     .:dOinpko;.
//      lNK'         ..,;::;;,'.........................           .;d0X0kc'.
//      .xXO'                                                 .;oOK0x:.
//       .cinpo.                                    .,:oxinpkxk0K0xc'.
//         .oinpkc,.                         .';cok0XNNNX0Oxoc,.
//           .;d0XX0kdlc:;,,,',,,;;:clodkO0inp0Okdl:,'..
//               .,coxO0KXXXXXXXinp0OOxdoc:,..
//                         ...
//       ⣠⣶⡾⠏⠉⠙⠳⢦⡀⠀  ⠀⢠⠞⠉⠙⠲⡀
//   ⠀⠀⣴⠿⠏⠀⠀⠀⠀⠀⠀⢳⡀⠀  ⡏⠀⠀ ⠀⠀ ⢷
// ⠀⠀⢠⣟⣋⡀⢀⣀⣀⡀⠀⣀⡀⣧⠀  ⢸⠀⠀⠀⠀   ⡇ ⠀
//  ⠀⢸⣯⡭⠁⠸⣛⣟⠆⡴⣻⡲⣿   ⣸⠀⠀OK.  ⡇
// ⠀⠀⣟⣿⡭⠀⠀⠀⠀⠀⢱⠀⠀⣿   ⢹⠀⠀⠀⠀⠀ ⡇
// ⠀⠀⠙⢿⣯⠄⠀⠀⠀⢀⡀⠀⠀⡿⠀  ⡇⠀⠀ ⠀ ⡼
// ⠀⠀⠀⠀⠹⣶⠆⠀⠀⠀⠀⠀⡴⠃ ⠀ ⠘⠤⣄⣠⠞⠀ ⠀
// ⠀⠀⠀⠀⢸⣷⡦⢤⡤⢤⣞⣁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ ⠀
// ⠀⢀⣤⣴⣿⣏⠁⠀⠀⠸⣏⢯⣷⣖⣦⡀⠀⠀⠀⠀⠀⠀
// ⢀⣾⣽⣿⣿⣿⣿⠛⢲⣶⣾⢉⡷⣿⣿⠵⣿⠀⠀⠀⠀⠀
// ⣼⣿⠍⠉⣿⡭⠉⠙⢺⣇⣼⡏⠀ ⠀⣄⢸⣿
// ⣿⣿⣧⣀⣿.........⣀⣰⣏⣘⣆⣀
/*

//      \                    / \  //\
//        \    |\___/|      /   \//  \\
//             /0  0  \__  /    //  | \ \    
//            /     /  \/_/    //   |  \  \  
//            @_^_@'/   \/_   //    |   \   \ 
//            //_^_/     \/_ //     |    \    \
//         ( //) |        \///      |     \     \
//       ( / /) _|_ /   )  //       |      \     _\
//     ( // /) '/,_ _ _/  ( ; -.    |    _ _\.-~        .-~~~^-.
//   (( / / )) ,-{        _      `-.|.-~-.           .~         `.
//  (( // / ))  '/\      /                 ~-. _ .-~      .-~^-.  \
//  (( /// ))      `.   {            }                   /      \  \
//   (( / ))     .----~-.\        \-'                 .~         \  `. \^-.
//              ///.----..>        \             _ -~             `.  ^-`  ^-_
//                ///-._ _ _ _ _ _ _}^ - - - - ~                     ~-- ,.-~
//                                                                   /.-~
// ----------------------// \\
// ---------------------// ¤ \\
// ---------------------\\ ¤ //
// ----------------------\\//
// -------------------- (___)
// ---------------------(___)
// ---------------------(___)
// ---------------------(___)_________
// -----------\_____/\__/----\__/\_____/
// ------------\ _°_[----------]_ _° /
// ----------------\_°_¤ ---- ¤_°_/
// --------------------\ __°__ /
// ---------------------|\_°_/|
// ---------------------[|\_/|]
// ---------------------[|[¤]|]
// ---------------------[|;¤;|]
// ---------------------[;;¤;;]
// --------------------;;;;¤]|]\
// -------------------;;;;;¤]|]-\
// ------------------;;;;;[¤]|]--\
// -----------------;;;;;|[¤]|]---\
// ----------------;;;;;[|[¤]|]|---|
// ---------------;;;;;[|[¤]|]|---|
// ----------------;;;;[|[¤]|/---/
// -----------------;;;[|[¤]/---/
// ------------------;;[|[¤/---/
// -------------------;[|[/---/
// --------------------[|/---/
// ---------------------/---/
// --------------------/---/|]
// -------------------/---/]|];
// ------------------/---/¤]|];;
// -----------------|---|[¤]|];;;
// -----------------|---|[¤]|];;;
// ------------------\--|[¤]|];;
// -------------------\-|[¤]|];
// ---------------------\|[¤]|]
// ----------------------\\¤// 
// -----------------------\|/
// ------------------------V

███████▓█████▓▓╬╬╬╬╬╬╬╬▓███▓╬╬╬╬╬╬╬▓╬╬▓█ 
████▓▓▓▓╬╬▓█████╬╬╬╬╬╬███▓╬╬╬╬╬╬╬╬╬╬╬╬╬█ 
███▓▓▓▓╬╬╬╬╬╬▓██╬╬╬╬╬╬▓▓╬╬╬╬╬╬╬╬╬╬╬╬╬╬▓█ 
████▓▓▓╬╬╬╬╬╬╬▓█▓╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬▓█ 
███▓█▓███████▓▓███▓╬╬╬╬╬╬▓███████▓╬╬╬╬▓█ 
████████████████▓█▓╬╬╬╬╬▓▓▓▓▓▓▓▓╬╬╬╬╬╬╬█ 
███▓▓▓▓▓▓▓╬╬▓▓▓▓▓█▓╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬▓█ 
████▓▓▓╬╬╬╬▓▓▓▓▓▓█▓╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬▓█ 
███▓█▓▓▓▓▓▓▓▓▓▓▓▓▓▓╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬▓█ 
█████▓▓▓▓▓▓▓▓█▓▓▓█▓╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬▓█ 
█████▓▓▓▓▓▓▓██▓▓▓█▓╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬██ 
█████▓▓▓▓▓████▓▓▓█▓╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬██ 
████▓█▓▓▓▓██▓▓▓▓██╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬██ 
████▓▓███▓▓▓▓▓▓▓██▓╬╬╬╬╬╬╬╬╬╬╬╬█▓╬▓╬╬▓██ 
█████▓███▓▓▓▓▓▓▓▓████▓▓╬╬╬╬╬╬╬█▓╬╬╬╬╬▓██ 
█████▓▓█▓███▓▓▓████╬▓█▓▓╬╬╬▓▓█▓╬╬╬╬╬╬███ 
██████▓██▓███████▓╬╬╬▓▓╬▓▓██▓╬╬╬╬╬╬╬▓███ 
███████▓██▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓╬╬╬╬╬╬╬╬╬╬╬████ 
███████▓▓██▓▓▓▓▓╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬▓████ 
████████▓▓▓█████▓▓╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬▓█████ 
█████████▓▓▓█▓▓▓▓▓███▓╬╬╬╬╬╬╬╬╬╬╬▓██████ 
██████████▓▓▓█▓▓▓╬▓██╬╬╬╬╬╬╬╬╬╬╬▓███████ 
███████████▓▓█▓▓▓▓███▓╬╬╬╬╬╬╬╬╬▓████████ 
██████████████▓▓▓███▓▓╬╬╬╬╬╬╬╬██████████ 
███████████████▓▓▓██▓▓╬╬╬╬╬╬▓███████████



                   ▒▓▓█████████████▓▓▒                      
              ▒████▓▓▒▒ ▒▒▒ ▒▒▒▒▒▒▓▓████▓▒                  
           ▒███▓                      ▒███████▓▓▒           
         ▒██▓    ▒▒███▓▓▒             ▓▓▒▒▒▒▒▓██████▓       
       ▒██▓   ▓███▓▒        ▒▒▒▒▒▒▒▒▒▒      ▒▓▓███▓████▒▒   
      ██▓  ▒▓██▓  ▒▓██████▓       ▒      ▒██▓▒   ▓███▒▓▒    
     ██  ▓███▒  ▒██▒    ▒▒██▓           ██▒    ▒████▒█      
    ██ ▒▓▒▓▓   ██           █▓         ██      ▒███  █▒     
   ▓█         ██            ▓█        ██             █▒     
   █▓         █▒  ████       █▒       ██            ███     
  ▒█       ▒▓▒█▓ ▓████▓     ▒█        ▒█▒          ██ █▒    
  ██     ▒▓▒▓▒██▒▒▓▓▓       ██         ▒████▓███████▓ █▒    
  █▓     ▒   ▒ ▒██▓▒     ▒██▓              ██▓      ▒██▓    
  █         ▓▒    ▒▓██████▓              ▒██   ▓█▓▓▒   ██   
 ▒█                          ▓▒▓▒▒▒▒▒▓▓▓▓██  ▓█▓ ▒▒██▒  ██  
 ▓█                          ▒▒▒▒▒▒▓▓▒  ██  ██▓ ▒ ▒ ██  ▒█  
 ██                       ▒▓▒▒▒▒▒▒▒▒   ██  ▓█ █▓ █▒█▓█▓  █  
 ██                         ▒▒▒▒▒ ▒▒  ▓█▓  ██ █▒▒█▒█▒▓█  █  
 ██                        ▒          ▓█   █▒    ▒▒  ▒█ ▓█  
 ▒█                        ▒▒▒▒▒▒▒▒▒▒  █▒  █▒        ▓█ █▓  
  █▓ ▒▒                                ▓█  █▒        █ ▒█   
  ▓█  ▒  ▒▒  ▒                          █  █▒       █▓ █▓   
   █▒  ▒  ▒  ▒▒                         █  █▒      ▓█  █    
   ██   ▒ ▒   ▒▒                        █  █▒      ██  █    
    █▓   ▒ ▒    ▒▒     ▒▒▒▒▒▒          ▒█ ▒█       █▒  █▓   
    ▓█    ▒▒     ▒▒      ▒▒▒▓▓▓▒       ██ ██       ██  ▓█   
     ██   ▒ ▒     ▒        ▒ ▒▒▓█▒    ▒█  █▓▒▓▓▓▒  ▓█   █▒  
     ▒█▒   ▒▒▒    ▒          ▒▒▒ ▒▓   ██ ▒█    ▒▓▓  ██  █▒  
      ▒█▒   ▒ ▒   ▒      ▒▒▒    ▒▒   ▒█  ██       █ ▒█  █▒  
       ▓█   ▒ ▒    ▒▒    ▓▒▒▓▓▓▒  ▓▒ ██  ██▒▒▒▒▓▒▓▓███  █▒  
        ██   ▒ ▒     ▒▒        ▓█▓   █▓  ██ ▓█ █ █  █▒  █▒  
         ██    ▒▒      ▒▒        ▒█▓ █▓  ▓█▒▒█▒█ █▒██  ▒█   
          ██▒    ▒   ▒   ▒▒        ▒▓██   ██    ▒▒██   ██   
           ▓██        ▒▒   ▒         ▓█    ▓███▓▓██   ██    
             ▓██▒      ▒▒▒▒▒          ██      ▒▒▒    ██     
               ▓███▒       ▒▒▒▒▒▒▒▒    ▓██▒       ▒███      
                 ▒▓███▓▒       ▒  ▒▒▒▒   ▒██▓██████▓        
                     ▒████▓▒▒               ▓██▒            
                         ▒▓████▓       ▓█████▒              
                              ▒█████████▒                   
                                  ▒▒▒                       
*/
















#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second
 
using namespace std;
 
int best(int l, int r, VI nums) {
    int ans = 0;
    VI count(27, 0);
    FOR(i, l, r + 1) count[nums[i]]++;
    REP1(i, 27) ans = max(ans, count[i]);
    return ans;
}
int n, ans = 0;
void go() {

    cin >> n;
    VI nums(n);
    VVI dp (27);
    REP(i, n) cin >> nums[i], dp[nums[i]].pb(i);
    ans = best(0, n - 1, nums);
    REP1(j, 27)
    REP(i, dp[j].size() / 2)
        ans = max(ans, (i + 1) * 2 + best(dp[j][i] + 1, dp[j][dp[j].size() - i - 1] - 1, nums));

    cout << ans << '\n';
}
int main () {

#ifdef ONLINE_JUDGE
#else
    freopen("in.txt", "r", stdin);
#endif
    _upgrade
    int T;
    cin >> T;
    while(T--) go();
    return 0;
}

 