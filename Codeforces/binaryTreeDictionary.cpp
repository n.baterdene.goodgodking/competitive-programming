#include <iostream>
#include <vector>

#define pb push_back
using namespace std;
struct TreeNode {
    int val;
    TreeNode *left, *right;
    TreeNode(int num) { 
        val = num;
        left = NULL;
        right = NULL;
    }
};
int height = 0, el = 0, need, bottom = 0;
vector <int> empty;
vector <int> levels;
void move(int idx, TreeNode *node) {
    if (levels.size() == idx) levels.pb(0);
    levels[idx]++;
    el++;
    height = max(height, idx + 1);
    if (node->right != NULL) move(idx + 1, node->right);
    if (node->left != NULL) move(idx + 1, node->left);
}
void btm(int idx, TreeNode *node, int power) {
    if (idx == height - 1) bottom += power;
    if (node->right != NULL) btm(idx + 1, node->right, power * 2 + 1);
    if (node->left != NULL) btm(idx + 1, node->left, power * 2);
}
int main () {
    TreeNode *root = new TreeNode(1);
    if (true) {
        TreeNode *lvl0r = new TreeNode(2);
        TreeNode *lvl0l = new TreeNode(2);
        // TreeNode *lvl1lr = new TreeNode(2);
        TreeNode *lvl1ll = new TreeNode(2);

        root->right = lvl0r;
        root->left = lvl0l;
        lvl0l->left = lvl1ll;
        // lvl0l->right = lvl1lr;
    }

    move(0, root);
    btm(0, root, 1);
    int num = 1;
    for (int i = 0; i < height; i ++) 
        num *= 2;

    if (num - 1 == el) {
        cout << "answer is : perfect BTree\n";
        return 0;
    }
    for(int i = 1; i < height; i++) {
        if (levels[i] != 2) break;
        if (i == levels.size() - 1 && el != 3) {
            cout << "answer is : full BTree\n";
            return 0;
        }
    }
    num /= 2;
    if (el * (el + 1) / 2 - bottom == num * (num - 1) / 2) 
        cout << "answer is : complete BTree\n";
    else 
        cout << "answer is : BTree\n";
    return 0;
}