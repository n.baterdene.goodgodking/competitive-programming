#include <bits/stdc++.h>
using namespace std;
long long n;
long long solve(long long num) {
    long long ans = 0;
    for (long long i = 1; i * max(1LL, num) <= n; i*=10) {
        long long lastDigits = (n % (10 * i));
        long long curDigit = n / (10 * i) * i + (lastDigits >= num * i ? min(lastDigits, i * num + i - 1) - i * num + 1 : 0);
        ans += curDigit;
    }
    return ans;
}

int main () {
    cin >> n;
    long long ans[10];
    long long allDigit = 0, curDigitCount = 1;
    for (long long i = 1; i <= n; i *= 10, curDigitCount++) {
        if (i * 10 - 1 <= n) allDigit += curDigitCount * i * 9;       
        else allDigit += curDigitCount * (n - i + 1);
    }
    for (long long i = 1; i < 10; i++) {
        ans[i] = solve(i);
        allDigit -= ans[i];
    }
    ans[0] = allDigit;
    for (long long i = 0; i < 10; i++) 
        cout << ans[i] << '\n';
    return 0;
}