#include <bits/stdc++.h>
using namespace std;
// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))
#define int long long
#define _upgrade ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define FOR(i, a, b) for (int i = a; i < b; i++)
#define FORB(i, a, b) for (int i = a - 1; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define umap unordered_map
#define pb push_back
#define mp make_pair
#define ff first
#define ss second
#define PQ priority_queue
#define Q queue

typedef long long ll;
typedef double d;
typedef unsigned uint;
typedef unsigned long long ull;
typedef unordered_map<int, int> umapII;
// PQ going up <int, VI, greater<int> >
typedef vector<int> VI;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<bool> VB;
typedef vector<umapII> VumapII;
typedef vector<ll> VLL;
typedef vector<VLL> VVLL;
typedef vector<d> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VPI> VVPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
 

VI readVI(int n) {
    VI a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVI readVVI(int n, int m) {
    VVI a(n, VI(m));
    REP(i, n) a[i] = readVI(m);
    return a;
}
VLL readVLL(ll n) {
    VLL a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVLL readVVLL(ll n, ll m) {
    VVLL a(n, VLL(m));
    REP(i, n) a[i] = readVLL(m);
    return a;
}

int N;
VLL nums;
void change3(int n) {
    int cur = n / 3;
    nums[0] = nums[1] = nums[2] = (nums[0] - cur);
    n -= cur * 3;
    if (n == 1) nums[0]--;
    if (n == 2) nums[0] = nums[1] = (nums[2] - 1);
}
void change2(int n) {
    sort(ALL(nums));
    if (nums[0] == nums[2]) return change3(n);
    int dif = nums[2] - nums[0];
    if (n > 2 * dif) {
        nums[1] = nums[2] = nums[0];
        return change3(n - 2 * dif);
    }
    nums[1] = nums[2] = (nums[1] - n / 2);
    if (n & 1) nums[1]--;
    return;
}
void change(int n) {
    sort(ALL(nums));
    if (n <= 0) return;
    int cur = nums[2] - nums[1];
    if (cur == 0) {
        return change2(n);
    }
    cur = min(cur, n);
    n -= cur;
    nums[2] -= cur;
    return change(n);
    
}
signed main () {
    _upgrade
    nums = readVLL(3);
    cin >> N;
    change(N);
    int ans = nums[0] * nums[1] * nums[2];
    if (ans < 0) ans = 0;
    cout << ans << '\n';
    return 0;
}
