#include <bits/stdc++.h>
using namespace std;

int main () {
    int t = 1;
    cin >> t;
    while(t--) {
        int n, m, ans = 0, num;
        cin >> n >> m;
        vector <int> cnt(105, 0);
        for (int i = 0; i < n + m; i++) {
            cin >> num;
            cnt[num]++;
        }
        for (int i = 0; i < 105; i++) 
            if (cnt[i]) ans++;

        cout << n + m - ans << '\n';
    }
    return 0;
}
