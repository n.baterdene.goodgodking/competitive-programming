// C++ program for centroid decomposition of Tree
#include <bits/stdc++.h>
using namespace std;
vector<int> sizes;
int OneCentroid(int root, const vector <vector <int> > &g, const vector <bool> &dead) {
    function <void (int, int)> get_sz = [&](int u, int prev) {
        sizes[u] = 1;
        for (auto v : g[u]) if (v != prev && !dead[v]) {
            get_sz(v, u);
            sizes[u] += sizes[v];
        }
    };
    get_sz(root, -1);
    
    int n = sizes[root];
    function <int (int, int)> dfs = [&](int u, int prev) {
        for (auto v : g[u]) if (v != prev && !dead[v]) {
            if (sizes[v] > n / 2) return dfs(v, u);
        }
        return u;
    };
    return dfs(root, -1);
    // -> centroids
    // int n = sz[root];
    // vector<int> centroid;
    // function<void (int, int)> dfs = [&](int u, int prev) {
    //         bool is_centroid = true;
    //         for (auto v : g[u]) if (v != prev && !dead[v]) {
    //                 dfs(v, u);
    //                 if (sz[v] > n / 2) is_centroid = false;
    //         }
    //         if (n - sz[u] > n / 2) is_centroid = false;
    //         if (is_centroid) centroid.push_back(u);
    // };
    // dfs(root, -1);
    // return centroid;
}
int n;
vector <vector <int> > paths;
int main() {
	freopen("in.txt", "r", stdin);
	cin >> n;
	paths.resize(n);
	sizes.resize(n);
	for (int i = 0; i < n-1; i++) {
		int a, b;
		cin >> a >> b;
		a--, b--;
		paths[a].push_back(b);
		paths[b].push_back(a);
	}
	vector <bool> dead(n, 0);
	cout << OneCentroid(0, paths, dead) << '\n';
	return 0;
}
