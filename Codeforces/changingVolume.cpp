#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
using namespace std;
void go() {
    int a, b;
    cin >> a >> b;
    int dif = abs(a - b);
    int ans = 0;
    ans += dif / 5;
    dif %= 5;
    ans += dif / 2;
    dif %= 2;
    ans += dif;
    cout << ans << '\n';
}
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int T;
    cin >> T;
    while (T--) go();
	return 0;
}