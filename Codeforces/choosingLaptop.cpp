#include <bits/stdc++.h>
using namespace std;

int main () {
    int n, ans = 0;
    cin >> n;
    vector <int> left;
    vector <bool> usable(n + 5, 1);
    vector <vector <int> > nums(n, vector <int>(4));

    for (int i = 0; i < n; i++) 
    for (int j = 0; j < 4; j++) 
        cin >> nums[i][j];
        
    for (int i = 0; i < n; i++) 
    for (int j = 0; j < n; j++) {
        int cnt = 0;
        for (int k = 0; k < 3; k++) 
            if (nums[i][k] < nums[j][k]) cnt++;

        if (cnt == 3) usable[i] = 0;
    }
    for (int i = 0; i < n; i++) 
        if (usable[i]) left.push_back(i);

    for (int i = 0; i < left.size(); i++) 
        if (nums[left[i]][3] < nums[left[ans]][3]) ans = i;
        
    cout << left[ans] + 1 << '\n';
    return 0;
}