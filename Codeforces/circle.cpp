#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
#define int double
int PI = M_PI;
using namespace std;
int findDist(pair <int, int>&a, pair <int, int>& b) {
    int x = abs(a.first - b.first);
    int y = abs(a.second - b.second);
    return sqrt(x * x + y * y);
}
int findS(int a, int b, int c) {
    int s = (a + b + c) / 2;
    s = (s * (s - a) * (s - b) * (s - c));
    return sqrt(s);
}
int findSsector(int a, int b) {
    return PI * a * a * (b / 360);
}
struct ball{
    pair <int, int> center;
    int radius;
};
vector <ball> balls;
bool In(int a, int b, int c) {
    if (c + b <= a || c + a <= b) return true;
    return false;
}
bool Out(int a, int b, int c) {
    if (a + b < c) return true;
    return false;
}
bool method(int a, int b, int c) {
    if (a > c || b > c) return true;
    return false;
}
signed main () {
    int a, b, c;
    for (int i = 0; i < 2; i++) {
        cin >> a >> b >> c;
        ball tmp;
        tmp.center = mp(a, b);
        tmp.radius = c;
        balls.pb(tmp);
    }
    c = findDist(balls[0].center, balls[1].center);
    a = balls[0].radius, b = balls[1].radius;
    if (In(a, b, c)) {
        int sector1 = findSsector(a, 360);
        int sector2 = findSsector(b, 360);
        cout << fixed << setprecision(2) << min(sector1, sector2) << '\n';
        return 0;
    }
    if (Out(a, b, c)) {
        cout << "0.00\n";
        return 0;
    }
    cout << a << ' ' << b << ' ' << c << '\n';
    int ontsog1 = acos ((a * a + c * c - b * b) / (2 * a * c));
    while (ontsog1 < 0) ontsog1 += 90;
    ontsog1 = ontsog1 * 180.0 / PI;
    int ontsog2 = acos ((b * b + c * c - a * a) / (2 * b * c));
    while (ontsog2 < 0) ontsog2 += 90;
    ontsog2 = ontsog2 * 180.0 / PI;
    ontsog1 *= 2;
    ontsog2 *= 2;
    cout << "here\n" << (a * a + c * c - b * b) / (2 * a * c) << '\n';
    cout << ontsog1 << ' ' << ontsog2 << '\n';
    int mid = a * a * 2 * (1 - cos(ontsog1 * PI / 180.0) );
    mid = sqrt(mid);
    // cout << cos(ontsog1 * PI / 180.0) << ' ' << cos(ontsog2 * PI / 180.0) << '\n';
    // cout << mid << '\n';
    int triangle1 = findS(a, a, mid);
    int triangle2 = findS(b, b, mid);
    int sector1 = findSsector(a, ontsog1);
    int sector2 = findSsector(b, ontsog2);
    if (method(a, b, c)) {
        int toirog = min(a, b);
        toirog = toirog * toirog * PI;
        cout << toirog << ' ' << sector1 << ' ' << sector2 << ' ' << triangle1 << ' ' << triangle2 << '\n';
        cout << toirog - max(triangle1, triangle2) - max(sector2, sector1) - min(triangle1, triangle2) - min(sector2, sector1) << '\n';
    } else {
        cout << fixed << setprecision(2) << sector1 + sector2 - triangle1 - triangle2 << '\n';
    }
    return 0;
}