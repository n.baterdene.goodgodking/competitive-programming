
#include <bits/stdc++.h> 
using namespace std; 
  
//Base class 
class User { 
    public: 
        string username, password, profilePic;
        User() {
            cin >> username;
            cout << "My name is " << username << ". I'm a user\n";
        }
}; 
   
// Sub class inheriting from Base Class(Parent) 
class Admin : public User { 
    public:
        vector <string> posts;
        string name;
        Admin() {
            cout << "I'm an admin user\n";
        }
}; 
  
int main() { 
    Admin Bilegt;

    return 0; 
}  