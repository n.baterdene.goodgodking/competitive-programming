#include <iostream>
using namespace std;
int main () {
    cout << "welcome to my programm\n";
    cout << "Enter the color's name that you wanted\n ";
    
    
    string a;
    cin >> a;
    //lets write colors that we can write as many we can write
    string colors[] = {
        "maroon",
        "red",
        "orange",
        "yellow",
        "olive",
        "purple",
        "pink",
        "white",
        "lime",
        "green",
        "navy",
        "blue",
        "aqua",
        "teal",
        "black",
        "silver",
        "gray" // I think it's enough
    };
    string color_codes[] = {
        "#800000",
        "#ffa500",
        "#ffff00",
        "#808000",
        "#800080",
        "#ff00ff",
        "#ff0000",
        "#ffffff",
        "#00ff00",
        "#008000",
        "#0000ff",
        "#00ffff",
        "#008080",
        "#000000",
        "#c0c0c0",
        "#808080"
    };
    int len = 17;
    bool used = false;
    for(int i = 0; i < len; i++){
        if(a == colors[i]){
            cout << color_codes[i];
            used = true;
        }
    }
    if(used == true)
        cout << " " << " is your color's code\n";
    else 
        cout << "we don't have color code that you entered\n";
    return 0;
}