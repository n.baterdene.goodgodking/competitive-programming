#include <bits/stdc++.h>
// #pragma GCC optimize("O3, unroll-loops")
// #pragma GCC target("avx2")
// #define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define sz(x) (int)x.size()

using ll = long long;
using db = double;
using ld = long double;
using uint = unsigned;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VS = vector<string>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using VD = vector<db>;
using PII = pair<int, int>;
using PLL = pair<ll, ll>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int bitCnt(int a) {
    bitset <64> b(a);
    return b.count();
}
int bitCnt(string a) {
    bitset <64> b(a);
    return b.count();
}

int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}

void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VPI& a) {
    for (auto el : a) {
        cout << el.ff << ',' << el.ss << ' ';
    }
    cout << '\n';
}

void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VVI& a) {
    for (auto el : a) {
        print(el);
    }
}

/*

|                    _                                                     
|  __   _           | |                                                    
| |   \| | ___  ___ | |_                                                   
| | |\ | |/ _ \/ __/| __|                                                  
| | | \  |  __/\__ \| |_                                                   
| |_|  \_|\___ /___/ \__|                                                  
|                                                      _         _   _     
|                                                     (_)       | | | |    
|  _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_   
| | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|  
| | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_   
| | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|  
| | |               __/ |                                                  
| |_|              |___/                   _                               
|                                  _      (_)                              
|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                               
| | '_ ` _ \ / _` |_   / _` |/ _` | |/ _` | |                              
| | | | | | | (_|  /  / (_| | (_| | | (_| | |                              
| |_| |_| |_|\__,_|____\__,_|\__,_|_|\__,_|_|                              
*/

const db PI = acos(-1.0); //M_PI;
const ll INFF = 4e18;
const int INF = 1.07e9;
const int MOD1 = 1e9 + 7;
const int MOD = 998244353; //7*19*2^23 +1;
const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0, 1, 0, -1};





const int N = 100 + 5;
const int M = 2e5 + 5;

int nums[N];

int n, m, k, q;
string s;
ll x[N], y[N];
bool isDuo[N];
int farthest[N];
PII nearestTwo[N];
ll dist(int a, int b) {
    return abs(x[a] - x[b]) + abs(y[a] - y[b]);
}
ll dist(PII a, PII b) {
    return abs(a.ff - b.ff) + abs(a.ss - b.ss);
}
ll fact[N], invFact[N];
ll pow(ll a, ll b, ll p) {
    a %= p;
    ll res = 1;
    while(b > 0) {
        if (b & 1) res = res * a % MOD;
        a = a * a % MOD;
        b /= 2;
    }
    return res;
}
ll inv(ll a, ll p) {
    return pow(a, p-2, p);
}
ll nCr(ll a, ll b) {
    return fact[a] * invFact[b] % MOD * invFact[a - b] % MOD;
}

void go () {
    int duoCnt = 0;
    cin >> n;
    m = n;
    for (int i = 1; i <= n; i++) {
        cin >> x[i] >> y[i];
    }
    if (n == 2) {
        cout << n * (n - 1) % MOD << "\n";
        return;
    }
    if (n == 3 && dist(1, 2) == dist(2, 3) && dist(2, 3) == dist(1, 3)) {
        cout << "9\n";
        return;
    }
    // for (int i = 1; i <= n; i++)
    // for (int j = 1; j <= n; j++) 
    //     cout << i << ' ' << j << ": " << dist(i, j) << '\n';
    for (int i = 1; i <= n; i++) {
        vector <pair <ll, int> > pts;
        for (int j = 1; j <= n; j++) {
            if (i == j) continue;
            pts.pb({dist(i, j), j});
        }
        sort(ALL(pts));
        if (pts[0].ff != pts[1].ff) farthest[i] = pts[0].ss;
        else farthest[i] = -1;
        // cout << i << ": " << pts[0].ff << ',' << pts[0].ss << ' ' << pts[1].ff << ',' << pts[1].ss << '\n';
        if (pts.size() > 2) {
            if (pts[0].ff == pts[1].ff && pts[1].ff != pts[2].ff) nearestTwo[i] = {pts[0].ss, pts[1].ss};
            else nearestTwo[i] = {-1, -1};
        }
    }
    for (int i = 1; i <= n; i++) {
        if (farthest[i] != -1 && farthest[farthest[i]] == i) {
            duoCnt++;
            isDuo[i] = 1;
        }
    }
    duoCnt /= 2;
    vector <int> ids;
    for (int i = 1; i <= n; i++) {
        if (isDuo[i]) continue;
        ids.pb(i);
    }
    // trio
    n = ids.size();
    int trioCnt = 0;
    for (int i = 0; i < n; i++) {
        if (nearestTwo[ids[i]].ff == -1) continue;
        int a = nearestTwo[ids[i]].ff;
        int b = nearestTwo[ids[i]].ss;
        set <int> A, B, C; 
        C = {ids[i], nearestTwo[ids[i]].ff, nearestTwo[ids[i]].ss};
        A = {a, nearestTwo[a].ff, nearestTwo[a].ss};
        B = {b, nearestTwo[b].ff, nearestTwo[b].ss};
        if (C == A && C == B) {
            // cout << ids[i] << ": " << a << "," << b << "\n";
            trioCnt++;
        }
    }
    n -= trioCnt;
    trioCnt /= 3;
    // cout << n << ' ' << duoCnt << ' ' << trioCnt << '\n';
    ll ans = 1;
    for (int i = 0; i < n; i++) {
        ans = ans * (m - i) % MOD;
    }
    // cout << ans << '\n';
    // solve for duos
    fact[0] = fact[1] = 1;
    for (int i = 2; i <= m+1; i++) fact[i] = fact[i-1] * i % MOD;
    invFact[m+1] = inv(fact[m+1], MOD);
    for (int i = m; i >= 0; i--) invFact[i] = invFact[i+1] * ll(i+1) % MOD;
    ll dp[m+5]; memset(dp, 0, sizeof(dp));

    for (int i = 0; i <= duoCnt; i++) {
        // choose duo
        int colors = duoCnt * 2 - i;
        ll cur = nCr(duoCnt, i) * fact[m-n] % MOD * invFact[m-n-colors] % MOD;
        dp[m-n-colors] = ans * cur % MOD;
    }
    ans = 0;
    for (int i = trioCnt * 3; i <= m; i++) {
        for (int j = 0; j <= trioCnt; j++) {
            int colors = trioCnt*3-j*2;
            ll cur = dp[i] * nCr(trioCnt, j) % MOD * fact[i] % MOD * invFact[i-colors] % MOD;
            ans = (ans + cur) % MOD;
        }
    }
    cout << ans << '\n';
}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("0.in", "r", stdin);
    freopen("0.out", "w", stdout);
#endif
    _upgrade
    int T = 1;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














