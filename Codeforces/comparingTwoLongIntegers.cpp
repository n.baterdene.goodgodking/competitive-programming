#include <bits/stdc++.h>
using namespace std;

int main () {
    string a, b;
    cin >> a >> b;
    int a1 = 0, b1 = 0;
    while(a1 < a.size() && a[a1] == '0') a1++;
    while(b1 < b.size() && b[b1] == '0') b1++;
    // cout << a << ' ' << b << '\n';
    a = a.substr(a1);
    b = b.substr(b1);
    // cout << a << ' ' << b << '\n';
    if (a == b) {
        cout << "=\n";
        return 0;
    }
    if (a.size() != b.size()) {
        cout << (a.size() > b.size() ? ">\n" : "<\n" );
        return 0;
    }
    for (int i = 0; i < a.size(); i++) {
        if (a[i] != b[i]) {
            cout << (a[i] > b[i] ? ">\n" : "<\n" );
            return 0;
        }
    }
    return 0;
}
