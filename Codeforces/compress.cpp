#include <iostream>
#include <vector>
#include <unordered_map>
#include <set>
#include <map>
using namespace std;
int main () {
    unordered_map <int, int> value;
    vector <int> sorted(1000000, 0);
    vector <int> numbers, ans;
    int n, tmp;
    cin >> n;
    for(int i = 0; i < n; i++) {
        cin >> tmp;
        numbers.push_back(tmp);
        sorted[tmp]++;
    }
    int idx = 1;
    for (int i = 0; i < 1000000; i++) {
        if(sorted[i] != 0) {
            value[i] = idx;
            idx++;
        }
    }
    cout << '\n';
    for(int i = 0; i < n; i++) 
        ans.push_back(value[numbers[i]]);

    for(int i = 0; i < n; i++) 
        cout << ans[i] << ' ';
    cout << '\n';
    return 0;
}