#include <iostream>
#include <vector>
#include <unordered_map>
#include <set>
#include <map>
using namespace std;
int main () {
    unordered_map <int, int> value;
    
    set <int> sorted;
    set <int> :: iterator it;

    vector <int> numbers, ans;
    
    int n, tmp;
    cin >> n;

    for(int i = 0; i < n; i++) {
        cin >> tmp;
        numbers.push_back(tmp);
        sorted.insert(tmp);
    }
    int idx = 1;

    for(it = sorted.begin(); it != sorted.end(); it++) {
        value[*it] = idx;
        idx++;
    }

    for(int i = 0; i < n; i++) 
        ans.push_back(value[numbers[i]]);

    for(int i = 0; i < n; i++) 
        cout << ans[i] << ' ';
    cout << '\n';
    return 0;
}