#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
using namespace std;
string num, curAns, first;
long long number;
vector <bool> used(10, false);
vector <string> ans;
bool No() {
    if (number >= 987654321) return true;
    return false;
}
void reset() {
    for (int i = 1; i < 10; i++) used[i] = false;
}
void update() {
    vector <bool> count(10, false);
    for (int i = 0; i < num.size(); i++) {
        while (count[num[i] - '0'] || num[i] == '0') {
            num[i]++;
            for (int j = i + 1; j < num.size(); j++) 
                num[j] = '1';
        }
        count[num[i] - '0'] = true;
    }

    for (int j = num.size() - 1; j >= 0; j--) {
        if (num[j] >= ':') {
            num[j] = '1';
            num[j - 1]++;
        } 
    }
    vector <bool> count1(10, false);
    for (int i = 0; i < num.size(); i++) {
        while (count1[num[i] - '0'] || num[i] == '0') {
            num[i]++;
        }
        count1[num[i] - '0'] = true;
    }
}
bool UP() {
    int len = num.size();
    string tmp = "";
    char cur = '9';
    for (int i = 0; i < len; i++) {
        tmp += cur;
        cur--;
    }
    if (tmp > num) return false;
    tmp = "";
    cur = '1';
    for (int i = 0; i <= len; i++) {
        tmp += cur;
        cur++;
    }
    ans.pb(tmp);
    return true;
}
void go() {
    reset();
    curAns = "";
    cin >> number;
    number++;
    num = to_string(number);
    first = to_string(number);
    if (No()) {
        ans.pb("0");
        return;
    }
    if (UP()) return;
    update();
    return ans.pb(num);
}
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int T;
    cin >> T;
    while(T--) go();
    for (int i = 0; i < ans.size(); i++) 
        cout << ans[i] << ' ';

    cout << '\n';
	return 0;
}