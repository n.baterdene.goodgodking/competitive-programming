#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
using namespace std;
vector <bool> chars (10, false);
bool check(string str) {
    for (int i = 0; i < str.size(); i++) 
        if(chars[str[i]]) return true;
    return false;
}
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int times;
    cin >> times;
    string key = to_string(times), str = "";
    for (int i = 0; i < key.size(); i++) 
        key[i - '0'] = true;
    
    int i, ans = 0;
    int idx = 0;
    for (i = 1; i < INT_MAX; i++) {
        string tmp = to_string(i);
        str += tmp;
        if(check(tmp)) {
            ans += str.size();
            str = "";
            continue;
        }
        int cur = str.find(key);
        if (cur != -1) {
            str = str.substr(cur + 1);
            times--;
            ans += (cur + 1);
        } else {
            if (str.size() > 7) {
                int size1 = str.size();
                // cout << str << ' ' << ans << '\n';
                str = str.substr(str.size() - 7);
                ans += (size1 - str.size());
                // cout << str << ' ' << ans << '\n';
            }
        }
        if (times == 0) break;
    }
    cout << ans << '\n';
	return 0;
}