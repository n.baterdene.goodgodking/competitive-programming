#include <bits/stdc++.h>
using namespace std;

class book {
private:
    void debug() {
        cout << name << ' ' << author << ' ' << released_date << ' ' << copies << ' ' << size << '\n';
    }
public: 
    string name, author, released_date;
    int copies, size;
    void showDetails() {
        cout << "----------\n"; 
        cout << "name : " << name << '\n';
        cout << "author : " << author << '\n';
        cout << "released int : " << released_date << '\n';
        cout << "number of copies printed : " << copies << '\n';
        cout << "number of pages : " << size << '\n';
        debug();
    }
    void init() {
        cout << "----------\n"; 
        cout << "enter the name of the book: ";
        cin >> name;
        cout << "enter the author of the book: ";
        cin >> author;
        cout << "enter the released date of the book: ";
        cin >> released_date;
        cout << "enter the copies of the book: ";
        cin >> copies;
        cout << "enter the number of pages of the book: ";
        cin >> size;
    }
};


int main () {
    book a;
    a.init();
    a.showDetails();
    // will not work: --> a.debug();
    // a.debug();
    return 0;
}
