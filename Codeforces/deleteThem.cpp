#include <bits/stdc++.h>
using namespace std;

bool check(string&a, string&b) {
    if (a.size() != b.size()) return 0;
    for (int i = 0; i < a.size(); i++) {
        if (a[i] != b[i] && b[i] != '?') return 0;
    }
    return 1;
}

int main () {
    int n, m, k;
    cin >> n >> m; 
    vector <string> str(n), in, out;
    vector <bool> used(n);
    for (int i = 0; i < n;i++) {
        cin >> str[i];
    }
    for (int i = 0; i < m; i++) {
        int idx;
        cin >> idx;
        used[idx - 1] = 1;
    }
    for (int i = 0; i < n; i++) {
        if (used[i]) in.push_back(str[i]);
        else out.push_back(str[i]);
    }
    k = in[0].size();
    for (int i = 0; i < m; i++) {
        if (in[i].size() != k) {
            cout << "No\n";
            return 0;
        }
    }
    int maxSize = 0;
    vector <set < char> > cnts(105);
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < in[i].size(); j++) {
            cnts[j].insert(in[i][j]);
        }
    }   
    // for (auto el : cnts) {
    //     for (auto el1: el) cout << el1 << ' ';
    //     cout << '\n';
    // }
    string res = "";
    for (int i = 0; i < 105; i++) {
        if (cnts[i].size() == 0) break;
        if (cnts[i].size() > 1) res += '?';
        else res += *cnts[i].begin();
    }
    // cout << res << '\n';
    for (int i = 0; i < out.size(); i++) {
        if (check(out[i], res)) {
            cout << "No\n";
            return 0;
        }
    }
    cout << "Yes\n" << res << '\n';
    return 0;
}
