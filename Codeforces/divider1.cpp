#include <bits/stdc++.h>
using namespace std;
 
long long x;
const int N = 1 << 15;
bool valid[10];
long long dp[11][N][2][2];
int digit[11];
int pw[11];
long long get(int pos, int rem, bool close, bool zero){
    if (pos == 11) return rem == 0 && zero == false;
    long long& ans = dp[pos][rem][close][zero];
    if (ans != -1) return ans;
    ans = 0;
    int to = 9;
    if (close) to = digit[pos];
    for(int here = 0; here < to; ++here) {
        if (valid[here] || (here == 0 && zero == true)){
            ans += get(pos + 1, (rem + here * pw[10 - pos]) % x, close & (here == digit[pos]), zero & (here == 0));
        }
    }
   
    return ans;
}
 
long long f(long long r){

    memset(dp, -1, sizeof dp);
    for(int i = 0; i < 11; ++i){
        digit[10 - i] = r % 10;
        r /= 10;
    }
 
    pw[0] = 1 % x;
    for(int i = 1; i < 11; ++i){
        pw[i] = pw[i - 1] * 10 % x;
    }
   
    return get(0, 0, true, true);
}
 
int main() {
    long long a, b;
    string s;
    cin >> x >> a >> b >> s;
    for(auto i: s) valid[i - '0'] = true;

    if (x >= N) {
        int ans = 0;
        long long start = (a / x) * x;
        if (start < a) start += x;
        while (start <= b) {
            bool ok = true;
            long long t = start;
            while(t > 0){
                ok &= valid[t % 10];
                t /= 10;
            }
            ans += ok;
            start += x;        
        }
        cout << ans << endl;
    } else {
        cout << f(b) - f(a - 1) << endl;
    }
   
    return 0;
}