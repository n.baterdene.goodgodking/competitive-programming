#include <bits/stdc++.h>
#define int long long 
using namespace std;
const int MOD = 1e9 + 7; // equal 10^9 + 7

// function for finding a^b under mod p
int pow(int a, int b, int p) { 
    int res = 1, cur = 1LL;
    while(cur <= b) {
        if (b & cur) res = res * a % p;
        a = (a * a) % p;
        cur <<= 1;
    }
    return res;
}

// function for finding inverse
int modInv(int a, int p) { 
    return pow(a, p - 2, p);
}

// function for finding sum: 1 + 2 + ... n under modu p
int findSum(int n, int p) {
    return (n & 1 ? (n + 1) / 2 * n : n / 2 * (n + 1)) % p;
}
signed main () { // use signed main to make (#define int long long) work
    int n, x, k, sum = 1, mul = 1, countDivs = 1, countDivs1 = 1;
    cin >> n;
    for (int i = 0; i < n; i++) {
        cin >> x >> k;
        //calculating number of divisors
        countDivs = countDivs * (k+1) % MOD;

        //calculating sum of divisors
        // new_sum equals = old_sum * (x_i^(k_i + 1) - 1) / (x_i - 1)
        int A = pow(x, k+1, MOD) - 1, B = modInv(x-1, MOD);
        /*
        A / B == A * B^(-1)
        A = (x_i^(k_i + 1) - 1)
        B = (x_i - 1)^(-1)
        */
        sum = sum * A % MOD * B % MOD;
        // calculating product of divisors
        /*
        new_mul = old_mul * [old_mul * x_i^(old_countDivs * 1)] * [old_mul * x_i^(old_countDivs * 2)] * ... * [old_mul * x_i^(old_countDivs * k_i)]
        new_mul = old_mul * old_mul * ... * old_mul * x^[(1 + 2 + ... + k_i) * old_countDivs]
        new_mul = old_mul^(k_i+1) * x_i^[k_i*(k_i+1) * old_countDivs / 2]
        */
        int times2 = findSum(k, MOD-1) * countDivs1 % (MOD-1);
        /*
        this time we have to use MOD-1 because if gcd(a, b) == 1 => a^(p-1) % p == 1
        since 10^9+7 is prime, x and MOD are coprime
        old_mul^times2 == old_mul^(k_i+1) * x_i^[k_i*(k_i+1) * old_countDivs / 2]
        */
        mul = pow(x, times2, MOD) * pow(mul, k+1, MOD) % MOD;
        // mul 
        countDivs1 = countDivs1 * (k+1) % (MOD-1);
        // we have to use countDivs1 separately from countDivs because the difference in mod
    }
    cout << countDivs << ' ' << sum << ' ' << mul << '\n';
}