#include <bits/stdc++.h>
using namespace std;

int main () {
    int t = 1;
    cin >> t;
    while(t--) {
        int n, sum = 0, num;
        cin >> n;
        for (int i = 0; i < n; i++) {
            cin >> num;
            sum += num;
        }
        cout << sum / n + (sum % n != 0) << '\n';
    }
    return 0;
}
