
#include <vector>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))

#define _upgrade ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
int main () {
    _upgrade
    int a, b;
    while(cin >> a >> b) {
        if (a + b == 0) break; 
        bool idx = 1;
        while(1) {
            if (a < b) swap(a, b);
            if (a % b == 0  || a - b > b) break;
            idx = !idx;
            a -= b;
        }
        if (idx) cout << "Stan wins\n";
        else cout << "Ollie wins\n";
    }

    return 0;
}