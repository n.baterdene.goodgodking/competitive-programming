#include <bits/stdc++.h>
using namespace std;

bool check(long long num) {
    long long tmp = num;
    while(tmp > 0) {
        long long digit = tmp % 10;
        if (digit > 0 && num % digit != 0) return 0; 
        tmp /= 10;
    }
    return 1; 
}

int main () {
    int t = 1;
    cin >> t;
    while(t--) {
        long long n;
        cin >> n;
        for (long long i = n; i < n + 2520 ; i++) 
        if (check(i)) {
            cout << i << '\n';
            break;
        }
    }
    return 0;
}
