#include <bits/stdc++.h>
using namespace std;

int main () {
    int t = 1;
    cin >> t;
    while(t--) {
        vector <int> nums(3);
        for (int i = 0; i < 3; i++) cin >> nums[i];
        sort(nums.begin(), nums.end());
        cout << nums[2] << '\n';
    }
    return 0;
}
