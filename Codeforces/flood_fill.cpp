#include <iostream>
#include <vector>
using namespace std;
vector < vector <char> > husnegt;
int shalgasan, n;
void flood_fill(int i, int j){
	if(j < n - 1 && husnegt[i][j + 1] == "#"){
		husnegt[i][j + 1] = ".";
		flood_fill(i, j + 1);
	}
	if(i < n - 1 && husnegt[i + 1][j] == "#"){
		husnegt[i + 1][j] = ".";
		flood_fill(i + 1, j);
	}
	if(j > 0 && husnegt[i][j - 1] == "#"){
		husnegt[i][j - 1] = ".";
		flood_fill(i, j - 1);
	}
	if(i > 0 && husnegt[i - 1][j] == "#"){
		husnegt[i - 1][j] = ".";
		flood_fill(i - 1, j);
	}
	return;
}
int main() {
	int num;
	cin >> n;
	for(int i = 0; i < n; i++){
		vector <char> tmp;
		for(int j = 0; j < n; j++){
			cin >> num;
			tmp.push_back(num);
		}
		husnegt.push_back(tmp);
	}
	int times = 0;
	for(int i = 0; i < n; i ++){
		for(int j = 0; j < n; j++){
			if(husnegt[i][j] == '#'){
				flood_fill(i, j);
				times++;
			}
		}
	}
	cout << times << '\n';
	return 0;
}
