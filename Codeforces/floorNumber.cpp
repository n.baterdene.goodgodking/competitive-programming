#include <bits/stdc++.h>
using namespace std;
int main () {
    int t = 1, n, m;
    cin >> t;
    while(t--) {
        cin >> n >> m;
        cout << 1 + ((n - 2 >= 0) ? (n - 2 + m - 1) / m : 0) << '\n';
    }
    return 0;
}