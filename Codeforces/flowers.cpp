#include <bits/stdc++.h>
using namespace std;
// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))

#define _upgrade ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define FOR(i, a, b) for (int i = a; i < b; i++)
#define FORB(i, a, b) for (int i = a - 1; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define umap unordered_map
#define pb push_back
#define mp make_pair
#define ff first
#define ss second
#define PQ priority_queue
#define Q queue

typedef long long ll;
typedef double d;
typedef unsigned uint;
typedef unsigned long long ull;
typedef unordered_map<int, int> umapII;
// PQ going up <int, VI, greater<int> >
typedef vector<int> VI;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<bool> VB;
typedef vector<umapII> VumapII;
typedef vector<ll> VLL;
typedef vector<VLL> VVLL;
typedef vector<d> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VPI> VVPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
 

VI readVI(int n) {
    VI a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVI readVVI(int n, int m) {
    VVI a(n, VI(m));
    REP(i, n) a[i] = readVI(m);
    return a;
}
VLL readVLL(ll n) {
    VLL a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVLL readVVLL(ll n, ll m) {
    VVLL a(n, VLL(m));
    REP(i, n) a[i] = readVLL(m);
    return a;
}


// const int MOD = int(1e9) + 7;
// const int INF = INT_MAX;
// const ll INFF = INT64_MAX;
// const d EPS = 1e-9;
// const d PI = acos(-1.0); //M_PI;
// const int moveX[] = {-1, 0, 1, 0};
// const int moveY[] = {0, 1, 0, -1};


struct node { 
    int first; 
    int second; 
}; 
struct node query(struct node *st, int l, int r, int p, int q, int index) { 
	struct node tmp,left,right; 
	if (p <= l && q >= r) return st[index]; 
	if (r < p || l > q) { 
        tmp.ff = INT_MAX; 
        tmp.ss = INT_MIN; 
        return tmp; 
	} 
	int mid = (l + r)>>1;
	left = query(st, l, mid, p, q, (index<<1)+1); 
	right = query(st, mid+1, r, p, q, (index+1)<<1); 
	tmp.ff = min(left.first, right.first); 
	tmp.ss = max(left.ss, right.ss); 
	return tmp; 
} 

struct node MaxMin(struct node *st, int n, int l, int r) { 
	struct node tmp; 
	if (l < 0 || r > n-1 || l > r) { 
		tmp.ff = INT_MIN; 
		tmp.ff = INT_MAX; 
		return tmp; 
	} 
	return query(st, 0, n-1, l, r, 0); 
} 

void build(VI& arr, int l, int r, struct node *st, int si) { 
	if (l == r) { 
		st[si].ff = arr[l]; 
		st[si].ss = arr[l]; 
		return ; 
	} 
    int mid = (l + r)>>1;
	build(arr, l, mid, st, (si<<1)+1); 
	build(arr, mid+1, r, st, (si+1)<<1); 
	st[si].ff = min(st[(si<<1) + 1].first, st[(si+1)<<1].first); 
	st[si].ss = max(st[(si<<1) + 1].ss, st[(si+1)<<1].ss); 
} 

struct node *init(VI&arr, int n) { 
	int x = (ceil(log2(n))); 
	int max_size = int(pow(2, x) - 1)<<1; 
	struct node *st = new struct node[max_size]; 
	build(arr, 0, n-1, st, 0); 
	return st; 
} 


int main() { 
    _upgrade
    // freopen("in.txt", "r", stdin);
    int n, T, a, b;
    // scanf("%d %d", &n, &T);
    cin >> n >> T;
    VI arr = readVI(n);
	struct node *st = init(arr, n); 
    while(T--) {
        // scanf("%d %d", &a, &b);
        cin >> a >> b;
        struct node result = MaxMin(st, n, a - 1, b - 1); 
        cout << result.ss - result.ff << '\n';
        // printf("%d\n", result.ss - result.ff);
    }

	return 0; 
} 
