
// compile and run 
// shell_cmd": "g++-10 \"${file}\" -std=c++17 && \"./a.out\"" || 

// combinatorics

VI fact(N), invFact(N);
int pow(int a, int b, int p) {
    int cur = 1, res = 1;
    a %= p;
    while(cur <= b) {
        if (cur&b) res = res * a % p;
        a = a * a % p;
        cur<<=1;
    }
    return res;
}
int inv(int a, int p) {
    return pow(a, p - 2, p);
}
int choose(int a, int b) {
    return fact[a] * invFact[b] % MOD * invFact[a-b] % MOD;
}
void init() {
    fact[0] = 1;
    REP1(i, N) fact[i] = fact[i-1]*i%MOD;
    invFact[N-1] = inv(fact[N-1], MOD);
    for (int i = N - 2; i >= 0; i--) invFact[i] = invFact[i+1] * (i+1)%MOD;
}
//convex hull

class ConvexHull {
private:
    int n;
    VPI points; // x, y;
    VPI lowerHalf, upperHalf;
    PII lowPoint, highPoint;
    static bool compBySlope(VI&a, VI&b) {
        if ((a[0] * b[1] == a[1] * b[0])) return (a[0] == b[0] ? a[1] < b[1] : a[0] < b[0]);
        return (a[0] * b[1] < a[1] * b[0]);
    };
    bool isOnLine(PII a, PII b, PII c) { // c in (a, b)
        VPI tmp = {a, b, c};
        sort(ALL(tmp));
        a = tmp[0], b = tmp[1], c = tmp[2];
        b = {a.ss - b.ss, a.ff - b.ff};
        c = {a.ss - c.ss, a.ff - c.ff};
        return (b.ff * c.ss == c.ff * b.ss);
    }
    void findLowestAndHighestPoints() {
        int id = 0, id1 = 0;
        for (int i = 1; i < n; i++) {
            PII pt = points[i];
            if (pt < points[id]) id = i;
            if (pt > points[id1]) id1 = i;
        }
        lowPoint = points[id];
        highPoint = points[id1];
    }
    void dividePoints() {
        VPI line = {lowPoint, highPoint};
        long long X = highPoint.ss - lowPoint.ss, Y = highPoint.ff - lowPoint.ff;
        for (auto &pt : points) {
            if (pt == highPoint || pt == lowPoint) continue;
            long long x = pt.ss - lowPoint.ss, y = pt.ff - lowPoint.ff;
            if (y * X >= Y * x) upperHalf.pb(pt);
            else lowerHalf.pb(pt);
        }
        lowerHalf.pb(lowPoint);
        upperHalf.pb(highPoint);
        swap(lowerHalf[0], lowerHalf.back());
        swap(upperHalf[0], upperHalf.back());
    }
    void sortPoints() {
        VVI lowerPoints, upperPoints; // (k = A, B), x, y
        for (auto &pt : lowerHalf) {
            int x = pt.ss - lowPoint.ss, y = pt.ff - lowPoint.ff; // slope: y / x
            lowerPoints.pb({y, x, pt.ff, pt.ss});
        }
        sort(ALL(lowerPoints), compBySlope);
        for (auto &pt : upperHalf) {
            int x = highPoint.ss - pt.ss, y = highPoint.ff - pt.ff; // slope: y / x
            upperPoints.pb({y, x, pt.ff, pt.ss});
        }
        sort(ALL(upperPoints), compBySlope);
        int id = 0;
        for (auto &pt : lowerPoints) points[id++] = {pt[2], pt[3]};
        for (auto &pt : upperPoints) points[id++] = {pt[2], pt[3]};
    }
    void buildConvexHull() {
        PII st = points[0], end = points.back(), cur = points[1], last;
        convexHull.pb(st);
        convexHull.pb(cur);
        points.pb(st);
        auto cmp = [&](PII& pt) -> bool {
            int sz = convexHull.size();
            PII x = convexHull[sz-1];
            PII y = convexHull[sz-2];
            if (isOnLine(x, y, pt)) {
                return 0;
            }
            x = {pt.ss - x.ss, pt.ff - x.ff};
            y = {pt.ss - y.ss, pt.ff - y.ff};
            swap(x, y);
            return (x.ff * y.ss < x.ss * y.ff);
        };
        for (int i = 2; i <= n; i++) {
            cur = points[i];
            while(cmp(cur)) 
                convexHull.pop_back();
            convexHull.pb(cur);
        }
        points.pop_back();
        convexHull.pop_back();
    }
public:
    VPI convexHull;
    ConvexHull(VPI& _points) {
        points = _points;
        n = points.size();
        findLowestAndHighestPoints();
        dividePoints();
        sortPoints();
        buildConvexHull();
    }
};

// pbds
#include <bits/stdc++.h>
using namespace std;

#include <bits/extc++.h>                         // pbds
using namespace __gnu_pbds;
typedef tree<int, null_type, less_equal<int>, rb_tree_tag, tree_order_statistics_node_update> ordered_set;
//To convert it into pairs use pair<int,int> instead of int and also make less_equal<pair<int,int>> 
template <typename T>
using ordered_multiset = tree<T, null_type, less_equal<T>, rb_tree_tag, tree_order_statistics_node_update>;
// To use ordered use ordered_multiset<type>S something like this

int main() {
  int n = 9;
  int A[] = { 2, 4, 7,10,15,23,50,65,71};        // as in Chapter 2
  ordered_multiset tree;
  for (int i = 0; i < n; ++i)                    // O(n log n)
    tree.insert(A[i]);
  // O(log n) select
  cout << *tree.find_by_order(0) << "\n";        // 1-smallest = 2
  cout << *tree.find_by_order(n-1) << "\n";      // 9-smallest/largest = 71
  cout << *tree.find_by_order(4) << "\n";        // 5-smallest = 15
  // O(log n) rank
  cout << tree.order_of_key(2) << "\n";          // index 0 (rank 1)
  cout << tree.order_of_key(71) << "\n";         // index 8 (rank 9)
  cout << tree.order_of_key(15) << "\n";         // index 4 (rank 5)
  return 0;
}


// suffix array 

namespace RadixSort {
    template <typename T>
    void sort(vector<T> &arr, const int max_element, int (*get_key)(T &), int begin = 0) {
        const int n = arr.size();
        vector<T> new_order(n);
        vector<int> count(max_element + 1, 0);

        for (int i = begin; i < n; i++)
            count[get_key(arr[i])]++;

        for (int i = 1; i <= max_element; i++)
            count[i] += count[i - 1];

        for (int i = n - 1; i >= begin; i--) {
            new_order[count[get_key(arr[i])] - (begin == 0)] = arr[i];
            count[get_key(arr[i])]--;
        }

        arr.swap(new_order);
    }

    template <typename T> void sort_pairs(vector<T> &arr, const int rank_size) {
    RadixSort::sort<T>(arr, rank_size, [](T &item) { return item.first.second; }, 0ll);
    RadixSort::sort<T>(arr, rank_size, [](T &item) { return item.first.first; }, 0ll);
    }
}
class Suffix_Array {
private:
    string s;
    int n;
    typedef pair<int, int> Rank;
    void build_ranks(const vector<pair<Rank, int>> &ranks, vector<int> &ret) {
        ret[ranks[0].second] = 1;
        for (int i = 1; i < n; i++) {
            if (ranks[i - 1].first == ranks[i].first)
                ret[ranks[i].second] = ret[ranks[i - 1].second];
            else
                ret[ranks[i].second] = ret[ranks[i - 1].second] + 1;
        }
    }
    vector<int> build_suffix_array() {
        vector<pair<Rank, int>> ranks(this->n);
        vector<int> arr(this->n);

        for (int i = 0; i < n; i++)
        ranks[i] = pair<Rank, int>(Rank(s[i], 0), i);

        RadixSort::sort_pairs(ranks, 256);
        build_ranks(ranks, arr);

        {
        int jump = 1;
        int max_rank = arr[ranks.back().second];
        while (max_rank != this->n) {
            for (int i = 0; i < this->n; i++) {
            ranks[i].first.first = arr[i];
            ranks[i].first.second = (i + jump < this->n ? arr[i + jump] : 0);
            ranks[i].second = i;
            }

            RadixSort::sort_pairs(ranks, n);
            build_ranks(ranks, arr);

            max_rank = arr[ranks.back().second];
            jump *= 2;
        }
        }

        vector<int> sa(this->n);
        for (int i = 0; i < this->n; i++)
        sa[arr[i] - 1] = i;
        return sa;
    }
    vector<int> build_lcp() {
        lcp.resize(n, 0);
        vector<int> inverse_suffix(this->n);

        for (int i = 0; i < this->n; i++)
        inverse_suffix[sa[i]] = i;

        int k = 0;

        for (int i = 0; i < this->n; i++) {
        if (inverse_suffix[i] == this->n - 1) {
            k = 0;
            continue;
        }

        int j = sa[inverse_suffix[i] + 1];

        while (i + k < this->n && j + k < this->n && s[i + k] == s[j + k])
            k++;

        lcp[inverse_suffix[i]] = k;

        if (k > 0)
            k--;
        }

        return lcp;
    }
public:
    vector<int> sa;
    vector<int> lcp;
    Suffix_Array(string &s) {
        this->n = s.size();
        this->s = s;
        this->sa = build_suffix_array();
        this->lcp = build_lcp();
    }
};


// sos dp:

for(int i = 0; i<(1<<N); ++i)
    bitmask[i] = nums[i];

for(int i = 0;i < N; ++i) 
for(int mask = 0; mask < (1<<N); ++mask){
    if(mask & (1<<i))
        bitmask[mask] += bitmask[mask^(1<<i)];
}