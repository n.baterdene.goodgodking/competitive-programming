#include <bits/stdc++.h>
using namespace std;
int main () {
    int n = 5;
    double sum = 0;
    int nums[5];
    for (int i = 0; i < 5; i++) {
        cin >> nums[i];
        sum += nums[i];
    }
    sum /= 5;
    int ans = 0;
    for (int i = 0; i < 5; i++) {
        if (nums[i] <= sum) continue;
        ans += nums[i];
    }
    cout << ans << '\n';
}