#include <bits/stdc++.h>
using namespace std;
// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))

#define _upgrade ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define II <int, int>
#define Q queue
#define ff first
#define ss second
#define rs resize
#define ins insert 
#define fr front() 
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define PQ priority_queue
#define umap unordered_map
#define sz(x) (int)x.size()

typedef long long ll;
int bitCnt(ll a) {
    bitset <64> b(a);
    return b.count();
}
const int INF = 2e9;


const int N = 100;
const int M = 10;
int n, m;
vector <vector <int> > cows(N, vector <int>(M));
map <ll, vector <int>> dp;
int solve(ll mask) {
    if (dp.count(mask)) return dp[mask][0];
    if (bitCnt(mask) == 1) {
        dp[mask] = {0, 0, 0};
        return 0;
    }
    vector <int> ans(3, INF);
    for (int property = 0; property < m; property++) {
        for (int X = 0; X < 3; X++) {
            ll yesMask = 0, noMask = 0;
            for (int i = 0; i < n; i++)
                if ((1ll<<i)&mask) {
                    if (cows[i][property] == X) yesMask += (1ll<<i);
                    else noMask += (1ll<<i);
                }
            if (!yesMask || !noMask) continue;
            int res = max(solve(yesMask), solve(noMask))+1;
            if (res < ans[0]) {
                ans = {res, property, X};
            }
        }
    }
    dp[mask] = ans;
    return ans[0];
}
bool val;
void ask(int pos, int X) {
    cout << "Q " << pos+1 << ' ' << char(X+'X') << '\n';
    cin >> val;
}
void ans(ll mask) {
    int answer = 0;
    for (int i = 0; i < n; i++) {
        if ((1ll<<i)&mask) answer = i+1;
    }
    cout << "C " << answer << '\n';
}

signed main () {
    cin >> n >> m;
    for (int i = 0; i < n; i++) {
        char a;
        for (int j = 0; j < m; j++) {
            cin >> a;
            cows[i][j] = a - 'X';
        }
    }
    ll mask = (1ll<<n)-1, pos, X;
    vector <int> tmp;
    solve(mask);
    while(bitCnt(mask) > 1) {
        tmp = dp[mask];
        pos = tmp[1], X = tmp[2];
        ask(pos, X);
        for (int i = 0; i < n; i++)
            if (mask & (1ll << i)) {
                if ((cows[i][pos] == X)^val) mask-=(1ll<<i);
            }
    }
    ans(mask);
    return 0;
}
