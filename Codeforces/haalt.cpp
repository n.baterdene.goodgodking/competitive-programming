#include <iostream>
#include <map>
#define mp make_pair
using namespace std;
map <pair <int, int> , int> seen;

int ans(int n, int in) {
    if (n == 0) return 1;
    if (seen.find(mp(n, in)) != seen.end()) 
        return seen[mp(n, in)];

    if (in == 0) return ans(n - 1, 1);
    int answer = 0;
    for (int i = 0; i < in; i++) 
        answer += ans(n - 1, in - i);
    return seen[mp(n, in)] = (ans(n - 1, in + 1) + answer);
}
int main () {
    int n; 
    cin >> n;
    if (n % 2 == 1) cout << 0 << '\n';
    else cout << ans(n / 2, 0) << '\n';
    return 0;
}