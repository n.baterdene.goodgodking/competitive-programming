#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int num, n, q;
vector <int> a;

void heap_up () { 
    int l;
    l = a.size();
    a.push_back(num);
    while(l != 0 && a[l] < a[(l - 1) / 2]){
        swap(a[l],a[(l - 1) / 2]);
        l = (l - 1) / 2;
    }
}
void heap_down() {
    cout << a[0] << ' ';
    n = a.size();
    swap(a[0], a[n-1]);
    a.pop_back();
    int i = 0;
    bool swapped = 0;
    while(swapped == 0){
        swapped = 1;
        if(a[i] > a[i * 2 + 1] && i * 2 + 1 < a.size() && a[i * 2 + 1] <= a[i * 2 + 2]){
            swap(a[i * 2 + 1], a[i]);     
            i = i * 2 + 1;
            swapped = 0;
        } else {
            if(a[i] > a[i * 2 + 2] && i * 2 + 2 < a.size() && a[i * 2 + 1] > a[i * 2 + 2]){
                swap(a[i * 2 + 2], a[i]); 
                i = i * 2 + 2;
                swapped = 0;
            }
        }
        if(i > n)
            break;
    }

}
int main () {
    cin >> n;
    for(int i = 0; i < n; i++){
        cin >> num;
        heap_up();
    }
    while(a.size() > 0){
        heap_down();
    }
    cout << "\n";
    return 0;
}