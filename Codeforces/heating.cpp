#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define int long long
#define ff first
#define ss second
#define umap unordered_map
using namespace std;
void go() {
    int n, m;
    cin >> m >> n;
    int tmp = n / m;
    int cur = tmp * tmp;
    int rem = n % m;
    int ans = cur * (m - rem);
    ans += (tmp + 1) * (tmp + 1) * rem;
    cout << ans << '\n';
}
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int T;
    cin >> T;
    while(T--) go();
	return 0;
}