// C++ implementation of Hopcroft Karp algorithm for 
// maximum matching 
#include<bits/stdc++.h> 
using namespace std; 
#define INF INT_MAX 

// A class to represent Bipartite graph for Hopcroft 
// Karp implementation 
class BipGraph  { 
	int m, n; 
	list<int> *adj; 
	int *pairU, *pairV, *dist; 

public: 
	BipGraph(int m, int n) { 
        this->m = m; 
        this->n = n; 
        adj = new list<int>[m+1]; 
    } 
	void addEdge(int u, int v) { 
	adj[u].push_back(v); // Add u to v’s list. 
} 
	bool bfs() { 
        queue<int> Q; //an integer queue 

        // First layer of vertices (set distance as 0) 
        for (int u=1; u<=m; u++)  { 
            // If this is a free vertex, add it to queue 
            if (pairU[u]==0) { 
                // u is not matched 
                dist[u] = 0; 
                Q.push(u); 
            } 

            // Else set distance as infinite so that this vertex 
            // is considered next time 
            else dist[u] = INF; 
        } 

        // Initialize distance to 0 as infinite 
        dist[0] = INF; 

        // Q is going to contain vertices of left side only. 
        while (!Q.empty()) { 
            // Dequeue a vertex 
            int u = Q.front(); 
            Q.pop(); 

            // If this node is not 0 and can provide a shorter path to 0 
            if (dist[u] < dist[0]) { 
                // Get all adjacent vertices of the dequeued vertex u 
                list<int>::iterator i; 
                for (i=adj[u].begin(); i!=adj[u].end(); ++i) { 
                    int v = *i; 

                    // If pair of v is not considered so far 
                    // (v, pairV[V]) is not yet explored edge. 
                    if (dist[pairV[v]] == INF) 
                    { 
                        // Consider the pair and add it to queue 
                        dist[pairV[v]] = dist[u] + 1; 
                        Q.push(pairV[v]); 
                    } 
                } 
            } 
        } 

        // If we could come back to 0 using alternating path of distinct 
        // vertices then there is an augmenting path 
        return (dist[0] != INF); 
    } 

	bool dfs(int u) { 
        if (u != 0) { 
            list<int>::iterator i; 
            for (i=adj[u].begin(); i!=adj[u].end(); ++i) { 
                // Adjacent to u 
                int v = *i; 

                // Follow the distances set by BFS 
                if (dist[pairV[v]] == dist[u]+1) { 
                    // If dfs for pair of v also returns 
                    // true 
                    if (dfs(pairV[v]) == true) { 
                        pairV[v] = u; 
                        pairU[u] = v; 
                        return true; 
                    } 
                } 
            } 

            // If there is no augmenting path beginning with u. 
            dist[u] = INF; 
            return false; 
        } 
        return true; 
    } 

    // Constructor 

	int hopcroftKarp() { 
        pairU = new int[m+1]; 
        pairV = new int[n+1]; 
        dist = new int[m+1]; 

        for (int u=0; u<=m; u++) 
            pairU[u] = 0; 
        for (int v=0; v<=n; v++) 
            pairV[v] = 0; 

        int result = 0; 
        while (bfs()) { 
            for (int u=1; u<=m; u++) 
                if (pairU[u]==0 && dfs(u)) 
                    result++; 
        } 
        return result; 
    } 

}; 

// Driver Program 
int main()  { 
	BipGraph g(4, 4); 
	g.addEdge(1, 2); 
	g.addEdge(1, 3); 
	g.addEdge(2, 1); 
	g.addEdge(3, 2); 
	g.addEdge(4, 2); 
	g.addEdge(4, 4); 

	cout << "Size of maximum matching is " << g.hopcroftKarp(); 

	return 0; 
} 
