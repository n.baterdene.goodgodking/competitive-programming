#include <bits/stdc++.h>
using namespace std;

int main () {
    int n;
    string s;
    cin >> n >> s;
    vector <bool> empty(15, 1);
    for (int i = 0; i < n; i++) {
        if (s[i] == 'L') {
            for (int j = 0; j < 10; j++) 
                if (empty[j]) {
                    empty[j] = 0;
                    break;
                }

            continue;
        }
        if (s[i] == 'R') {
            for (int j = 9; j >= 0; j--) 
                if (empty[j]) {
                    empty[j] = 0;
                    break;
                }
            
            continue;
        } 
        empty[s[i] - '0'] = 1;
    }

    for (int i = 0; i < 10; i++) {
        cout << !empty[i];
    }
    cout << "\n";
    return 0;
}