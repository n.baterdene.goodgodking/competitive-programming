#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
#define umap unordered_map
using namespace std;
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int n, m, k;
    cin >> n >> m >> k;
    int tmp = k * 2;
    int time = 0;
    if (k == 0) {
        cout << 0 << '\n';
        return 0;
    }
    if (k < n) {
        time = m * 2;
    } else {
        while (tmp > 0) {
            tmp -= n;
            time += m;
        }
    }
    cout << time << '\n';
	return 0;
}