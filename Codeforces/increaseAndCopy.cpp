#include <bits/stdc++.h>
using namespace std;

int main () {
    int t = 1;
    cin >> t;
    while(t--) {
        int n;
        cin >> n;
        int sq = sqrt(n);
        cout << (sq - 2) + (n / sq) + (n % sq != 0) << '\n';
    }
    return 0;
}
