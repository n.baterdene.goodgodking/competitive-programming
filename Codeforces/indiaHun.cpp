#include <iostream>
#include <vector>
#include <unordered_set>
using namespace std;

vector<vector<int>> findPairsWithGivenDifference( const vector<int>& nums, int k) 
{
  // your code goes here
  vector <vector <int> > ans;
  int arraySize = nums.size();
  unordered_set<int> seen; // O(1)
  for (int currentIndex = 0; currentIndex < arraySize; currentIndex++) {
    int currentElement = nums[currentIndex];
    seen.insert(currentElement);
  }

  // Marking each number as seen
  for (int currentIndex = 0; currentIndex < arraySize; currentIndex++) {
    int x = nums[currentIndex];
    if (seen.find(x + k) != seen.end()) {
      ans.push_back({x + k, x});
    }
  }
  return ans;
}
int main() {
  return 0;
}
/*
 all pairs [x,y] 
 
 [1, 2, 3, 4,5 .....] k = 0
my answer: [1, 1], [2, 2] ...
expected : empty
 
 
[4,1], 3
Expected:
 
[[4,1]]
Actual:
 
[[1,-2]]

 seen:  4 1
 1 -> x - 3
[]
*/


/*
arr = [0, -1, -2, 2, 1], k = 1
output: [[1, 0], [0, -1], [-1, -2], [2, 1]] 

[[0, -1], [-1, -2], [1, 0], [2, 1]]

arr = [1, 7, 5, 3, 32, 17, 12], k = 17
output: []
*/