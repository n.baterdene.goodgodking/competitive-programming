#include <bits/stdc++.h>
using namespace std;

int main () {
    int n, num;
    const int N = 100005;
    cin >> n;
    vector <vector <int> > positions(N);
    vector <bool> exist(N, false);
    for (int i = 0; i < n; i++) {
        cin >> num;
        positions[num].push_back(i);
        exist[num] = true;
    }
    vector <vector <int> > answers; //
    for (int i = 0; i < N; i++) {
        if (exist[i]) {
            bool isAnswer = true;
            if (positions[i].size() == 1) {
                answers.push_back({i, 0});
                continue;
            }
            int dif = positions[i][1] - positions[i][0];
            for (int j = 0; j < positions[i].size() - 1; j++) {
                if (positions[i][j + 1] - positions[i][j] != dif) {
                    isAnswer = false;
                    break;
                }
            }
            if (isAnswer) {
                answers.push_back({i, dif});
            }
        }
    }
    cout << answers.size() << "\n";
    for (auto el : answers) {
        cout << el[0] << ' ' << el[1]  << '\n';
    }
    return 0;
}
