#include<bits/stdc++.h>
using namespace std;

void computeLPSArray(string b, int m, vector <int>& lps) {
    int len = 0;
    lps[0] = 0;
    int i = 1;
    while (i < m) {
        if (b[i] == b[len]) {
            len++;
            lps[i] = len;
            i++;
        } else {
            if (len != 0) {
                len = lps[len-1];
            } else {
                lps[i] = 0;
                i++;
            }
        }
    }
}
void KMPSearch(string b, string a) {
    int m = b.size();
    int n = a.size();
    vector <int> lps(m);
    computeLPSArray(b, m, lps);
 
    int i = 0;  
    int j = 0;  
    while (i < n) {
        if (b[j] == a[i]) {
            j++;
            i++;
        }
        if (j == m) {
            cout << "Found pattern at index " << i - j << '\n';
            j = lps[j-1];
        } else if (i < n && b[j] != a[i]) {
            if (j != 0) j = lps[j-1];
            else i = i+1;
        }
    }
}
 
 
// Driver program to test above function
int main() {
    string a, b;
    cin >> a >> b;
   
    KMPSearch(b, a);
    return 0;
}
