#include <bits/stdc++.h>
using namespace std;

int main () {
    long long n, m, sum = 0, mini = INT_MAX;
    cin >> n >> m;
    vector <long long> nums(n);
    for (int i = 0; i < n; i++) {
        cin >> nums[i];
        sum += nums[i];
        mini = min(mini, nums[i]);
    }
    if (sum < m) {
        cout << "-1\n";
        return;
    }
    cout << min(mini, (sum - m) / n) << '\n';
    return 0;
}
