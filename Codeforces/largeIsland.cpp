class Solution {
public:
    int size1, size2, len, type = 1;
    vector <vector <int>> islands;
    void myFunc(int i, int j, vector < vector < int > >&gridcopy){
        gridcopy[i][j] = -1;
        islands[i][j] = type;
        if(i < 0 && gridcopy[i - 1][j] == 1){
            len++;
            myFunc(i - 1, j, gridcopy);
        }
        if(i < size1 - 1 && gridcopy[i + 1][j] == 1){
            len++;
            myFunc(i + 1, j, gridcopy);
        }
        if(j < size2 - 1 && gridcopy[i][j + 1] == 1){
            len++;
            myFunc(i , j + 1, gridcopy);
        }
        if(j > 0 && gridcopy[i][j - 1] == 1){
            len++;
            myFunc(i , j - 1, gridcopy);
        }
    }
    int largestIsland(vector<vector<int>>& gridcopy) {
        size1 = gridcopy.size();
        size2 = gridcopy[0].size();
        islands = gridcopy;
        for(int i = 0; i < size1; i++) {
            for(int j = 0; j < size2; j++) {
                if(gridcopy[i][j] == 1){
                    type++;
                    len = 1;
                    myFunc(i , j, gridcopy);
                    for(int i1 = 0; i1 < size1; i1++) 
                        for(int j1 = 0; j1 < size2; j1++) 
                            if(gridcopy[i1][j1] == -1) gridcopy[i1][j1] = len;
                }
            }
        }
        for(int i = 0; i < size1; i++) {
            for(int j = 0; j < size2; j++) 
                cout << gridcopy[i][j] << ' ';
            cout << '\n';
        }
        int ans = 1;
        bool orson = false;
        for(int i = 0; i < size1; i++) {
            for(int j = 0; j < size2; j++) {
                int tmpAns = 1;
                vector <int> islandtypes;
                if(gridcopy[i][j] == 0) {
                    orson = true;
                    cout << "here is ans --> "<< tmpAns << '\n';
                    if(i > 0) {
                        cout << "orson1\n";
                        tmpAns += gridcopy[i - 1][j];
                        islandtypes.push_back(islands[i - 1][j]);
                    }
                    cout << "here is ans --> "<< tmpAns << '\n';
                    if(j < size2 - 1) {
                        cout << "orson2\n";
                        for(int id = 0; id < islandtypes.size(); id++){
                            if(islands[i][j + 1] == islandtypes[id]){
                                tmpAns -= gridcopy[i][j + 1];
                                break;
                            }
                        }
                        tmpAns += gridcopy[i][j];
                        islandtypes.push_back(islands[i][j + 1]);
                    }
                    cout << "here is ans --> "<< tmpAns << '\n';
                    if(i < size1 - 1) {
                        cout << "orson3\n";
                        for(int id = 0; id < islandtypes.size(); id++){
                            if(islands[i + 1][j] == islandtypes[id]){
                                tmpAns -= gridcopy[i + 1][j];
                                break;
                            }
                        }
                        tmpAns += gridcopy[i + 1][j];
                        islandtypes.push_back(islands[i + 1][j]);
                    }
                    cout << "here is ans --> "<< tmpAns << '\n';
                    if(j > 0) {
                        cout << "orson4\n";
                        for(int id = 0; id < islandtypes.size(); id++){
                            if(islands[i][j - 1] == islandtypes[id]){
                                tmpAns -= gridcopy[i][j - 1];
                                break;
                            }
                        }
                        tmpAns += gridcopy[i][j - 1];
                        islandtypes.push_back(islands[i][j - 1]);
                    }
                    cout << "here is ans --> "<< tmpAns << '\n';
                    cout << "===============\n";
                }
                if(tmpAns > ans) ans = tmpAns;
            }
        }
        if(orson) return ans;
        return size1 * size2;
    }
};