#include <bits/stdc++.h>
#define FOR(i, a, b) for (int i = a; i < b; i++)
#define FORB(i, a, b) for (int i = a - 1; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i <= a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define pb push_back
#define mp make_pair
#define umap unordered_map
#define ff first
#define ss second
 
typedef long long ll;
typedef double d;
typedef unsigned uint;
typedef unsigned long long ull;
 
typedef vector<int> VI;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<ll> VLL;
typedef vector<VLL> VVLL;
typedef vector<d> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
 
const int MOD = int(1e9) + 7;
const int INF = INT_MAX;
const ll INFF = INT64_MAX;
const d EPS = 1e-9;
const d PI = acos(-1.0); //M_PI;
const int moveX[] = {-1, 0, 1, 0};
const int moveY[] = {0, 1, 0, -1};
class Solution {
public:
    int n;

    string largestMultipleOfThree(vector<int>& digits) {
        VVI count(3);
        sort(ALL(digits));
        reverse(ALL(digits));
        n = digits.size();
        REP(i, n) count[digits[i] % 3].pb(digits[i]);
        VI ans;
        int tmp = min(count[1].size(), count[2].size());
        REP(i, tmp) {
            ans.pb(count[2][i]);
            ans.pb(count[1][i]);
        }
        REP(i, count[3].size()) ans.pb(count[3][i]);
        sort(ALL(ans));
        reverse(ALL(ans));
        string res = "";
        REP(i, ans.size()) {
            res += to_string(ans[i]);
        }
        return res;
    }
};