#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define mp make_pair
using namespace std;
int main () {
    ll n, m, msg;
    cin >> n >> m;
    vector <ll> house(n);
    for (int i = 0; i < n; i++) 
        cin >> house[i];


    ll alreadyOverHouses = 0, count = 0;

    for (int i = 0; i < m; i++) { // o(m)
        cin >> msg;
        while (msg - alreadyOverHouses > house[count]) { // ene ihdee o(n) yvah ba for davtagdah bolgond dahihgui
            alreadyOverHouses += house[count];
            count++;
        }
        cout << count + 1 << ' ' << msg - alreadyOverHouses << '\n';
    }
    //solution by Bat-Erdene
    // total time complexity o(n + m) = o(n)
    return 0;
}