#include <bits/stdc++.h>
using namespace std;
mt19937 rng(chrono::high_resolution_clock::now().time_since_epoch().count() + reinterpret_cast<unsigned long>(new int) + *(new unsigned long));
void delay(int ms) {
  clock_t now, start;
  start = now = clock();
  while (double(now - start) / CLOCKS_PER_SEC < double(ms) / 1000) {
    now = clock();
  }
}

vector <string> msg = { 
    "|                    _                                                     \n"
    "|  __   _           | |                                                    \n"
    "| |   \\| | ___  ___ | |_                                                   \n"
    "| | |\\ | |/ _ \\/ __/| __|                                                  \n"
    "| | | \\  |  __/\\__ \\| |_                                                   \n"
    "| |_|  \\_|\\___ /___/ \\__|                                                  \n"
    "|                                                      _         _   _     \n"
    "|                                                     (_)       | | | |    \n"
    "|  _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_   \n"
    "| | '_ \\| '__/ _ \\ / _` | '__/ _` | '_ ` _ \\| '_ ` _ \\| / __/ __| __| __|  \n"
    "| | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \\__ \\__ \\ |_| |_   \n"
    "| | .__/|_|  \\___/ \\__, |_|  \\__,_|_| |_| |_|_| |_| |_|_|___/___/\\__|\\__|  \n"
    "| | |               __/ |                                                  \n"
    "| |_|              |___/                   _                               \n"
    "|                                  _      (_)                              \n"
    "|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                               \n"
    "| | '_ ` _ \\ / _` |_   / _` |/ _` | |/ _` | |                              \n"
    "| | | | | | | (_|  /  / (_| | (_| | | (_| | |                              \n"
    "| |_| |_| |_|\\__,_|____\\__,_|\\__,_|_|\\__,_|_|                              \n"
};

vector <string> msg1 = { 
    "                                                     _         _   _   ",
    "                                                    (_)       | | | |  ",
    " _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_ ",
    "| '_ \\| '__/ _ \\ / _` | '__/ _` | '_ ` _ \\| '_ ` _ \\| / __/ __| __| __|",
    "| |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \\__ \\__ \\ |_| |_ ",
    "| .__/|_|  \\___/ \\__, |_|  \\__,_|_| |_| |_|_| |_| |_|_|___/___/\\__|\\__|",
    "| |               __/ |                                                ",
    "|_|              |___/                                                 ",
};
int n = msg.size(), m = msg[0].size();
vector <string> printing, init(n, string(m, ' '));

void animate() {
    printing = init;
    set <pair <int, int> > vals;
    for (int i = 0; i < n; i++)
    for (int j = 0; j < m; j++) {
        vals.insert({i, j});
    }
    while(!vals.empty()){
        system("clear");
        vector <pair <int, int> > cur(vals.begin(), vals.end());
        shuffle(cur.begin(), cur.end(), rng);
        int choose = min((int)cur.size(), (int)(10 + rng() % 6)); // 10-15
        for (int i = 0; i < choose; i++) {
            int a, b;
            tie(a, b) = cur[i];
            printing[a][b] = msg[a][b];
            vals.erase({a, b});
        }
        for (auto el : printing) cout << el << '\n';
        delay(40);  
    }
    delay(200);
    animate();
}
int main() {
    animate();
    return 0;
}












