#include <bits/stdc++.h>
using namespace std;
int main () {
    long long n, m, ans = 0;
    
    cin >> n >> m;
    while (n != 0) {
        n /= m;
        ans++;
    }
    cout << ans << '\n';
    return 0;
}