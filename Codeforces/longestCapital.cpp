#include <bits/stdc++.h>
using namespace std;
const int N = 1e3 + 5;
const int M = 1e3 + 5;
unordered_map <int, pair <int, int> > ansX, ansY;
char grid[N][M];
bool vis[N][M];
int dirX[] = {0, 0, 1, -1};
int dirY[] = {1, -1, 0, 0};
int n, m, l, r, t, b;
void travel(int i, int j) {
    if (vis[i][j]) return;
    l = min(l, i);
    r = max(r, i);
    t = min(t, j);
    b = max(b, j);
    vis[i][j] = 1;
    for (int k = 0; k < 4; k++) {
        int x = i + dirX[k];
        int y = j + dirY[k];
        if (x < 0 || x >= n || y < 0 || y >= m) continue;
        travel(x, y);
    }
}
void print() {
    cout << "-------\n";
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            cout << vis[i][j] << ' ';
        }
        cout << '\n';
    }
}
int main () {
    cin >> n >> m;
    unordered_map <int, pair <int, int> > ansX, ansY;
    memset(vis, 0, sizeof(vis));
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            cin >> grid[i][j];
            if (grid[i][j] == '.') vis[i][j] = 1;
        }
    }
    int ans = 0;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            if (vis[i][j]) continue;
            l = r = i;
            t = b = j;
            travel(i, j);
            ans = max(r - l + 1, ans);
            ans = max(b - t + 1, ans);
        }
    }
    cout << ans << '\n';
}