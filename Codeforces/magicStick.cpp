#include <bits/stdc++.h>
#define int long long
using namespace std;
void solve(int A, int B) {
    if(A >= B) {
        cout << "yes\n";
        return;
    }
    if(A == 1) {
        cout << "no\n";
        return;
    }
    if(A == 3 || (A == 2 && B != 3)) {
        cout << "No\n";
        return ;
    }
    cout << "Yes\n";
}

signed main () {
    int t;
    cin >> t;

    for(int i = 0; i < t; i++) {
        int A, B;
        cin >> A >> B;
        solve(A, B);
    }

    return 0;
}