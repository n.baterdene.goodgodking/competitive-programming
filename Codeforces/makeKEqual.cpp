#include <bits/stdc++.h>
using namespace std;
#define _upgrade ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define FOR(i, a, b) for (int i = a; i < b; i++)
#define FORB(i, a, b) for (int i = a - 1; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define pb push_back
#define mp make_pair
#define ff first
#define ss second

typedef long long ll;
typedef vector<ll> VLL;
typedef pair<ll, ll> PLL;
typedef vector<PLL> VPL;
 
VPL nums, up, down;
ll fromEnd(ll, ll);

ll fromSt(ll i, ll needed) {
    if (needed <= 0) return 0;
    ll ans = fromEnd(i, needed - up[i - 1].ss);
    needed = min(up[i - 1].ss , needed);
    // ans += up[i].ff - (up[i - 1].ss - needed) * (nums[i].ff - nums[i - 1].ff);
    ans += (up[i - 1].ff + needed + up[i - 1].ss * (nums[i].ff - nums[i - 1].ff - 1));
    return ans;
}
ll fromEnd(ll i, ll needed) {
    if (needed <= 0) return 0;
    ll ans = fromSt(i, needed - down[i + 1].ss);
    needed = min(down[i + 1].ss, needed);
    // ans += down[i].ff - (down[i + 1].ss - needed) * (nums[i + 1].ff - nums[i].ff);
    ans += (down[i + 1].ff + needed + down[i + 1].ss * (nums[i + 1].ff - nums[i].ff - 1));
    return ans;
}


signed main () {
#ifdef ONLINE_JUDGE
#else
    freopen("in.txt", "r", stdin);
//	freopen("out.txt", "w", stdout);
#endif

    _upgrade
    
    ll n, m, tmp;
    cin >> n >> m;
    VLL temp(n);
    REP(i, n) cin >> temp[i];
    sort(ALL(temp));
    REP(i, n) {
        tmp = temp[i];
        if (nums.size() == 0 || nums.back().ff != tmp) 
            nums.pb(mp(tmp, 1));
        else
            nums.back().ss++;
    }
    n = nums.size();
    up.resize(n);
    down.resize(n);
    up[0] = mp(0, nums[0].ss); 
    down[n - 1] = mp(0, nums.back().ss);

    // first val, second count
    REP1(i, n) {
        up[i].ff = up[i - 1].ff + up[i - 1].ss * (nums[i].ff - nums[i - 1].ff);
        up[i].ss = up[i - 1].ss + nums[i].ss;
        int j = n - i - 1;
        down[j].ff = down[j + 1].ff + down[j + 1].ss * (nums[j + 1].ff - nums[j].ff);
        down[j].ss = down[j + 1].ss + nums[j].ss;
    }
    // REP(i, n) {
    //     cout << i << " : " << up[i].ff << ' ' << up[i].ss << ' ' << down[i].ff << ' ' << down[i].ss << '\n';
    // }
    ll ans = min(fromEnd(0, m - nums[0].ss), fromSt(n - 1, m - nums[n - 1].ss));
    // cout << "Meet " << nums[0].ff << ' ' << fromSt(0, m - nums[0].ss) << '\n';
    REP1(i, n - 1) {
        // cout << "Meet " << nums[i].ff << ' ' << fromSt(i, m - nums[i].ss) << ' ' << fromEnd(i, m - nums[i].ss) << '\n'; 
        ll cur = min(fromSt(i, m - nums[i].ss), fromEnd(i, m - nums[i].ss));
        ans = min(cur, ans);
    }
    // cout << "Meet " << nums.back().ff << ' ' << fromSt(n - 1, m - nums[n - 1].ss) << '\n';
    cout << ans << '\n';
    return 0;
}