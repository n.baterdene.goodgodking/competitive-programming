#include <bits/stdc++.h>
using namespace std;
int ender = -INT_MAX, ans = 0;
void mergeSort(vector <int>&nums, int st, int end) {
    if (st == end) return;
    int mid = (st + end)>>1;
    mergeSort(nums, st, mid);
    mergeSort(nums, mid+1, end);
    vector <int> temp;
    int x = st, y = mid+1, a = mid - st + 1;
    while(x <= mid || y <= end) {
        if (y > end || (x <= mid && nums[x] < nums[y])) {
            temp.push_back(nums[x++]);
            a--;
        } else {
            temp.push_back(nums[y++]);
            ans += a;
        } 
    }
    for (int i = 0; i < temp.size(); i++) {
        nums[i+st] = temp[i];
    }
}
int main () {
    int n;
    cin >> n;
    vector <int> nums(n);
    for (int i = 0; i < n; i++) cin >> nums[i];
    for (auto el : nums) ender = max(ender, el);
    mergeSort(nums, 0, n-1);
    // for (int i = 0; i < n; i++) cout << nums[i] << ' ';
    // cout << '\n';
    // cout << "inversion: " << ans << '\n';
    return 0;
}

/*
8 
1 2 3 4 5 6 7 8
8
8 7 6 5 4 3 2 1
*/

