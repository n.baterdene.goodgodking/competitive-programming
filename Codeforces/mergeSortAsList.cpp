#include <iostream>
#include <vector>
#include <list>
using namespace std;
pair <list <int>, list <int> > split(list <int>& numbers) {
    int size = numbers.size();
    list <int> first, second;
    list <int> :: iterator it = numbers.begin();
    int half = size / 2;
    for (int i = 0; i < size; i++) {
        if (i < half)
            first.push_back(*it);
        else 
            second.push_back(*it);
        it++;
    }
    return make_pair(first, second);
}
list <int> conquer(list <int> first, list <int> second) {
    list <int> ans;
    list <int> :: iterator it, end;
    list <int> :: iterator it1 = first.begin();
    list <int> :: iterator it2 = second.begin();
    while (it1 != first.end() && it2 != second.end()) {
        if (*it1 > *it2) {
            ans.push_back(*it2);
            it2++;
        } else {
            ans.push_back(*it1);
            it1++;
        }
    }
    if (it1 == first.end()) {
        it = it2;
        end = second.end();
    } else {
        it = it1;
        end = first.end();
    }
    while(it != end) {
        ans.push_back(*it);
        it++;
    }
    return ans;
}
list <int> mergeSort(list <int>& numbers) {
    if (numbers.size() == 1) 
        return numbers;
    pair <list <int>, list <int> > splited = split(numbers);
    return conquer(mergeSort(splited.first),
                   mergeSort(splited.second));
}
int main () {
    list <int> numbers; 
    int n, number;
    cin >> n;
    for (int i = 0; i < n; i++) {
        cin >> number;
        numbers.push_back(number);
    }
    numbers = mergeSort(numbers);
    list <int> :: iterator it;

    for (it = numbers.begin(); it != numbers.end(); it++) 
        cout << *it << ' ';

    cout << '\n';
    return 0;
}