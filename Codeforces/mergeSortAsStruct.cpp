#include <iostream>
#include <vector>
using namespace std;




struct numbers {
    vector <int> val;
    numbers * left, * right;
};
numbers * root = NULL;
void divide(numbers * head) {
    int size = head->val.size();
    if(size == 1) 
        return;
    
    numbers * tmp1 = new numbers(), * tmp2 = new numbers();
    for(int i = 0; i < size; i++) {
        if(i < size / 2) 
            tmp1->val.push_back(head->val[i]);
        else 
            tmp2->val.push_back(head->val[i]);
    }
    head->left = tmp1;
    head->right = tmp2;
    tmp1->left = NULL;
    tmp2->left = NULL;
    tmp1->right = NULL;
    tmp2->right = NULL;
    divide(tmp1);
    divide(tmp2);
}
void conquer(numbers * head) {
    if(head->left == NULL || head->right == NULL) 
        return;

    conquer(head->left);
    conquer(head->right);
    numbers * tmp1, * tmp2;
    tmp1 = head->left;
    tmp2 = head->right;
    int idx1 = 0, idx2 = 0, i = 0;
    while(idx1 < tmp1->val.size() || idx2 < tmp2->val.size()) {
        if(idx1 == tmp1->val.size()) {
            head->val[i] = tmp2->val[idx2];
            idx2++;
            i++;
            continue;
        }
        if(idx2 == tmp2->val.size()) {
            head->val[i] = tmp1->val[idx1];
            idx1++;
            i++;
            continue;
        }
        if(tmp1->val[idx1] > tmp2->val[idx2]) {
            head->val[i] = tmp2->val[idx2];
            idx2++;
        } else {
            head->val[i] = tmp1->val[idx1];
            idx1++;
        }
        i++;
    }
    for(int i = 0; i < head->val.size(); i++) 
        cout << head->val[i] << ' ';
    cout << '\n';
}
void mergeSort(numbers * root) {
    divide(root);
    conquer(root);
}
int main () {
    int n, tmp;
    cin >> n;
    root = new numbers();
    for(int i = 0; i < n; i++) {
        cin >> tmp;
        root->val.push_back(tmp);
    }
    mergeSort(root);
    cout << "Numbers: ";
    if(root == NULL) 
        return 0;

    for(int i = 0; i < n; i++) 
        cout << root->val[i] << ' ';

    cout << '\n';
    return 0;
}