#include<bits/stdc++.h> 

// #define int long long
using namespace std; 
string toStr(vector <int>&a) {
    string ans = "";
    for (int i = 0; i < a.size(); i++) ans += char(a[i] + '0') + '.';
    return ans;
}
signed main() { 
	int n;
	cin >> n;
    vector <vector <int> > nums(n, vector <int>(n));
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            cin >> nums[i][j];
        }
        sort(nums[i].begin(), nums[i].end());
    }

    set <pair <int, vector <int> > > Q, Q1;

    int sum = 0;
    for (int i = 0; i < n; i++) sum += nums[i][0];
    vector <int> tmp(n, 0);
    Q.insert(make_pair(sum, tmp));

    // unordered_map <string, bool> seen;
    map <vector <int>, bool> seen1;
    int cnt = 0;
    while(!Q.empty()) {
        if (Q.size() > n - cnt) {
            int cnt1 = cnt;
            for (auto el: Q) {
                if (cnt1++ == n) break;
                Q1.insert(el);
            } 
            swap(Q, Q1);
            Q1.clear();
        }
        if (cnt++ == n) break;
        tmp.clear();
        tmp = (Q.begin())->second;
        int cur = (Q.begin())->first;
        Q.erase(Q.begin());
        if (seen1.find(tmp) != seen1.end()) continue;
        seen1[tmp] = 1;
        cout << cur << ' ';
        for (int i = 0; i < n; i++) {
            if (tmp[i] == n - 1) continue;
            int dif = nums[i][tmp[i] + 1] - nums[i][tmp[i]];
            tmp[i]++;
            Q.insert(make_pair(cur + dif, tmp));
            tmp[i]--;
        }
    }
    cout << '\n';
	return 0; 
} 
