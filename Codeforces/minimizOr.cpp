#include <bits/stdc++.h>
// #define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define sz(x) (int)x.size()

using ll = long long;
using db = double;
using ld = long double;
using uint = unsigned;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VS = vector<string>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using VD = vector<db>;
using PII = pair<int, int>;
using PLL = pair<ll, ll>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int bitCnt(int a) {
    bitset <64> b(a);
    return b.count();
}
int bitCnt(string a) {
    bitset <64> b(a);
    return b.count();
}

int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}

void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VPI& a) {
    for (auto el : a) {
        cout << el.ff << ',' << el.ss << ' ';
    }
    cout << '\n';
}

void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VVI& a) {
    for (auto el : a) {
        print(el);
    }
}

/*

|                    _                                                     
|  __   _           | |                                                    
| |   \| | ___  ___ | |_                                                   
| | |\ | |/ _ \/ __/| __|                                                  
| | | \  |  __/\__ \| |_                                                   
| |_|  \_|\___ /___/ \__|                                                  
|                                                      _         _   _     
|                                                     (_)       | | | |    
|  _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_   
| | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|  
| | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_   
| | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|  
| | |               __/ |                                                  
| |_|              |___/                   _                               
|                                  _      (_)                              
|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                               
| | '_ ` _ \ / _` |_   / _` |/ _` | |/ _` | |                              
| | | | | | | (_|  /  / (_| | (_| | | (_| | |                              
| |_| |_| |_|\__,_|____\__,_|\__,_|_|\__,_|_|                              
*/

const db PI = acos(-1.0); //M_PI;
const ll INFF = 4e18;
const int INF = 2e9;
const int MOD = 1e9 + 7;
const int MOD1 = 998244353; //7*17*2^23 +1;
const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0, 1, 0, -1};





const int N = 1e5 + 5;
const int M = 2e5 + 5;
const int BLOCK = 320;
int nums[N];
int ans[N];

int n, m, k, q;
string s;
struct Query {
    int l, r, id;
    bool operator < (const Query&a) const {
        return mp(l / BLOCK, r) < mp(a.l / BLOCK, a.r);
    }
};
struct Trie {
    Trie *a[2];
    int cnt, val, lvl;
    Trie() {
        cnt = val = lvl = 0;
        a[0] = a[1] = NULL;
    }
};
Trie *root = new Trie();
void add(int x) {
    cout << LINE;
    cout << "add: " << x << '\n';
    Trie *cur = root;
    cur->cnt++;
    int pre = 0;
    for (int i = 29; i >= 0; i--) {
        int digit = (x & (1<<i) ? 1 : 0);
        if (!cur->a[digit]) cur->a[digit] = new Trie();
        pre |= x & (1<<i);
        cur = cur->a[digit];
        cur->val = pre;
        cur->lvl = i;
        cur->cnt++;
        cout << cur->val << ' ' << cur->lvl << ' ' << cur->cnt << '\n';
    }
}
void rem(int x) {
    // cout << "rem: " << x << '\n';
    Trie *cur = root;
    cur->cnt--;
    for (int i = 29; i >= 0; i--) {
        int digit = (x & (1<<i) ? 1 : 0);
        cur = cur->a[digit];
        cur->cnt--;
    }
}

int getVal(Trie *cur) {
    if (cur->lvl == 0) return cur->val;
    if (cur->a[0] && cur->a[0]->cnt > 0) return getVal(cur->a[0]);
    return getVal(cur->a[1]);
}
int calc1(Trie *cur, int curV) {
    if (cur->lvl == 0) return curV|cur->val;
    int res = INF;
    if (cur->a[0] && ccur->a[0]->cnt > 0) {
        res = calc1(cur->a[0], curV);
        if (cur->a[1] && cur->a[1]->cnt > 0 && curV&(1<<cur->lvl)) res = calc1(cur->a[1], curV);
    } else {
        if (cur->a[1] && cur->a[1]->cnt > 0) res = calc1(cur->a[1], curV);
    }

    return res;
}
int calc(Trie *cur) {
    if (cur->a[0] && cur->a[0]->cnt > 1) return calc(cur->a[0]);
    int res = INF;
    if (cur->a[1] && cur->a[1]->cnt > 1) res = calc(cur->a[1]);
    if (cur->a[0] && cur->a[0]->cnt > 0 &&
        cur->a[1] && cur->a[1]->cnt > 0) res = min(res, getVal(cur->a[0]));
    return res;
}
void go () {
    cin >> n;
    for (int i = 1; i <= n; i++) {
        cin >> nums[i];
    }
    cin >> q;
    vector <Query> queries;
    for (int i = 1; i <= q; i++) {
        int l, r; cin >> l >> r;
        queries.pb({l, r, i});
    }
    sort(ALL(queries));
    int l = queries[0].l, r = l-1;
    for (auto& query : queries) {
        while (r+1 <= query.r) add(nums[++r]);
        while (r > query.r) rem(nums[r--]);
        while (l-1 >= query.l) add(nums[--l]);
        while (l < query.r) rem(nums[l++]);
        // cout << LINE;
        // cout << "CALC: " << query.l << ',' << query.r << '\n';
        ans[query.id] = calc(root);
    }
    while (l <= r) rem(nums[l++]);
    for (int i = 1; i <= n; i++) cout << ans[i] << "\n";
}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("0.in", "r", stdin);
    freopen("0.out", "w", stdout);
#endif
    _upgrade
    int T = 1;
    cin >> T;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














