#include <bits/stdc++.h>
#define pb push_back
#define ALL(x) x.begin(), x.end()
using namespace std;
using ll = long long;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());

string s;
bool cmp(vector <string> a, vector <string> b) {
    return a == b;
}
string solve(string input) {
    ifstream in(input.c_str());
    ll n; in >> n;
    string a, b, c; in >> a >> b >> c;
    string A, B, C; in >> A >> B >> C;
    ll used = 2;
    if (cmp({a, b, c}, {A, B, C})) used = 0;
    else if (cmp({b, a, c}, {A, B, C})) used = 1;
    else if (cmp({c, b, a}, {A, B, C})) used = 1;
    else if (cmp({a, c, b}, {A, B, C})) used = 1;
    in.close();
    if ((used % 2) != (n % 2)) return "No";
    return "Yes";    
}
void create(string input, string output) {
    ofstream in(input.c_str());
    ofstream out(output.c_str());
    ll n = rng() & 1;
    ll m = ll(1e18)-n;
    vector <string> words = {"plane", "car", "robot"};
    in << m << '\n';
    shuffle(ALL(words), rng);
    for (auto el : words) in << el << ' '; in << '\n';
    shuffle(ALL(words), rng);
    for (auto el : words) in << el << ' '; in << '\n';
    in.close();
    out << solve(input) << '\n';
    out.close();
}
string getStrDigit(int x) {
    string val = to_string(x);
    if (val.size() == 1) val = "0"+val;
    return val;
}
int main () {
    for (int i = 0; i <= 20; i++) {
        string input = "../../examsMadeByMe/input/input"+getStrDigit(i)+".txt";
        string output = "../../examsMadeByMe/output/output"+getStrDigit(i)+".txt";
        // cout << rng() << '\n';
        create(input, output);
    }
    return 0;
}