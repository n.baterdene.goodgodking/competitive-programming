#include <bits/stdc++.h>
using namespace std;
string multi(string a, string b) {
    int n = a.size();
    string ans = "";
    int sanasan = 0;
    for (int i = 0; i < n; i++) {
        int sum = 0;
        for (int j = 0; j <= i; j++) {
            int A = a[j] - '0';
            int B = b[i - j] - '0';
            int cur = A * B + sanasan;
            sum += cur;
        }
        sanasan = sum / 10;
        sum = sum % 10;
        ans = char('0' + sum) + ans;
    }

    for (int i = 0; i < n-1; i++) {
        int sum = 0;
        for (int j = 0; j <= i; j++) {
            int A = a[j] - '0';
            int B = b[i - j] - '0';
            int cur = A * B + sanasan;
            sum += cur;
        }
        sanasan = sum / 10;
        sum = sum % 10;
        ans = char('0' + sum) + ans;
    }

}
int main () {
    int n, m;
    char x;
    string a = "", b = "";
    cin >> n;
    for (int i = 0; i < n; i++) {
        cin >> x;
        a += x;
    }
    cin >> m;
    for (int i = 0; i < m; i++) {
        cin >> x;
        b += x;
    }
    if (a.size() < b.size()) swap(a, b);
    while(b.size() < a.size()) {
        b = '0' + b;
    }
    string ans = multi(a, b);
}