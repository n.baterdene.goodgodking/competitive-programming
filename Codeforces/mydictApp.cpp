#include <bits/stdc++.h>
using namespace std;
// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))

#define _upgrade ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define FOR(i, a, b) for (int i = a; i < b; i++)
#define FORB(i, a, b) for (int i = a - 1; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define umap unordered_map
#define pb push_back
#define mp make_pair
#define ff first
#define ss second
#define PQ priority_queue
#define Q queue

typedef long long ll;
typedef double d;
typedef unsigned uint;
typedef unsigned long long ull;
typedef unordered_map<int, int> umapII;
typedef vector<int> VI;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<bool> VB;
typedef vector<umapII> VumapII;
typedef vector<ll> VLL;
typedef vector<VLL> VVLL;
typedef vector<d> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef pair<string, string> PSS;

map <string, PSS> wordDict;
int n = 0;
VS words;
void update() {
    ofstream out("dict.in");
    for (auto el : wordDict) 
        out << el.ff << ": " << el.ss.ff << '\n' << el.ss.ss << "\n\n";
}
string randChoose(VS&a) {
    int m = a.size();
	srand(time(NULL));
    int x = rand() % m;
    string b = a[x];
    swap(a[x], a.back());
    a.pop_back();
    return b;
}
VS shuffle() {
    VS tmp = words, tmp1;
    while(!tmp.empty()) tmp1.pb(randChoose(tmp));
    return tmp1;
}
void read() {
    ifstream in("dict.in");
    string str;
    while(getline(in, str)) {
        if (str.empty()) continue;
        if (str.find(':') != -1) {
            int a = str.find(':');
            string word = str.substr(0, a);
            if(word[0] >= 'A' && word[0] <= 'Z') word[0] = word[0] - 'A' + 'a';
            if(str[a + 1] == ' ') a++;
            string ans1 = str.substr(a + 1), ans2;
            getline(in, ans2);
            words.pb(word);
            n++;
            wordDict[word] = mp(ans1, ans2);
        }
    }
}

void saveScore(string& res) {
    ifstream in("myScores.in");
    ofstream out("myData.txt", ios_base::app); // append instead of overwrite
    string str;
    int count = 1;
    while(getline(in, str)) {
        REP(i, 4) getline(in, str);
        count++;
    }
    out << "data #" << count <<  ":\n" << res;
}
void myQuiz() {
    VS newPermutation = shuffle();
    string tmp;
    vector <string> wrong;
    int score = 0, hints = 5, total = 0;;
    REP(i, n) {
        int cur;
        system("clear");
        cout << "if testing done and stop: 0\nif know this word: 1\nif don't know this word: 2\nif use hint: 3\nleftHint: " << hints << '\n';
        cout << "word# " << i + 1 << " : " << newPermutation[i] << '\n';
        cin >> cur;
        if (cur == 0) break;
        total++;
        if (cur == 1) score++;
        if (cur <= 2) {
            if (cur == 2) wrong.pb(newPermutation[i]);
            cout << "the answer was : " << wordDict[newPermutation[i]].ss << '\n';
            cout << "input anything to continue!\n";
            cin >> tmp;
            continue;
        }
        if (hints == 0) continue;
        cout << "hint is: " << wordDict[newPermutation[i]].ff << '\n';
        hints--;
        cin >> cur;

        if (cur == 1) score++;
        if (cur == 2) wrong.pb(newPermutation[i]);
        cout << "the answer was : " << wordDict[newPermutation[i]].ss << '\n';
        cout << "input anything to continue!\n";
        cin >> tmp;
    }
    system("clear");
    string res = "hints used: " + to_string(5 - hints) + "\nscore: " + to_string(score) + '/' + to_string(total) + "\npercent: " + to_string(d(score) / d(total) * 100.0) + "%\n";
    cout << "hints used: " << 5 - hints << "\nscore: " << score << "/" << total << "\npercent: " << d(score) / d(total) * 100.0 << "%\n";
    saveScore(res);
    int x;
    cout << "if you want to show the words that you didn' t memorise enter 1 otherwise 2\n";
    cin >> x;
    if (x == 2) return;
    REP(i, wrong.size()) {
        system("clear");
        cout << "word: " << words[i] << " : " << wordDict[words[i]].ss << '\n';
        cout << "if you want to continue enter 1 else 2\n";
        cin >> x;
        if(x == 2) break;
    }
}
int main () {
    _upgrade
    read();
    update();
    myQuiz();
    return 0;
}
