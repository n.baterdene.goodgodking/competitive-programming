#include <bits/stdc++.h>
#pragma GCC optimize("Ofast,unroll-loops,fast-math")
#pragma GCC target("sse,avx2")
// #define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define sz(x) (int)x.size()

using ll = long long;
using db = double;
using ld = long double;
using uint = unsigned;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VS = vector<string>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using VD = vector<db>;
using PII = pair<int, int>;
using PLL = pair<ll, ll>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int bitCnt(int a) {
    bitset <64> b(a);
    return b.count();
}
int bitCnt(string a) {
    bitset <64> b(a);
    return b.count();
}

int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}

void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VPI& a) {
    for (auto el : a) {
        cout << el.ff << ',' << el.ss << ' ';
    }
    cout << '\n';
}

void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VVI& a) {
    for (auto el : a) {
        print(el);
    }
}

/*

|                    _                                                     
|  __   _           | |                                                    
| |   \| | ___  ___ | |_                                                   
| | |\ | |/ _ \/ __/| __|                                                  
| | | \  |  __/\__ \| |_                                                   
| |_|  \_|\___ /___/ \__|                                                  
|                                                      _         _   _     
|                                                     (_)       | | | |    
|  _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_   
| | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|  
| | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_   
| | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|  
| | |               __/ |                                                  
| |_|              |___/                   _                               
|                                  _      (_)                              
|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                               
| | '_ ` _ \ / _` |_   / _` |/ _` | |/ _` | |                              
| | | | | | | (_|  /  / (_| | (_| | | (_| | |                              
| |_| |_| |_|\__,_|____\__,_|\__,_|_|\__,_|_|                              
*/

const db PI = acos(-1.0); //M_PI;
const ll INFF = 4e18;
const int INF = 1.07e9;
const int MOD = 1e9 + 7;
const int MOD1 = 998244353; //7*17*2^23 +1;
const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0, 1, 0, -1};





const int N = 5e5 + 5;
const int M = 3*N;
const int BLOCK = 2;
int n, m, k, q;
int nums[3][N];
int dp[N/BLOCK + 5];
int dp1[N/BLOCK + 5];
int par[M];
bool vis[M];
int find(int a) {
    if (par[a] == a) return a;
    return par[a] = find(par[a]);
}
void merge(int a, int b) {
    a = find(a);
    b = find(b);
    par[a] = b;
}
void go () {
    cin >> n;
    int cur = 1;
    iota(par+1, par+n*3+1, 1);
    for (int i = 0; i < 3; i++) {
        for (int j = 1; j <= n; j++) {
            char c; cin >> c;
            if (c == '1') nums[i][j] = cur++;
            if (nums[i][j-1] > 0 && nums[i][j] > 0) {
                merge(nums[i][j], nums[i][j-1]);
            }
            if (i > 0 && nums[i][j] > 0 && nums[i-1][j] > 0) {
                merge(nums[i][j], nums[i-1][j]);
            }
        }
    }
    for (int i = 0; i < 3; i++) 
    for (int j = 1; j <= n; j++) 
        nums[i][j] = find(nums[i][j]);
    for (int l = 1; l <= n; l+=BLOCK) {
        int r = min(l+BLOCK-1, n);
        int cur = 0;
        for (int j = l; j <= r; j++) {
            for (int i = 0; i < 3; i++) {
                if (nums[i][j] == 0) continue;
                if (vis[nums[i][j]]) continue;
                vis[nums[i][j]] = 1;
                cur++;
            }
        }
        dp[r/BLOCK] = cur;
        cout << l << " " << r << ": " << dp[r/BLOCK] << ',' << dp1[r/BLOCK] << '\n';
    }
    // cin >> q;
    // for (int t = 0; t < q; t++) {
    //     int l, r; cin >> l >> r;
    // }
}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("0.in", "r", stdin);
    freopen("0.out", "w", stdout);
#endif
    _upgrade
    int T = 1;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














