#include <bits/stdc++.h>
// #define int long long
using namespace std;
// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))

#define _upgrade ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define II <int, int>
#define Q queue
#define ff first
#define bk back()
#define ss second
#define rs resize
#define ins insert 
#define fr front() 
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define PQ priority_queue
#define umap unordered_map
#define sz(x) (int)x.size()

typedef long long ll;
typedef double db;
typedef unsigned uint;
typedef unsigned long long ull;
typedef unordered_map<int, int> umapII;
// PQ going up <int, VI, greater<int> >
typedef vector<int> VI;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<bool> VB;
typedef vector<VB> VVB;
typedef vector<VVB> VVVB;
typedef vector<umapII> VumapII;
typedef vector<ll> VLL;
typedef vector<VLL> VVLL;
typedef vector<VVLL> VVVLL;
typedef vector<VVVLL> VVVVLL;
typedef vector<db> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VPI> VVPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
typedef vector<VVVI> VVVVI;
typedef vector<VVVVI> VVVVVI;
// mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
 
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
void readI(int& a) {
    cin >> a;
}
void readI(int& a, int&b) {
    cin >> a >> b;
}
void readI(int& a, int&b, int&c) {
    cin >> a >> b >> c;
}
VI readVI(int n) {
    VI a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVI readVVI(int n, int m) {
    VVI a(n, VI(m));
    REP(i, n) a[i] = readVI(m);
    return a;
}
VLL readVLL(ll n) {
    VLL a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVLL readVVLL(ll n, ll m) {
    VVLL a(n, VLL(m));
    REP(i, n) a[i] = readVLL(m);
    return a;
}


int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}

void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VPI& a) {
    for (auto el : a) {
        cout << el.ff << ',' << el.ss << ' ';
    }
    cout << '\n';
}

void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
const int MOD = 1e9 + 7;
const int MOD1 = 998244353;//119*2^23 +1;
const int INF = 2e9;
const ll INFF = INT64_MAX;
const db EPS = 1e-9;
const db PI = acos(-1.0); //M_PI;
const int moveX[] = {-1, 0, 1, 0};
const int moveY[] = {0, 1, 0, -1};
/*
|      ⣠⣶⡾⠏⠉⠙⠳⢦⡀⠀  ⠀⢠⠞⠉⠙⠲⡀
|    ⣴⠿⠏⠀⠀⠀⠀⠀⠀⢳⡀⠀  ⡏⠀⠀ ⠀⠀ ⢷
|⠀⠀⢠⣟⣋⡀⢀⣀⣀⡀⠀⣀⡀⣧⠀  ⢸⠀⠀⠀⠀   ⡇ ⠀
| ⠀⢸⣯⡭⠁⠸⣛⣟⠆⡴⣻⡲⣿   ⣸⠀⠀OK.  ⡇
|⠀⠀⣟⣿⡭⠀⠀⠀⠀⠀⢱⠀⠀⣿   ⢹⠀⠀⠀⠀⠀ ⡇
|⠀⠀⠙⢿⣯⠄⠀⠀⠀⢀⡀⠀⠀⡿⠀  ⡇⠀⠀ ⠀ ⡼
|⠀⠀⠀⠀⠹⣶⠆⠀⠀⠀⠀⠀⡴⠃ ⠀ ⠘⠤⣄⣠⠞⠀ ⠀
|⠀⠀⠀⠀⢸⣷⡦⢤⡤⢤⣞⣁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ ⠀
|⠀⢀⣤⣴⣿⣏⠁⠀⠀⠸⣏⢯⣷⣖⣦⡀⠀⠀⠀⠀⠀⠀
|⢀⣾⣽⣿⣿⣿⣿⠛⢲⣶⣾⢉⡷⣿⣿⠵⣿⠀⠀⠀⠀⠀
|⣼⣿⠍⠉⣿⡭⠉⠙⢺⣇⣼⡏⠀ ⠀⣄⢸⣿
|⣿⣿⣧⣀⣿.........⣀⣰⣏⣘⣆⣀
      
|                                                   _         _   _
|                                                   (_)       | | | |
 _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_
| '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|
| |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_
| .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|
| |               __/ |
|_|              |___/
 _   _      _ _    __        __         _     _ 
| | | | ___| | | __\ \      / /__  _ __| | __| |
| |_| |/ _ \ | |/ _ \ \ /\ / / _ \| '__| |/ _` |
|  _  |  __/ | | (_) \ V  V / (_) | |  | | (_| |
|_| |_|\___|_|_|\___/ \_/\_/ \___/|_|  |_|\__,_|                                  
 ____     __    _______    _______   __________
|     \  |  |  |  _____|  /  _____) |____  ____|
|  |\  \ |  |  | |__     (  (_____      |  |
|  | \  \|  |  |  __|     \_____  \     |  | 
|  |  \     |  | |_____    _____)  )    |  | 
|__|   \____|  |_______|  (_______/     |__| 
 
 
 


*/




int n, t1, counter = 0;
const int N = 1e5 + 5;
VB vis(N), vis1(N);
VI sizes(N);
VVI dp(N, VI(2));

VPI removedEdges;
map <PII, bool> isRemoved, newM;
bool O = 0;
bool check(int a) {
    if (a < 0 || a > n) return 1;
    return 0;
}
void init() {
    removedEdges.clear();
    isRemoved.clear();
    REP(i, n+5) vis[i] = 0;
    REP(i, n+5) vis1[i] = 0;
    REP1(i, n+5) sizes[i] = 0;
    REP(i, n+5) dp[i][0] = 0, dp[i][1] = 0;
}
bool customCmp(const PII& a, const PII& b) {
    return a.ss <= b.ss;
}
void dfs(int pos, int par, VVI&paths) {
    for (int i = 0; i < paths[pos].size(); i++) {
        int el = paths[pos][i];
        if (el == -1) continue;
        if (el == par) continue;
        if (el < 1 || el > n) continue;
        dfs(el, pos, paths);
    }
    VVI curChildren; // pos, dp[pos][0], dp[pos][1]
    VPI curChildren1;
    for (int i = 0; i < paths[pos].size(); i++) {
        int el = paths[pos][i];
        if (el == -1) continue;
        if (el == par) continue;
        if (el < 1 || el > n) continue;
        if (isRemoved.find({pos, el}) != isRemoved.end()) continue;
        curChildren1.pb({el, dp[el][1] - dp[el][0]});
        // curChildren.pb({el, dp[el][0], dp[el][1]});
    }
    if (curChildren.size() > 1)
        sort(ALL(curChildren1), customCmp);
    for (auto el1 : curChildren1) {
        int el = el1.ff;
        VI tmp = {el, dp[el][0], dp[el][1]};
        curChildren.pb(tmp);
    }
    while(curChildren.size() > 2) {
        if (curChildren.empty()) break;
        VI cur = curChildren.back();
        curChildren.pop_back();
        sizes[pos]--;
        sizes[cur[0]]--;
        removedEdges.pb({pos, cur[0]});
        isRemoved[{pos, cur[0]}] = 1;
        dp[pos][0] += cur[1];
        dp[pos][1] += cur[1];
    }
    int sz = curChildren.size();
    for (auto cur : curChildren) {
        dp[pos][0] += cur[2];
        dp[pos][1] += cur[2];
    }
    if (pos != par && curChildren.size() == 2) {
        dp[pos][1] = dp[pos][1] + 1 - curChildren[1][2] + curChildren[1][1];
        if (pos != 1 && sz > 0 && dp[pos][0] >= dp[pos][1]) {
            VI cur = curChildren.back();
            sizes[pos]--;
            sizes[cur[0]]--;
            removedEdges.pb({pos, cur[0]});
            isRemoved[{pos, cur[0]}] = 1;
        } else {
            sizes[pos]--;
            sizes[par]--;
            removedEdges.pb({par, pos});
            isRemoved[{par, pos}] = 1;
        }
    }
}
int line(int cur, int par, VVI&paths) {
    vis[cur] = 1;
    if (cur != par && sizes[cur] == 1) return cur;
    for (int i = 0; i < paths[cur].size(); i++) {
        int el = paths[cur][i];
        if (el == -1) continue;
        if (el == par) continue;
        if (el < 1 || el > n) continue;
        if (isRemoved.find({cur, el}) != isRemoved.end() || isRemoved.find({el, cur}) != isRemoved.end()) 
            continue;
        
        return line(el, cur, paths);
    }
    return cur;
}
void go () {
    cin >> n;
    init();
    VVI paths(n+1);
    REP1(i, n) {
        int a, b;
        cin >> a >> b;
        sizes[a]++;
        sizes[b]++;
        paths[a].pb(b);
        paths[b].pb(a);
    }
    dfs(1, 1, paths);    
    VPI lines;
    REP1(i, n+1) {
        if (vis[i] || sizes[i] > 1) continue;
        lines.pb({i, line(i, i, paths)});
    }
    if (t1 == 267) return;
    cout << removedEdges.size() << '\n';
    REP1(i, removedEdges.size()+1) {
        PII x = removedEdges[i-1], y = lines[i], z = lines[i-1];
        cout << x.ff << ' ' << x.ss << ' ' << z.ss << ' ' << y.ff << '\n';
    }
}



signed main () {

#ifdef ONLINE_JUDGE
#else
    freopen("in.txt", "r", stdin);
//	freopen("out.txt", "w", stdout);
#endif
    _upgrade
    int T = 1;
    cin >> T;
    while(T--) go();
    return 0;
}

/* stuff you should look for
	* int overflow, array bounds
	* special cases (n=1?)
    
*/