#include <bits/stdc++.h>
// #define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define sz(x) (int)x.size()

using ll = long long;
using db = double;
using uint = unsigned;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VS = vector<string>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VVVVB = vector<VVVB>;
using VVVVVB = vector<VVVVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using VVVLL = vector<VVLL>;
using VVVVLL = vector<VVVLL>;
using VD = vector<db>;
using PII = pair<int, int>;
using PDD = pair<db, db>;
using PLL = pair<ll, ll>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
using VVVVI = vector<VVVI>;
using VVVVVI = vector<VVVVI>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int bitCnt(int a) {
    bitset <64> b(a);
    return b.count();
}
int bitCnt(string a) {
    bitset <64> b(a);
    return b.count();
}
VI readVI(int n) {
    VI a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVI readVVI(int n, int m) {
    VVI a(n, VI(m));
    REP(i, n) a[i] = readVI(m);
    return a;
}
VLL readVLL(ll n) {
    VLL a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVLL readVVLL(ll n, ll m) {
    VVLL a(n, VLL(m));
    REP(i, n) a[i] = readVLL(m);
    return a;
}


int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}

// void print(VLL& a) {
//     for (auto el : a) {
//         cout << el << ' ';
//     }
//     cout << '\n';
// }
// void print(VI& a) {
//     for (auto el : a) {
//         cout << el << ' ';
//     }
//     cout << '\n';
// }
// void print(VPI& a) {
//     for (auto el : a) {
//         cout << el.ff << ',' << el.ss << ' ';
//     }
//     cout << '\n';
// }

// void print(VI& a, int n) {
//     int cnt = 0;
//     for (auto el : a) {
//         if (cnt++ == n) break;
//         cout << el << ' ';
//     }
//     cout << '\n';
// }
// void print(VVI& a) {
//     for (auto el : a) {
//         print(el);
//     }
// }
const int MOD = 1e9 + 7;
const int MOD1 = 998244353;//7*17*2^23 +1;
const int INF = 1.07e9;
const ll INFF = INT64_MAX;
const db EPS = 1e-9;
const db PI = acos(-1.0); //M_PI;
const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0, 1, 0, -1};
/*
|      ⣠⣶⡾⠏⠉⠙⠳⢦⡀⠀  ⠀⢠⠞⠉⠙⠲⡀
|    ⣴⠿⠏⠀⠀⠀⠀⠀⠀⢳⡀⠀  ⡏⠀⠀ ⠀⠀ ⢷
|⠀⠀⢠⣟⣋⡀⢀⣀⣀⡀⠀⣀⡀⣧⠀  ⢸⠀⠀⠀⠀   ⡇ ⠀
| ⠀⢸⣯⡭⠁⠸⣛⣟⠆⡴⣻⡲⣿   ⣸⠀⠀OK.  ⡇
|⠀⠀⣟⣿⡭⠀⠀⠀⠀⠀⢱⠀⠀⣿   ⢹⠀⠀⠀⠀⠀ ⡇
|⠀⠀⠙⢿⣯⠄⠀⠀⠀⢀⡀⠀⠀⡿⠀  ⡇⠀⠀ ⠀ ⡼
|⠀⠀⠀⠀⠹⣶⠆⠀⠀⠀⠀⠀⡴⠃ ⠀ ⠘⠤⣄⣠⠞⠀ ⠀
|⠀⠀⠀⠀⢸⣷⡦⢤⡤⢤⣞⣁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ ⠀
|⠀⢀⣤⣴⣿⣏⠁⠀⠀⠸⣏⢯⣷⣖⣦⡀⠀⠀⠀⠀⠀⠀
|⢀⣾⣽⣿⣿⣿⣿⠛⢲⣶⣾⢉⡷⣿⣿⠵⣿⠀⠀⠀⠀⠀
|⣼⣿⠍⠉⣿⡭⠉⠙⢺⣇⣼⡏⠀ ⠀⣄⢸⣿
|⣿⣿⣧⣀⣿.........⣀⣰⣏⣘⣆⣀
      
|                                                    _         _   _
|                                                   (_)       | | | |
 _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_
| '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|
| |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_
| .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|
| |               __/ |
|_|              |___/
 _   _      _ _    __        __         _     _ 
| | | | ___| | | __\ \      / /__  _ __| | __| |
| |_| |/ _ \ | |/ _ \ \ /\ / / _ \| '__| |/ _` |
|  _  |  __/ | | (_) \ V  V / (_) | |  | | (_| |
|_| |_|\___|_|_|\___/ \_/\_/ \___/|_|  |_|\__,_|                                  
 ____     __    _______    _______   __________
|     \  |  |  |  _____|  /  _____) |____  ____|
|  |\  \ |  |  | |__     (  (_____      |  |
|  | \  \|  |  |  __|     \_____  \     |  | 
|  |  \     |  | |_____    _____)  )    |  | 
|__|   \____|  |_______|  (_______/     |__| 
 
 
 


*/











int n, m;
const int N = 2e5+5;
struct Group {
    ll sum, cnt, id;
    Group() {}
    void init(ll a, ll b, ll c) {
        sum = a; 
        cnt = b; 
        id = c; 
    }
    bool operator < (const Group& a) const {
        return sum*a.cnt > a.sum*cnt;
    }
} tmp;
VLL teachers;
pair <int, int> pos[N];

ll a[N], b[N], c[N];

void go () {
    cin >> n >> m;
    teachers = readVLL(n);
    sort(LLA(teachers));
    vector <Group> groups;
    n = m;
    for (int i = 0; i < n+1; i++) {
        a[i] = b[i] = c[i] = 0;
    }
    int studentId = 1;
    for (int i = 1; i <= n; i++) {
        int k, x; cin >> k;
        ll sum = 0;
        for (int j = 0; j < k; j++) {
            cin >> x;
            sum += x;
            pos[studentId++] = {x, i};
        }
        tmp.init(sum, k, i);
        groups.pb(tmp);
    }
    sort(ALL(groups));
    int x = INF, y = 0, pos1[n+1];
    for (int i = 0; i < n; i++) {
        pos1[groups[i].id] = i;
        if (teachers[i]*groups[i].cnt >= groups[i].sum) a[i] = 1;
        if (i+1 < n && teachers[i+1]*groups[i].cnt >= groups[i].sum) b[i] = 1;
        if (i-1 >=0 && teachers[i-1]*groups[i].cnt >= groups[i].sum) c[i] = 1;
        if (a[i] == 0) {
            x = min(x, i);
            y++;
        }
    }
    for (int i = 1; i < n; i++) {
        a[i] += a[i-1];
        b[i] += b[i-1];
        c[i] += c[i-1];
    }
    for (int i = 1; i < studentId; i++) {
        int val, seg;
        tie(val, seg) = pos[i];
        seg = pos1[seg];
        Group& oldGroup = groups[seg];
        Group newGroup = oldGroup;
        newGroup.sum -= val;
        newGroup.cnt--;
        int l = 0, r = n-1, tar = r;
        while(l <= r) {
            int mid = (l+r)>>1;
            if (teachers[mid]*newGroup.cnt >= newGroup.sum) {
                tar = mid;
                l = mid+1;
            } else {
                r = mid-1;
            }
        }
        if (y == 0) { // all can match currently
            if (newGroup.sum*oldGroup.cnt <= oldGroup.sum*newGroup.cnt) {
                cout << 1;
                continue;
            }
            l = tar, r = seg;
            if (
                teachers[tar]*newGroup.cnt >= newGroup.sum &&
                (r == 0 || b[r-1] - (l==0 ? 0 : b[l-1]) == r-l)
                ) cout << 1;
            else  
                cout << 0;
        } else { // all can't match currently
            if (newGroup.sum*oldGroup.cnt >= oldGroup.sum*newGroup.cnt) {
                cout << 0;
                continue;
            }
            l = seg, r = tar;
            if (
                (teachers[tar]*newGroup.cnt >= newGroup.sum) &&
                (l <= x) &&
                (c[r] - c[l] == r - l) &&
                a[l-1] == l &&
                a[n-1] - a[r] == n-1-r
                ) cout << 1;
            else  
                cout << 0;
        }
    }
    cout << "\n";
}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
#endif
    _upgrade
    int T = 1;
    cin >> T;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














