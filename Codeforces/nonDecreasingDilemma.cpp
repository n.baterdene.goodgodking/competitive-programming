#include <bits/stdc++.h>
// #define int long long
using namespace std;
// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))

#define _upgrade ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define rs resize
#define ins insert 
#define fr front() 
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define umap unordered_map
#define sz(x) (int)x.size()

typedef long long ll;
typedef double db;
typedef unsigned uint;
typedef unsigned long long ull;
typedef unordered_map<int, int> umapII;
// PQ going up <int, VI, greater<int> >
typedef vector<int> VI;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<bool> VB;
typedef vector<VB> VVB;
typedef vector<VVB> VVVB;
typedef vector<ll> VLL;
typedef vector<VLL> VVLL;
typedef vector<VVLL> VVVLL;
typedef vector<VVVLL> VVVVLL;
typedef vector<db> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef pair<int, int> PII;
typedef pair<db, db> PDD;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VPI> VVPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
typedef vector<VVVI> VVVVI;
typedef vector<VVVVI> VVVVVI;
VI readVI(int n) {
    VI a(n);
    REP(i, n) cin >> a[i];
    return a;
}
/*
|      ⣠⣶⡾⠏⠉⠙⠳⢦⡀⠀  ⠀⢠⠞⠉⠙⠲⡀
|    ⣴⠿⠏⠀⠀⠀⠀⠀⠀⢳⡀⠀  ⡏⠀⠀ ⠀⠀ ⢷
|⠀⠀⢠⣟⣋⡀⢀⣀⣀⡀⠀⣀⡀⣧⠀  ⢸⠀⠀⠀⠀   ⡇ ⠀
| ⠀⢸⣯⡭⠁⠸⣛⣟⠆⡴⣻⡲⣿   ⣸⠀⠀OK.  ⡇
|⠀⠀⣟⣿⡭⠀⠀⠀⠀⠀⢱⠀⠀⣿   ⢹⠀⠀⠀⠀⠀ ⡇
|⠀⠀⠙⢿⣯⠄⠀⠀⠀⢀⡀⠀⠀⡿⠀  ⡇⠀⠀ ⠀ ⡼
|⠀⠀⠀⠀⠹⣶⠆⠀⠀⠀⠀⠀⡴⠃ ⠀ ⠘⠤⣄⣠⠞⠀ ⠀
|⠀⠀⠀⠀⢸⣷⡦⢤⡤⢤⣞⣁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ ⠀
|⠀⢀⣤⣴⣿⣏⠁⠀⠀⠸⣏⢯⣷⣖⣦⡀⠀⠀⠀⠀⠀⠀
|⢀⣾⣽⣿⣿⣿⣿⠛⢲⣶⣾⢉⡷⣿⣿⠵⣿⠀⠀⠀⠀⠀
|⣼⣿⠍⠉⣿⡭⠉⠙⢺⣇⣼⡏⠀ ⠀⣄⢸⣿
|⣿⣿⣧⣀⣿.........⣀⣰⣏⣘⣆⣀
      
|                                                    _         _   _
|                                                   (_)       | | | |
 _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_
| '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|
| |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_
| .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|
| |               __/ |
|_|              |___/
 _   _      _ _    __        __         _     _ 
| | | | ___| | | __\ \      / /__  _ __| | __| |
| |_| |/ _ \ | |/ _ \ \ /\ / / _ \| '__| |/ _` |
|  _  |  __/ | | (_) \ V  V / (_) | |  | | (_| |
|_| |_|\___|_|_|\___/ \_/\_/ \___/|_|  |_|\__,_|                                  
 ____     __    _______    _______   __________
|     \  |  |  |  _____|  /  _____) |____  ____|
|  |\  \ |  |  | |__     (  (_____      |  |
|  | \  \|  |  |  __|     \_____  \     |  | 
|  |  \     |  | |_____    _____)  )    |  | 
|__|   \____|  |_______|  (_______/     |__| 
 
 
 


*/





VI nums;
ll Value(ll a) {
    return a * (a+1) / 2;
}
int curLen;
struct Node {
    ll val;
    int lSize, rSize;
    bool oneArr;
    Node *left, *right;
    Node() {
        oneArr = lSize = rSize = val = 0;
        left = right = NULL;
    }
    void fix(int mid) {
        val = left->val + right->val;
        lSize = left->lSize;
        rSize = right->rSize;
        oneArr = 0;
        if (nums[mid] <= nums[mid+1]) {
            curLen = left->rSize + right->lSize;
            val -= Value(left->rSize) + Value(right->lSize) - Value(curLen);
            if (left->oneArr) lSize = curLen;
            if (right->oneArr) rSize = curLen;
            oneArr = (left->oneArr & right->oneArr);
        }
    }
};
Node* fix(Node*l, Node*r, int mid) {
    Node* res = new Node();
    res->val = l->val + r->val;
    res->lSize = l->lSize;
    res->rSize = r->rSize;
    if (nums[mid] <= nums[mid+1]) {
        curLen = l->rSize + r->lSize;
        res->val -= Value(l->rSize) + Value(r->lSize) - Value(curLen);
        if (l->oneArr) res->lSize = curLen;
        if (r->oneArr) res->rSize = curLen;
        res->oneArr = (l->oneArr & r->oneArr);
    }
    return res;
}
void build(int st, int end, Node *head) {
    if (st == end) {
        head->lSize = head->rSize = 1;
        head->val = 1;
        head->oneArr = 1;
        return;
    }
    int mid = (st + end) >> 1;
    head->left = new Node();
    head->right = new Node();
    build(st, mid, head->left);
    build(mid + 1, end, head->right);
    head->fix(mid);
}
Node *edge = new Node();
Node* query(int st, int end, int l, int r, Node *head) {
    if (end < l || st > r) return edge;
    if (l <= st && end <= r) return head;
    int mid = (st + end) >> 1;
    return fix(query(st, mid, l, r, head->left), query(mid + 1, end, l, r, head->right), mid);
}
void update(int st, int end, int idx, int num, Node *head) {
    if (st == end) {
        nums[idx] = num;
        return;
    }
    int mid = (st + end) >> 1;
    if (mid >= idx) update(st, mid, idx, num, head->left);
    else update(mid + 1, end, idx, num, head->right);
    head->fix(mid);
}
ll num, n, m, x;
signed main () {

#ifndef ONLINE_JUDGE
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
#endif
    _upgrade
    cin >> n >> m;
    nums = readVI(n);
    Node *root = new Node();
    build(0, n-1, root);
    for (int i = 0; i < m; i++) {
        int a, b, c;
        cin >> a >> b >> c;
        if (a == 1) update(0, n-1, b-1, c, root);
        else cout << query(0, n-1, b-1, c-1, root)->val << '\n';
        
    }
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














