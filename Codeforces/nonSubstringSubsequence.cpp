#include <bits/stdc++.h>
using namespace std;

int n, m;
string s, tmp;
const int N = 105;
bool solve(string tmp) {
    int m = tmp.size(), idx = 0;
    for (int i = 0; idx < m && i < n; i++) 
        if (s[i] == tmp[idx]) idx++;

    return idx == m;
}
int main () {
    int t = 1;
    cin >> t;
    while(t--) {
        cin >> n >> m >> s;
        while(m--) {
            int a, b;
            bool done = 0;
            cin >> a >> b;
            tmp = "";
            for (int i = a - 1; i < b; i++)
                tmp += s[i];

            for (int i = 0; i < tmp.size(); i++) 
            if (solve(tmp.substr(0, i) + tmp[i] + tmp.substr(i))) {
                done = 1;
                break;
            }
            
            if (!done) cout << "NO\n";
            else cout << "YES\n";
        }
    }
    return 0;
}