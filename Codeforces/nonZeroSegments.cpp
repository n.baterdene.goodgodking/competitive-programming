#include <bits/stdc++.h>
using namespace std;
int main () {
    int n, ans = 0;
    cin >> n;
    map <long long, bool> seen;
    long long sum = 0, num;
    seen[0] = 1;
    for (int i = 0; i < n; i++) {
        cin >> num;
        sum += num;
        if (seen.find(sum) != seen.end()) {
            ans++;
            seen.clear();
            seen[0] = 1;
            sum = num;
        } 
        seen[sum] = 1;
    }
    cout << ans << '\n';
    return 0;
}