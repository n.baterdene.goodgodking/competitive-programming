#include <bits/stdc++.h>
#define pb push_back
using namespace std;
int main () {
     string line[3][10] = {
        {" _  ", "     ", "  _  ", " _  ", "     ", "  _  ", "  _  ", " _  ", "  _  ", "  _  "},
        {"| | ", "   | ", "  _| ", " _| ", " |_| ", " |_  ", " |_  ", "  | ", " |_| ", " |_| "},
        {"|_| ", "   | ", " |_  ", " _| ", "   | ", "  _| ", " |_| ", "  | ", " |_| ", "  _| "},
    };
    string myNumber;
    cin >> myNumber;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < myNumber.size(); j++) 
            cout << line[i][int(myNumber[j] - '0')];
        cout << '\n';
    }
    return 0;
}