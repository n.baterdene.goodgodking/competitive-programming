#include <bits/stdc++.h>
using namespace std;

int main () {
    int t = 1;
    cin >> t;
    while(t--) {
        int n, m, sum = 0, mini = INT_MAX, num, cnt = 0;
        cin >> n >> m;
        for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++) {
            cin >> num;
            sum += abs(num);
            mini = min(mini, abs(num));
            if (num < 0) cnt++;
        }
        cout << sum - (cnt & 1 ? mini * 2 : 0) << '\n';
    }
    return 0;
}