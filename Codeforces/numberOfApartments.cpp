#include <bits/stdc++.h>
using namespace std;

int main () {
    int t;
    cin >> t;
    while(t--) {
        int n;
        cin >> n;
        if (n == 4 || n < 3) {
            cout << "-1\n";
            continue;
        }
        vector <int> ans(10, 0);
        ans[3] = (n / 3) - 1;
        int rem = n % 3;
        if (rem == 0) ans[3]++;
        if (rem == 1) ans[3]--, ans[7]++;
        if (rem == 2) ans[5]++;

        // n = 0,1,2 mod(3) -> 3 => 0, 5  => 2, 7 =>1 n 
        cout << ans[3] << ' ' << ans[5] << ' ' << ans[7] << '\n';
    }
    return 0;
}
