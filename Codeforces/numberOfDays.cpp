#include <bits/stdc++.h>
using namespace std;
#define FOR(i, a, b) for (int i = a; i < b; i++)
#define FORB(i, a, b) for (int i = a - 1; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i <= a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define pb push_back
#define mp make_pair
#define umap unordered_map
#define ff first
#define ss second
 
typedef long long ll;
typedef double d;
typedef unsigned uint;
typedef unsigned long long ull;
 
typedef vector<int> VI;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<ll> VLL;
typedef vector<VLL> VVLL;
typedef vector<d> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
 
const int MOD = int(1e9) + 7;
const int INF = INT_MAX;
const ll INFF = INT64_MAX;
const d EPS = 1e-9;
const d PI = acos(-1.0); //M_PI;
const int moveX[] = {-1, 0, 1, 0};
const int moveY[] = {0, 1, 0, -1};
class Solution {
public:

    int change(string a) {
        stringstream b(a);
        int tmp;
        a >> tmp;
        return tmp;
    }
    bool x (int a) {
        if (a % 400 == 0) return 1;
        if (a % 100 == 0) return 0;
        if (a % 4 == 0) return 1;
        return 0;
    }
    int add(int a, int b) {
        int sum = 0;
        FOR(i, a, b + 1) {
            if (x(i)) sum++;
            sum += 365;
        }
    }
    int add1(int a, int b, int c, int d) {
        a--, b--;
        
        int sum = 0;
        REP(i, 12)
        FOR(i, a - 1, b) {
            sum += months[i];
            if (i == 1 && x(d)) sum++;
        }
        return sum;
    }

    int add1;
    int months[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    int daysBetweenDates(string date1, string date2) {
        int year1, year2, month1, month2, day1, day2;
        string tmp;
        tmp = date1.substr(0, 4);
        year1 = change(tmp);
        tmp = date2.substr(0, 4);
        year2 = change(tmp);
        tmp = date1.substr(5, 2);
        month1 = change(tmp);
        tmp = date2.substr(5, 2);
        month2 = change(tmp);
        tmp = date1.substr(8, 2);
        day1 = change(tmp);
        tmp = date2.substr(8, 2);
        day2 = change(tmp);
        int day = add(year1, year2);
        day += add1(month1, month2, year1, year2);
        day += (day2 - day1);
        return day;
    }
};
