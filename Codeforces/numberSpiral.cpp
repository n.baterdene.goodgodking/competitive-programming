#include <iostream>
#define int long long
using namespace std;
signed main () {
    int n, x, y, ans;
    cin >> n;
    for(int i = 0; i < n; i++) {
        cin >> y >> x;
        int used = max(x, y) - 1;
        int counted = used * used;
        if(x > y) {
            //baruun
            if (used % 2 == 1) 
                ans = counted + y;
            else 
                ans = counted + used + used - y + 2;
        } else {
            //door
            if (used % 2 == 0) 
                ans = counted + x;
            else 
                ans = counted + used + used - x + 2;
        }
        cout << ans << '\n';
    }

    return 0;
}