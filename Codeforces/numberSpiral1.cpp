#include <bits/stdc++.h>
using namespace std;
int main () {
    int n;
    cin >> n;
    int x = 0, y = 0;
    int ans[n + 5][n + 5];
    memset(ans, 0, sizeof(ans));
    int moveX[] = {1, 0, -1, 0};
    int moveY[] = {0, 1, 0, -1};
    int dir = 0, cnt = 1;

    while(cnt <= n * n) {
        if (ans[y][x] == 0) ans[y][x] = cnt++;
        int newX = x + moveX[dir], newY = y + moveY[dir];
        if (newX < 0 || newY < 0 || newX >= n || newY >= n || ans[newY][newX] != 0) {
            dir = (dir + 1) % 4;
        } else {
            x = newX, y = newY;
        }
    }

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            cout << setw(2) << ans[i][j] << ' ';
        }
        cout << '\n';
    }
    return 0;
}