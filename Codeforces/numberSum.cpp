#include <bits/stdc++.h>
using namespace std;
int main () {
    string a, b;
    cin >> a >> b;
    if (a.size() > b.size()) swap(a, b);
    b = '0' + b;
    while(a.size() < b.size()) a = '0' + a;
    int add = 0;
    for (int i = a.size() - 1; i >= 0; i--) {
        int x = a[i] - '0', y = b[i] - '0';
        a[i] = (x + y + add) % 10 + '0';
        add = (x + y + add) / 10;
    }
    if (a[0] == '0') {
        reverse(a.begin(), a.end());
        a.pop_back();
        reverse(a.begin(), a.end());
    }
    cout << a << '\n';
    return 0;
}