#include <bits/stdc++.h>
using namespace std;
int main () {
    int t = 1;
    cin >> t;
    while(t--) {
        long long n, k;
        cin >> n >> k;
        if (k & 1) k = 1;
        else k = 2;
        vector <int> nums(n);
        for (int i = 0; i < n; i++) cin >> nums[i];
        for (int j = 0; j < k; j++) {
            int maxi = nums[0];
            for (int i = 0; i < n; i++) maxi = max(maxi, nums[i]);
            for (int i = 0; i < n; i++) nums[i] = maxi - nums[i];
        }
        for (int i = 0; i < n; i++) cout << nums[i] << ' ';
        cout << '\n';
    }
    return 0;
}