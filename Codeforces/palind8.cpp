#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
using namespace std;
int curNum;
string aToBaseB(int n, int m) {
    string ans = "";
    while(n != 0) {
        ans += to_string(n % m);
        n /= m;
    }
    reverse(ans.begin(), ans.end());
    return ans;
}
int returnBase(string n, int m) {
    int ans = 0, cur = 1;
    // cout << "haha I'm going to become a crazy man: " << n << ' ' << ans << ' ' << cur << " " << m << '\n';
    for (int i = 1; i <= n.size(); i++) {
        if (n[n.size() - i] == '\0') continue;
        ans += (n[n.size() - i] - '0') * cur;
        cur *= m;
    }
    // cout << "convert: " << n << ' ' << ans << ' ' << m << ' ' << '\n';
    return ans;
}
int strToInt(string str) {
    stringstream tmp(str);
    int num;
    tmp >> num;
    return num;
}
bool checkPal(string number) {
    int st = 0, end = number.size() - 1;
    while (st < end) {
        if (number[st] != number[end]) return false;
        st++, end--;
    }
    return true;
}
int fix(string str, int add) {
    // cout << "hereherheherhehrehr: " << str << ' ';
    int num = strToInt(str);
    num = returnBase(to_string(num), 8);
    num += add;
    string tmp = aToBaseB(num, 8);
    // cout << tmp << '\n';
    return strToInt(tmp);
}
string findNext(string number) {
    if (checkPal(number)) return number;
    int n = number.size();
    int equal = n / 2;
    string firstHalf, secondHalf;
    char mid = '\0';
    if (n & 1) mid = number[equal];
    firstHalf = number.substr(0, equal);
    secondHalf = number.substr(n - equal);
    // cout << "here: " << number << ' ' << firstHalf << ' ' << secondHalf << '\n';
    string numFirstHalf;
    numFirstHalf = firstHalf;
    reverse(numFirstHalf.begin(), numFirstHalf.end());
    int num1 = strToInt(numFirstHalf);
    int num2 = strToInt(secondHalf);
    string up, low;
    if (num1 == num2) return number;
    // cout << "first half and second half: " << num1 << ' ' << num2 << '\n';
    bool cont = true;
    if (num1 > num2) {
        up = firstHalf + mid + numFirstHalf;
        if (n & 1 && mid - 1 != '0' - 1) {
            mid--;
            low = firstHalf + mid + numFirstHalf;
        } else {
            num1 = fix (firstHalf, -1);
            if (to_string(num1).size() != equal) cont = false;
            if (cont) {
                numFirstHalf = to_string(num1);
                firstHalf = numFirstHalf;
                reverse(firstHalf.begin(), firstHalf.end());
                low = firstHalf + mid + numFirstHalf;
            }
        }
    } else {
        low = firstHalf + mid + numFirstHalf;
        if (n & 1 && mid + 1 != '8') {
            mid++;
            up = firstHalf + mid + numFirstHalf;
        } else {
            num1 = fix (firstHalf, 1);
            if (to_string(num1).size() != equal) cont = false;
            if (cont) {
                numFirstHalf = to_string(num1);
                firstHalf = numFirstHalf;
                reverse(firstHalf.begin(), firstHalf.end());
                up = numFirstHalf + mid + firstHalf;
            }
        }
    }
    // cout << "wakanda fureva: " << low << ' ' << 8 << '\n';
    num1 = returnBase(low, 8);
    num2 = returnBase(up, 8);
    // cout << "lower and upper pal: " << low << ' ' << up << '\n';
    // cout << "in base 8 to 10: " << num1 << ' ' << num2 << "\n";
    if (abs(curNum - num1) >= abs(curNum - num2)) return up;
    return low;
}
vector <int> answer;
void go() {
    cin >> curNum;
    string number = aToBaseB(curNum, 8);
    // cout << "in base: " << curNum << ' ' << number << '\n';
    string nextPal = findNext(number);
    // cout << returnBase(nextPal, 8) << '\n';
    answer.pb(returnBase(nextPal, 8));
    // cout << "----------------\n";
}
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int T;
    cin >> T;
    while (T--) go();
    for (int i = 0; i < answer.size(); i++) 
        cout << answer[i] << ' ';
    cout << '\n';
    return 0;
}