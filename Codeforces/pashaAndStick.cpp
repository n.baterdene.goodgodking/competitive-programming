#include <bits/stdc++.h>
using namespace std;

int main () {
    long long n;
    cin >> n;
    if (n & 1) n = 0;
    cout << ((n / 2) - 1) / 2 << '\n'; //  a+b = n/2 1+(n-1) = 2+(n-2) = ... = (n-1)+1
    return 0;
}
// 1 2 3 4 .. 9
// 5 7 8 9