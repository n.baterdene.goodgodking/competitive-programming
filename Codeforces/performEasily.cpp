// CPP program to illustrate substr() 
#include <bits/stdc++.h> 
using namespace std;
int main () {
    const int N = 6;
    int n;
    vector <int> cords(N), frets(1e5 + 5);
    set <vector <int> > bfs;
    for (int i = 0; i < N; i++) cin >> cords[i];
    cin >> n;
    sort(cords.begin(), cords.end());
    for (int i = 0; i < n; i++) {
        cin >> frets[i];
        bfs.insert({frets[i] - cords.back(), frets[i], 5});
    }
    int ans = INT_MAX;
    while (!bfs.empty()) {
        vector <int> l = *bfs.begin();
        vector <int> r = *(--bfs.end());
        ans = min(ans, r[0] - l[0]);
        if (l[2] == 0) break;
        bfs.erase(bfs.begin());
        bfs.insert({l[1] - cords[l[2] - 1], l[1], l[2] - 1});
    }
    cout << ans << '\n';
    return 0;
}
