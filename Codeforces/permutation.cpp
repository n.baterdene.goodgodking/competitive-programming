#include <iostream>
#include <set>
#include <vector>
#include <algorithm>
#include <map>
#include <cmath>
#include <cstring>
using namespace std;

vector < vector < int > > ans;
vector < int > now;
int n;
bool used[11];

void rec(int len){
    if( len == n ){
        ans.push_back(now);
        return;
    }

    for(int i = 1 ; i <= n ; i++){
        if( !used[i] ){
            used[i] = 1;
            now.push_back(i);

            rec( len+1 );

            used[i] = 0;
            now.pop_back();
        }
    }
}

int main () {
    cin >> n;
    rec( 0 );

    for(int i = 0 ; i < ans.size() ; i++){
        for(int j = 0 ; j < n ; j++){
            // cout << "Б ";
            cout << ans[i][j] << ' ';
        } cout << endl;
    }
}