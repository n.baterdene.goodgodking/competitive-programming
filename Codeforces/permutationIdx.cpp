#include<bits/stdc++.h>

using namespace std;

int fact(int n) {
    return (n <= 1) ? 1 : n * fact(n - 1);
}
int findSmallerInRight(vector <int>& arr, int low, int high) {
    int countRight = 0, i;
    for (i = low + 1; i <= high; ++i)
        if (arr[i] < arr[low])
            ++countRight;
    return countRight;
}
int findRank(vector <int>& arr) {
    int len = arr.size();
    int mul = fact(len);
    int rank = 1;
    int countRight;
    int i;
    for (i = 0; i < len; ++i) {
        mul /= len - i;
        countRight = findSmallerInRight(arr, i, len - 1);
        rank += countRight * mul;
    }
    return rank;
}
int main() {
	vector <int> arr = {1,2,5,4,3};
	cout << findRank(arr) << "\n";
}
