#include <iostream>
using namespace std;
int main () {
    int n;
    cin >> n;
    if (n == 1) {
        cout << 1 << ' ';
        return 0    ;
    }
    if (n < 4) {
        cout << "NO SOLUTION\n";
        return 0;
    }
    int num = 2;
    while (num <= n) {
        cout << num << ' ';
        num += 2;
    }
    num = 1;
    while(num <= n) {
        cout << num << ' ';
        num += 2;
    }
    return 0;
}