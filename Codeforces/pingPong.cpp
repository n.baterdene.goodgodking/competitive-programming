#include <bits/stdc++.h>
using namespace std;
// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))

#define _upgrade ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define II <int, int>
#define Q queue
#define ff first
#define bk back()
#define ss second
#define rs resize
#define ins insert 
#define fr front() 
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define PQ priority_queue
#define umap unordered_map
#define sz(x) (int)x.size()

typedef long long ll;
typedef double db;
typedef unsigned uint;
typedef unsigned long long ull;
typedef unordered_map<int, int> umapII;
// PQ going up <int, VI, greater<int> >
typedef vector<int> VI;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<bool> VB;
typedef vector<VB> VVB;
typedef vector<VVB> VVVB;
typedef vector<umapII> VumapII;
typedef vector<ll> VLL;
typedef vector<VLL> VVLL;
typedef vector<VVLL> VVVLL;
typedef vector<VVVLL> VVVVLL;
typedef vector<db> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VPI> VVPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
typedef vector<VVVI> VVVVI;
typedef vector<VVVVI> VVVVVI;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
 

int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
void readI(int& a) {
    cin >> a;
}
void readI(int& a, int&b) {
    cin >> a >> b;
}
void readI(int& a, int&b, int&c) {
    cin >> a >> b >> c;
}
VI readVI(int n) {
    VI a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVI readVVI(int n, int m) {
    VVI a(n, VI(m));
    REP(i, n) a[i] = readVI(m);
    return a;
}
VLL readVLL(ll n) {
    VLL a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVLL readVVLL(ll n, ll m) {
    VVLL a(n, VLL(m));
    REP(i, n) a[i] = readVLL(m);
    return a;
}

void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}

void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
const int MOD = 1e9 + 7;
const int INF = INT_MAX;
const ll INFF = INT64_MAX;
const db EPS = 1e-9;
const db PI = acos(-1.0); //M_PI;
const int moveX[] = {-1, 0, 1, 0};
const int moveY[] = {0, 1, 0, -1};
/*
//       ⣠⣶⡾⠏⠉⠙⠳⢦⡀⠀  ⠀⢠⠞⠉⠙⠲⡀
//   ⠀⠀⣴⠿⠏⠀⠀⠀⠀⠀⠀⢳⡀⠀  ⡏⠀⠀ ⠀⠀ ⢷
// ⠀⠀⢠⣟⣋⡀⢀⣀⣀⡀⠀⣀⡀⣧⠀  ⢸⠀⠀⠀⠀   ⡇ ⠀
//  ⠀⢸⣯⡭⠁⠸⣛⣟⠆⡴⣻⡲⣿   ⣸⠀⠀OK.  ⡇
// ⠀⠀⣟⣿⡭⠀⠀⠀⠀⠀⢱⠀⠀⣿   ⢹⠀⠀⠀⠀⠀ ⡇
// ⠀⠀⠙⢿⣯⠄⠀⠀⠀⢀⡀⠀⠀⡿⠀  ⡇⠀⠀ ⠀ ⡼
// ⠀⠀⠀⠀⠹⣶⠆⠀⠀⠀⠀⠀⡴⠃ ⠀ ⠘⠤⣄⣠⠞⠀ ⠀
// ⠀⠀⠀⠀⢸⣷⡦⢤⡤⢤⣞⣁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ ⠀
// ⠀⢀⣤⣴⣿⣏⠁⠀⠀⠸⣏⢯⣷⣖⣦⡀⠀⠀⠀⠀⠀⠀
// ⢀⣾⣽⣿⣿⣿⣿⠛⢲⣶⣾⢉⡷⣿⣿⠵⣿⠀⠀⠀⠀⠀
// ⣼⣿⠍⠉⣿⡭⠉⠙⢺⣇⣼⡏⠀ ⠀⣄⢸⣿
// ⣿⣿⣧⣀⣿.........⣀⣰⣏⣘⣆⣀
      
|                                                   _         _   _
|                                                   (_)       | | | |
|_ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_
| '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|
| |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_
| .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|
| |               __/ |
|_|              |___/
 _   _      _ _    __        __         _     _ 
| | | | ___| | | __\ \      / /__  _ __| | __| |
| |_| |/ _ \ | |/ _ \ \ /\ / / _ \| '__| |/ _` |
|  _  |  __/ | | (_) \ V  V / (_) | |  | | (_| |
|_| |_|\___|_|_|\___/ \_/\_/ \___/|_|  |_|\__,_|                                  


 
 
 


*/










int findDist(VI&a, VI&b) {
    return abs(a[0] - b[0]) + abs(a[1] - b[1]);
}

void go () {
    VVI points = readVVI(4, 2);
    VVI points1 = points, tmp;
    for (auto el : points) swap(el[0], el[1]); 
    sort(ALL(points));
    sort(ALL(points1));
    int a, b, c, d, n;
    int ans = points[3][0] - points[2][0] + points[1][0] - points[0][0] + 2 * (points[2][0] - points[1][0]);
    ans += points1[3][1] - points1[2][1] + points1[1][1] - points1[0][1] + 2 * (points1[2][1] - points1[1][1]);
    // ans = INF;
    VB used(4, 0);
    a = points[0][0];
    b = points[3][0];
    c = points1[0][0];
    d = points1[3][0];
    n = max(b - a, d - c);
    tmp = {{a, c}, {a, c + n}, {b, c}, {b, c + n}};
    for (int i = 0; i < 4; i++) {
        used[i] = 1;
        for (int j = 0; j < 4; j++) {
            if (used[j]) continue;
            used[j] = 1;
            for (int k = 0; k < 4; k ++) {
                if (used[k]) continue;
                used[k] = 1;
                for (int m = 0; m < 4; m++) {
                    if (used[m]) continue;
                    used[k] = 1;
                    int curAns = 0;
                    REP(I, 4) curAns += findDist(tmp[I], points[I]);
                    ans = min(ans, curAns);
                    used[k] = 0;
                }
                used[k] = 0;
            }
            used[j] = 0;
        }
        used[i] = 0;
    }
    a = points[0][0];
    b = points[3][0];
    c = points1[0][0];
    d = points1[3][0];
    n = min(b - a, d - c);
    tmp = {{a, c}, {a, c + n}, {b, c}, {b, c + n}};
    for (int i = 0; i < 4; i++) {
        used[i] = 1;
        for (int j = 0; j < 4; j++) {
            if (used[j]) continue;
            used[j] = 1;
            for (int k = 0; k < 4; k ++) {
                if (used[k]) continue;
                used[k] = 1;
                for (int m = 0; m < 4; m++) {
                    if (used[m]) continue;
                    used[k] = 1;
                    int curAns = 0;
                    REP(I, 4) curAns += findDist(tmp[I], points[I]);
                    ans = min(ans, curAns);
                    used[k] = 0;
                }
                used[k] = 0;
            }
            used[j] = 0;
        }
        used[i] = 0;
    }
    a = points[1][0];
    b = points[2][0];
    c = points1[1][0];
    d = points1[2][0];
    n = min(b - a, d - c);
    tmp = {{a, c}, {a, c + n}, {b, c}, {b, c + n}};
    for (int i = 0; i < 4; i++) {
        used[i] = 1;
        for (int j = 0; j < 4; j++) {
            if (used[j]) continue;
            used[j] = 1;
            for (int k = 0; k < 4; k ++) {
                if (used[k]) continue;
                used[k] = 1;
                for (int m = 0; m < 4; m++) {
                    if (used[m]) continue;
                    used[m] = 1;
                    int curAns = 0;
                    REP(I, 4) curAns += findDist(tmp[I], points[I]);
                    ans = min(ans, curAns);
                    used[m] = 0;
                }
                used[k] = 0;
            }
            used[j] = 0;
        }
        used[i] = 0;
    }
    a = points[1][0];
    b = points[2][0];
    c = points1[1][0];
    d = points1[2][0];
    n = max(b - a, d - c);
    tmp = {{a, c}, {a, c + n}, {b, c}, {b, c + n}};
    for (int i = 0; i < 4; i++) {
        used[i] = 1;
        for (int j = 0; j < 4; j++) {
            if (used[j]) continue;
            used[j] = 1;
            for (int k = 0; k < 4; k ++) {
                if (used[k]) continue;
                used[k] = 1;
                for (int m = 0; m < 4; m++) {
                    if (used[m]) continue;
                    used[k] = 1;
                    int curAns = 0;
                    REP(I, 4) curAns += findDist(tmp[I], points[I]);
                    ans = min(ans, curAns);
                    used[k] = 0;
                }
                used[k] = 0;
            }
            used[j] = 0;
        }
        used[i] = 0;
    }
    cout << ans << '\n';
}



signed main () {

#ifdef ONLINE_JUDGE
#else
    freopen("in.txt", "r", stdin);
//	freopen("out.txt", "w", stdout);
#endif
    _upgrade
    int T = 1;
    cin >> T;
    while(T--) go();
    return 0;
}

/* stuff you should look for
	* int overflow, array bounds
	* special cases (n=1?)
*/