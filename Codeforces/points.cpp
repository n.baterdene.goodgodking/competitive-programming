#include <iostream>
#include <vector>
using namespace std;
int a, b;
struct Point {
    vector <pair <int, int> > points;
    void pointmove(int number, char move) {
        if(move == 'u') points[number].first++;
        if(move == 'd') points[number].first--;
        if(move == 'l') points[number].second--;
        if(move == 'r') points[number].second++;
    }
    void read() {
        cin >> a >> b;
        points.push_back(make_pair(a, b));
        cin >> a >> b;
        points.push_back(make_pair(a, b));
    }
    void print() {
        cout << points[0].first << ' ' << points[0].second << '\n';
        cout << points[1].first << ' ' << points[1].second << '\n';
    }
};

int main () {
    Point board;
    board.read();
    int moves;
    cin >> moves;
    char move;
    int number;
    for(int i = 0; i < moves; i++) {
        cin >> number >> move;
        board.pointmove(number, move);
    }
    board.print();
    return 0;
}