#include <bits/stdc++.h> 
using namespace std; 
#define ll long long
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
bool solve(ll num, ll pow, ll mod) {
	ll ans = 1;
	for (int i = 0; i <= 32; i++) {
		if ((1<<i) & pow) ans = (ans * num) % mod;
		num = (num * num) % mod;
	}
	return (ans != 1);
}
bool check(ll num) {
	if (num <= 1) return 0;
	for (int i = 0; i < 99; i++) {
 
		ll randNum = rng() % (num - 1) + 1;
		if (solve(randNum, num - 1, num)) return 0;
	}
	return 1;
}
int main () {
	// freopen("in.txt", "r", stdin);
	// srand(time(0));
	ll num;
	while(cin >> num) {
		// cout << num << " : ";
		if (check(num)) cout << "YES\n";
		else cout << "NO\n";
	}
} 