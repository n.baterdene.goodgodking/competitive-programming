#include <bits/stdc++.h>
using namespace std;

bool checkIfPrime(int num) {
	for (int i = 2; i <= sqrt(num) + 1 && i < num; i++) 
		if (num % i == 0) return false;

	return true;
}

int main () {
    int t = 1;
    cin >> t;
    while(t--) {
		int n, sum = 0;
		cin >> n;
		sum = n;
		while(checkIfPrime(sum) == false or checkIfPrime(sum - n + 1)) sum++;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (i != j) cout << 1 << ' ';
				else cout << sum - n + 1 << ' ';
			}
			cout << '\n';
		}
	}
    return 0;
}
