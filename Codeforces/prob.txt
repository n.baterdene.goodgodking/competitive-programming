Таньд A, B, C, D 4н тоо байв.(A <= B <= C <= D)
(A <= X <= B <= Y <= C <= Z <= D) Байх X, Y, Z гурвалжны 3н тал байдаг (X, Y, Z) хос хэд байх вэ?
1 <= A, B, C, D <= 5 * 10^5
sample input 1:
1 2 3 4
sample output 1:
4
explanation 1: 
(1, 3, 3), (2, 2, 3), (2, 3, 3), (2, 3, 4)

sample input 2:
1 2 2 5
sample output 2:
3
explanation 2: 
(1, 2, 2), (2, 2, 2), (2, 2, 3)

sample input 2:
500000 500000 500000 500000
sample output 2:
1
explanation 2: 
(500000, 500000, 500000)