#include <bits/stdc++.h>
#define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define rs resize
#define ins insert 
#define fr front() 
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define umap unordered_map
#define sz(x) (int)x.size()

typedef long long ll;
typedef double db;
typedef unsigned uint;
typedef unsigned long long ull;
typedef unordered_map<int, int> umapII;
// PQ going up <int, VI, greater<int> >
typedef vector<int> VI;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<bool> VB;
typedef vector<VB> VVB;
typedef vector<VVB> VVVB;
typedef vector<VVVB> VVVVB;
typedef vector<VVVVB> VVVVVB;
typedef vector<ll> VLL;
typedef vector<VLL> VVLL;
typedef vector<VVLL> VVVLL;
typedef vector<VVVLL> VVVVLL;
typedef vector<db> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef pair<int, int> PII;
typedef pair<db, db> PDD;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VPI> VVPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
typedef vector<VVVI> VVVVI;
typedef vector<VVVVI> VVVVVI;
// mt19937 rng(chrono::high_resolution_clock::now().time_since_epoch().count() + reinterpret_cast<unsigned long>(new int) + *(new unsigned long));
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int bitCnt(int a) {
    bitset <64> b(a);
    return b.count();
}
int bitCnt(string a) {
    bitset <64> b(a);
    return b.count();
}
VI readVI(int n) {
    VI a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVI readVVI(int n, int m) {
    VVI a(n, VI(m));
    REP(i, n) a[i] = readVI(m);
    return a;
}
VLL readVLL(ll n) {
    VLL a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVLL readVVLL(ll n, ll m) {
    VVLL a(n, VLL(m));
    REP(i, n) a[i] = readVLL(m);
    return a;
}


int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}

void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VPI& a) {
    for (auto el : a) {
        cout << el.ff << ',' << el.ss << ' ';
    }
    cout << '\n';
}

void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VVI& a) {
    for (auto el : a) {
        print(el);
    }
}
const int MOD = 1e9 + 7;
const int MOD1 = 998244353;//119*2^23 +1;
const ll INFF = INT64_MAX;
const db EPS = 1e-9;
const db PI = acos(-1.0); //M_PI;
const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0, 1, 0, -1};
/*
|      ⣠⣶⡾⠏⠉⠙⠳⢦⡀⠀  ⠀⢠⠞⠉⠙⠲⡀
|    ⣴⠿⠏⠀⠀⠀⠀⠀⠀⢳⡀⠀  ⡏⠀⠀ ⠀⠀ ⢷
|⠀⠀⢠⣟⣋⡀⢀⣀⣀⡀⠀⣀⡀⣧⠀  ⢸⠀⠀⠀⠀   ⡇ ⠀
| ⠀⢸⣯⡭⠁⠸⣛⣟⠆⡴⣻⡲⣿   ⣸⠀⠀OK.  ⡇
|⠀⠀⣟⣿⡭⠀⠀⠀⠀⠀⢱⠀⠀⣿   ⢹⠀⠀⠀⠀⠀ ⡇
|⠀⠀⠙⢿⣯⠄⠀⠀⠀⢀⡀⠀⠀⡿⠀  ⡇⠀⠀ ⠀ ⡼
|⠀⠀⠀⠀⠹⣶⠆⠀⠀⠀⠀⠀⡴⠃ ⠀ ⠘⠤⣄⣠⠞⠀ ⠀
|⠀⠀⠀⠀⢸⣷⡦⢤⡤⢤⣞⣁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ ⠀
|⠀⢀⣤⣴⣿⣏⠁⠀⠀⠸⣏⢯⣷⣖⣦⡀⠀⠀⠀⠀⠀⠀
|⢀⣾⣽⣿⣿⣿⣿⠛⢲⣶⣾⢉⡷⣿⣿⠵⣿⠀⠀⠀⠀⠀
|⣼⣿⠍⠉⣿⡭⠉⠙⢺⣇⣼⡏⠀ ⠀⣄⢸⣿
|⣿⣿⣧⣀⣿.........⣀⣰⣏⣘⣆⣀
      
|                                                    _         _   _
|                                                   (_)       | | | |
 _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_
| '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|
| |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_
| .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|
| |               __/ |
|_|              |___/
 _   _      _ _    __        __         _     _ 
| | | | ___| | | __\ \      / /__  _ __| | __| |
| |_| |/ _ \ | |/ _ \ \ /\ / / _ \| '__| |/ _` |
|  _  |  __/ | | (_) \ V  V / (_) | |  | | (_| |
|_| |_|\___|_|_|\___/ \_/\_/ \___/|_|  |_|\__,_|                                  
 ____     __    _______    _______   __________
|     \  |  |  |  _____|  /  _____) |____  ____|
|  |\  \ |  |  | |__     (  (_____      |  |
|  | \  \|  |  |  __|     \_____  \     |  | 
|  |  \     |  | |_____    _____)  )    |  | 
|__|   \____|  |_______|  (_______/     |__| 
 
 
 


*/











int n, m;
const int N = 1e5+1;
const int M = 4*N;
VI nums(N);
VI node(M), node1(M), lazy(M);
int sum(int l, int r) {
    return (r * (r + 1) - l * (l - 1)) / 2;
}
int len(int l, int r) {
    return r - l + 1;
}
PII merge(PII a, PII b) {
    return {a.ff + b.ff, a.ss + b.ss};
}
void propagate(int head, int l, int r) {
    int val = lazy[head], mid = (l+r)>>1;
    lazy[head] = 0;
    node[head*2+1] += sum(l, mid) * val;
    node[head*2+2] += sum(mid+1, r) * val;
    node1[head*2+1] += len(l, mid) * val;
    node1[head*2+2] += len(mid+1, r) * val;
    lazy[head*2+1] += val;
    lazy[head*2+2] += val;
}
void build(int l, int r, int head) {
    if (l == r) {
        node[head] = nums[l] * l;
        node1[head] = nums[l];
        return;
    }
    int mid = (l+r)>>1;
    build(l, mid, head*2+1);
    build(mid+1, r, head*2+2);
    node[head] = node[head*2+1] + node[head*2+2];
    node1[head] = node1[head*2+1] + node1[head*2+2];
}
void update(int l, int r, int L, int R, int val, int head) {
    if (l > R || L > r) return;
    if (L <= l && r <= R) {
        node[head] += sum(l, r) * val;
        node1[head] += len(l, r) * val;
        lazy[head] += val;
        // cout << "(l, r): " << l << ' ' << r << " -> " << node1[head] << '\n';
        return;
    }
    propagate(head, l, r);
    int mid = (l+r)>>1;
    update(l, mid, L, R, val, head*2+1);
    update(mid+1, r, L, R, val, head*2+2);
    node1[head] = node1[head*2+1] + node1[head*2+2];
    node[head] = node[head*2+1] + node[head*2+2];
    // cout << "(l, r): " << l << ' ' << r << " -> " << node1[head] << '\n';
}
PII query(int l, int r, int L, int R, int head) {
    if (L > r || l > R) return {0, 0};
    if (L <= l && r <= R) {
        return {node[head], node1[head]};
    }
    propagate(head, l, r);
    int mid = (l+r)>>1;
    return merge(
        query(l, mid, L, R, head*2+1),
        query(mid+1, r, L, R, head*2+2)
    );
}
void go () {
    cin >> n >> m;
    for (int i = 1; i <= n; i++) cin >> nums[i];
    // cout << n << ' ' << m << '\n';
    build(1, n, 0);
    // cout << query(1, n, 1, 1, 0).ff << ' ' << nums[1] << '\n';
    // cout << query(1, n, 2, 2, 0).ff << ' ' << nums[2] << '\n';
    // cout << query(1, n, 3, 3, 0).ff << ' ' << nums[3] << '\n';
    // cout << query(1, n, 4, 4, 0).ff << ' ' << nums[4] << '\n';
    // cout << query(1, n, 5, 5, 0).ff << ' ' << nums[5] << '\n';
    while(m--) {
        int a, b, c;
        cin >> a;
        if (a == 1) { // update
            cin >> a >> b >> c;
            update(1, n, a, b, c, 0);
        } else { // query
            cin >> a >> b;
            int l = a-1;
            tie(a, b) = query(1, n, a, b, 0); // ladder, linear
            // cout << a << ' ' << b << ": ";
            cout << a - b*l << '\n';
        }
    }
}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
#endif
    _upgrade
    int T = 1;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














