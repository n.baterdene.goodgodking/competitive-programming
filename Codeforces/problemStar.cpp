#include <iostream>
#include <vector>
using namespace std;

const int N = 11;

int num[12], j = 0;
bool used[12], yes;
vector < int > numbers;
vector < vector < int > > res;

int pattern[5][4] = {
    {1, 2, 4, 5},
    {5, 6, 8, 9},
    {9, 10, 2, 3},
    {3, 4, 6, 7},
    {7, 8, 10, 11}
};

void myfunc(int len){
    if(len == N){
        int sum = -1;
        for(int i = 0 ; i < 5 ; i++){
            int now = 0;
            for(int ii = 0 ; ii < 4 ; ii++){
                now += num[ pattern[i][ii]-1 ];
            }
            if( sum == -1 ) sum = now;
            if( now != sum ){
                return ;
            }
        }
        vector < int > tmp;
        for(int i = 0 ; i < N ; i++){
            tmp.push_back(num[i]);
        }
        res.push_back(tmp);
        return;
    }
    for(int id = 1; id < N+1; id++){
        if(used[id] == false){
            used[id] = true;

            num[len] = id;
            myfunc(len+1);

            used[id] = false;
        }
    }
    
    return;
}
int main () {
    myfunc(0);

    cout << res.size() << endl;
    for( auto now : res ){
        for(int x : now ){
            cout << x << ' ';
        } cout << endl;
    }
    return 0;
}