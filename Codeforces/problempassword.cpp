#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <vector>

using namespace std;
int main () {
	ifstream in("users.in");
    vector< pair <string, string> > users;
    string name = "", code;
    while(!in.eof()){
        in >> name >> code;
        if (name != "")
            users.push_back(make_pair(name, code));
    }
    int n = users.size();

    cout << "enter your username\n";
    cin >> name;
    cout << "enter your password\n";
    cin >> code;

    int i = 0, found = -1;
    for (auto el : users){
        //cout << el.first << " " << el.second << endl;
        if (name == el.first && code == el.second){
            found = i;
            break;
        }
        i++;    
    }

    if (found == -1){
        cout << "login failed" << endl;
    } else {
        cout << "login success for user number: " << found << endl;
    }

	return 0;   
}