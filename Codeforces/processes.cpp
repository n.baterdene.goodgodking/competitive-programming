#include <bits/stdc++.h>
using namespace std;
#define _upgrade ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define REP(i, a) for (int i = 0; i < a; i++)
#define ts to_string
#define pb push_back
typedef vector<int> VI;
typedef vector<VI> VVI;

//  _   _      _ _    __        __         _     _ 
// | | | | ___| | | __\ \      / /__  _ __| | __| |
// | |_| |/ _ \ | |/ _ \ \ /\ / / _ \| '__| |/ _` |
// |  _  |  __/ | | (_) \ V  V / (_) | |  | | (_| |
// |_| |_|\___|_|_|\___/ \_/\_/ \___/|_|  |_|\__,_|                                  













struct Node {
    int val;
    Node *l, *r;
    void addSide() {
        l = new Node();
        r = new Node();
    }
    Node() {
        l = r = NULL;
    }
};
Node *segRoot;
int n, q, headProcess, curIdx;
const int N = 1e5 + 5;
VVI paths(N);
VI storages(N), par(N), numsOnLine(N);
VVI elements(N);
unordered_map <int, int> idxOfElementInLine;

void build (Node *head, int l, int r) {
    if (l == r) {
        head->val = numsOnLine[l];
        return;
    }
    head->addSide();
    int mid = (l + r) >> 1;
    build(head->l, l, mid);
    build(head->r, mid + 1, r);
    head->val = max(head->l->val, head->r->val);
}

int query(Node *head, int l, int r, int st, int end) {
    if (l > r || r < st || end < l) return 0;
    if (st <= l && r <= end) return head->val;
    int mid = (l + r) >> 1;
    return max(
        query(head->l, l, mid, st, end),
        query(head->r, mid + 1, r, st, end)
    );
}

void update(Node *head, int l, int r, int idx, int num) {
    if (l == r) {
        numsOnLine[idx] = num;
        head->val = num;
        return;
    }
    int mid = (l + r) >> 1;
    if (mid >= idx) update(head->l, l, mid, idx, num);
    else update(head->r, mid + 1, r, idx, num);
    head->val = max(
        head->l->val, 
        head->r->val
    );
}

void dfs(int pos) {
    int st = curIdx;
    for (auto nextPos : paths[pos]) {
        dfs(nextPos);
    }
    idxOfElementInLine[storages[pos]] = curIdx;
    numsOnLine[curIdx] = storages[pos];
    elements[pos] = {st, curIdx++};
}
/*

    1
    / \
    2  3
    2 3 1
    0 1 2
*/

void init() {
    cin >> n >> q;
    REP(i, n) cin >> storages[i];
    REP(i, n) par[i] = -1;
    REP(i, n - 1) {
        int a, b;
        cin >> a >> b;
        paths[a].pb(b);
        par[b] = a;
    }
    headProcess = 0;
    while(par[headProcess] != -1) headProcess = par[headProcess];
    curIdx = 0;
    dfs(headProcess);
    segRoot = new Node();
    build(segRoot, 0, n - 1);
}
void go () {
    init();
    while(q--) {
        int pos;
        cin >> pos;
        int res = query(segRoot, 0, n - 1, elements[pos][0], elements[pos][1]);
        if (res != 0)
        update(segRoot, 0, n - 1, idxOfElementInLine[res], 0);
        cout << res << '\n';
    }
    REP(i, n) paths[i].clear();
}


signed main () {
    _upgrade
    // freopen("in.txt", "r", stdin);
    int T = 1;
    cin >> T;
    while(T--) go();
    return 0;
}