#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
#define umap unordered_map
#define INF INT_MAX
#define MOD 1e9 + 7
using namespace std;
int findMed(int st, int n, vector <int>& numbers) {
    sort(numbers.begin() + st, numbers.begin() + st + n);
    return numbers[st + n / 2];
}
int part(int st, int end, int num, vector <int>& numbers) {
    int pos = st; 
    for (; pos < end; pos++)
        if (numbers[pos] == num) break;
    swap(numbers[pos], numbers[end]);
    pos = st;
    for (int i = st; i < end; i++) {
        if (numbers[i] <= num) {
            swap(numbers[pos], numbers[i]);
            pos++;
        }
    }
    swap(numbers[pos], numbers[end]);
    return pos;
}
int myFind(int st, int end, int m, vector <int>& numbers) {
    if (m <= 0 || m > end - st + 1) return INT_MAX;
    int n = end - st + 1;
    int i = 0;
    vector <int> med;
    for (; i < n / 5; i++) 
        med.pb(findMed(st + i * 5, 5, numbers));

    if (i * 5 != n) {
        med.pb(findMed(st + i * 5, n % 5, numbers));
        i++;
    }

    int midMed = myFind(0, i - 1, i / 2, med);
    int pos = part(st, end, midMed, numbers);
    if (pos - st == m - 1) return numbers[pos];

    if (pos - st > m - 1) return myFind(st, pos - 1, m, numbers);
    return myFind(pos + 1, end, m - pos + st - 1, numbers);
    
} 
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int n, m, tmp;
    cin >> n >> m;
    vector <int> numbers(n);
    for (int i = 0; i < n; i++) 
        cin >> numbers[i];
    cout << myFind(0, n - 1, m, numbers) << '\n';
	return 0;
}