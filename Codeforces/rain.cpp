#include <bits/stdc++.h>
// #pragma GCC optimize("O3, unroll-loops")
// #pragma GCC target("avx2")
// #define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define ub upper_bound 
#define sz(x) (int)x.size()
#define chmin(a,b) a=min(a,b)
#define chmax(a,b) a=max(a,b)

using db = double;
using ld = long double;
using sint = short int;
using ll = long long;
using uint = unsigned int;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using PII = pair<int, int>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using PLL = pair<ll, ll>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}
/*

|                    _                                                     
|  __   _           | |                                                    
| |   \| | ___  ___ | |_                                                   
| | |\ | |/ _ \/ __/| __|                                                  
| | | \  |  __/\__ \| |_                                                   
| |_|  \_|\___ /___/ \__|                                                  
|                                                      _         _   _     
|                                                     (_)       | | | |    
|  _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_   
| | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|  
| | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_   
| | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|  
| | |               __/ |                                                  
| |_|              |___/                   _                               
|                                  _      (_)                              
|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                               
| | '_ ` _ \ / _` |_   / _` |/ _` | |/ _` | |                              
| | | | | | | (_|  /  / (_| | (_| | | (_| | |                              
| |_| |_| |_|\__,_|____\__,_|\__,_|_|\__,_|_|                              
*/

const db PI = acos(-1.0); //M_PI;
const ll INFF = 4e18;
const int INF = 1.07e9;
const int MOD = 1e9 + 7;
const int MOD1 = 998244353; //7*19*2^23 +1;
const int dx[] = {-1, 0, 1, 0, -1, -1, 1, 1};
const int dy[] = {0, 1, 0, -1, -1, 1, -1, 1};




const int N = 2e5 + 5;
const int M = 2e5 + 5;

int x[N], p[N];

ll n, m, k, q;
string s;
void go () {
    cin >> n >> k; // m threshold
    for (int i = 1; i <= n; i++) cin >> x[i] >> p[i];
    map <int, int> comp;
    vector <int> pts; pts.reserve(n*3);
    { // compress
        set <int> _pts;
        for (int i = 1; i <= n; i++) {
            _pts.insert(x[i]-p[i]);
            _pts.insert(x[i]);
            _pts.insert(x[i]+p[i]);
        }
        int ptr = 0;
        for (auto el : _pts) {
            pts.pb(el);
            comp[el] = ptr++;
        }
    }
    m = sz(pts);
    ll cnt[m]; memset(cnt, 0, sizeof(cnt));
    for (int i = 1; i <= n; i++) {
        cnt[comp[x[i]-p[i]]]++;
        cnt[comp[x[i]]] -= 2;
        cnt[comp[x[i]+p[i]]]++;
    }
    for (int i = 1; i < m; i++) cnt[i] += cnt[i-1];
    ll sum[m]; sum[0] = 0;
    for (int i = 1; i < m; i++) sum[i] = sum[i-1] + cnt[i-1]*(pts[i]-pts[i-1]);
    ll lineA = -1e18, lineB = -1e18;
    for (int i = 0; i < m; i++) {
        if (sum[i] > k) {
            ll h = sum[i]-k;
            ll pos = pts[i];
            chmax(lineA, h-pos);
            chmax(lineB, h+pos);
        }
    }
    for (int i = 1; i <= n; i++) {
        ll h = p[i];
        ll pos = x[i];
        cout  << ((lineA <= h-pos && h+pos >= lineB) ? "1" : "0");
    }
    cout << '\n';
}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("0.in", "r", stdin);
    freopen("0.out", "w", stdout);
#endif
    _upgrade
    int T = 1;
    cin >> T;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














