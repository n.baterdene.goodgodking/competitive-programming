#include <iostream>
#include <map>
using namespace std;
map <int, bool> numbers;
int ans = 1;
void my_func(int number){
    while(number % 10 == 0)
        number /= 10;
    if(numbers.find(number) != numbers.end()){
        return;
    } else {
        numbers[number] = true;
        ans++;
        return my_func(number + 1);
    }
}
int main () {
    int num;
    cin >> num;
    numbers[num] = true;
    my_func(num + 1);
    cout << ans << '\n';

    return 0;
}