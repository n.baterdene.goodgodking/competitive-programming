#include<bits/stdc++.h> 

// #define int long long
using namespace std; 
int getMaxArea(vector<int>& nums) { 
	nums.push_back(0);
	int n = nums.size(), pos = 0, ans = 0;
	if (nums.size() == 1) return 0;
	stack <int> tmp;
	while (pos < n){
		if (tmp.empty() || nums[pos] >= nums[tmp.top()])
			tmp.push(pos++);
		else {
			int tmp1 = tmp.top();
			tmp.pop();
			int range;
			if (tmp.empty()) range = pos;
			else range = pos - tmp.top() - 1;
			ans = max(ans, range * nums[tmp1]);
		}
	}
	return ans;
}
signed main() { 

	ios_base::sync_with_stdio(0); 
	cin.tie(0); 
	cout.tie(0);
	int n;
	cin >> n;
	vector <int> nums(n);
	for (int i = 0; i < n; i++) cin >> nums[i];
	cout << getMaxArea(nums) << '\n'; 
	return 0; 
} 
