#include <bits/stdc++.h>
using namespace std;

int solve(vector <int>&a, int n) {
    int ans = 0, cur = 0;
    for (int i = 0; i < n; i++) {
        cur += a[i];
        ans = max(ans, cur);
    }
    return ans;
}

int main () {
    int t = 1;
    cin >> t;
    while(t--) {
        int n, m;
        cin >> n;
        vector <int> a(n);
        for (int i = 0; i < n; i++) cin >> a[i];
        cin >> m;
        vector <int> b(m);
        for (int i = 0; i < m; i++) cin >> b[i];
        int ans = solve(a, n) + solve(b, m);
        cout << ans << '\n';
    }
    return 0;
}