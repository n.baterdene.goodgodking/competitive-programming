#include <bits/stdc++.h>
using namespace std;

int main () {
    int t = 1;
    cin >> t;
    while(t--)  {
        string s;
        cin >> s;
        int cnt = 0, n = s.size(), idx = 0;
        if (n & 1) {
            cout << "NO\n";
            continue;
        }
        if (s[0] == ')' || s.back() == '(') cout << "NO\n";
        else cout << "YES\n";
    }
    return 0;
}
