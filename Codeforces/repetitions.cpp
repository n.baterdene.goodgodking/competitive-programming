#include <iostream>
using namespace std;
int main () {
    string str;
    cin >> str;
    int count = 0, cur = 1;
    int n = str.size();
    for (int i = 1; i < n; i++) {
        if (str[i] == str[i - 1]) 
            cur++;
        else {
            count = max(count, cur);
            cur = 1;
        }
    }
    count = max(count, cur);
    cout << count << '\n';
    
    return 0;
}