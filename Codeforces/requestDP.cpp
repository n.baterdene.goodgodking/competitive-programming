#include <bits/stdc++.h>
using namespace std;
#define _upgrade ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define pb push_back
#define mp make_pair
#define ff first
#define ss second
#define PII pair <int, int> 
#define INF INT_MAX
vector <int> numbers, pow2(1, 1);
PII notAns = mp(INF, -INF);
vector <vector <PII> > dp(5e4 + 5, vector <PII>(105, notAns));
signed main () {
#ifdef ONLINE_JUDGE
#else
    freopen("in.txt", "r", stdin);
	// freopen("out.txt", "w", stdout);
#endif
    _upgrade
    int n, T, tmp, a, b;
    cin >> n >> T;
    for (int i = 0; i < n; i++) {
        cin >> tmp;
        numbers.pb(tmp);
    }
    int m;
    for (int i = 0; i < 30; i++) {
        m = i;
        if (pow2[i] * 2 >= 1e8) break;
        pow2.pb(pow2[i] * 2);
    }
    for (int i = 0; i < n; i++) {
        dp[i][0] = mp(numbers[i], numbers[i]);
    }
    for (int j = 1; j < m; j++) 
    for (int i = 0; i < n; i++) {
        if (i + pow2[j - 1] > n) continue;
        dp[i][j] = mp(
            min(dp[i][j - 1].ff, dp[i + pow2[j - 1]][j - 1].ff),
            max(dp[i][j - 1].ss, dp[i + pow2[j - 1]][j - 1].ss)
        );
        // cout << dp[i][j].ff << ' ' << dp[i][j].ss << '\n'; 
    }
    while(T--) {
        cin >> a >> b;
        a--, b--;

        int dif = b - a + 1;

        int cur = 0;
        for (int i = 0; i < m; i++) {
            if (pow2[i] <= dif) cur = i;
            else break;
        }
        dif -= pow2[cur];
        PII cur1 = mp(
            min(dp[a][cur].ff, dp[a + dif][cur].ff),
            max(dp[a][cur].ss, dp[a + dif][cur].ss)
        );
        cout << cur1.ss - cur1.ff << '\n';
    }
    return 0;
}
