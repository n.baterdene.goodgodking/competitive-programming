#include <bits/stdc++.h>
using namespace std;
#define _upgrade ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define pb push_back
#define mp make_pair
#define ff first
#define ss second
#define PII pair <int, int>
#define INF INT_MAX
vector <int> numbers;
PII notAns = mp(INF, -INF);
struct Node {
    PII val; // min, max
    Node *l, *r;
    Node() {
        l = r = NULL;
    }
    void initSide() {
        l = new Node();
        r = new Node();
    }
};

Node *root = new Node();
void init(int st, int end, Node *head) {
    if (st == end) {
        head->val = mp(numbers[st], numbers[st]);
        return;
    }
    int mid = (st + end)>>1;
    head->initSide();
    init(st, mid, head->l);
    init(mid + 1, end, head->r);
    head->val = mp(
        min(head->l->val.ff, head->r->val.ff),
        max(head->l->val.ss, head->r->val.ss)
    );
}
PII query(int st, int end, int l, int r, Node *head) {
    if (st > r || end < l) return notAns;
    if (l <= st && end <= r) return head->val;
    int mid = (st + end)>>1;
    PII a, b;
    a = query(st, mid, l, r, head->l);
    b = query(mid + 1, end, l, r, head->r);
    return mp(
        min(a.ff, b.ff),
        max(a.ss, b.ss)
    );
}
signed main () {

#ifdef ONLINE_JUDGE
#else
    freopen("in.txt", "r", stdin);
	// freopen("out.txt", "w", stdout);
#endif
    _upgrade
    int n, T, tmp, a, b;
    cin >> n >> T;
    for (int i = 0; i < n; i++) {
        cin >> tmp;
        numbers.pb(tmp);
    }
    init(0, n - 1, root);
    while(T--) {
        cin >> a >> b;
        PII cur = query(0, n - 1, a - 1, b - 1, root);
        cout << cur.ss - cur.ff << '\n';
    }
    return 0;
}
