#include <bits/stdc++.h>
#define int long long
using namespace std;

int n;
const int N = 45;
vector <int> shat(N, -1), dp(N, -1); // int shat[n+5];
int hariu(int pos) {
    if (pos <= 0) return 0;
    if (dp[pos] != -1) return dp[pos];
    dp[pos] = 0;
    dp[pos] = shat[pos] + min(
        hariu(pos-1),
        hariu(pos-2)
    );
    return dp[pos];
}
signed main () {
    cin >> n;
    for (int i = 1; i <= n; i++) {
        cin >> shat[i];
    }
    cout << hariu(n) << endl;
    return 0;
}
