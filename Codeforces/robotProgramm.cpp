#include <bits/stdc++.h>
using namespace std;

void solve () {
    long long a, b;
    cin >> a >> b;
    if (a > b) swap(a, b);
    long long ans = 2 * a; 
    b -= a;
    if (b == 0) {
        cout << ans << '\n';
        return;
    }
    cout << ans + b * 2 - 1 << '\n';
}

int main () {
    int t = 1;
    cin >> t;
    while(t--) solve();
    return 0;
}