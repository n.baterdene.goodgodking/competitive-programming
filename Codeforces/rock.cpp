#include <iostream>
#include <cstring>
#include <vector>
#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;
int main() {
	
	// your code here
	int n, tmp, sum = 0;
	cin >> n;
	vector<int> nums;
	
	for(int i = 0; i < n; i++){
		cin >> tmp;
		sum += tmp;
		nums.push_back(tmp);
	}
    int arr[sum];
    memset(arr, 0, sizeof(arr));
    arr[0] = 1;
	for(int i = 0; i < n; i++){
		int num = nums[i];
        for (int i = sum; i >= 0; i--) {
            if (arr[i] == 1) {
                arr[i + num] = 1;
            }
        }
	}
    cout << '\n';
    int res = sum;
    for(int i = 0; i <= sum; i++){
        if (arr[i] == 1 && res > abs(sum - i * 2)) {
            res = abs(sum - i * 2);
        }
    }
    cout << res << "\n";    
	return 0;
}