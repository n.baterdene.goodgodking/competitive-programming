#include <bits/stdc++.h>
using namespace std;

int n, k;
const int N = 20;

vector <int> pows(N, 1);

vector <int> baseK(int a) {
    vector <int> res;
    while(a > 0) {
        res.push_back(a % k);
        a /= k;
    }
    while(res.size() < N) res.push_back(0);
    return res;
}

// Xor function for finding z by X, Y:  where X xor Z = Y 
int nXor(int a, int b) {
    vector <int> A, B, C;
    A = baseK(a), B = baseK(b);
    for (int i = 0; i < N; i++) 
        C.push_back((B[i] - A[i] + k)%k);

    int res = 0;
    for (int i = 0; i < N; i++) 
        res += pows[i] * C[i];
    
    return res;
}

int x;
bool check(int a) {
    cout << a << endl;
    cin >> x;
    return x;
}

void go () {
    cin >> n >> k;
    /*  
        Later we will convert base K number to base 10
        so in order to make that part easier we can create array with power of k
        if k is equal to 2 the max size is log(2, 2*10^5) which is 17.6 so we make the maximum size 20
        but if k is equal to 100 we can't calculate 100^20. 
        That's why we need to calculate the ones which are lower than n
    */
    for (int j = 1; j < N; j++) {
        pows[j] = pows[j-1] * k;
        if (pows[j] > n) break;
    }

    int changed = 0, cur, checked = 0;
    // check if answer is 0.
    if (check(0)) return;
    /*
    first we checked if the answer is 0 
    now we check if the answer is 1, if the answer is 2 and so on.
    */
    for (int i = 1; i < n; i++) {
        /* 
            now here is the tricky part
            if k = 2 we don't care about which order to use this nXor function
            but if k isn't equal to 2 it's different
            each time we ask a query it changes depending on the query
            so we have to check it alternatively. 
            And to do that if I is even we use the normal one otherwise the one gives us normal one's inverse answer
            we define inverse one as `X where `X xor X == 0
        */
        cur = (i & 1 ? nXor(i, i-1) : nXor(i-1, i));
        if (check(cur)) return;
    }

}

int main () {
    int T = 1;
    cin >> T;
    while(T--) go();
    return 0;
}