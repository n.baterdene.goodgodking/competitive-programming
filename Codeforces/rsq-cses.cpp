#include<iostream>
#define int long long
using namespace std;

const int N = 200005;

int n, queries, a[N], tree[1<<19];

void build(int l, int r, int x) {
    if( l == r ) {
        tree[x] = a[l];
        return;
    }
    int mid = (l+r) >> 1;
    build( l, mid, x*2 );
    build( mid+1, r, x*2+1);
    tree[x] = tree[x*2] + tree[x*2+1] ;
}

int query(int l, int r, int p, int q, int x) {
    if(r < p || l > q) return 0;
    if(p < l && r < q) return tree[x];
    if(l == r) return tree[x];
    int mid = l + (r - l) / 2;
    return query(l, mid, p, q, x * 2) + query(mid + 1, r, p, q, x * 2 + 1);
}
void update(int l, int r, int p, int q, int x) {
    if(l == r) {
        tree[x] = q;
        return;
    }
    int mid = l + (r - l) / 2;
    if(mid > p) update(l, mid, p, q, x * 2);
    else update(mid + 1, r, p, q, (x * 2) + 1);

    tree[x] = tree[x * 2 + 1] + tree[x * 2];
}

signed main(){
    // freopen("out.txt", "w", stdout);
    // scanf("%lld %lld", &n, &queries);
    cin >> n >> queries;
    for(int i = 1 ; i <= n ; i++){
        scanf("%lld", &a[i]);
        // cin >> a[i];
    }
    build(1, n, 1);
    int type, p, q;
    for(int i = 0; i < queries; i++) {
        scanf("%lld %lld %lld", &type, &p, &q);
        // cin >> type >> p >> q;
        if(type == 1) {
            a[p] = q;
            update(1, n, p - 1, q, 1);
        } else 
            cout << query(1, n, p, q, 1) << '\n';
    }   
    return 0;
}