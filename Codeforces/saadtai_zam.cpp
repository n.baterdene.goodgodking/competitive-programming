#include <iostream> 
using namespace std;
int main () {
    int n, m, q = 1;
    cin >> n >> m;
    char useg[(n * m) + 1];
    int kord[n + 1][m + 1];
    memset(kord, 0, sizeof(kord));
    for(int i = 1; i <= n; i ++){
        for(int j = 1; j <= m; j++){
            cin >> useg[q];
            if(useg[q] == 'x')
                kord[i][j] = -1;
            q++;
        }
    }
    for(int i = 1; i <= n; i++){
        for(int j = 1; j <= m; j++){
            if(kord[i][j] == -1)
                kord[i][j] = 0;
            else {
                kord[i][j] = kord[i][j - 1] + kord[i - 1][j];
            }
            if(i == 1 && j == 1)
            kord[1][1] = 1;
            cout << kord[i][j] << " ";
        }
        cout <<"\n";
    }
    cout << kord[n][m] << endl;
    return 0;
}