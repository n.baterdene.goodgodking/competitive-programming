#include <bits/stdc++.h>
using namespace std;
// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))

#define _upgrade ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define II <int, int>
#define Q queue
#define ff first
#define bk back()
#define ss second
#define rs resize
#define ins insert 
#define fr front() 
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define PQ priority_queue
#define umap unordered_map
#define sz(x) (int)x.size()

typedef long long ll;
typedef double db;
typedef unsigned uint;
typedef unsigned long long ull;
typedef unordered_map<int, int> umapII;
// PQ going up <int, VI, greater<int> >
typedef vector<int> VI;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<bool> VB;
typedef vector<umapII> VumapII;
typedef vector<ll> VLL;
typedef vector<VLL> VVLL;
typedef vector<db> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VPI> VVPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
 

int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
void readI(int& a) {
    cin >> a;
}
void readI(int& a, int&b) {
    cin >> a >> b;
}
void readI(int& a, int&b, int&c) {
    cin >> a >> b >> c;
}
VI readVI(int n) {
    VI a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVI readVVI(int n, int m) {
    VVI a(n, VI(m));
    REP(i, n) a[i] = readVI(m);
    return a;
}
VLL readVLL(ll n) {
    VLL a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVLL readVVLL(ll n, ll m) {
    VVLL a(n, VLL(m));
    REP(i, n) a[i] = readVLL(m);
    return a;
}


const int MOD = int(1e9) + 7;
const int INF = INT_MAX;
const ll INFF = INT64_MAX;
const db EPS = 1e-9;
const db PI = acos(-1.0); //M_PI;
const int moveX[] = {-1, 0, 1, 0};
const int moveY[] = {0, 1, 0, -1};

//                                        .....'',;;::cccllllllllllllcccc:::;;,,,''...'',,'..
//                             ..';cldkO00KXNNNNXXXinp000OOinpinpkxxxxxddoooddddddxxxxinpinpOO0XXKx:.
//                       .':ok0KXXXNXK0kxolc:;;,,,,,,,,,,,;;,,,''''''',,''..              .'lOXKd'
//                  .,lx00Oxl:,'............''''''...................    ...,;;'.             .oKXd.
//               .cinpinpc'...'',:::;,'.........'',;;::::;,'..........'',;;;,'.. .';;'.           'kNKc.
//            .:kXXk:.    ..       ..................          .............,:c:'...;:'.         .dNNx.
//           :0NKd,          .....''',,,,''..               ',...........',,,'',,::,...,,.        .dNNx.
//          .xXd.         .:;'..         ..,'             .;,.               ...,,'';;'. ...       .oNNo
//          .0K.         .;.              ;'              ';                      .'...'.           .oXX:
//         .oNO.         .                 ,.              .     ..',::ccc:;,..     ..                lXX:
//        .dNX:               ......       ;.                'cxOinp0OXWWWWWWWNX0kc.                    :KXd.
//      .l0N0;             ;d0inpinpKXK0ko:...              .l0X0xc,...lXWWWWWWWWKO0Kx'                   ,ONKo.
//    .lKNKl...'......'. .dXWN0inpk0NWWWWWN0o.            :KN0;.  .,cokXWWNNNNWNinpxONK: .,:c:.      .';;;;:lk0XXx;
//   :KN0l';ll:'.         .,:lodxxkO00KXNWWWX000k.       oXNx;:oinpX0kdl:::;'',;coxinpd, ...'. ...'''.......',:lxKO:.
//  oNNk,;c,'',.                      ...;xNNOc,.         ,d0X0xc,.     .dOd,           ..;dOKXK00000Ox:.   ..''dKO,
// 'KW0,:,.,:..,oxinpkdl;'.                'inp'              ..           .dXX0o:'....,:oOXNN0d;.'. ..,lOKd.   .. ;KXl.
// ;XNd,;  ;. l00kxoooxKXKx:..ld:         ;inp'                             .:dkO000000Okxl;.   c0;      :inp;   .  ;XXc
// 'XXdc.  :. ..    '' 'kNNNinpinp,      .,dKNO.                                   ....       .'c0NO'      :X0.  ,.  xN0.
// .kNOc'  ,.      .00. ..''...      .l0X0d;.             'dOkxo;...                    .;oinpXK0KNXx;.   .0X:  ,.  lNX'
//  ,inpdl  .c,    .dNK,            .;xXWKc.                .;:coOXO,,'.......       .,lx0XXOo;...oNWNXinp:.'KX;  '   dNX.
//   :XXkc'....  .dNWXl        .';l0NXNKl.          ,lxinpkxo' .cK0.          ..;lx0XNX0xc.     ,0Nx'.','.kXo  .,  ,KNx.
//    cXXd,,;:, .oXWNNKo'    .'..  .'.'dinp;        .cooollox;.xXXl     ..,cdOKXXX00NXc.      'oKWK'     ;k:  .l. ,0Nk.
//     cXNx.  . ,KWX0NNNXOl'.           .o0Ooldk;            .:c;.':lxOinpK0xo:,.. ;XX:   .,lOXWWXd.      . .':,.lKXd.
//      lXNo    cXWWWXooNWNXinpo;'..       .lk0x;       ...,:ldk0KXNNOo:,..       ,OWNOxO0KXXNWNO,        ....'l0Xk,
//      .dNK.   oNWWNo.cXK;;oOXNNXK0kxdolllllooooddxk00inpinp0kdoc:c0No        .'ckXWWWNXkc,;kNKl.          .,kXXk,
//       'KXc  .dNWWX;.xNk.  .kNO::lodxkOXWN0OkxdlcxNKl,..        oN0'..,:ox0XNWWNNWXo.  ,ONO'           .o0Xk;
//       .ONo    oNWWN0xXWK, .oNKc       .ONx.      ;X0.          .:XNinpNNWWWWNinpl;kNk. .cKXo.           .ON0;
//       .xNd   cNWWWWWWWWKOinpNXxl:,'...;0Xo'.....'lXK;...',:lxk0KNWWWWNNKOd:..   lXKclON0:            .xNk.
//       .dXd   ;XWWWWWWWWWWWWWWWWWWNNNNNWWNNNNNNNNNWWNNNNNNWWWWWNXKNNk;..        .dNWWXd.             cXO.
//       .xXo   .ONWNWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWNNK0ko:'..OXo          'l0NXx,              :inp,
//       .OXc    :XNk0NWXKNWWWWWWWWWWWWWWWWWWWWWNNNX00NNx:'..       lXKc.     'lONN0l.              .oXK:
//       .KX;    .dNKoON0;lXNkcld0NXo::cd0NNO:;,,'.. .0Xc            lXXo..'l0NNKd,.              .c0Nk,
//       :XK.     .xNX0NKc.cXXl  ;KXl    .dN0.       .0No            .xNXOKNXOo,.               .l0Xk;.
//      .dXk.      .lKWN0d::OWK;  lXXc    .OX:       .ONx.     . .,cdk0XNXOd;.   .'''....;c:'..;xKXx,
//      .0No         .:dOKNNNWNKOxkXWXo:,,;ONk;,,,,,;c0NXOxxkO0XXNXKOdc,.  ..;::,...;lol;..:xKXOl.
//      ,XX:             ..';cldxkOO0inpKXXXXXXXXXXinpinpK00Okxdol:;'..   .';::,..':llc,..'linpXkc.
//      :NX'    .     ''            ..................             .,;:;,',;ccc;'..'linpX0d;.
//      lNK.   .;      ,lc,.         ................        ..,,;;;;;;:::,....,linpX0d:.
//     .oN0.    .'.      .;ccc;,'....              ....'',;;;;;;;;;;'..   .;oOXX0d:.
//     .dN0.      .;;,..       ....                ..''''''''....     .:dOinpko;.
//      lNK'         ..,;::;;,'.........................           .;d0X0kc'.
//      .xXO'                                                 .;oOK0x:.
//       .cinpo.                                    .,:oxinpkxk0K0xc'.
//         .oinpkc,.                         .';cok0XNNNX0Oxoc,.
//           .;d0XX0kdlc:;,,,',,,;;:clodkO0inp0Okdl:,'..
//               .,coxO0KXXXXXXXinp0OOxdoc:,..
//                         ...
//       ⣠⣶⡾⠏⠉⠙⠳⢦⡀⠀  ⠀⢠⠞⠉⠙⠲⡀
//   ⠀⠀⣴⠿⠏⠀⠀⠀⠀⠀⠀⢳⡀⠀  ⡏⠀⠀ ⠀⠀ ⢷
// ⠀⠀⢠⣟⣋⡀⢀⣀⣀⡀⠀⣀⡀⣧⠀  ⢸⠀⠀⠀⠀   ⡇ ⠀
//  ⠀⢸⣯⡭⠁⠸⣛⣟⠆⡴⣻⡲⣿   ⣸⠀⠀OK.  ⡇
// ⠀⠀⣟⣿⡭⠀⠀⠀⠀⠀⢱⠀⠀⣿   ⢹⠀⠀⠀⠀⠀ ⡇
// ⠀⠀⠙⢿⣯⠄⠀⠀⠀⢀⡀⠀⠀⡿⠀  ⡇⠀⠀ ⠀ ⡼
// ⠀⠀⠀⠀⠹⣶⠆⠀⠀⠀⠀⠀⡴⠃ ⠀ ⠘⠤⣄⣠⠞⠀ ⠀
// ⠀⠀⠀⠀⢸⣷⡦⢤⡤⢤⣞⣁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ ⠀
// ⠀⢀⣤⣴⣿⣏⠁⠀⠀⠸⣏⢯⣷⣖⣦⡀⠀⠀⠀⠀⠀⠀
// ⢀⣾⣽⣿⣿⣿⣿⠛⢲⣶⣾⢉⡷⣿⣿⠵⣿⠀⠀⠀⠀⠀
// ⣼⣿⠍⠉⣿⡭⠉⠙⢺⣇⣼⡏⠀ ⠀⣄⢸⣿
// ⣿⣿⣧⣀⣿.........⣀⣰⣏⣘⣆⣀
      
//  _   _      _ _    __        __         _     _ 
// | | | | ___| | | __\ \      / /__  _ __| | __| |
// | |_| |/ _ \ | |/ _ \ \ /\ / / _ \| '__| |/ _` |
// |  _  |  __/ | | (_) \ V  V / (_) | |  | | (_| |
// |_| |_|\___|_|_|\___/ \_/\_/ \___/|_|  |_|\__,_|                                  


















void go () {
    int n, x;
    cin >> x;
    n = 2 * x + 1;
    cout << n << '\n';
}



signed main () {

#ifdef ONLINE_JUDGE
#else
    freopen("in.txt", "r", stdin);
//	freopen("out.txt", "w", stdout);
#endif
    _upgrade
    int T = 1;
    while(T--) go();
    return 0;
}

/* stuff you should look for
	* int overflow, array bounds
	* special cases (n=1?)
*/