#include <iostream>
#include <set>
#include <vector>
#include <algorithm>
#include <map>
#include <cmath>
#include <cstring>
using namespace std;
int n;
vector < int > answers;
void myfunc(int length, vector <bool> used, int current_num){
    if( length >= n ){
        answers.push_back( current_num );
        return;
    }

    /*
    cout << current_num << " : ";
    for(int i = 0 ; i < used.size() ; i++){
        cout << used[i] << ' ';
    } cout << endl;
    */
   
    for(int i = 1; i <= n; i++){
        if(used[i] == false){
            vector < bool > tmp = used;
            tmp[i] = true;
            myfunc(length+1, tmp, current_num * 10 + i);
        }
    }
}
int main () {
    cin >> n;
    for(int i = 1; i <= n; i ++){
        vector <bool> used(n+1, 0);
        used[i] = 1;
        myfunc(1, used, i );
    }    
    for(int i = 0; i < answers.size(); i++)
        cout << answers[i] << endl;
    
    cout << '\n';
    return 0;
}