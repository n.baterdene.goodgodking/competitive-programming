#include <bits/stdc++.h>
using namespace std;

int main() {
    int t;
    cin >> t;
    while(t--) {
        int n, N, idx = 0;
        cin >> n;
        map <int, vector <int> > nums;
        int past = -1;
        for (int i = 0; i < n; i++) {
            int num;
            cin >> num;
            if (num == past) continue;
            nums[num].push_back(idx++);
            past = num;
        }
        int ans = n;
        for (auto el : nums) {
            vector <int> cur = el.second;
            int curAns = cur.size() + 1;
            if (cur[0] == 0) curAns--;
            if (cur.back() == idx - 1) curAns--;
            ans = min(ans, curAns);
        }
        cout << ans << '\n';
    }
}