#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    long long n;
    cin >> n;
    if(n == 2){
        cout << 2 << "\n";
        return 0;
    }
    if(n == 3){
        cout << 4 << "\n";
        return 0;
    }
    long long shat[n + 1];
    shat[0] = 1;
    shat[1] = 1;
    shat[2] = 2;
    for(long long i = 3; i <= n; i++){
        shat[i] = shat[i - 1] + shat[i - 2] + shat[i - 3];
    }
    cout << shat[n] <<  "\n";
    
    return 0;
}
