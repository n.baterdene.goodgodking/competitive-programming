#include <iostream>
#include <vector>

using namespace std;
int findSt(const vector<int>& nums) {
    int l = 0, r = nums.size() - 1;
    while (l <= r) {
        int mid = (l + r) / 2;
        if (mid == 0 || nums[mid] < nums[mid - 1])
            return mid;
        if (nums[mid] > nums[0]) l = mid + 1;
        else r = mid - 1;
    }
    return 0;
}
int shiftedArrSearch( const vector<int>& nums, int num ) {
    int st = findSt(nums), n = nums.size();
  
    int idx = 0;
    for (int i = n; i > 0; i /= 2) {
        while(idx + i < n && nums[(idx + i + st) % n] <= num) 
            idx += i;
    }
    return (st + idx) % n;
}

int main() {
  return 0;
}