#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
#define umap unordered_map
using namespace std;
vector <char> moves(3);
map <pair <vector <ll>, int>, ll > seen;
ll ans(vector <ll> numbers, int cur) {
    // cout << "\n------------------\n";
    // for (auto el: numbers) 
    //     cout << el << ' ';
    // for (int i = cur; i < 3; i++) 
    //     cout << moves[i] << ' ';
    if (seen.find(mp(numbers, cur)) != seen.end()) return seen[mp(numbers, cur)];
    if (cur == 3) return numbers[0];
    ll curAns = INT64_MAX;
    for (int i = 0; i < numbers.size(); i++) 
    for (int j = i + 1; j < numbers.size(); j++) {
        vector <ll> tmp;
        for (int idx = 0; idx < numbers.size(); idx++) 
            if (idx != i && idx != j) tmp.pb(numbers[idx]);

        if (moves[cur] == '*') tmp.pb(numbers[i] * numbers[j]);
        else tmp.pb(numbers[i] + numbers[j]);
        curAns = min(curAns, ans(tmp, cur + 1));
    }
    return seen[mp(numbers, cur)] = curAns;
}
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    vector <ll> numbers(4), extra;
    for (int i = 0; i < 4; i++) 
        cin >> numbers[i];

    for (int i = 0; i < 3; i++) 
        cin >> moves[i];
    
    cout << ans(numbers, 0) << '\n';
	return 0;
}