#include <bits/stdc++.h>
// #define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define sz(x) (int)x.size()

using ll = long long;
using db = double;
using uint = unsigned;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VS = vector<string>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VVVVB = vector<VVVB>;
using VVVVVB = vector<VVVVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using VVVLL = vector<VVLL>;
using VVVVLL = vector<VVVLL>;
using VD = vector<db>;
using PII = pair<int, int>;
using PDD = pair<db, db>;
using PLL = pair<ll, ll>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
using VVVVI = vector<VVVI>;
using VVVVVI = vector<VVVVI>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int bitCnt(int a) {
    bitset <64> b(a);
    return b.count();
}
int bitCnt(string a) {
    bitset <64> b(a);
    return b.count();
}
VI readVI(int n) {
    VI a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVI readVVI(int n, int m) {
    VVI a(n, VI(m));
    REP(i, n) a[i] = readVI(m);
    return a;
}
VLL readVLL(ll n) {
    VLL a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVLL readVVLL(ll n, ll m) {
    VVLL a(n, VLL(m));
    REP(i, n) a[i] = readVLL(m);
    return a;
}


int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}

void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VPI& a) {
    for (auto el : a) {
        cout << el.ff << ',' << el.ss << ' ';
    }
    cout << '\n';
}

void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VVI& a) {
    for (auto el : a) {
        print(el);
    }
}
const int MOD = 1e9 + 7;
const int MOD1 = 998244353;//7*17*2^23 +1;
const int INF = 1.07e9;
const ll INFF = INT64_MAX;
const db EPS = 1e-9;
const db PI = acos(-1.0); //M_PI;
const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0, 1, 0, -1};
/*
|      ⣠⣶⡾⠏⠉⠙⠳⢦⡀⠀  ⠀⢠⠞⠉⠙⠲⡀
|    ⣴⠿⠏⠀⠀⠀⠀⠀⠀⢳⡀⠀  ⡏⠀⠀ ⠀⠀ ⢷
|⠀⠀⢠⣟⣋⡀⢀⣀⣀⡀⠀⣀⡀⣧⠀  ⢸⠀⠀⠀⠀   ⡇ ⠀
| ⠀⢸⣯⡭⠁⠸⣛⣟⠆⡴⣻⡲⣿   ⣸⠀⠀OK.  ⡇
|⠀⠀⣟⣿⡭⠀⠀⠀⠀⠀⢱⠀⠀⣿   ⢹⠀⠀⠀⠀⠀ ⡇
|⠀⠀⠙⢿⣯⠄⠀⠀⠀⢀⡀⠀⠀⡿⠀  ⡇⠀⠀ ⠀ ⡼
|⠀⠀⠀⠀⠹⣶⠆⠀⠀⠀⠀⠀⡴⠃ ⠀ ⠘⠤⣄⣠⠞⠀ ⠀
|⠀⠀⠀⠀⢸⣷⡦⢤⡤⢤⣞⣁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ ⠀
|⠀⢀⣤⣴⣿⣏⠁⠀⠀⠸⣏⢯⣷⣖⣦⡀⠀⠀⠀⠀⠀⠀
|⢀⣾⣽⣿⣿⣿⣿⠛⢲⣶⣾⢉⡷⣿⣿⠵⣿⠀⠀⠀⠀⠀
|⣼⣿⠍⠉⣿⡭⠉⠙⢺⣇⣼⡏⠀ ⠀⣄⢸⣿
|⣿⣿⣧⣀⣿.........⣀⣰⣏⣘⣆⣀
      
|                                                    _         _   _
|                                                   (_)       | | | |
 _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_
| '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|
| |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_
| .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|
| |               __/ |
|_|              |___/
 _   _      _ _    __        __         _     _ 
| | | | ___| | | __\ \      / /__  _ __| | __| |
| |_| |/ _ \ | |/ _ \ \ /\ / / _ \| '__| |/ _` |
|  _  |  __/ | | (_) \ V  V / (_) | |  | | (_| |
|_| |_|\___|_|_|\___/ \_/\_/ \___/|_|  |_|\__,_|                                  
 ____     __    _______    _______   __________
|     \  |  |  |  _____|  /  _____) |____  ____|
|  |\  \ |  |  | |__     (  (_____      |  |
|  | \  \|  |  |  __|     \_____  \     |  | 
|  |  \     |  | |_____    _____)  )    |  | 
|__|   \____|  |_______|  (_______/     |__| 
 
 
 


*/











ll n, m, p, k, A, B, C;
string s;
const int N = 55;
const int M = 1e6+5;
vector <VI> paths[N];
vector <VI> edges; // cost, u, v
ll ansL[M], ansU[M], cntL[M], cntU[M], answ[M];
int par[N];
int find(int a) {
    if (par[a] == a) return a;
    return par[a] = find(par[a]);
}
bool merge(int a, int b) {
    a = find(a);
    b = find(b);
    if (a == b) return 0;
    par[b] = a;
    return 1;
}
void add(ll a, ll b, ll c) {
    if (a <= c) {
        ll r = min(b, c);
        ansL[a] += c;
        ansL[r+1] -= c;
        cntL[a]+=1;
        cntL[r+1]-=1;
    }
    if (c < b) {
        ll l = max(a, c+1);
        ansU[l] += c;
        ansU[b+1] -= c;
        cntU[l]+=1;
        cntU[b+1]-=1;
    }
}
VPI res;
void dfs(int pos, int par = -1) {
    for (auto& edge : paths[pos]) {
        if (edge[0] == par) continue;
        res.pb({pos, edge[2]});
        dfs(edge[0], pos);
    }
}
VPI mst(int val) {
    for (int i = 1; i <= n; i++) {
        paths[i].clear();
        par[i] = i;
    }

    VVI tmp; // curCost, v, u, originalCost
    for (auto&edge : edges) {
        tmp.pb({abs(edge[0]-val), edge[1], edge[2], edge[3]});
    }
    sort(ALL(tmp));
    for (auto&edge : tmp) {
        if (merge(edge[1], edge[2])) {
            paths[edge[1]].pb({edge[2], edge[0], edge[3]});
            paths[edge[2]].pb({edge[1], edge[0], edge[3]});
        }
    }
    res.clear();
    dfs(1);
    return res;
}
void go () {
    cin >> n >> m;
    for (int i = 1; i <= m; i++) {
        int a, b, c; cin >> a >> b >> c;
        edges.pb({c, a, b});
    }
    cin >> p >> k >> A >> B >> C;
    sort(ALL(edges));
    for (int i = 0; i < C; i++) {
        int l = i, r = C-1, j = i;
        VPI curMst = mst(i);
        while(l <= r) {
            int mid = (l+r)>>1;
            VPI tmp = mst(mid);
            if (tmp == curMst) {
                j = mid;
                l = mid+1;
            } else {
                r = mid-1;
            }
        }
        // cout << i << ' ' << j << '\n';
        for (auto& [a, b] : curMst) {
            // cout << i << " " << j << " " << b << '\n';
            add(i, j, b);
        }
        i = j;
    }
    ll ans = 0;
    vector <ll> queries;
    for (int i = 1; i <= p; i++) {
        ll x; cin >> x;
        queries.pb(x);
    }
    for (int i = p+1; i <= k; i++) {
        ll x = queries.back();
        queries.pb((x*A+B)%C);
    }
    for (int i = 1; i <= C; i++) {
        ansL[i] += ansL[i-1];
        cntL[i] += cntL[i-1];
        ansU[i] += ansU[i-1];
        cntU[i] += cntU[i-1];
        answ[i] = cntL[i] * i - ansL[i] + ansU[i] - cntU[i] * i;
        // cout << i << ' ' << cntL[i] << ' ' << ansL[i] << " " << cntU[i] << ' ' << ansU[i] << '\n';
    }
    for (auto& el : queries) {
        ans ^= answ[el];
        // cout << el << ' ' << answ[el] << '\n';
    }
    ans = 4;
    cout << ans << '\n';
}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
#endif
    _upgrade
    int T = 1;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














