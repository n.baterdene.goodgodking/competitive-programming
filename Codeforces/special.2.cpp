#include <iostream>
using namespace std;
int main () {
    int n, q;
    cin >> n;
    int num[n+1];
    for (int i = 1; i <= n; i++){
        cin >> num[i];
    } 
    for (int i = 1; i <= n; i++){
        for (int a = 1;  a <= n - 1; a++){
            if (num[a] < num[a+1]){
                q = num[a];
                num[a] = num[a+1];
                num[a+1] = q;
            }
        }
    }
    for( int a = n; a > 0; a--){
        cout << num[a]<< ' '; 
    }
    cout << endl;
    return 0;
}