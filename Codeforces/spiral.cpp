#include <iostream>
#include <cstring>
#include <iomanip>
using namespace std;
int main () {
    int n;
    cin >> n;
    int husnegt1[n + 2][n + 2],husnegt2[n + 2][n + 2];
    memset(husnegt1, 0, sizeof(husnegt1));
    memset(husnegt2, 0, sizeof(husnegt2));
    for(int i = 0; i <= n + 1; i++){
        for(int j = 0; j <= n + 1; j++){
            if(i == 0){
                husnegt2[i][j] = -1;
            }
            if(i == n + 1){
                husnegt2[i][j] = -1;
            }
            if(j == 0){
                husnegt2[i][j] = -1;
            }
            if(j == n + 1){
                husnegt2[i][j] = -1;
            }

        }
    }
    int i = 1, j = 1, q = 1, number = 1;
    while(number <= n * n){
        husnegt1[i][j] = number;
        husnegt2[i][j] = -1;
        number++;
        if(q == 1){
            if(husnegt2[i][j + 1] == 0){
                j++;
            } else{
                q = 2;
            }
        }
        if(q == 2){
            if(husnegt2[i + 1][j] == 0){
                i++;
            } else{
                q = 3;
            }
        }
        if(q == 3){
            if(husnegt2[i][j - 1] == 0){
                j--;
            } else{
                q = 4;
            }
        }
        if(q == 4){
            if(husnegt2[i - 1][j] == 0){
                i--;
            } else{
                j++;
                q = 1;
            }
        }
    }
    for(int i = 1; i <= n; i++){
        for(int j = 1; j <= n; j++){
            cout << setw(3) << husnegt1[i][j];
        }
        cout << "\n";
    }

    return 0;
}