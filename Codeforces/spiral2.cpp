#include <iostream>
#include <cstring>
#include <iomanip>
using namespace std;
int main () {
    int n, m;
    cin >> n >> m;
    int husnegt1[n + 2][m + 2],husnegt2[n + 2][m + 2];
    memset(husnegt1, 0, sizeof(husnegt1));
    memset(husnegt2, 0, sizeof(husnegt2));
    for(int i = 0; i <= n + 1; i++){
        for(int j = 0; j <= m + 1; j++){
            if(i == 0){
                husnegt2[i][j] = -1;
            }
            if(i == n + 1){
                husnegt2[i][j] = -1;
            }
            if(j == 0){
                husnegt2[i][j] = -1;
            }
            if(j == m + 1){
                husnegt2[i][j] = -1;
            }

        }
    }
    int i = 1, j = 1, q = 1, number = 1;
    while(number <= n * m / 2){
        if(q == 1){
            husnegt1[i][j] = number;
            husnegt2[i][j] = -1;
            number++;
            q = 2;
            //i++;
            if (number == 2)
                j++;
            else {
                i++;
                j--;
            }
        }
        if(q == 2){
            husnegt1[i][j] = number;
            husnegt2[i][j] = -1;
            number++;
            if(husnegt2[i + 1][j - 1] != -1){
                i ++;
                j--;
            } else{
                i++;
                q = 3;
            }
        }
        if(q == 3){
            husnegt1[i][j] = number;
            husnegt2[i][j] = -1;
            number++;
            q = 4;
            i--;
            j++;
        }
        if(q == 4){
            husnegt1[i][j] = number;
            husnegt2[i][j] = -1;
            number++;
            if(husnegt2[i - 1][j + 1] != -1){
                i--;
                j++;
            } else {
                j++;
                q = 1;
            }
           
        }
    }
     while(number <= n * m){
        if(q == 1){
            husnegt1[i][j] = number;
            husnegt2[i][j] = -1;
            number++;
            q = 2;
            //i++;
            if (number == 2)
                j++;
            else {
                i++;
                j--;
            }
        }
        if(q == 2){
            husnegt1[i][j] = number;
            husnegt2[i][j] = -1;
            number++;
            if(husnegt2[i + 1][j - 1] != -1){
                i ++;
                j--;
            } else{
                j++;
                q = 3;
            }
        }
        if(q == 3){
            husnegt1[i][j] = number;
            husnegt2[i][j] = -1;
            number++;
            q = 4;
            i--;
            j++;
        }
        if(q == 4){
            husnegt1[i][j] = number;
            husnegt2[i][j] = -1;
            number++;
            if(husnegt2[i - 1][j + 1] != -1){
                i--;
                j++;
            } else {
                i++;
                q = 1;
            }
           
        }
    }
    for(int i = 1; i <= n; i++){
        for(int j = 1; j <= m; j++){
            cout << setw(3) << husnegt1[i][j];
        }
        cout << "\n";
    }

    return 0;
}