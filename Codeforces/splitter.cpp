#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
struct Node {
	int val;
	Node *next;
};

struct LinkedList{
	
	Node *head, *tail, *pointer1, *pointer2;

	LinkedList() {
		head = NULL;
	}
	void insert(int number) {
		if (head == NULL) {
			head = new Node();
			head->val = number;
			head->next = NULL;
			tail = head;
			return;
		}
		Node *node = new Node();
		node->val = number;
		tail->next = node;
		tail = tail->next;
	}

	void build(vector <int>& numbers) {
		int n = numbers.size();

		for (int i = 0; i < n; i++) 
			insert(numbers[i]);
	}
    void split() {

        if (head == NULL) 
            return;

        pointer1 = head;
        pointer2 = head;
        Node *node1, *node2;
        while (true) {
            for(int i = 0; i < 2; i ++) {
                if(pointer2->next != NULL) 
                    pointer2 = pointer2->next;
                else 
                    return;
            }
            pointer1 = pointer1->next;
            
        }

        return;
    }
};

int main () {
	int n;
	cin >> n;
	vector <int> numbers(n, 0);
	for (int i = 0; i < n; i++)
		cin >> numbers[i];

	LinkedList mysort;
	mysort.build(numbers);
    mysort.split();
    cout << (mysort.pointer1)->val << ' ' << (mysort.pointer2)->val << '\n';
	return 0;
}

/*
1 2 3 4 5 6 7 8
.
. .
  .   .
    .     .
      .       .


 */