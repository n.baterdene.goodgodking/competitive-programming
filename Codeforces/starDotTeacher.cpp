#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

void fill(vector<string>& mesh, vector<int>& squares, int idx) {
    if (idx == squares.size())
        return;
    
    for (int i = 0; i < squares[idx]; i++){
        mesh[i][squares[idx] - 1] = '*';
        mesh[squares[idx] - 1][i] = '*';
    }

    fill(mesh, squares, idx + 1);
}

int main () {
    int n, max_size = 0;
    cin >> n;
    vector<int> squares(n);
    for (int i = 0; i < n; i++){
        cin >> squares[i];
        max_size = max(max_size, squares[i]);
    }

    string tmp;
    tmp.resize(max_size, ' ');
    vector<string> mesh(max_size, tmp);

    fill(mesh, squares, 0);

    for (int i = 0; i < mesh.size(); i++)
        cout << mesh[i] << endl;

    return 0;
}