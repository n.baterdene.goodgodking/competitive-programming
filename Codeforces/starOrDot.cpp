#include <iostream>
#include <vector>

#define pb push_back
using namespace std;
int main () {
    int n, cur;
    cin >> n;
    int maximum = 0;
    vector <bool> len;
    for (int i = 0; i < n; i++) {
        cin >> cur;
        if (cur > maximum) {
            while (cur > maximum) {
                if (maximum + 1 == cur) len.pb(true);
                else len.pb(false);               
                maximum++;
            }
        } else {
            len[cur] = true;
        }
    }
    if (len.size() > 0) len[0] = true;
    // for (int i = 0; i < len.size(); i++) {
    //     for (int j = 0; j < len.size(); j++) {
    //         int pos = max(i, j);
    //         if (i == 0 || j == 0) pos = 0;
    //         if (len[pos]) cout << '*';
    //         else cout << '.';
    //     }
    //     cout << '\n';
    // }
    return 0;
}