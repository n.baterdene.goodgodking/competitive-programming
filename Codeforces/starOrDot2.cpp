#include <iostream>
#include <vector>

#define pb push_back
using namespace std;
void fill(int i, int j, vector <string>& table) {
    if(j == 0) return;
    if(i >= j) return fill(i, j - 1, table);
    else return fill(i + 1, j, table);
}
int main () {
    int n, cur;
    vector <int> val;
    cin >> n; 
    for(int i = 0; i < n; i++) {
        cin >> cur;
        val.pb(cur);
    }
    sort(val.begin(), val.end());
    string tmp = "";
    for(int i = 0; i < val.back(); i++) 
        tmp += '.';

    vector <string> table(val.back(), tmp);
    for(int i = 0; i < val.size() - 1; i++) 
        fill(0, val[i], table);
    
    for(int i = 0; i < val.back(); i++) {
        for(int j = 0; j < val.back(); j++) {
            if(i == 0 || j == 0 || j == val.back() - 1 || i == val.back() - 1) cout << '*';
            else cout << table[i][j];
        }
        cout << '\n';
    }
    return 0;
}