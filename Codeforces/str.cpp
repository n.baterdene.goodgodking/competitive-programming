#include <iostream>
using namespace std;
string myFunc(string str, int n){
    if(n == 0) return str;
        string new_str;
        for(int i = 0; i < str.size(); i++){
            if(str[i] == '1') new_str += "10";
            else new_str += "01";
        }
    return myFunc(new_str.x, n - 1);
}
int main () {
    freopen("in.txt", "w", stdout);   
    string str = "0";
    int max = 30;
    cout << myFunc(str, max) << '\n';

    return 0;
}