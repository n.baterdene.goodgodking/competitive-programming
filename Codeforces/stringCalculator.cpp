#include <bits/stdc++.h>
using namespace std;

#define _upgrade ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define II <int, int>
#define Q queue
#define ff first
#define ss second
#define rs resize
#define ins insert 
#define fr front() 
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define PQ priority_queue
#define umap unordered_map

typedef long long ll;
typedef double db;
typedef unsigned uint;
typedef unsigned long long ull;
typedef unordered_map<int, int> umapII;
#define LL (ll)
// PQ going up <int, VI, greater<int> >
typedef vector<int> VI;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<bool> VB;
typedef vector<VB> VVB;
typedef vector<VVB> VVVB;
typedef vector<umapII> VumapII;
typedef vector<ll> VLL;
typedef vector<VLL> VVLL;
typedef vector<VVLL> VVVLL;
typedef vector<VVVLL> VVVVLL;
typedef vector<db> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VPI> VVPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
typedef vector<VVVI> VVVVI;
typedef vector<VVVVI> VVVVVI;
mt19937 rng(chrono::high_resolution_clock::now().time_since_epoch().count() + reinterpret_cast<unsigned long>(new int) + *(new unsigned long));

int dx[] = {1, -1, 0, 0, 1, 1, -1, -1};
int dy[] = {0, 0, 1, -1, 1, -1, 1, -1};
const int MOD = 1e9+7;
const int INF = 1e8;

struct calculator {
    string s;
    int n;
    bool isNum(int st) {
        return (st < n && s[st] <= '9' && s[st] >= '0');
    }
    PLL extractNum(int st) {
        ll val = 0, last = st;
        for (int i = st; i < n; i++) {
            if (isNum(i)) {
                val = val * 10 + s[i] - '0';
                last = i;
            }
            else break;
        }
        return mp(val, last);
    }
    PLL solve(int st) {
        VI exps(1, 1), nums;
        ll val, ed, last = st, res = 0;
        for (int i = st; i < n; i++) {
            last = i;
            if (s[i] == '(' || isNum(i)) {
                tie(val, ed) = (s[i] == '(') ? solve(i+1) : extractNum(i);
                nums.pb(val);
                i = ed;
                continue;
            }
            if (s[i] == ')') break;
            if (s[i] == '+') exps.pb(1);
            if (s[i] == '-') exps.pb(-1);
        }
        for (int i = 0; i < nums.size(); i++) 
            res += nums[i]*exps[i + (nums.size() != exps.size())];
        return mp(res, last);
    }
    int calculate(string _s) {
        s = "";
        for (auto& el : _s) 
            if (el != ' ') s += el;
        
        n = s.size();
        int res = 0;
        return solve(0).ff;
    }
};


int main() {
    return 0;
}

























