#include <iostream>
#include <algorithm>
#include <vector>
#include <iomanip>
using namespace std;

int main () {
    string a, b;
    cin >> a >> b;
    string ans = "";
    vector < vector <char> > dp( a.size() + 1, vector <char> (b.size() + 1) );
    int size1 = a.size(), size2 = b.size();
    for (int i = 1; i <= size1; ++i)
        for (int j = 1; j <= size2; ++j)
            if (a[i - 1] == b[j - 1]) dp[i][j] = dp[i - 1][j - 1] + 1;
            else dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);

    int i = size1, j = size2;
    while(i > 0 || j > 0){
        if (i > 0 && dp[i][j] == dp[i - 1][j]) {
            ans = a[i - 1] + ans;
            i--;
        } else {
            if (j > 0 && dp[i][j] == dp[i][j - 1]) {
                ans = b[j - 1] + ans;
                j--;
            } else {
                ans = a[i - 1] + ans;
                i--;
                j--;
            }
        }
    }
    cout << ans << '\n';
    return 0;
}