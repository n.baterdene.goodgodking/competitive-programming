#include <iostream>
#include <algorithm>
#include <vector>
#include <iomanip>
using namespace std;
int main () {
    //start
    string a, b;
    cin >> a >> b;
    
    if(a.size() < b.size()) swap(a, b);
    
    int size1 = a.size(), size2 = b.size();
    vector < vector <string> > answers(size1 + 1, vector <string>(size2 + 1, ""));
    vector < vector <int> > dp(size1 + 1, vector <int>(size2 + 1, 0));
    vector < vector <int> > ind(size1 + 1, vector <int>(size2 + 1, -1));

    for(int i = 1; i <= size1; i++) {
        for(int j = 1; j <= size2; j++) {
            int ok = 0, index = 0;

            for(int k = i-1; k >= 0; k--) {
                if(a[k] == b[j-1]) ok = 1, index = k;
                int now = dp[k][j-1] + ok;
                if(now > dp[i][j]) {
                    if(ok) ind[i][j] = index;
                    else ind[i][j] = -1;
                    dp[i][j] = now;
                }
            }
            if(dp[i][j-1] > dp[i][j]) {
                dp[i][j] = dp[i][j-1];
                ind[i][j] = -1;
            }
        }
    }
    string ans = a, now = "";
    
    int l = size1, r = size2;
    while(r > 0) {
        int i = ind[l][r];
        if(i == -1) {
            now = b[r-1] + now;
            r--;
        } else {
            ans = ans.substr(0, i) + now + ans.substr(i, ans.size() - i);
            now = "";
            r--;
            l = i;
        }
    }
    ans = now + ans;
    cout << ans << endl;
    return 0;
}