#include <bits/stdc++.h>
using namespace std;
int n, m;
string s, vowels = "AEIOUYaeiouy", ans = "";
int main () {
    cin >> s;
    n = s.size();
    m = vowels.size();
    for (int i = 0; i < n; i++) {
        bool isVowel = false;
        for (int j = 0; j < m; j++) {
            if (s[i] == vowels[j]) isVowel = true;
        }
        if (isVowel) continue;
        char addingCharacter = s[i];
        if ('A' <= addingCharacter && addingCharacter <= 'Z') {
            addingCharacter = addingCharacter - 'A' + 'a';
        }
        ans = ans + '.' + addingCharacter;
    }
    cout << ans << endl;
    return 0;
}
