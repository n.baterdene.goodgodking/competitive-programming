#include <iostream>
using namespace std;
struct stak {
    int val;
    stak * next;
};
stak * head;
void insert(int num) {
    if(head == NULL) {
        head = new stak();
        head->val = num;
        head->next = NULL;
        return;
    }
    stak * node = new stak();
    node->val = num;
    node->next = head;
    head = node;
}
void pop() {
    stak * pointer = head;
    head = head->next;
    delete pointer;
}
int main () {
    stak numbers;
    int num;
    string cmd;
    while(true) {
        cin >> cmd;
        if(cmd == "insert") {
            cin >> num;
            insert(num);
        } 
        if(cmd == "pop") 
            pop();

        if(cmd == "done")
            break;
    }
    stak * pointer = head;
    cout << "Numbers: ";
    if(pointer == NULL) {
        cout << '\n';
        return 0;
    }
    while(pointer->next != NULL) {
        cout << pointer->val << ' ';
        pointer = pointer->next;
    }
    cout << pointer->val << '\n';

    return 0;
}