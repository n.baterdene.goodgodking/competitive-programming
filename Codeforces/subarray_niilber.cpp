#include <iostream>
using namespace std;
int main () {
    int n;
    cin >> n;
    int num[n], answer, max_ans, st = 0, end = n;
    for(int i = 0; i < n; i++){
        cin >> num[i];
        if(i == 0)
            max_ans = num[i];
        answer = num[i];
        if(i == st && num[i] < 0){
            if(answer > max_ans)
                max_ans = answer;
            st++;
        }
    }
    for(int i = st + 1; i < n; i++){
        answer += num[i];
        if(answer < 0){
            answer = 0;
        }
        if(answer > max_ans){
            max_ans = answer;
        }

    }
    cout << max_ans<< endl;
    return 0;
}