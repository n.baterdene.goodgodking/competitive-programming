#include <bits/stdc++.h>

#define pb push_back
using namespace std;
vector <vector <int> > answers;
vector <int> numbers;
int n;
void makeSubSet(int idx, vector <int>& cur) {
    if (idx > n) return;
    if (idx == n) {
        if (cur.size() != 0) answers.pb(cur);
        return;
    }
    makeSubSet(idx + 1, cur);
    cur.pb(numbers[idx]);
    makeSubSet(idx + 1, cur);
    cur.pop_back();
}
int main () {
    cin >> n;
    numbers.resize(n);
    for (int i = 0; i < n; i++)
        cin >> numbers[i];

    vector <int> tmp;
    cout << "here\n";
    makeSubSet(0, tmp);
    for (int i = 0; i < answers.size(); i++) {
        for (int j = 0; j < answers[i].size(); j++) 
            cout << answers[i][j] << ' ';
        cout << '\n';
    } 
    return 0;
}