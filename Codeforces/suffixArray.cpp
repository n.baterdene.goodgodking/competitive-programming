#include <bits/stdc++.h>
// #pragma GCC optimize("O3")
// #pragma GCC optimize("unroll-loops")
// #pragma GCC target("avx2")
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define FOR(i, a, b) for (int i = a; i < b; i++)
#define FORB(i, a, b) for (int i = a - 1; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i <= a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define pb push_back
#define mp make_pair
#define umap unordered_map
#define ff first
#define ss second
 
typedef long long ll;
typedef double d;
typedef unsigned uint;
typedef unsigned long long ull;
 
typedef vector<int> VI;
typedef vector<bool> VB;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<ll> VLL;
typedef vector<VLL> VVLL;
typedef vector<d> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
 
const int MOD = int(1e9) + 7;
const int INF = INT_MAX;
const ll INFF = INT64_MAX;
const d EPS = 1e-9;
const d PI = acos(-1.0); //M_PI;
const int moveX[] = {-1, 0, 1, 0};
const int moveY[] = {0, 1, 0, -1};



typedef pair<int, int> Rank;

namespace RadixSort {
    template <typename T>
    void sort(vector<T> &arr, const int max_element, int (*get_key)(T &), int begin = 0) {
        const int n = arr.size();
        vector<T> new_order(n);
        vector<int> count(max_element + 1, 0);

        for (int i = begin; i < n; i++)
            count[get_key(arr[i])]++;

        for (int i = 1; i <= max_element; i++)
            count[i] += count[i - 1];

        for (int i = n - 1; i >= begin; i--) {
            new_order[count[get_key(arr[i])] - (begin == 0)] = arr[i];
            count[get_key(arr[i])]--;
        }

        arr.swap(new_order);
    }

    template <typename T> void sort_pairs(vector<T> &arr, const int rank_size) {
    RadixSort::sort<T>(arr, rank_size, [](T &item) { return item.first.second; }, 0ll);
    RadixSort::sort<T>(arr, rank_size, [](T &item) { return item.first.first; }, 0ll);
    }
}
class SuffixArray {
private:
    string s;
    int n;
    typedef pair<int, int> Rank;
    void build_ranks(const vector<pair<Rank, int>> &ranks, vector<int> &ret) {
        ret[ranks[0].second] = 1;
        for (int i = 1; i < n; i++) {
            if (ranks[i - 1].first == ranks[i].first)
                ret[ranks[i].second] = ret[ranks[i - 1].second];
            else
                ret[ranks[i].second] = ret[ranks[i - 1].second] + 1;
        }
    }
    vector<int> build_suffix_array() {
        vector<pair<Rank, int>> ranks(this->n);
        vector<int> arr(this->n);

        for (int i = 0; i < n; i++)
        ranks[i] = pair<Rank, int>(Rank(s[i], 0), i);

        RadixSort::sort_pairs(ranks, 256);
        build_ranks(ranks, arr);

        {
        int jump = 1;
        int max_rank = arr[ranks.back().second];
        while (max_rank != this->n) {
            for (int i = 0; i < this->n; i++) {
            ranks[i].first.first = arr[i];
            ranks[i].first.second = (i + jump < this->n ? arr[i + jump] : 0);
            ranks[i].second = i;
            }

            RadixSort::sort_pairs(ranks, n);
            build_ranks(ranks, arr);

            max_rank = arr[ranks.back().second];
            jump *= 2;
        }
        }

        vector<int> sa(this->n);
        for (int i = 0; i < this->n; i++)
        sa[arr[i] - 1] = i;
        return sa;
    }
    vector<int> build_lcp() {
        lcp.resize(n, 0);
        vector<int> inverse_suffix(n);

        for (int i = 0; i < n; i++)
        inverse_suffix[sa[i]] = i;

        int k = 0;

        for (int i = 0; i < n; i++) {
        if (inverse_suffix[i] == n - 1) {
            k = 0;
            continue;
        }

        int j = sa[inverse_suffix[i] + 1];

        while (i + k < n && j + k < n && s[i + k] == s[j + k])
            k++;

        lcp[inverse_suffix[i]] = k;

        if (k > 0)
            k--;
        }

        return lcp;
    }
public:
    vector<int> sa;
    vector<int> lcp;
    SuffixArray(string &s) {
        this->n = s.size();
        this->s = s;
        this->sa = build_suffix_array();
        this->lcp = build_lcp();
    }
};




















































