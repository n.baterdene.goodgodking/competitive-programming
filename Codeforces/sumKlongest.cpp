#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;
unordered_map <int, pair <int, int> > seen; 
vector <int> numbers, dp;
int main () {
    int n, sum, tmp, neg, hoyr;
    cin >> n;
    for(int i = 0; i < n; i++) {
        cin >> tmp;
        numbers.push_back(tmp);

        if(i > 0) dp.push_back(dp[i - 1] + tmp);

        else dp.push_back(tmp);
        
        if(seen.find(dp[i]) == seen.end()) 
            seen[dp[i]] = make_pair(i + 1,i + 1);
        else 
            seen[dp[i]].second++;
    }   
    cin >> sum;
    for(int i = 0; i < n; i++) 
        cout << dp[i] << ' ';
    cout << '\n';
    int maximum = -1;
    for(int i = dp.size() - 1; i >= 0; i--) 
        if(seen.find(dp[i] - sum) != seen.end()) 
            maximum = max(seen[dp[i]].second - seen[dp[i] - sum].first, maximum);
    if(maximum != -1) cout << maximum << '\n';
    return 0;
}