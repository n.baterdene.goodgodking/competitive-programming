#include <bits/stdc++.h>
#define FOR(i, a, b) for (int i = a; i < b; i++)
#define FORB(i, a, b) for (int i = a - 1; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i <= a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define pb push_back
#define mp make_pair
#define umap unordered_map
#define ff first
#define ss second
 
typedef long long ll;
typedef double d;
typedef unsigned uint;
typedef unsigned long long ull;
 
typedef vector<int> VI;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<ll> VLL;
typedef vector<VLL> VVLL;
typedef vector<d> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
 
const int MOD = int(1e9) + 7;
const int INF = INT_MAX;
const ll INFF = INT64_MAX;
const d EPS = 1e-9;
const d PI = acos(-1.0); //M_PI;
const int moveX[] = {-1, 0, 1, 0};
const int moveY[] = {0, 1, 0, -1};

class Solution {
public:
    
    int n;
    const int maxNum = 2e4 + 5; //IHDEE 20000
    unordered_map <int, int> counter; // TOOG TOOLOHOD ASHIGLAV
    VI binaryIdxTree; // binary indexed tree
    VLL pow2; //2 iin  zergiig hadgalah massive

    void addVal(vector<int>& binaryIdxTree, int idx) {
        if (idx > binaryIdxTree.size()) return;
        binaryIdxTree[idx]++;
        return addVal(binaryIdxTree, idx + (idx & -idx));
    }
    
    int findVal(vector<int>& binaryIdxTree, int idx) {
        if (idx <= 0) return 0;
        return  binaryIdxTree[idx] + findVal(binaryIdxTree, idx - (idx & -idx));
    }
    int sumSubseqWidths(vector<int>& numbers) {
        n = numbers.size();
        binaryIdxTree.resize(maxNum + 100, 0);
        pow2.resize(maxNum);
        pow2[0] = 1;
        REP(i, n) {
            addVal(binaryIdxTree, numbers[i] + 1);
            counter[numbers[i]]++;  // ene too hed orsong tooloh
        }
        //INIT POW2
        REP1(i, n + 1) 
            pow2[i] = pow2[i - 1] * 2 % MOD; 

        ll ans = 0, up, down, i;
        for (auto el : counter) {
            int i = el.ff, curCount = el.ss;
            if (curCount == 0) continue;
            down = findVal(binaryIdxTree, i);
            up = n - down - curCount;
            
            // max - min ni ene massiviin width
            // ene ni ymar neg jisheenii max uyd (pow2[down] ooroos ni baga too baigaa ) ba ene uyd + pow2[down]
            // ene ni ymar neg jisheenii min uyd (pow2[up]  ooroos ni ih too baigaa )ba ene uyd + pow2[up]
            ll curAns = (pow2[down] - pow2[up]) % MOD * (pow2[curCount] - 1) % MOD;
            ans = (ans + (curAns * i)) % MOD;
        }
        return (ans + MOD) % MOD;
        
    }
};