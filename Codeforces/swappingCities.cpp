#include "swap.h"
#include <bits/stdc++.h>
using namespace std;
int n, m;
const int N = 1e5 + 5;
const int M = 2e5 + 5;
const int INF = 1e9 + 5;
vector <vector <int> > paths;
vector <int> par(N), sizes(N), deg(N), asc(N), line(N);
int find(int a) {
	if (par[a] == a) return a;
	return find(par[a]);
}
void init(int n1, int m1, vector<int> U, vector<int> V, vector<int> W) {
	n = n1, m = m1;
	for (int i = 0; i < n; i++) {
		par[i] = i;
		sizes[i] = 1;
		asc[i] = line[i] = INF;
	}
	for (int i = 0; i < m; i++) {
		paths.push_back({W[i], U[i], V[i]});
	}
	sort(paths.begin(), paths.end());
	for (int i = 0; i < m; i++) {
		int a = paths[i][1], b = paths[i][2], cost = paths[i][0];
		deg[a]++, deg[b]++;
		int A = find(a), B = find(b);
		if (A != B) {
			if (sizes[A] < sizes[B]) swap(A, B);
			par[B] = A, asc[B] = cost;
			if (line[B] < INF) line[A] = min(line[A], cost);
			sizes[A] += sizes[B];
		}
		if (deg[a] > 2 || deg[b] > 2 || A == B) line[A] = min(line[A], cost);
	}
}

int getMinimumFuelCapacity(int x, int y) {
	int ans = INF, l = 0, r = INF;
	while(l <= r) {
		int mid = (l + r) / 2;
		int a = x, b = y;
		bool skip = 0;
		while(asc[a] <= mid && !skip) {
			if (par[a] == a) skip = 1;
			a = par[a];
		}
		while(asc[b] <= mid && !skip) {
			if (par[b] == b) skip = 1;
			b = par[b];
		}
		if ((a == b && line[a] <= mid) || skip) {
			if (!skip) ans = min(ans, mid);
			r = mid - 1;
		} else {
			l = mid + 1;
			x = a, y = b;
		}
	}
  	return (ans == INF ? -1 : ans);
}