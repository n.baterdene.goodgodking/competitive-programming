#include <bits/stdc++.h>
using namespace std;

int main () {
    int t = 1, n, m, a, b, c, d;
    cin >> t;
    while(t--) {
        cin >> n >> m;
        bool notAns = 1;
        for (int i = 0; i < n; i++) {
            cin >> a >> b >> c >> d;
            if (b == c) notAns = 0;
        }
        if (m & 1 || notAns) {
            cout << "NO\n";
            continue;
        }
        cout << "YES\n";
    }
    return 0;
}