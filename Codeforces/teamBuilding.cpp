#include <bits/stdc++.h>
// #pragma GCC optimize("O3, unroll-loops")
// #pragma GCC target("avx2")
// #define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define sz(x) (int)x.size()
#define chmin(a, b) a=min(a,(b))
#define chmax(a, b) a=max(a,(b))

using ll = long long;
using db = double;
using ld = long double;
using uint = unsigned;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using PII = pair<int, int>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using PLL = pair<ll, ll>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}
/*

|                    _                                                     
|  __   _           | |                                                    
| |   \| | ___  ___ | |_                                                   
| | |\ | |/ _ \/ __/| __|                                                  
| | | \  |  __/\__ \| |_                                                   
| |_|  \_|\___ /___/ \__|                                                  
|                                                      _         _   _     
|                                                     (_)       | | | |    
|  _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_   
| | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|  
| | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_   
| | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|  
| | |               __/ |                                                  
| |_|              |___/                   _                               
|                                  _      (_)                              
|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                               
| | '_ ` _ \ / _` |_   / _` |/ _` | |/ _` | |                              
| | | | | | | (_|  /  / (_| | (_| | | (_| | |                              
| |_| |_| |_|\__,_|____\__,_|\__,_|_|\__,_|_|                              
*/

const db PI = acos(-1.0); //M_PI;
const ll INFF = 4e18;
const int INF = 1.07e9;
const int MOD = 1e9 + 7;
const int MOD1 = 998244353; //7*19*2^23 +1;
const int dx[] = {-1, 0, 1, 0, -1, -1, 1, 1};
const int dy[] = {0, 1, 0, -1, -1, 1, -1, 1};




const int N = 1e5 + 5;
const int P = 7;
const int K = 1e5 + 5;


int n, m, k, q, p; // p - positions, k - audience
vector <int> people[N];
ll dp1[1<<P];
ll dp2[1<<P][P+1];
void go () {
    // not optimal solution
    // the time complexity of this solution is O(N*P^2*2^P)
    // while the optimal solution's time complexity is O(N*P*2^P)
    cin >> n >> p >> k;
    for (int i = 0; i < n; i++) {
        int x; cin >> x;
        people[i].pb(x);
    }
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < p; j++) {
            int x; cin >> x;
            people[i].pb(x);
        }
        reverse(ALL(people[i]));
    }
    sort(people, people+n, [&](VI&a, VI&b)  {
        if (a[p] != b[p]) return a[p] > b[p];
        return a < b;
    });
    ll ans = 0, sum = 0;
    memset(dp1, -0x3f, sizeof(dp1));
    memset(dp2, -0x3f, sizeof(dp2));
    int zero = dp1[0];
    int fullMask = (1<<p)-1;
    dp1[0] = 0;
    for (int i = 0; i < k; i++) {
        sum += people[i][p];
        for (int mask = fullMask; mask >= 0; mask--) {
            if (dp1[mask] == zero) continue;
            for (int j = 0; j < p; j++) {
                if (mask & (1<<j)) continue;
                chmax(dp1[mask|(1<<j)], dp1[mask]+people[i][j]-people[i][p]);
            }
        }
    }
    dp2[0][0] = 0;
    for (int i = k; i < n; i++) { // n
        for (int mask = fullMask; mask >= 0; mask--) // 2^p
        for (int audience = p; audience >= 0; audience--) { // p
            if (dp2[mask][audience] == zero) continue;
            if (audience < p) chmax(dp2[mask][audience+1], dp2[mask][audience]+people[i][p]);
            for (int j = 0; j < p; j++)  // p
                if (!(mask & (1<<j))) 
                    chmax(dp2[mask|(1<<j)][audience], dp2[mask][audience]+people[i][j]);
        }
    }
    for (int mask = 0; mask <= fullMask; mask++) {
        chmax(ans, dp1[mask]+dp2[fullMask^mask][__builtin_popcount(mask)]);
    }
    cout << ans+sum << '\n';
}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("0.in", "r", stdin);
    freopen("0.out", "w", stdout);
#endif
    _upgrade
    int T = 1;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














    