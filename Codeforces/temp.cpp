#include <bits/stdc++.h>
// #pragma GCC optimize("O3, unroll-loops")
// #pragma GCC target("avx2")
// #define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define ub upper_bound 
#define umap unordered_map
#define sz(x) (int)x.size()
#define chmin(a,b) a=min(a,b)
#define chmax(a,b) a=max(a,b)

using db = double;
using ld = long double;
using sint = short int;
using ll = long long;
using uint = unsigned int;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using PII = pair<int, int>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using PLL = pair<ll, ll>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}
/*

|                    _                                                     
|  __   _           | |                                                    
| |   \| | ___  ___ | |_                                                   
| | |\ | |/ _ \/ __/| __|                                                  
| | | \  |  __/\__ \| |_                                                   
| |_|  \_|\___ /___/ \__|                                                  
|                                                      _         _   _     
|                                                     (_)       | | | |    
|  _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_   
| | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|  
| | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_   
| | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|  
| | |               __/ |                                                  
| |_|              |___/                   _                               
|                                  _      (_)                              
|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                               
| | '_ ` _ \ / _` |_   / _` |/ _` | |/ _` | |                              
| | | | | | | (_|  /  / (_| | (_| | | (_| | |                              
| |_| |_| |_|\__,_|____\__,_|\__,_|_|\__,_|_|                              
*/

const db PI = acos(-1.0); //M_PI;
const ll INFF = 4e18;
const int INF = 1.07e9;
const int MOD = 1e9 + 7;
const int MOD1 = 998244353; //7*19*2^23 +1;
const int dx[] = {-1, 0, 1, 0, -1, -1, 1, 1};
const int dy[] = {0, 1, 0, -1, -1, 1, -1, 1};




const int N = 50 + 5;
const int M = 2e5 + 5;

int nums[N];

int n, m, k, q;
string s;
char c[N][N];
int dp[N][N];
int dp1[N][N];
void go () {
    cin >> n >> m;
    PII st, ed;
    memset(dp, 0x3f, sizeof(dp));
    memset(dp1, 0x3f, sizeof(dp1));
    queue <PII> bfs;
    for (int i = 1; i <= n; i++) 
    for (int j = 1; j <= m; j++) {
        cin >> c[i][j];
        if (c[i][j] == 'D') { // ed
            ed = {i, j};
            dp[i][j] = 2e9;
        } else if (c[i][j] == 'S') { // st
            st = {i, j};
            dp1[i][j] = 0;
            dp[i][j] = 2e9;
        } else if (c[i][j] == 'X') { // wall
            dp[i][j] = 2e9;
        } else if (c[i][j] == '*') { // virus
            dp[i][j] = 0;
            bfs.push({i, j});
        }
    }
    int zero = dp[0][0];
    while(!bfs.empty()) {
        int x, y; tie(x, y) = bfs.front(); bfs.pop();
        for (int i = 0; i < 4; i++) {
            int xx = x + dx[i];
            int yy = y + dy[i];
            if (xx < 1 || yy < 1 || xx > n || yy > m || dp[xx][yy] != zero) continue;
            dp[xx][yy] = dp[x][y]+1;
            bfs.push({xx, yy});
        }
    }
    bfs.push(st);
    while(!bfs.empty()) {
        int x, y; tie(x, y) = bfs.front(); bfs.pop();
        // cout << x << ' ' << y << ' ' << dp1[x][y] << ' ' << dp[x][y] << '\n';
        if (dp1[x][y] >= dp[x][y] || c[x][y] == 'X') continue;
        // cout << "HERE\n";
        for (int i = 0; i < 4; i++) {
            int xx = x + dx[i];
            int yy = y + dy[i];
            if (xx < 1 || yy < 1 || xx > n || yy > m || c[xx][yy] == 'X' || dp1[x][y]+1 >= dp[xx][yy] || dp1[xx][yy] != zero) continue;
            dp1[xx][yy] = dp1[x][y]+1;
            bfs.push({xx, yy});
        }
    }
    // for (int i = 1; i <= n; i++)
    // for (int j = 1; j <= m; j++)
    //     cout << dp[i][j] << "," << dp1[i][j] << " \n"[j==m];
    // cout << zero << '\n';
    cout << (zero != dp1[ed.ff][ed.ss] ? to_string(dp1[ed.ff][ed.ss]) : "bolomjgui") << '\n';
}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("0.in", "r", stdin);
    freopen("0.out", "w", stdout);
#endif
    _upgrade
    int T = 1;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














