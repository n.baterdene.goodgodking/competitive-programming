#include <iostream>
#include <algorithm>
using namespace std;

int main() {
	int n;
	cin >> n;
	pair <int, int> ans[n];
	int num,num2[n];
	for(int i = 0; i < n; i++){
		cin >> num;
		ans[i] = make_pair(num, i + 1);
		num2[i] = num;
	}
	sort(num2, num2 + n);
	for(int i = n - 1; i >= 0; i--){
		int ok = 0;
		for(int j = 0; ok == 0; j++){
			if(ans[j].first == num2[i]){
				ok = 1;
				cout << ans[j].second << ' ';
				ans[j].first = -1;
			}
		}
	}
	return 0;
}