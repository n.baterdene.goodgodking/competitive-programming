#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

vector<int> meetingPlanner( const vector<vector<int>>& a, const vector<vector<int>>& b, int dur)  {
    int p1 = 0, p2 = 0;

    while (p1 < a.size() && p2 < b.size()) {
        int start = max(a[p1][0], b[p2][0]);
        int end = min(a[p1][1], b[p2][1]);
        if (start + dur <= end)
            return {start, start + dur};

        if (a[p1][1] < b[p2][1]) p1++;
        else p2++;
    }
    return {};
}
                       
int main() {
  return 0;
}
