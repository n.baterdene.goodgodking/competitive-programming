def tetgelegt_soril_20(n):
    """
      	($a >2$, $a \in \mathbb{N}$) 
    """
    # parameter

    ## random parameter

    [a_state, a_frac] = number_generator('natural', False, 'mixed', n, 2, False)

    while a_frac < 3: 
        [a_state, a_frac] = number_generator('natural', False, 'mixed', n, 2, False)

    ## calc_parameter
    

    ## operation


    ## coeff


    # bodlogonii uguulber 
    state = rf""" 
    $\displaystyle\int \sin^{{ {a_state} }} x\cdot \cos x\, dx = ?$


    """
    
    # correct answer 
    
    ans_frac = a_frac + 1
    ans_state = specific_generate_rational_number(ans_frac , 0 , 1 , 1, n , 'rndm', 1)[0]
    ans_state = rf""" $\dfrac{{\sin^{{ {ans_state} }} x }}{{ {ans_state} }}+c$ """
    
    
    # wrong answer 
    ## 1st wrong answer 
    wrong1_frac = -a_frac / (1-2*a_frac**2)
    wrong1_state = specific_generate_rational_number(wrong1_frac , 0 , 1 , 1, n , 'rndm', 1)[0]
    wrong1_state = rf""" $\dfrac{{\sin^{{ {a_state} }} x}}{{ {a_state} }}+c$ """
    ## 2nd wrong answer 
 
    wrong2_state = rf""" $\dfrac{{\sin ^{{ {a_state} }} x}}{{\cos x}}$  """
    ## 3rd wrong answer 
    wrong3_state = rf"""  $\sin^{{ {a_state} }} x+c$ """
    ## 4th wrong answer
    wrong4_frac = a_frac + 1
    wrong4_state = specific_generate_rational_number(wrong4_frac , 0 , 1 , 1, n , 'rndm', 1)[0]
    wrong4_state = rf""" $\dfrac{{\sin^{{ {wrong4_state} }}x}}{{\cos x}}+c$"""
    
    # HINT
    hint = rf""" $f(x) = a^x \Rightarrow f^{{-1}}(x) = \log_a x $ """

    # Solution
    solution = rf""" $ f^{{-1}}(x) =  \log_{{ {a_state} }} \dfrac{{x}}{{{a_state}^{{ {b_state} }}}} \Rightarrow $ 
    $ g(f^{{-1}}(x)) = {a_state}^{{ \log_{{ {a_state} }} \dfrac{{x}}{{{a_state}^{{ {b_state} }}}} }} = \dfrac{{x}}{{{a_state}^{{ {b_state} }}}} $
    """
    
    return state, ans_state, wrong1_state, wrong2_state, wrong3_state, wrong4_state, hint, solution