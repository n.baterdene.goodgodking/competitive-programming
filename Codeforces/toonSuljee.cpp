#include <bits/stdc++.h>
#define pb push_back
using namespace std;
int n = 10, answer = 0;
unordered_map <int, bool> numbers;
vector <vector <int> > allAns;
void solve(vector <int>& ans, int used) {
    if (ans[0] == 0) return;
    // cout << used << '\n';
    if (used == n) {
        long long tmp = ans[0];
        for (int i = 1; i < n; i++) 
            tmp = tmp * 10 + ans[i];
        
        if (tmp % 11111 == 0) answer++;
        return;
    }
    for (int i = 0; i < n; i++) {
        if (!numbers[i]) continue;
        numbers[i] = false;
        ans[used] = i;
        solve(ans, used + 1);
        numbers[i] = true;
    }
}

int main () {
    for (int i = 0; i < n; i++) 
        numbers[i] = true;
    vector <int> ans(10);
    solve(ans, 0);
    cout << answer << "\n";
    return 0;
}