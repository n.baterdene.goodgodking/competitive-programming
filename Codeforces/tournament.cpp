#include<bits/stdc++.h> 
#define mp make_pair
const int N = 1025;

using namespace std; 
int n, m, ini, idx = 0, p, q;
vector <vector <string> > tour[2];
unordered_map <string, vector <string> > score;
unordered_map <string, unordered_map <string, bool> > seen;
vector <string> solve(int h, int pos) {
	vector <string> ans, l, r;
	string cur = tour[1][h][pos];
	if (cur != "?") ans.push_back(cur);

	if (h == 0) return score[cur] = ans;
	l = solve(h - 1, pos * 2);
	r = solve(h - 1, pos * 2 + 1);
	for (auto el : l) 
		if (el != cur) ans.push_back(el);
	for (auto el : r) 
		if (el != cur) ans.push_back(el);
	if (tour[1][h + 1][pos / 2] != cur) {
		for (auto el : ans) {
			seen[cur][el] = 1;
		}
	}
	if (tour[1][h][pos] != "?") return ans;
	return score[cur] = ans;
}
vector <string> solve2(int h, int pos) {
	vector <string> ans, l, r;
	string cur = tour[0][h][pos];
	if (cur != "?") ans.push_back(cur);

	if (h == 0) return ans;
	l = solve(h - 1, pos * 2);
	r = solve(h - 1, pos * 2 + 1);

	for (auto el : l) 
		if (el != cur) ans.push_back(el);

	for (auto el : r) 
		if (el != cur) ans.push_back(el);
	if (cur == "bayaraa") {
		cout << "here: " << h << ' ' << pos << '\n';
	}	
	if (tour[0][h + 1][pos / 2] != cur) {
		if (score.find(cur) != score.end()) {
			for (auto el : score[cur]) {
				ans.push_back(el);
			}
		}
		for (auto el : ans) {
			seen[cur][el] = 1;
		}
	}
	if (tour[0][h][pos] != "?") return ans;
	return ans;
}
vector <string> solveFind(int h, int pos) {
	vector <string> ans, a, b;
	if (tour[0][h][pos] != "?") {
		ans.push_back(tour[0][h][pos]);
		return ans;
	}
	if (h == 0) return ans;
	a = solveFind(h - 1, pos * 2);
	b = solveFind(h - 1, pos * 2 + 1);
	for (auto el1 : a) {
		for (auto el2 : b) {
			if (seen[el2].find(el1) == seen[el2].end()) {
				ans.push_back(el2);
				break;
			}
		}
	} 
	for (auto el1 : b) {
		for (auto el2 : a) {
			if (seen[el2].find(el1) == seen[el2].end()) {
				ans.push_back(el2);
				break;
			}
		}
	} 
	return ans;
}
signed main() { 
	if (1) {
		// freopen("in.txt", "r", stdin);
		ios_base::sync_with_stdio(0); 
		cin.tie(0); 
		cout.tie(0);
		tour[0].resize(15, vector <string>(N));
		tour[1].resize(15, vector <string>(N));
	}
	cin >> n;
	ini = n;
	string str;
	while(ini > 0) {
		for (int i = 0; i < ini; i++) {
			cin >> str;
			tour[0][idx][i] = str;
		}
		ini /= 2;
		idx++;
	}
	p = idx - 1;
	cin >> m;
	idx = 0;
	ini = m;
	while(ini > 0) {
		for (int i = 0; i < ini; i++) {
			cin >> str;
			tour[1][idx][i] = str;
		}
		ini /= 2;
		idx++;
	}
	q = idx - 1;
	idx = 0;
	vector <string> ans;
	if (tour[0][p][0] == "?") {
		for (int i = 0; i < 2; i++) {
			if (tour[0][p - 1][i] != "?")
			ans.push_back(tour[0][p - 1][i]);
		}
	} else {
		ans.push_back(tour[0][p][0]);
	}
	cout << ans.size() << '\n';
	for (auto el : ans) cout << el << '\n';
	// cout << '\n';
	return 0; 
} 
