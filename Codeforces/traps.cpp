#include <bits/stdc++.h>
// #pragma GCC optimize("O3, unroll-loops")
// #pragma GCC target("avx2")
#define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define sz(x) (int)x.size()

using ll = long long;
using db = double;
using ld = long double;
using uint = unsigned;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VS = vector<string>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using VD = vector<db>;
using PII = pair<int, int>;
using PLL = pair<ll, ll>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int bitCnt(int a) {
    bitset <64> b(a);
    return b.count();
}
int bitCnt(string a) {
    bitset <64> b(a);
    return b.count();
}

int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}

void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VPI& a) {
    for (auto el : a) {
        cout << el.ff << ',' << el.ss << ' ';
    }
    cout << '\n';
}

void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VVI& a) {
    for (auto el : a) {
        print(el);
    }
}

/*

|                    _                                                     
|  __   _           | |                                                    
| |   \| | ___  ___ | |_                                                   
| | |\ | |/ _ \/ __/| __|                                                  
| | | \  |  __/\__ \| |_                                                   
| |_|  \_|\___ /___/ \__|                                                  
|                                                      _         _   _     
|                                                     (_)       | | | |    
|  _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_   
| | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|  
| | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_   
| | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|  
| | |               __/ |                                                  
| |_|              |___/                   _                               
|                                  _      (_)                              
|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                               
| | '_ ` _ \ / _` |_   / _` |/ _` | |/ _` | |                              
| | | | | | | (_|  /  / (_| | (_| | | (_| | |                              
| |_| |_| |_|\__,_|____\__,_|\__,_|_|\__,_|_|                              
*/

const db PI = acos(-1.0); //M_PI;
const ll INFF = 4e18;
const int INF = 1.07e15;
const int MOD = 1e9 + 7;
const int MOD1 = 998244353; //7*19*2^23 +1;
const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0, 1, 0, -1};





const int N = 2e5 + 5;
const int M = 4*N;

int nums[N];
int node[M], lazy[M];
int n, m, k, q;
string s;
void build(int l, int r, int head) {
    lazy[head] = 0;
    if (l == r) {
        if (l > n - k) node[head] = l - (n-k) + nums[l];
        else node[head] = INF;
        return;
    }
    int mid = (l+r)/2;
    build(l, mid, head*2+1);
    build(mid+1, r, head*2+2);
    node[head] = min(node[head*2+1], node[head*2+2]);
}
void propagate(int head) {
    if (lazy[head] == 0) return;
    int x = lazy[head]; lazy[head] = 0;
    node[head*2+1] += x;
    lazy[head*2+1] += x;
    node[head*2+2] += x;
    lazy[head*2+2] += x;
}
void update(int l, int r, int L, int R, int head) {
    if (l > R || L > r) return;
    if (L <= l && r <= R) {
        node[head]++;
        lazy[head]++;
        return;
    }
    propagate(head);
    int mid = (l+r)/2;
    update(l, mid, L, R, head*2+1);
    update(mid+1, r, L, R, head*2+2);
    node[head] = min(node[head*2+1], node[head*2+2]);
}
void update1(int l, int r, int id, int val, int head) {
    if (l == r) {
        node[head] = val;
        lazy[head] = 0;
        return;
    }
    propagate(head);
    int mid = (l+r)/2;
    if (id <= mid) update1(l, mid, id, val, head*2+1);
    else update1(mid+1, r, id, val, head*2+2);
    node[head] = min(node[head*2+1], node[head*2+2]);
}
int get(int l, int r, int val, int head) {
    if (l == r) return l;
    propagate(head);
    int mid = (l+r)/2;
    if (node[head*2+1] == val) return get(l, mid, val, head*2+1);
    return get(mid+1, r, val, head*2+2);
}
void dfs(int l, int r, int head) {
    if (l == r) {
        cout << (node[head] >= INF ? -1 : node[head]) << " \n"[r==n];
        return;
    }
    propagate(head);
    int mid = (l+r)/2;
    dfs(l, mid, head*2+1);
    dfs(mid+1, r, head*2+2);
}
void go () {
    cin >> n >> k;
    int ans = 0, cur;
    for (int i = 1; i <= n; i++) {
        cin >> nums[i];
        if (i <= n - k) ans += nums[i];
    }
    build(1, n, 0);
    int ext = 0;
    for (int i = n - k; i > 0; i--) {
        // dfs(1, n, 0);
        int curBest = node[0];
        if (curBest+ext < nums[i]) {
            ans = ans + curBest+ext - nums[i];
            // cout << " + " << curBest+ext << " - " << nums[i] << '\n';
            int pt = get(1, n, curBest, 0);
            update(1, n, 1, pt, 0);

            update1(1, n, i, 1+nums[i], 0);
            update1(1, n, pt, INF, 0);
            ext--;
        }
        ext++;
    }
    // dfs(1, n, 0);
    cout << ans << '\n';
}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("0.in", "r", stdin);
    freopen("0.out", "w", stdout);
#endif
    _upgrade
    int T = 1;
    cin >> T;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/













