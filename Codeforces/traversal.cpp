#include <iostream>

using namespace std;

struct treeNode{
    int val;
    treeNode *right, *left;
    treeNode(int x) {
        val = x;
        right = left = NULL;
    }
};
treeNode *root;
void preOrderTravelsal(treeNode *head) {
    if (head == NULL) return;
    cout << head->val << ' ';
    preOrderTravelsal(head->left);
    preOrderTravelsal(head->right);
}

void inOrderTravelsal(treeNode *head) {
    if (head == NULL) return;
    inOrderTravelsal(head->left);
    cout << head->val << ' ';
    inOrderTravelsal(head->right);
}

void postOrderTravelsal(treeNode *head) {
    if (head == NULL) return;
    postOrderTravelsal(head->left);
    postOrderTravelsal(head->right);
    cout << head->val << ' ';

}
int main () {
    root = new treeNode(0);
    // construct treenode
    if (true) {
        treeNode *lvl0l = new treeNode(10);
        treeNode *lvl0r = new treeNode(100);
        treeNode *lvl1ll = new treeNode(20);
        treeNode *lvl1lr = new treeNode(110);
        treeNode *lvl1rl = new treeNode(110);
        treeNode *lvl1rr = new treeNode(200);
        root->left = lvl0l;
        root->right = lvl0r;
        lvl0l->left = lvl1ll;
        lvl0l->right = lvl1lr;
        lvl0r->left = lvl1rl;
        lvl0r->right = lvl1rr;
    }
    cout << "pre order travelsal: ";
    preOrderTravelsal(root);
    cout << '\n';

    cout << "in order travelsal: ";
    inOrderTravelsal(root);
    cout << '\n';

    cout << "post order travelsal: ";
    postOrderTravelsal(root);
    cout << '\n';
    return 0;    
}