#include <iostream>
#include <vector>

#define pb push_back
using namespace std;
struct TreeNode {
    int val;
    TreeNode *left, *right;
    TreeNode(int num) { 
        val = num;
        left = NULL;
        right = NULL;
    }
};
vector <vector <string> > response;
vector <string> empty;
void pushNull(int idx) {
    if (idx == response.size()) response.pb(empty);
    response[idx].pb("NULL");
}
void build(int idx, TreeNode* node) {
    if (idx == response.size()) response.pb(empty);
    response[idx].pb(to_string(node->val));
    if (node->left == NULL && node->right == NULL) return;
    if (node->left != NULL) build(idx + 1, node->left);
    else pushNull(idx + 1);
    
    if (node->right != NULL) build(idx + 1, node->right);
    else pushNull(idx + 1);
}
int main () {
    TreeNode *root = new TreeNode(1);
    if (true) {
        TreeNode *lvl0r = new TreeNode(2);
        TreeNode *lvl1rl = new TreeNode(3);
        TreeNode *lvl2rll = new TreeNode(4);
        TreeNode *lvl2rlr = new TreeNode(5);
        root->right = lvl0r;
        lvl0r->left = lvl1rl;
        lvl1rl->left = lvl2rll;
        lvl1rl->right = lvl2rlr;
    }
    build(0, root);
    for (int i = 0; i < response.size(); i++) {
        for (int j = 0; j < response[i].size(); j++) 
            cout << response[i][j] << ' ';
        cout << '\n';
    }
    return 0;
}