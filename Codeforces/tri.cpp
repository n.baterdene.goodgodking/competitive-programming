#include <bits/stdc++.h>
#define pb push_back
#define int long long
using namespace std;
unordered_map <int, bool> possible;
set <char> numbers;
bool yes = false;
void makePos(string a){
    int n = a.size();
    for (int i = 0; i < 9; i++) 
        possible[i] = false;

    for (int i = 0; i < n; i++) {
        possible[a[i] - '0'] = true;
        numbers.insert(a[i]);
    }
}
int strToInt(string a) {
    stringstream tmp(a);
    int ans;
    tmp >> ans;
    return ans;
}

void ans(vector <int>& answers, int n, int times, int past) {
    // cout << n << ' ' << times << '\n';
    if (times <= 0 || n < past) return;
    if (times == 1) {
        string tmp = to_string(n);
        for (int i = 0; i < tmp.size(); i++) 
            if (!possible[tmp[i] - '0']) return;

        cout << answers[0] << ' ' << answers[1] << ' ' << n << '\n';

        yes = true;
        return;
    }
    
    int len = to_string(n).size();
    queue <string> bfs;
    string top = "";
    bfs.push(top);
    while (bfs.size() > 0) {
        top = bfs.front();
        bfs.pop();
        answers.pb(strToInt(top));
        if (top != "" && answers.back() != 0 && n % answers.back() == 0 && past <= answers.back()) 
            ans(answers, n / answers.back(), times - 1, answers.back());
        if (yes) return;
        answers.pop_back();
        if (top.size() == len) continue;
        for (auto el : numbers) {
            if (top.size() == 0 && el == 0) continue;
            bfs.push(top + el);
        }
    }
}
signed main () {
    int n;
    cin >> n;
    string number = to_string(n);
    vector <int> answers;
    makePos(number);
    if (possible[1]) {
        cout << 1 << ' ' << 1 << ' ' << n << '\n';
        return 0;
    }
    ans(answers, n, 3, 0);
    if (!yes) cout << "shiidgui\n";
    return 0;
}