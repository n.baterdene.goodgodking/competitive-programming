#include <iostream>
#include <cstring>
#include <vector>
#include <algorithm>
using namespace std;
int main () {
    vector <int> numbers;
    int n, tmp, sum = 0;
    cin >> n;
    for(int i = 0; i < n; i++){
        cin >> tmp;
        sum += tmp;
        numbers.push_back(tmp);
    }
    if(n == 1){
        cout << 1 <<'\n';
        return 0;
    }
    sort(numbers.begin(), numbers.end());
    sum /= 2;
    tmp = 0;
    int times = 0;
    for(int i = numbers.size() - 1; i >= 0; i--){
        tmp += numbers[i];
        times++;  
        if(tmp > sum){
            cout << times << '\n';
            return 0;
        }
    }
    return 0;
}