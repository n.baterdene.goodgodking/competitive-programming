#include <iostream>
#define int long long
using namespace std;
int answer(int i) {
    if(i == 1) return 0;
    if(i == 2) return 6;
    if(i == 3) return 28;
    if(i == 4) return 96;
    if(i == 5) return 252;
    int ans = 8;
    int mid = i - 2;
    ans += 4 * mid * 3;
    mid = i - 4;
    ans += 4 * mid * 4;
    ans += 4 * 4;
    ans += mid * 4 * 6;
    ans += mid * mid * 8;
    int extra = 0;
    if(i > 5) 
        extra = (i - 4) * 6;
    return (i * i * (i * i - 1) - ans) / 2 + extra;
}
signed main () {
    int n;
    cin >> n;
    for(int i = 1; i <= n; i++)  {
        cout << answer(i) << '\n';
    }
    return 0;
}