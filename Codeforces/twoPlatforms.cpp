#include<bits/stdc++.h>

using namespace std;

const int N = 1e5 + 5, inf = 1e9;
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
#define all(a) a.begin(),a.end()
#define sz(x) (int)x.size()
typedef vector<int> vi;
typedef pair<int,int> ii;
typedef vector<ii> vii;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());

void solve(){
	int n, k;
	cin >> n >> k;
	vi x(n), y(n);
	vi w(n), mx(n + 5, 0);
	for(int i = 0; i < n; i++){
		cin >> x[i];
	}
	for(int i = 0; i < n; i++){
		cin >> y[i];
	}
	sort(all(x));
	for(int i = 0; i < n; i++){
		int idx = upper_bound(all(x), x[i] + k) - x.begin() - 1;
		if(idx < n && idx >= i){
			w[i] = idx - i + 1;
		}
	}
	for(int i = n - 1; i >= 0; i--){
		mx[i] = max(mx[i + 1], w[i]);
	}
	int ans = 0;
	for(int i = 0; i < n; i++){
		ans = max(ans, w[i] + mx[i + w[i]]);
	}
	cout << ans << '\n';
}
int main(){
	ios_base::sync_with_stdio(false);cin.tie(0);cout.tie(0);
	int t;
	cin >> t;
	while(t--)solve();
	return 0;
}
// N-ee shalga
// Bitgii buuj ug
