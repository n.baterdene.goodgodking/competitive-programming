#include <bits/stdc++.h>
#define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define sz(x) (int)x.size()

using ll = long long;
using db = double;
using ld = long double;
using uint = unsigned;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VS = vector<string>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VVVVB = vector<VVVB>;
using VVVVVB = vector<VVVVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using VVVLL = vector<VVLL>;
using VVVVLL = vector<VVVLL>;
using VD = vector<db>;
using PII = pair<int, int>;
using PDD = pair<db, db>;
using PLL = pair<ll, ll>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
using VVVVI = vector<VVVI>;
using VVVVVI = vector<VVVVI>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int bitCnt(int a) {
    bitset <64> b(a);
    return b.count();
}
int bitCnt(string a) {
    bitset <64> b(a);
    return b.count();
}
VI readVI(int n) {
    VI a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVI readVVI(int n, int m) {
    VVI a(n, VI(m));
    REP(i, n) a[i] = readVI(m);
    return a;
}
VLL readVLL(ll n) {
    VLL a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVLL readVVLL(ll n, ll m) {
    VVLL a(n, VLL(m));
    REP(i, n) a[i] = readVLL(m);
    return a;
}


int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}

void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VPI& a) {
    for (auto el : a) {
        cout << el.ff << ',' << el.ss << ' ';
    }
    cout << '\n';
}

void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VVI& a) {
    for (auto el : a) {
        print(el);
    }
}
const int MOD = 998244353;//7*17*2^23 +1;
const int INF = 1.07e9;
const ll INFF = INT64_MAX;
const db EPS = 1e-9;
const db PI = acos(-1.0); //M_PI;
const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0, 1, 0, -1};
/*
|      ⣠⣶⡾⠏⠉⠙⠳⢦⡀⠀  ⠀⢠⠞⠉⠙⠲⡀
|    ⣴⠿⠏⠀⠀⠀⠀⠀⠀⢳⡀⠀  ⡏⠀⠀ ⠀⠀ ⢷
|⠀⠀⢠⣟⣋⡀⢀⣀⣀⡀⠀⣀⡀⣧⠀  ⢸⠀⠀⠀⠀   ⡇ ⠀
| ⠀⢸⣯⡭⠁⠸⣛⣟⠆⡴⣻⡲⣿   ⣸⠀⠀OK.  ⡇
|⠀⠀⣟⣿⡭⠀⠀⠀⠀⠀⢱⠀⠀⣿   ⢹⠀⠀⠀⠀⠀ ⡇
|⠀⠀⠙⢿⣯⠄⠀⠀⠀⢀⡀⠀⠀⡿⠀  ⡇⠀⠀ ⠀ ⡼
|⠀⠀⠀⠀⠹⣶⠆⠀⠀⠀⠀⠀⡴⠃ ⠀ ⠘⠤⣄⣠⠞⠀ ⠀
|⠀⠀⠀⠀⢸⣷⡦⢤⡤⢤⣞⣁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ ⠀
|⠀⢀⣤⣴⣿⣏⠁⠀⠀⠸⣏⢯⣷⣖⣦⡀⠀⠀⠀⠀⠀⠀
|⢀⣾⣽⣿⣿⣿⣿⠛⢲⣶⣾⢉⡷⣿⣿⠵⣿⠀⠀⠀⠀⠀
|⣼⣿⠍⠉⣿⡭⠉⠙⢺⣇⣼⡏⠀ ⠀⣄⢸⣿
|⣿⣿⣧⣀⣿.........⣀⣰⣏⣘⣆⣀
      
|                                                    _         _   _
|                                                   (_)       | | | |
 _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_
| '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|
| |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_
| .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|
| |               __/ |
|_|              |___/
 _   _      _ _    __        __         _     _ 
| | | | ___| | | __\ \      / /__  _ __| | __| |
| |_| |/ _ \ | |/ _ \ \ /\ / / _ \| '__| |/ _` |
|  _  |  __/ | | (_) \ V  V / (_) | |  | | (_| |
|_| |_|\___|_|_|\___/ \_/\_/ \___/|_|  |_|\__,_|                                  
 ____     __    _______    _______   __________
|     \  |  |  |  _____|  /  _____) |____  ____|
|  |\  \ |  |  | |__     (  (_____      |  |
|  | \  \|  |  |  __|     \_____  \     |  | 
|  |  \     |  | |_____    _____)  )    |  | 
|__|   \____|  |_______|  (_______/     |__| 
 
 
 


*/









const int N = 2e5 + 5;
const int M = 4*N;

struct Node {
    ll cnt, mul;
    Node() {
        cnt = 0;
        mul = 1;
    }
} zero;
int s[N], t[N];
ll fact[N], invFact[N];
Node node[M];
int n, m, k, dif, total;

ll pow(ll a, ll b, ll p) {
    ll cur = 1, res = 1;
    a %= p;
    while(cur <= b) {
        if (cur&b) res = res * a % p;
        a = a * a % p;
        cur<<=1;
    }
    return res;
}
ll inv(ll a, ll p) {
    return pow(a, p - 2, p);
}
ll choose(ll a, ll b) {
    // cout << "HERE: " << a << ' ' << b << '\n';
    if (b <= 0) return 1;
    return fact[a] * invFact[b] % MOD * invFact[a-b] % MOD;
}
void init() {
    fact[0] = 1;
    for (int i = 1; i < N; i++) fact[i] = fact[i-1]*i%MOD;
    invFact[N-1] = inv(fact[N-1], MOD);
    for (int i = N - 2; i >= 0; i--) invFact[i] = invFact[i+1] * (i+1)%MOD;
}
Node merge(Node a, Node b) {
    Node res;
    res.cnt = a.cnt+b.cnt;
    res.mul = a.mul*b.mul%MOD;
    return res;
}
void update(int l, int r, int id, int val, int head) {
    if (l == r) {
        node[head].cnt+=val;
        // node[head].mul = invFact[node[head].cnt];
        node[head].mul = fact[node[head].cnt];
        return;
    }
    int mid = (l+r)>>1;
    if (id <= mid) update(l, mid, id, val, head*2+1);
    else update(mid+1, r, id, val, head*2+2);
    node[head] = merge(node[head*2+1], node[head*2+2]);
}

Node query(int l, int r, int L, int R, int head) {
    if (l > R || L > r) return zero;
    if (L <= l && r <= R) return node[head];
    int mid = (l+r)>>1;
    return merge(
        query(l, mid, L, R, head*2+1),
        query(mid+1, r, L, R, head*2+2)
    );
}
void go () {
    init();
    cin >> n >> m;
    int lim = 2e5;
    for (int i = 1; i <= n; i++) {
        int x; cin >> x;
        update(1, lim, x, 1, 0);
    }
    for (int i = 1; i <= m; i++) cin >> t[i];
    ll ans = 0, cur;
    int mn = min(n, m);
    for (int i = 1; i <= mn; i++) {
        Node lhs = query(1, lim, 1, t[i]-1, 0);
        Node rhs = query(1, lim, t[i], lim, 0);
        if (lhs.cnt > 0) {
            ll cur = fact[lhs.cnt] * fact[rhs.cnt] % MOD;
            ll invCur = lhs.mul * rhs.mul % MOD;
            cur = cur * inv(invCur, MOD) % MOD;
            cur = cur * choose(n - i, lhs.cnt - 1) % MOD;
            ans = (ans + cur) % MOD;
        }
        int x = query(1, lim, t[i], t[i], 0).cnt;
        if (x == 0) break;
        if (i == mn && i < m) {
            // cout << "HERE\n";
            ans = (ans+1)%MOD;
        }
        update(1, lim, t[i], -1, 0);
    }
    cout << ans << '\n';

}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
#endif
    _upgrade
    int T = 1;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














