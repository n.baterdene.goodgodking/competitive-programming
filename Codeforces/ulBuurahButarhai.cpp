#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
using namespace std;
int main () {
    int n;
    vector <pair <double, string> > numbers;
    cin >> n;
    for (double i = 1; i <= n; i++) {
        for (double j = i + 1; j <= n; j++) {
            for(int st = i; st > 1; st --) {
                int x = i, y = j;
                if((x % st == 0) && (y % st == 0)) 
                    break;
                else if(st == 2) numbers.push_back(make_pair(i / j, to_string(x) + '/' + to_string(y)));
            }
        }
    }
    for(int i = 1; i <= n; i++) {
        double y = i, one = 1;
        numbers.push_back(make_pair(one / y, to_string(1) + '/' + to_string(i)));
    }
    sort(numbers.begin(), numbers.end());
    cout << "Numbers: ";
    for(int i = numbers.size() - 1; i >= 0; i--) {
        cout << numbers[i].second << ' ';
    }
    return 0;
}