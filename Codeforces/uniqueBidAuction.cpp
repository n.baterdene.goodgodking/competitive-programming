#include <bits/stdc++.h>
using namespace std;

int main() {
    int t;
    cin >> t;
    while(t--) {
        int n;
        map <int, int> nums;
        cin >> n;
        vector <int> idx(n + 5, 0);
        for (int i = 1; i <= n; i++) {
            int num;
            cin >> num;
            nums[num]++;
            idx[num] = i;
        }
        int ans = -1;
        bool done = 0;
        for (auto el : nums) {
            if (el.second == 1) {
                ans = idx[el.first];
                break;
            }
        }

        cout << ans << '\n';
    }
}