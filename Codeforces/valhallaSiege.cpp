#include <bits/stdc++.h>
using namespace std;
long long n, q, nums[200005], cur, sum, curAns, num, i;
int main(){
    cin >> n >> q;
    for (;i<n;i++) cin >> num, nums[i] = num + (i > 0 ? nums[i-1] : 0);
    while (q--) {
        cin >> num;
        sum += num;
        auto j = upper_bound(nums, nums + n, sum) - nums;
        if (j == n) cur = sum = 0;
        else cur = j;
        cout << n - cur << '\n';
    }
}