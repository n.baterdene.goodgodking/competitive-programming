#include <bits/stdc++.h>
#define FOR(i, a, b) for (int i = a; i < b; i++)
#define FORB(i, a, b) for (int i = a - 1; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i <= a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define pb push_back
#define mp make_pair
#define umap unordered_map
#define ff first
#define ss second
 
typedef long long ll;
typedef double d;
typedef unsigned uint;
typedef unsigned long long ull;
 
typedef vector<int> VI;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<ll> VLL;
typedef vector<VLL> VVLL;
typedef vector<d> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
typedef vector<bool> VB;

const int MOD = int(1e9) + 7;
const int INF = INT_MAX;
const ll INFF = INT64_MAX;
const d EPS = 1e-9;
const d PI = acos(-1.0); //M_PI;
const int moveX[] = {-1, 0, 1, 0};
const int moveY[] = {0, 1, 0, -1};
class Solution {
public:
    bool checkHead(int cur, VVI&childs, int n) {
        VB visited(n, 0);
        queue <int> bfs;
        bfs.push(cur);
        while(!bfs.empty()) {
            int tmp = bfs.front();
            bfs.pop();
            if (visited[tmp]) return 0;
            visited[tmp] = 1;
            REP(i, childs[tmp].size()) bfs.push(childs[tmp][i]);
        }
        REP(i, n) 
            if(!visited[i]) return 0;
        return 1;
    }
    bool validateBinaryTreeNodes(int n, vector<int>& leftChild, vector<int>& rightChild) {
        VVI childs(n);
        REP(i, n) {
            if (leftChild[i] != -1) childs[i].pb(leftChild[i]);
            if (rightChild[i] != -1) childs[i].pb(rightChild[i]);
        }
        REP(i, n) 
            if (checkHead(i, childs, n)) return 1;
        
        return 0;
    }
};