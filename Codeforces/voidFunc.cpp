#include <iostream>
#include <vector>
using namespace std;


void max(vector < int > numbers, int &r) {
    for(int i = 0; i < numbers.size(); i++) {
        if(numbers[i] > r){
            r = numbers[i];
        }
    }
}
int main () {
    vector <int> numbers;
    int r = INT_MIN, tmp, n; 
    cin >> n;
    for(int i = 0; i < n; i++) {
        cin >> tmp;
        numbers.push_back(tmp);
    }
    cout << r << '\n';
    max(numbers, r);
    cout << r << '\n';

    return 0;
}