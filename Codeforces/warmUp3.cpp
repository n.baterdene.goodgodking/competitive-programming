#include <bits/stdc++.h>
using namespace std;
int main () {
    vector <int> counts(1e6 + 5, 0);
    int n, tmp, ans = 0;
    cin >> n;
    for (int i = 0; i < n; i++) {
        cin >> tmp;
        counts[tmp + 1e5]++;
    }
    for (int i = 0; i <= 2e5; i++) {
        if (counts[i] > counts[ans]) ans = i;
    }
    cout << ans - 1e5 << ' ' << counts[ans] << '\n';
    return 0;
}