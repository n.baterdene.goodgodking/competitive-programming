#include <bits/stdc++.h>
// #define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define sz(x) (int)x.size()

using ll = long long;
using db = double;
using uint = unsigned;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VS = vector<string>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VVVVB = vector<VVVB>;
using VVVVVB = vector<VVVVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using VVVLL = vector<VVLL>;
using VVVVLL = vector<VVVLL>;
using VD = vector<db>;
using PII = pair<int, int>;
using PDD = pair<db, db>;
using PLL = pair<ll, ll>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
using VVVVI = vector<VVVI>;
using VVVVVI = vector<VVVVI>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int bitCnt(int a) {
    bitset <64> b(a);
    return b.count();
}
int bitCnt(string a) {
    bitset <64> b(a);
    return b.count();
}
VI readVI(int n) {
    VI a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVI readVVI(int n, int m) {
    VVI a(n, VI(m));
    REP(i, n) a[i] = readVI(m);
    return a;
}
VLL readVLL(ll n) {
    VLL a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVLL readVVLL(ll n, ll m) {
    VVLL a(n, VLL(m));
    REP(i, n) a[i] = readVLL(m);
    return a;
}


int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}

void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VPI& a) {
    for (auto el : a) {
        cout << el.ff << ',' << el.ss << ' ';
    }
    cout << '\n';
}

void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VVI& a) {
    for (auto el : a) {
        print(el);
    }
}
const int MOD = 1e9 + 7;
const int MOD1 = 998244353;//7*17*2^23 +1;
const int INF = 1.07e9;
const ll INFF = INT64_MAX;
const db EPS = 1e-9;
const db PI = acos(-1.0); //M_PI;
const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0, 1, 0, -1};
/*
|      ⣠⣶⡾⠏⠉⠙⠳⢦⡀⠀  ⠀⢠⠞⠉⠙⠲⡀
|    ⣴⠿⠏⠀⠀⠀⠀⠀⠀⢳⡀⠀  ⡏⠀⠀ ⠀⠀ ⢷
|⠀⠀⢠⣟⣋⡀⢀⣀⣀⡀⠀⣀⡀⣧⠀  ⢸⠀⠀⠀⠀   ⡇ ⠀
| ⠀⢸⣯⡭⠁⠸⣛⣟⠆⡴⣻⡲⣿   ⣸⠀⠀OK.  ⡇
|⠀⠀⣟⣿⡭⠀⠀⠀⠀⠀⢱⠀⠀⣿   ⢹⠀⠀⠀⠀⠀ ⡇
|⠀⠀⠙⢿⣯⠄⠀⠀⠀⢀⡀⠀⠀⡿⠀  ⡇⠀⠀ ⠀ ⡼
|⠀⠀⠀⠀⠹⣶⠆⠀⠀⠀⠀⠀⡴⠃ ⠀ ⠘⠤⣄⣠⠞⠀ ⠀
|⠀⠀⠀⠀⢸⣷⡦⢤⡤⢤⣞⣁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ ⠀
|⠀⢀⣤⣴⣿⣏⠁⠀⠀⠸⣏⢯⣷⣖⣦⡀⠀⠀⠀⠀⠀⠀
|⢀⣾⣽⣿⣿⣿⣿⠛⢲⣶⣾⢉⡷⣿⣿⠵⣿⠀⠀⠀⠀⠀
|⣼⣿⠍⠉⣿⡭⠉⠙⢺⣇⣼⡏⠀ ⠀⣄⢸⣿
|⣿⣿⣧⣀⣿.........⣀⣰⣏⣘⣆⣀
      
|                                                    _         _   _
|                                                   (_)       | | | |
 _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_
| '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|
| |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_
| .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|
| |               __/ |
|_|              |___/
 _   _      _ _    __        __         _     _ 
| | | | ___| | | __\ \      / /__  _ __| | __| |
| |_| |/ _ \ | |/ _ \ \ /\ / / _ \| '__| |/ _` |
|  _  |  __/ | | (_) \ V  V / (_) | |  | | (_| |
|_| |_|\___|_|_|\___/ \_/\_/ \___/|_|  |_|\__,_|                                  
 ____     __    _______    _______   __________
|     \  |  |  |  _____|  /  _____) |____  ____|
|  |\  \ |  |  | |__     (  (_____      |  |
|  | \  \|  |  |  __|     \_____  \     |  | 
|  |  \     |  | |_____    _____)  )    |  | 
|__|   \____|  |_______|  (_______/     |__| 
 
 
 


*/









const int N = 2e5 + 5;
const int M = 2e5 + 5;

int nums[N];
bool vis[N];
int dp[N][4]; // yes, yesC, no, noC
int n, m, k;
vector <int> paths[N];
void dfs(int pos, int par) {
	if (par != pos && paths[pos].size() == 1) {
		dp[pos][0] = 1;
		dp[pos][1] = 1;
		dp[pos][2] = 0;
		dp[pos][3] = 1;
		return;
	}
	for (auto& el : paths[pos]) {
		if (el == par) continue;
		dfs(el, pos);
	}
	dp[pos][0] = 1;
	dp[pos][3] = 1;
	for (auto& el : paths[pos]) {
		if (el == par) continue;
		int yes = dp[el][0];
		int yesC = dp[el][1];
		int no = dp[el][2];
		int noC = dp[el][3];
		dp[pos][0] += no;
		dp[pos][1] += noC+1;
		// if (pos == 3) cout << yes << ' ' <<yesC << " " << no << " " << noC << "\n";

		if (yes > no) {
			// if (pos == 3) cout << "A\n";
			dp[pos][2] += yes;
			dp[pos][3] += yesC+(paths[el].size() > 1);
		} else {
			// if (pos == 3) cout << "B\n";
			dp[pos][2] += no;
			dp[pos][3] += noC;
		}
	}
	// cout << pos << ": "; for (int i = 0; i < 4; i++) cout << dp[pos][i] << " \n"[i==3];
}
void dfs1(int pos, int par, int state) {
	if (state == 2) nums[pos] = 1;
	for (auto& el : paths[pos]) {
		if (el == par) continue;
		if (state == 0) dfs1(el, pos, 2);
		else { // state = 2
			if (dp[el][0] > dp[el][2] || (dp[el][0] == dp[el][2] && dp[el][1] > dp[el][3])) 
				dfs1(el, pos, 0);
			else 
				dfs1(el, pos, 2);
		}
	}
	if (nums[pos] != 1)
	for (auto& el : paths[pos]) {
		nums[pos] += nums[el];
	}
}
void go () {
    cin >> n;
    for (int i = 1; i < n; i++) {
    	int a, b; cin >> a >> b;
    	paths[a].pb(b);
    	paths[b].pb(a);
    }
    int maxS = 0;
    for (int i = 1; i <= n; i++) {
    	maxS = max(maxS, int(paths[i].size()));
    }
    int answ, sumw;
    if (maxS < 3) {
    	queue <PII> bfs;
    	for (int i = 1; i <= n; i++) {
    		if (paths[i].size() == 1)
    			bfs.push({i, 0});
    	}
    	while(!bfs.empty()) {
    		int x, y;
    		tie(x, y) = bfs.front();
    		bfs.pop();
    		if (vis[x]) continue;
    		vis[x] = 1;
    		nums[x] = y;
    		for (auto& el : paths[x]) bfs.push({el, y^1});
    	}
    	answ = sumw = 0;
    	for (int i = 1; i <= n; i++) {
    		if (nums[i] == 0) {
	    		bool a = 0;
	    		for (auto& el : paths[i]) 
	    			a |= nums[el] == 0;
	    		if (a) {
	    			nums[i] = 1;
	    		} else {
	    			nums[i] = paths[i].size();
	    		}
	    	}
    	}
    } else {
	    dfs(1, 1);
	    int ans = 0;
	    if (dp[1][2] > dp[1][ans] || (
	    	dp[1][2] == dp[1][ans] && dp[1][3] < dp[1][ans+1])
	    	) {
	    	ans = 2;
	    }
    	dfs1(1, 1, ans);
	}
	answ = 0;
	sumw = 0;
	for (int i = 1; i <= n; i++) {
		int x = 0;
		for (auto& el : paths[i]) 
			x += nums[el];
		answ += (x == nums[i]);
		sumw += nums[i];
	}
    cout << answ << ' ' << sumw << '\n';
    for (int i = 1; i <= n; i++) cout << nums[i] << " \n"[i==n];
}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
#endif
    _upgrade
    int T = 1;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














