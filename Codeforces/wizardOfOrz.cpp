#include <bits/stdc++.h>
using namespace std;

int main () {
    int t = 1;
    cin >> t;
    while(t--) {
        int n;
        cin >> n;
        if (n == 1) {
            cout << "9\n";
            continue;;
        }
        vector <int> ans(n);
        ans[0] = 9;
        int cur = 8;
        for (int i = 1; i < n; i++) {
            ans[i] = cur;
            cur = (cur + 1) % 10;
        }
        for (int i = 0; i < n; i++) cout << ans[i];
        cout << "\n";
    }
    return 0;
}