#include <bits/stdc++.h>
// #pragma GCC optimize("O3, unroll-loops")
// #pragma GCC target("avx2")
// #define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define sz(x) (int)x.size()

using ll = long long;
using db = double;
using ld = long double;
using uint = unsigned;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VS = vector<string>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using VD = vector<db>;
using PII = pair<int, int>;
using PLL = pair<ll, ll>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int bitCnt(int a) {
    bitset <64> b(a);
    return b.count();
}
int bitCnt(string a) {
    bitset <64> b(a);
    return b.count();
}

int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}

void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VPI& a) {
    for (auto el : a) {
        cout << el.ff << ',' << el.ss << ' ';
    }
    cout << '\n';
}

void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VVI& a) {
    for (auto el : a) {
        print(el);
    }
}

/*

|                    _                                                     
|  __   _           | |                                                    
| |   \| | ___  ___ | |_                                                   
| | |\ | |/ _ \/ __/| __|                                                  
| | | \  |  __/\__ \| |_                                                   
| |_|  \_|\___ /___/ \__|                                                  
|                                                      _         _   _     
|                                                     (_)       | | | |    
|  _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_   
| | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|  
| | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_   
| | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|  
| | |               __/ |                                                  
| |_|              |___/                   _                               
|                                  _      (_)                              
|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                               
| | '_ ` _ \ / _` |_   / _` |/ _` | |/ _` | |                              
| | | | | | | (_|  /  / (_| | (_| | | (_| | |                              
| |_| |_| |_|\__,_|____\__,_|\__,_|_|\__,_|_|                              
*/

const db PI = acos(-1.0); //M_PI;
const ll INFF = 4e18;
const int INF = 1.07e9;
const int MOD = 1e9 + 7;
const int MOD1 = 998244353; //7*17*2^23 +1;
const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0, 1, 0, -1};





const int N = 1e5 + 5;
const int M = 17;

bool isRemoved[N];
int nums[N], sizes[N], dp[N];
int par[N], jump[N][M], lvl[N];
vector <int> paths[N];
int n, m, k, q;
string s;
// lca
void dfs(int pos, int p) {
    jump[pos][0] = p;
    lvl[pos] = lvl[p] + 1;
    for (int j = 1; j < M; j++)
        jump[pos][j] = jump[jump[pos][j-1]][j-1];
    for (auto& a : paths[pos]) {
        if (a == p) continue;
        dfs(a, pos);
    }
    return;
}
void up(int&a, int len) {
    for (int i = 0; i < M; i++) {
        if (len & (1<<i)) a = jump[a][i];
    }
}
int LCA(int a, int b) {
    if (lvl[a] < lvl[b]) swap(a, b);
    up(a, lvl[a]-lvl[b]);
    if (a == b) return a;
    for (int i = M-1; i >= 0; i--) {
        if (jump[a][i] != jump[b][i]) {
            a = jump[a][i];
            b = jump[b][i];
        }
    }
    return jump[a][0];
}
int getDist(int a, int b) {
    int lca = LCA(a, b);
    return lvl[a] + lvl[b] - 2 * lvl[lca];
}
// centroid
int getSz(int pos, int p) {
    sizes[pos] = 1;
    // cout << pos << " ";
    for (auto& a : paths[pos]) {
        if (a == p || isRemoved[pos]) continue;
        sizes[pos] += getSz(a, pos);
    }
    return sizes[pos];
}
int getCentroid(int pos, int p, int curSz) {
    for (auto& a : paths[pos]) {
        if (a == p || isRemoved[a]) continue;
        if (sizes[a] > curSz / 2) return getCentroid(a, pos, curSz);
    }
    return pos;
}
void centroidDecomposition(int pos, int p) {
    pos = getCentroid(pos, pos, getSz(pos, pos));
    par[pos] = p;
    isRemoved[pos] = 1;
    for (auto& el : paths[pos]) {
        if (!isRemoved[el]) centroidDecomposition(el, pos);
    }
}
void update(int pos) {
    int tmp = pos;
    while(tmp != 0) {
        dp[tmp] = min(dp[tmp], getDist(tmp, pos));
        tmp = par[tmp];
    }
}
int query(int pos) {
    int ans = INF, tmp = pos;
    while (tmp != 0) {
        ans = min(ans, getDist(pos, tmp) + dp[tmp]);
        tmp = par[tmp];
    }
    return ans;
}
void go () {
    cin >> n >> m;
    for (int i = 1; i < n; i++) {
        int a, b; cin >> a >> b;
        paths[a].pb(b);
        paths[b].pb(a);
    }
    dfs(1, 1);
    centroidDecomposition(1, 0);
    for (int i = 1; i <= n; i++) dp[i] = INF;
    update(1);
    for (int i = 1; i <= m; i++) {
        int a; cin >> a;
        if (a == 1) {
            cin >> a; update(a);
        } else {
            cin >> a;
            cout << query(a) << '\n';
        }
    }
}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("0.in", "r", stdin);
    freopen("0.out", "w", stdout);
#endif
    _upgrade
    int T = 1;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














