#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
#define LINE "----------------------\n"
using namespace std;
using PII = pair <int,  int>;
using VI = vector <int>;
const int N = 21;
int dx[] = {-1, 0, 1, 0};
int dy[] = {0, 1, 0, -1};
string directions = "NESW";
int n[2], m[2], k[2], X[2], Y[2], T;
char s[2][N][N];
vector <vector <int> > guards[2]; // x, y, dir, len
bool occupied[2][12][N][N][N][N]; // maze, from(x, y), pos(x, y);
// VI par[12][N][N][N][N];
int par[12][N][N][N][N];

PII getPos(VI& guard, int time) {
	int x = guard[0], y = guard[1], dir = guard[2], l = guard[3];
	time %= l * 2;
	if (time > l) time = 2 * l - time;
	x = x + dx[dir] * time;
	y = y + dy[dir] * time;
	return {x, y};
}
int Hash(int a, int b, int c, int d, int e) {
	return a*21*21*21*21+b*21*21*21+c*21*21+d*21+e;
}
void hsah(int& a, int& b, int& c, int& d, int& e, int x) {
	e = x % 21;
	x /= 21;
	d = x % 21;
	x /= 21;
	c = x % 21;
	x /= 21;
	b = x % 21;
	x /= 21;
	a = x % 21;
}
signed main() {
	// freopen("in.txt", "r", stdin);
	// freopen("out.txt", "w", stdout);
	ios::sync_with_stdio(0);
	cin.tie(0);
	cout.tie(0);
	for (int t = 0; t < 2; t++) {
		cin >> n[t] >> m[t];
		for (int i = 1; i <= n[t]; i++)
		for (int j = 1; j <= m[t]; j++) {
			char& x = s[t][i][j]; cin >> x;
			if (x == 'X') {
				X[t] = i;
				Y[t] = j;
			}
		}
		cin >> k[t];
		for (int i = 1; i <= k[t]; i++) {
			char dir;
			int x, y, l; cin >> x >> y >> l >> dir;
			int tmp = 0;
			while(directions[tmp] != dir) tmp++;
			guards[t].pb({x, y, tmp, l-1});
		}
	}

	for (int t = 0; t < 2; t++)
	for (auto& guard : guards[t])
	for (int time = 0; time < 12; time++) {
		int a, b, a1, b1;
		tie(a, b) = getPos(guard, time);
		tie(a1, b1) = getPos(guard, time+1);
		occupied[t][time][a][b][a][b] = 1;
		occupied[t][time][a][b][a1][b1] = 1;
	}
	queue <VI> bfs; // time, x1, y1, x2, y2, X1, Y1, X2, Y2
	memset(par, -1, sizeof(par));
	par[0][X[0]][Y[0]][X[1]][Y[1]] = Hash(-1, -1, -1, -1, -1);
	bfs.push({0, X[0], Y[0], X[1], Y[1]});
	bool found = 0;
	s[0][0][0] = s[1][0][0] = '.';
	while(!bfs.empty()) {
		vector <int> tmp = bfs.front();
		bfs.pop();
		int time, t, x1, y1, x2, y2; time = tmp[0], x1 = tmp[1], y1 = tmp[2], x2 = tmp[3], y2 = tmp[4], t = time%12;
		if (x1 + y1 + x2 + y2 == 0) {
			T = time;
			found = 1;
			break;
		}
		for (int i = 0; i < 4; i++) {
			int a1, b1, a2, b2;
			a1 = x1 + dx[i];
			b1 = y1 + dy[i];
			if (a1 < 1 || b1 < 1 || a1 > n[0] || b1 > m[0]) 
				a1 = b1 = 0;
			a2 = x2 + dx[i];
			b2 = y2 + dy[i];
			if (a2 < 1 || b2 < 1 || a2 > n[1] || b2 > m[1]) 
				a2 = b2 = 0;
			if (a1*b1 != 0 && s[0][a1][b1] == '#') tie(a1, b1) = mp(x1, y1);
			if (a2*b2 != 0 && s[1][a2][b2] == '#') tie(a2, b2) = mp(x2, y2);
			if (a1*b1 != 0 && (occupied[0][t][a1][b1][x1][y1] || occupied[0][(t+1)%12][a1][b1][a1][b1])) continue;
			if (a2*b2 != 0 && (occupied[1][t][a2][b2][x2][y2] || occupied[1][(t+1)%12][a2][b2][a2][b2])) continue;
			if (par[(t+1)%12][a1][b1][a2][b2] != -1) continue;
			par[(t+1)%12][a1][b1][a2][b2] = Hash(x1, y1, x2, y2, i);
			bfs.push({time+1, a1, b1, a2, b2});
		}
	}
	if (!found) {
		cout << "-1\n";
		return 0;
	}
	int x1 = 0, y1 = 0, x2 = 0, y2 = 0;
	vector <char> stk;
	while(1) {
		int a, b, c, d, e;
		hsah(a, b, c, d, e, par[T%12][x1][y1][x2][y2]);
		if (e == -1) break;
		stk.pb(directions[e]);
		tie(x1, y1) = mp(a, b);
		tie(x2, y2) = mp(c, d);
		T--;
	}
	cout << stk.size() << '\n';
	while(!stk.empty()) {
		cout << stk.back() << '\n';
		stk.pop_back();
	}
}
/*
	2: 
	*. -> .* -> (*.) 2
	3
	*.. -> .*. -> ..* -> .*. -> (*..) 4
	4
	*... -> .*.. -> ..*. -> ...* -> ..*. -> .*.. -> (*...) 6
	12moves
*/






















