#include <bits/stdc++.h>
using namespace std;
int n, testCase, target, d, cur, change;
int nums[10], has[300];
bool used[10];
class IO {
private:
	string inFile, outFile;
	ifstream in;
	ofstream out;
public: 
	IO () {}
	void init(string inFile, string outFile) {
		in.close();
		out.close();
		in.open(inFile.c_str());
		out.open(outFile.c_str());
	}
	void print() {
		for (int i = 1; i < 10; i++) {
			out << nums[i] << ' ';
			has[nums[i]]++;
		}
		cout << '\n';
	}
	void start() {
		// in >> testCase >> n;
		in >> n;
		for (int i = 0; i < 300; i++) has[i] = 0;
		for (int i = 0; i < 10; i++) used[i] = 0;
		// out << "FILE reverse " << testCase << '\n';
	}
	void show(int pos) {
		assert(nums[pos] == target);
		used[pos] = 1;
		out << "P " << pos << '\n';
		target--;
	}
	void assign(int a, int b) {
		if (a == 0 || b == 0) return;
		out << "S " << a << ' ' << b << '\n';
		has[nums[b]]--;
		nums[b] = nums[a] + 1;
		has[nums[b]]++;
	}
} io;

void solve() {
	io.start();
	nums[0] = -100; 
	target = n;
	// the answer is within range 0...4
	if (n <= 8) {
		for (int i = 1; i < 10; i++) 
			nums[i] = i - 1;
	} else if (n <= 44) {
		d = 1;
		nums[9] = n;
		for (int i = 1; i < 9; i++) 
			nums[9-i] = max(0, nums[9-i+1] - (1+d*i));
	} else if (n <= 101) {
		d = 2;
		nums[9] = n;
		nums[8] = nums[9]-d-1;
		nums[7] = nums[8]-2*d-1;
		int D = 3, X = 2;
		for (int i = 6; i > 0; i--) {
			nums[i] = max(0, nums[i+1]-d*D-X);
			D++, X++;
		}
	} else if (n <= 173) { // actually n <= 173 but because of bad testcase and wrong answer it has to be between 112 and 139
		d = 3;
		nums[9] = n;
		nums[8] = nums[9]-d-1;
		nums[7] = nums[8]-2*d-1;
		int D = 12;
		for (int i = 6; i > 0; i--) {
			nums[i] = max(0, nums[i+1]-D);
			D += 6;
		}
	} else { // limit 257
		d = 4;
		nums[9] = n;
		nums[8] = nums[9]-d-1;
		nums[7] = nums[8]-2*d-1;
		int D = 18;
		for (int i = 6; i > 0; i--) {
			nums[i] = max(0, nums[i+1]-D);
			D += 9;
		}
	}
	io.print();
	while (1) {
		cur = 0;
		while(cur < 10 && nums[cur] != target) cur++;
		io.show(cur);
		if (target < 0) break;
		int need = 0, ext;
		while(need <= target &&!has[target-need]) need++;
		change = 0;
		for (int i = 1; i < 10; i++) 
			if (nums[i] == target-need) change = i;
		if (change && need > 0) {
			io.assign(change, cur);
			for (int j = 1; j < need; j++)
				io.assign(cur, cur);
		}
		ext = d - need;
		change = 0;
		if (ext == d) {
			// don't need to find cur
			for (int i = 1; i < 10; i++) {
				bool x = 1;
				for (int j = 1; j <= d; j++) {
					if (has[nums[i]+j]) x = 0;
				}
				if (nums[i] >= nums[change] && 
					nums[i]+d < target &&  
					!has[nums[i]+d] && 
					!has[nums[i]+d+1] &&
					x) change = i;
			}
		} else {
			// find cur
			for (int i = 1; i < 10; i++) {
				bool x = 1;
				for (int j = 1; j <= d; j++) {
					if (has[nums[i]+j]) x = 0;
				}
				if (nums[i]+d < target && 
					nums[i] > nums[change] && 
					!has[nums[i]+d] && 
					used[i] &&
					x) change = i;
			}
			cur = change;
		}
		if (change && ext > 0) {
			io.assign(change, cur);
			for (int j = 1; j < ext; j++) io.assign(cur, cur);
		}
	}

}
signed main() {
	int st = 15, ed = 16;
	// for all testcases: 
	for (int i = st; i <= ed; i++) {
		string digit = to_string(i);
		if (digit.size() == 1) digit = "0"+digit;
		string input = "tests/"+digit;
		string output = "test-outputs/"+digit+".out";
		io.init(input, output);
		solve();
	}
	// solve();
}





















