#include <bits/stdc++.h>
#define usint int
using namespace std;

usint n, t;
const usint N = (2e4)+5;
const usint M = (64e3)+1;

mt19937 rng(chrono::high_resolution_clock::now().time_since_epoch().count() + reinterpret_cast<unsigned long>(new int) + *(new unsigned long));

bool inside(int x, int y, int x1, int y1, int a, int b) {
	if (x > x1) swap(x, x1);
	if (y > y1) swap(y, y1);
	return x < a && a < x1 && y < b && b < y1;
}
signed main() {
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	cout.tie(0);
	cin >> n >> t;
	usint ans = n+1, ptA = 1, ptB = 2;
	vector <vector <usint> > pts;
	for (usint i = 1; i <= n; i++) {
		usint a, b;
		cin >> a >> b;
		pts.push_back({a, b, i});
	}
	shuffle(pts.begin(), pts.end(), rng);
	clock_t be;
	be = clock();
	for (int i = 0; i < n; i++)
	for (int j = i+1; j < n && ans > t; j++) {
		int cnt = 2;
		if ((double)(clock() - be) / CLOCKS_PER_SEC >= 0.99) break;
		for (int k = 0; k < n; k++) {
			if (k == i || k == j) continue;
			cnt += inside(
				pts[i][0], pts[i][1],
				pts[j][0], pts[j][1],
				pts[k][0], pts[k][1]
			);
		}
		if (cnt >= t && cnt < ans) {
			ans = cnt;
			ptA = pts[i][2];
			ptB = pts[j][2];
		}
	}
	cout << ptA << ' ' << ptB << '\n';
}
// 9

















