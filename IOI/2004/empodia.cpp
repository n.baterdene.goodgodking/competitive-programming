#include <bits/stdc++.h>
using namespace std;
int n;
const int N = (2e6)+5;
const int M = 4*N;
const int INF = 1.07e9;
vector <int> nums(N);
vector <vector <int> > vals(2*N); // store ID
vector <pair <int, int> > node(M), ans; // minimum, maximum

pair <int, int> merge(pair <int, int> a, pair <int, int> b) {
	return {
		min(a.first, b.first),
		max(a.second, b.second)
	};
}
void build(int l, int r, int head) {
	if (l == r) {
		node[head] = {nums[l], nums[l]};
		return;
	}
	int mid =(l+r)>>1;
	build(l, mid, head*2+1);
	build(mid+1, r, head*2+2);
	node[head] = merge(node[head*2+1], node[head*2+2]);
}
pair <int, int> query(int l, int r, int L, int R, int head) {
	if (l > R || L > r) return {INF, -INF};
	if (L <= l && r <= R) return node[head];
	int mid =(l+r)>>1;
	return merge(
		query(l, mid, L, R, head*2+1), 
		query(mid+1, r, L, R, head*2+2)
	);
}
signed main() {
	// freopen("empodia.in", "r", stdin);
	// freopen("empodia.out", "w", stdout);
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	cout.tie(0);
	cin >> n;
	for (int i = 1; i <= n; i++) 
		cin >> nums[i];
	nums[0] = -INF;
	build(0, n, 0);
	vector <int> stk(1, 0);
	int curSt = -1;
	for (int i = 1; i <= n; i++) {
		int id = nums[i] - i + n;
		while(nums[stk.back()] > nums[i]) {
			int bk = stk.back(), id1 = nums[bk]-bk+n;
			stk.pop_back();
			if (vals[id1].back() == bk) vals[id1].pop_back();
		}
		while(!vals[id].empty()) {
			int bk = vals[id].back();
			pair <int, int> minMax = query(0, n, bk, i, 0);
			if (minMax.second > nums[i]) break;
			if (bk < curSt || minMax.first < nums[bk]) {
				vals[id].pop_back();
				continue;
			}
			curSt = i;
			ans.push_back({bk, i});
			vals[id].clear();
			break;
		}
		vals[id].push_back(i);
		stk.push_back(i);
	}
	cout << ans.size() << '\n';
	for (int i = 0; i < ans.size(); i++)
		cout << ans[i].first << ' ' << ans[i].second << '\n';
}