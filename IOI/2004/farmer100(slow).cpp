#include <bits/stdc++.h>
#define int long long
using namespace std;
int n, m, k, ans, sum, sum1;
const int INF = 1e16;
signed main() {
	ios_base::sync_with_stdio(0);
	cout.tie(0);
	cin.tie(0);
	// freopen("farmer.in", "r", stdin);
	// freopen("farmer.out", "w", stdout);
	// input
	cin >> n >> m >> k; // n <= 150,000  m, k <= 2000
	vector <int> possible(n+5, -1);
	possible[0] = 0;
	int maxAns = 0;
	for (int i = 0; i < m; i++) {
		int x;
		cin >> x;
		maxAns += x;
		for (int j = n; j >= 0; j--) {
			if (possible[j] != -1 && x + j <= n) {
				possible[x+j] = max(possible[x+j], possible[j]+x);
			}
		}
	}
	m = n;
	while(possible[m]==-1) m--;
	ans = possible[m];
	int Ans = 0;
	if (m < maxAns) {
		Ans = possible[m] + (n - m - 1);
	}
	n -= m;
	vector <int> vals;
	for (int i = 0; i < k; i++) {
		int x;
		cin >> x;
		vals.push_back(x);
	}
	sort(vals.begin(), vals.end());
	while(!vals.empty() && n > 0) {
		int bk = vals.back();
		vals.pop_back();
		int pick = min(n, bk);
		n -= pick;
		ans += max(0ll, pick - 1);
	}
	ans = max(ans, Ans);
	cout << ans << '\n';
}