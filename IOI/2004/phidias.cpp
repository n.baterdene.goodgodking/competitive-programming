#include <bits/stdc++.h>
using namespace std;
int H, W, n;
const int N = 205;
const int M = 605;
const int INF = M*M;
vector <pair <int, int> > shapes(N);
vector <vector <int> > dp(M, vector<int>(M, -1));
int solve(int h, int w) {
	if (h == 0 || w == 0) return 0;
	int& ans = dp[h][w];
	if (ans != -1) return ans;
	ans = h*w;
	for (int i = 0; i < n; i++) {
		int x = shapes[i].first, y = shapes[i].second;
		if (x > h || y > w) continue;
		int curAns = min(
			solve(x, w-y) + solve(h-x, w),
			solve(h-x, y) + solve(h, w-y)
		);
		ans = min(ans, curAns);
	}
	return ans;
}
signed main() {
	ios_base::sync_with_stdio(0);
	cout.tie(0);
	cin.tie(0);
	cin >> H >> W >> n;
	for (int i = 0; i < n; i++) cin >> shapes[i].first >> shapes[i].second;

	cout << solve(H, W) << '\n';
}