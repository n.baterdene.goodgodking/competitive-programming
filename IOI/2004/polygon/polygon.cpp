#include <bits/stdc++.h>
#define ff first
#define ss second
#define pb push_back
#define mp make_pair
#define ALL(x) x.begin(),x.end()
#define LINE "--------------\n"
using namespace std;
using PII = pair <int, int>;
using VPI = vector <PII>;
using ll = long long;
using VI = vector <int>;

class IO {
private:
	ifstream in;
	ofstream out;
	int x; 
public:
	IO() {}
	void init(string& s, string& s1, int id) {
		in.close();
		out.close();
		in.open(s.c_str());
		s1 = "outputs/" + s1 + ".out";
		out.open(s1.c_str());
		// out << "#FILE polygon " << id-1 << '\n';
	}
	void print(vector <PII>&a, vector <PII>&b) {
		if (a.empty()) {
			out << "-1\n";
			// cout << "-1\n";
			return;
		}
		out << a.size() << '\n';
		for (auto& [x, y] : a) out << x << ' ' << y << '\n';
		out << b.size() << '\n';
		for (auto& [x, y] : b) out << x << ' ' << y << '\n';
		// cout << a.size() << '\n';
		// for (auto& [x, y] : a) cout << x << ' ' << y << '\n';
		// cout << b.size() << '\n';
		// for (auto& [x, y] : b) cout << x << ' ' << y << '\n';
	}
	int get() {
		in >> x;
		// cin >> x;
		return x;
	}
	// debug
	void show(int x) {
		out << x << ' ';
	}
	void showLine() {
		out << '\n';
	}
} io;
const int N = 1000;
vector <PII> nums;
vector <PII> segs;
int sz[N], n, m;
int gcd(int a, int b) {
	if (a == 0) return b;
	return gcd(b%a, a);
}
PII st;
void order(VPI& a) {
	int id = 0;
	for (int i = 1; i < a.size(); i++) {
		if (a[i] < a[id]) id = i;
	}
	PII st = a[id];
	sort(ALL(nums), [&](PII&a, PII&b) -> bool {
		if (a == st) return 1;
		if (b == st) return 0;
		ll x1 = a.ff - st.ff;
		ll y1 = a.ss - st.ss;
		ll x2 = b.ff - st.ff;
		ll y2 = b.ss - st.ss;
		return y1 * x2 < y2 * x1;
	});
}
void showAns(vector <int> pts) {
	vector <PII> a;
	vector <PII> b;
	m = pts.size();
	a.pb({0, 0});
	// for (auto el : pts) cout << el << ' '; cout << '\n';
	for (int i = 0; i < m - 2; i+=2) {
		int cur = pts[i], l = pts[i+1];
		int x = a.back().ff + segs[cur].ff * l;
		int y = a.back().ss + segs[cur].ss * l;
		sz[cur] -= l;
		a.pb({x, y});
	}
	sz[pts[m-2]] -= pts[m-1];
	b.pb(nums[0]);
	for (int i = 0; i < n; i++) {
		if (sz[i] == 0) continue;
		int x = b.back().ff + segs[i].ff * sz[i];
		int y = b.back().ss + segs[i].ss * sz[i];
		b.pb({x, y});
	}
	b.pop_back();
	// order(a);
	// order(b);
	// reverse(ALL(a));
	// reverse(ALL(b));
	io.print(a, b);
}
void solve() {
	nums.clear();
	segs.clear();
	n = io.get();
	for (int i = 0; i < n; i++) {
		int a = io.get(), b = io.get();
		nums.pb({a, b});
	}
	order(nums);
	for (int i = 0; i < n; i++) {
		int j = (i+1)%n;
		int x = nums[j].ff - nums[i].ff;
		int y = nums[j].ss - nums[i].ss;
		segs.pb({x, y});
	}
	ll total = 0;
	for (int i = 0; i < n; i++) {
		int x, y; tie(x, y) = segs[i];
		x = abs(x);
		y = abs(y);
		sz[i] = gcd(x, y);
		segs[i].ff /= sz[i];
		segs[i].ss /= sz[i];
		total += sz[i];
		// cout << sz[i] << " \n"[i==n-1];
	}
	map <PII, set <VI> > ids;
	auto add = [&](int x, int y, int i, int j, int a, int b) {
		if (!ids.count({x, y}) || ids.size() < 7) ids[{x, y}].insert({i, j, a, b});
	};
	for (int i = 0; i < n; i++)
	for (int j = i+1; j < n; j++) {
		for (int a = 1; a <= sz[i]; a++)
		for (int b = 1; b <= sz[j]; b++) {
			int x = a * segs[i].ff + b * segs[j].ff;
			int y = a * segs[i].ss + b * segs[j].ss;
			add(x, y, i, j, a, b);
		}
	}
	// check 4
	for (int i = 0; i < n; i++)
	for (int j = i+1; j < n; j++) {
		for (int a = 1; a <= sz[i]; a++)
		for (int b = 1; b <= sz[j]; b++) {
			int x = a * segs[i].ff + b * segs[j].ff;
			int y = a * segs[i].ss + b * segs[j].ss;
			if (ids.count({-x, -y})) {
				set <VI>& cur = ids[{-x, -y}];
				for (auto el : cur) {
					int l = el[0], r = el[1], L = el[2], R = el[3];
					if (l == i || r == i || l == j || r == j || a + b + L + R == total) continue;
					// cout << a << ' ' << b << ' ' << L << ' ' << R << ' ' << total << '\n';
					return showAns({i, a, j, b, l, L, r, R});
				}
			}
		}
	}
	// check 3
	for (int i = 0; i < n; i++) {
		for (int a = 1; a <= sz[i]; a++) {
			int x = a * segs[i].ff;
			int y = a * segs[i].ss;
			if (ids.count({-x, -y})) {
				set <VI>& cur = ids[{-x, -y}];
				for (auto el : cur) {
					int l = el[0], r = el[1], L = el[2], R = el[3];
					if (l == i || r == i) continue;
					return showAns({i, a, l, L, r, R});
				}
			}
		}
	}
	// check 2
	for (int i = 0; i < n; i++)
	for (int j = i+1; j < n; j++) {
		for (int a = 1; a <= sz[i]; a++)
		for (int b = 1; b <= sz[j]; b++) {
			int x = a * segs[i].ff + b * segs[j].ff;
			int y = a * segs[i].ss + b * segs[j].ss;
			if (x == 0 && y == 0) {
				return showAns({i, a, j, b});
			}
		}
	}
}

int main() {
	int st = 1, end = 11;
	// st = end = 6;
	for (int i = st; i <= end; i++) {
		string cur = to_string(i), cur1 = to_string(i);
		if (cur.size() == 1) cur = "0"+cur;
		if (cur1.size() == 1) cur1 = "0"+cur1;
		io.init(cur, cur1, i);
		solve();
	}
}















