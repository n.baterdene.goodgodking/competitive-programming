#include <bits/stdc++.h>
#define LINE "--------------\n"
using namespace std;
const int N = 1e6 + 5;
int n, m, a, b, mid;
int nums[2][N], has[N];

auto merge = [](int&a, int&b, int A, int B) {
	int l = max(a, A);
	int r = min(b, B);
	a = l, b = r;
};
auto add = [](int l, int r, bool x) {
	has[l]++;
	has[r+1]--;
};
int get(int id) {
	int l = 0, r = n/2, ans = r;
	while (l <= r) {
		for (int i = 0; i < n; i++) has[i] = 0;
		mid = (l+r)>>1;
		for (int i = 0; i < n; i++) {
			int curL = (nums[id][i] + 2 * n - mid - i)%n;
			int curR = curL + 2 * mid;
			if (0 <= curL && curR < n) add(curR+1, n-1, (id == 1 && mid == 3));
			if (0 < curL && curR < n) add(0, curL-1, (id == 1 && mid == 3));
			if (curR >= n) add(curR-n+1, curL-1, (id == 1 && mid == 3));
		}
		int x = 0;
		bool res = 0;
		for (int i = 0; i < n && !res; i++) {
			x += has[i];
			if (x == 0) res = 1;
		}
		if (res) {
			ans = mid;
			r = mid-1;
		} else {
			l = mid+1;
		}
	}
	return ans;
}
signed main() {
	ios::sync_with_stdio(0);
	cin.tie(0);
	cout.tie(0);
	// freopen("in.txt", "r", stdin);
	// freopen("out.txt", "w", stdout);// d > n - d => n < 2d
	cin >> n;
	for (int i = 0; i < n; i++) {
		cin >> nums[0][i];
		nums[0][i]--;
		nums[1][n-1-i] = nums[0][i];
	}
	// int ans = get(1);
	int ans = min(get(0), get(1));
	cout << ans << '\n';
}
