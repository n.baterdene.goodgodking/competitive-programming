#include <bits/stdc++.h>
using namespace std;
const int N = 255;
int n, m, k, l, w;
int nums[N][N], dp[2][N], dp1[2][N]; // 0, i => 1<->i
signed main() {
	freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
	cin >> l >> w >> n >> k;
	for (int i = 0; i < n; i++) {
		int a, b; cin >> a >> b;
		nums[a][b]++;
	}
	for (int i = 1; i <= l; i++) 
	for (int j = 1; j <= w; j++) 
		nums[i][j] += (nums[i-1][j] + nums[i][j-1]) - nums[i-1][j-1];
	
	auto per = [&](int a, int b, int A, int B) {
		return 2 * ((A - a) + (B - b) + 2);
	};
	auto get = [&](int a, int b, int A, int B) {
		return nums[A][B] - nums[a-1][B] - nums[A][b-1] + nums[a-1][b-1];
	};
	memset(dp, 0x3f, sizeof(dp));
	memset(dp1, 0x3f, sizeof(dp1));
	for (int i = 1; i <= l; i++)
	for (int j = i; j <= l; j++) {
		int L = 1, R = 0, cur = 0;
		while(R <= w) {
			if (cur == k) {
				int tmp = per(i, L, j, R);
				dp[0][j] = min(dp[0][j], tmp);
				dp[1][i] = min(dp[1][i], tmp);
				// cout << i << ' ' << L << " " << j << ' ' << R << '\n';
			}
			if (cur < k) {
				cur = get(i, L, j, ++R);
			} else {
				cur = get(i, ++L, j, R);
			}
		}
	}
	for (int i = 1; i <= w; i++)
	for (int j = i; j <= w; j++) {
		int L = 1, R = 0, cur = 0;
		while(R <= l) {
			if (cur == k) {
				// if ((i == 1 && j == 3) || (i == 4 && j == 5))
				// cout << L << " " << i << ' ' << R << ' ' << j << '\n';
				int tmp = per(L, i, R, j);
				dp1[0][j] = min(dp1[0][j], tmp);
				dp1[1][i] = min(dp1[1][i], tmp);
			}
			if (cur < k) {
				cur = get(L, i, ++R, j);
			} else {
				cur = get(++L, i, R, j);
			}
		}
	}
	for (int i = 1; i <= l; i++) dp[0][i] = min(dp[0][i], dp[0][i-1]);
	for (int i = l; i > 0; i--) dp[1][i] = min(dp[1][i], dp[1][i+1]);

	for (int i = 1; i <= w; i++) dp1[0][i] = min(dp1[0][i], dp1[0][i-1]);
	for (int i = w; i > 0; i--) dp1[1][i] = min(dp1[1][i], dp1[1][i+1]);


	int answ = 1e9;
	for (int i = 1; i < l; i++) answ = min(answ, dp[0][i]+dp[1][i+1]);
	for (int i = 1; i < w; i++) answ = min(answ, dp1[0][i]+dp1[1][i+1]);
	answ == 1e9 ? (cout << "NO" << endl) : (cout << answ << "\n");
}