#include <bits/stdc++.h>
#define mp make_pair
using namespace std;
using ll = long long;

int n;
const int N = 5e6+5;
ll a[N];
ll b[N];
const int INF = 1e15 + 5;
signed main () {
	ios::sync_with_stdio(0);
	cin.tie(0);
	cout.tie(0);
	// freopen("in.txt", "r", stdin);
	// freopen("out.txt", "w", stdout);
	cin >> n;
	for (int i = 1; i <= n; i++) {
		cin >> b[i]; // b[i] == (a[i] + a[i+1]) / 2
	}
	ll l = b[1], r = b[2];
	for (int i = 3; i <= n; i++) {
		if (b[i] < b[i-1]) {
			cout << "0\n";
			return 0;
		}
		// cout << i-1 << ": " << l << ' ' << r << '\n';
		ll l1 = 2*b[i-1] - r, r1 = 2*b[i-1] - l;
		tie(l, r) = mp(l1, r1);
		l1 = 1, r1 = b[i];
		l = max(l, l1);
		r = min(r1, r);
		if (l > r) {
			cout << "0\n";
			return 0;
		}
	}
	cout << r - l + 1 << '\n';
}