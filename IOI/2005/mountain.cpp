#include <bits/stdc++.h>
// #define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);

int n, m;
const int INF = 2e9;
const int N = (1.6e6) + 5;
struct Node {
    int maxi, sum, lazy, l, r;
    Node() {
        sum = 0; // sum of segment
        lazy = INF; // add this val to this segment
        maxi = 0; // peak of segment
    }
};
vector <Node> node(N);
map <int, int> st, fin;
vector <pair <int, int> >seg(N);
// map <int, pair <int, int> > seg;
int len(int a, int b) {
    return seg[b].second - seg[a].first + 1;
}
void addSeg(int a, int b, int segNo) {
    st[a] = fin[b] = segNo;
    seg[segNo] = {a, b};
}
// segTree {
void merge(Node& c, Node& a, Node& b) {
    c.sum = a.sum + b.sum;
    c.maxi = max(a.maxi, a.sum+b.maxi);
}
void propagate(int head, int l, int r) {
    int val = node[head].lazy, mid = (l+r)>>1;
    if (val == INF) return;
    node[head].lazy = INF;
    node[head*2+1].sum = len(l, mid) * val;
    node[head*2+2].sum = len(mid+1, r) * val;
    node[head*2+1].lazy = val;
    node[head*2+2].lazy = val;
    node[head*2+1].maxi = (val <= 0 ? val : val * len(l, mid));
    node[head*2+2].maxi = (val <= 0 ? val : val * len(mid+1, r));
}
void update(int l, int r, int L, int R, int val, int head) {
    if (l > R || L > r) return;
    if (L <= l && r <= R) {
        node[head].sum = (val * len(l, r));
        node[head].lazy = val;
        node[head].maxi = (val <= 0 ? val : val * len(l, r));
        return;
    }
    propagate(head, l, r);
    int mid = (l+r)>>1;
    update(l, mid, L, R, val, head*2+1);
    update(mid+1, r, L, R, val, head*2+2);
    merge(node[head], node[head*2+1], node[head*2+2]);
}
int query(int l, int r, int val, int head) {
    if (l == r) return l;
    propagate(head, l, r);
    int mid = (l+r)>>1;
    if (node[head*2+1].maxi > val) 
        return query(l, mid, val, head*2+1);
    return query(mid+1, r, val-node[head*2+1].sum, head*2+2);
}
int get(int l, int r, int val, int head) {
    if (l == r) {
        return node[head].sum;
    }
    propagate(head, l, r);
    int mid = (l+r)>>1;
    if (val <= mid) 
        return get(l, mid, val, head*2+1);
    return node[head*2+1].sum + get(mid+1, r, val, head*2+2);
}
// }
void go () {
    cin >> n;
    char x;
    int a, b, c;
    vector <vector <int> > ops;
    set <int> allPoint;
    allPoint.insert(0);
    while(cin >> x) {
        if (x == 'E') break;
        if (x == 'I') { // update
            cin >> a >> b >> c;
            // a--, b--;
            ops.push_back({a, b, c});
            allPoint.insert(a);
            allPoint.insert(b);
        } else { // query
            cin >> a;
            ops.push_back({a});
        }
    }
    vector <int> pts;
    for (auto el : allPoint) pts.push_back(el);
    m = pts.size();
    int segCnt = 0;
    for (int i = 0; i < m-1; i++) {
        a = pts[i], b = pts[i+1];
        addSeg(a, a, segCnt++);
        if (a+1 < b) addSeg(a+1, b-1, segCnt++);
        if (i==m-2) addSeg(b, b, segCnt);
    }
    m = segCnt;
    for (int j = 0; j < ops.size(); j++) {
        if (ops[j].size() == 3) {
            a = ops[j][0];
            b = ops[j][1];
            c = ops[j][2];
            update(0, segCnt, st[a], fin[b], c, 0);
        } else {
            a = ops[j][0];
            if (node[0].maxi <= a) {
                cout << n << '\n';
                continue;
            }
            int res = query(0, segCnt, a, 0);
            if (res == 0) {
                cout << 0 << '\n';
                continue;
            }
            int A = get(0, segCnt, res-1, 0), B = get(0, segCnt, res, 0), ans = seg[res-1].second;
            int k = (B - A) / (seg[res].second - seg[res].first + 1);
            ans += (a - A) / (k);
            cout << ans << '\n';
        }
    }
}



signed main () {

#ifndef ONLINE_JUDGE
    // freopen("in.txt", "r", stdin);
    // freopen("out.txt", "w", stdout);
#endif
    _upgrade
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    go();

    return (0-0); //<3
}














