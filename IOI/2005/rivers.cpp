#include <bits/stdc++.h>
#define pb push_back
#define ff first
#define ss second
using namespace std;
using PII = pair <int, int>;
int n, m, k;
const int N = 105;
const int K = 55;
int dp[N][N][K];
int sz[N], neighbor[N], child[N], dist[N];
vector <PII> paths[N];
int total;
void dfs(int pos, int len, int rootLen) {
	dist[pos] = rootLen;
	// for (auto& [a, b] : paths[pos]) 
	for (int i = 0; i < paths[pos].size(); i++) {
		int a, b; tie(a, b) = paths[pos][i];
		dfs(a, b, rootLen+b);
		sz[pos] += sz[a];
	}
	total += sz[pos] * len;
	if (!paths[pos].empty()) child[pos] = paths[pos][0].ff;
	for (int i = 1; i < paths[pos].size(); i++) {
		int u = paths[pos][i-1].ff, v = paths[pos][i].ff;
		neighbor[u] = v;
	}
}
int solve(int pos, int par, int rem) {
	if (pos == 0) return 0;
	int& res = dp[pos][par][rem];
	if (res != -1) return res;
	res = 0;
	for (int i = 0; i < rem; i++) 
		res = max(res, solve(child[pos], pos, i) + solve(neighbor[pos], par, rem-i-1));
	if (rem > 0) res += (dist[pos]-dist[par]) * sz[pos];
	for (int i = 0; i <= rem; i++) {
		res = max(res, solve(child[pos], par, i) + solve(neighbor[pos], par, rem-i));
	}
	return res;
}
signed main() {
	ios::sync_with_stdio(0);
	cin.tie(0);
	cout.tie(0);
	// freopen("in.txt", "r", stdin);
	// freopen("out.txt", "w", stdout);
	cin >> n >> k;
	for (int i = 1; i <= n; i++) {
		int p, l; cin >> sz[i] >> p >> l;
		paths[p].pb({i, l});
	}
	dfs(0, 0, 0);
	memset(dp, -1, sizeof(dp));
	int res = total - solve(child[0], 0, k);
	cout << res << '\n';

}