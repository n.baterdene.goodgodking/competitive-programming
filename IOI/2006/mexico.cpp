#include <bits/stdc++.h>
#define LINE "-----------------\n"
#define ff first
#define ss second
#define pb push_back
using namespace std;
using PII = pair <int, int>;
int n, m;
const int N = 1005;
bool road[N][N], vis[N];
int par[N];
PII seg[N];
vector <int> paths[N];
int find(int a) {
	if (par[a] == a) return a;
	return par[a] = find(par[a]);
}
int merged;
void merge(int a, int b, int l, int r) {
	if (merged < n-1) {
		cout <<"MERGE: " << l+1 << ' ' << r+1 << '\n';
		paths[l].pb(r);
		paths[r].pb(l);
		merged++;
	}
	seg[a].ss = seg[b].ss;
	par[b] = a;
}
signed main() {
	ios::sync_with_stdio(0);
	cin.tie(0);
	cout.tie(0);
	freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
	cin >> n >> m;
	for (int i = 1; i <= m; i++) {
		int a, b; cin >> a >> b;
		a--, b--;
		road[a][b] = road[b][a] = 1;
	}
	queue <int> bfs;
	for (int i = 0; i < n; i++) {
		seg[i] = {i, i};
		par[i] = i;
		int a = (i+1)%n;
		if (road[a][i]) bfs.push(i);
	}
	while(!bfs.empty()) {
		int x = bfs.front(); bfs.pop();
		if (vis[x]) continue;
		vis[x] = 1;
		int id = find(x);
		int R = (seg[id].ss+1)%n;
		int id1 = find(R);
		cout << LINE;
		for (int i = 0; i < n; i++) cout << find(i) << ' '; cout << endl;
		if (id != id1) {
			cout << "HERE: " << id << " " << id1 <<'\n';
			merge(id, id1, seg[id].ss, R);
		}
		int l, r; tie(l, r) = seg[id];
		int nx = (r+1)%n;
		int pv = (l+n-1)%n;
		cout << l+1 << "," << r+1 << ":\n";
		if (find(nx) != id && road[r][nx]) {
			cout << "PUSH1: " << seg[id].ff+1 << '\n';
			bfs.push(seg[id].ff);
		}
		if (find(pv) != id && road[l][pv]) {
			cout << "PUSH2: " << seg[find(pv)].ss+1 << '\n';
			bfs.push(seg[find(pv)].ss);
		}
	}
	
	return 0;

}