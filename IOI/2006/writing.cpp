#include <bits/stdc++.h>
#define pb push_back
using namespace std;
int n, m, nonZero = 0, ans;
string s1, s2;
int cnt[300];
void add(int a, int b) {
	nonZero -= cnt[a] != 0;
	cnt[a] += b;
	nonZero += cnt[a] != 0;
}
signed main() {
	ios::sync_with_stdio(0);
	cin.tie(0);
	cout.tie(0);
	// freopen("in.txt", "r", stdin);
	// freopen("out.txt", "w", stdout);
	cin >> n >> m;
	cin >> s1 >> s2;
	s2.pb('0');
	for (int i = 0; i < n; i++) add(s1[i], 1);
	for (int i = 0; i < n-1; i++) add(s2[i], -1);
	for (int i = n-1; i < m; i++) {
		add(s2[i], -1);
		ans += nonZero == 0;
		add(s2[i-n+1], 1);
	}
	cout << ans << '\n';
}