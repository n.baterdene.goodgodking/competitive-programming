#include "cave.h"
#include <bits/stdc++.h>
using namespace std;
int n;
// tryCombination
void exploreCave(int N) {
    n = N;
    int lastAns = 0;
    int value[n], switches[n]; memset(value, 0, sizeof(value)); memset(switches, -1, sizeof(switches));
    bool assigned[N]; memset(assigned, 0, sizeof(assigned));
    int assignCnt = 0;
    bool done = 0;
    for (int i = 0; i < n && !done; i++) {
        int nums[n], tar = 0;
        for (int j = 0; j < n; j++) {
            nums[j] = 0;
            if (assigned[j]) nums[j] = value[j];
        }
        int cur = tryCombination(nums);
        if (cur == -1) {
            for (int j = 0; j < n; j++) value[j] = nums[j];
            break;
        } else if(assignCnt==n-1 && cur == n-1) {
            // cout << "HERE?\n";
            for (int j = 0; j < n; j++) {
                nums[j] = 1;
                if (assigned[j]) nums[j] = value[j];
            }
            for (int j = 0; j < n; j++) value[j] = nums[j];
            break;
        }
        if (cur == i) tar = 1; // did not open
        int l = 0, r = n-1, res = r;
        while(l <= r && !done) {
            // checkLeft
            int mid = (l+r)>>1;
            int L = l, R = mid;
            for (int j = 0; j < n; j++) {
                if (assigned[j]) nums[j] = value[j];
                else {
                    if (L <= j && j <= R) {
                        nums[j] = tar;
                    } else {
                        nums[j] = tar^1;
                    }
                }
            }
            cur = tryCombination(nums);
            if (cur == -1) {
                for (int j = 0; j < n; j++) 
                    value[j] = nums[j];
                
                done = 1;
                break;
            }
            if (cur == i) { // did not open
                l = mid+1;
            } else {
                r = mid;
            }
            if (l == r) {
                res = l;
                break;
            }
        }
        if (!done) {
            switches[res] = i;
            assigned[res] = 1; assignCnt++;
            value[res] = tar;
        }
    }
    for (int i = 0; i < n; i++) {
        if (switches[i] == -1) {
            value[i] ^= 1;
            switches[i] = tryCombination(value);
            value[i] ^= 1;
        }
    }
    answer(value, switches);
}














