#include "dreaming.h"
#include <bits/stdc++.h>
#define ff first
#define ss second
#define pb push_back
#define ALL(x) x.begin(),x.end()
using namespace std;
using VI = vector <int>;
using PII = pair <int, int>;
const int N = 1e5+5;
vector <PII> paths[N];
bool vis[N];
int n, m, L, sizes[N], len;
PII dp[N];
 
PII getFarthest(int pos, int par = 0) {
    dp[pos] = {0, pos};
    for (auto [a, b] : paths[pos]) {
        if (a == par) continue;
        PII tmp = getFarthest(a, pos);
        if (dp[pos].ff < tmp.ff + b) dp[pos] = {tmp.ff + b, tmp.ss};
    }
    return dp[pos];
}
int getSizes(int pos, int par = 0) {
    vis[pos] = 1;
    for (auto [a, b] : paths[pos]) {
        if (a == par) continue;
        sizes[pos] = max(sizes[pos], b + getSizes(a, pos));
    }
    return sizes[pos];
}
VI findCentroid(int pos, int par = 0, int pre = 0) {
    int suf = len - pre;
    VI res = {pos, pre, suf};
    for (auto [a, b] : paths[pos]) {
        if (a == par) continue;
        int x = pre+b, y = sizes[a];
        if (max(pre, suf) >= max(x, y)) {
            VI tmp = findCentroid(a, pos, pre+b);
            if (max(pre, suf) > max(tmp[1], tmp[2])) res = tmp;
        }
    }
    return res;
}
VI getCentroid(int pos) {
    len = getSizes(pos);
    return findCentroid(pos);
}
int travelTime(int _N, int _M, int _L, int _A[], int _B[], int _T[]) {
    n = _N;
    m = _M;
    L = _L;
    vector <int> pts = {-1, -1, -1, -1};
    for (int i = 0; i < _M; i++) {
        // paths[_A[i]].pb({_B[i], _T[i]});
        // paths[_B[i]].pb({_A[i], _T[i]});
        paths[_A[i]+1].pb({_B[i]+1, _T[i]});
        paths[_B[i]+1].pb({_A[i]+1, _T[i]});
    }
    int res = 0, maxOnePoint = 0;
    for (int i = 1; i <= n; i++) {
        if (vis[i]) continue;
        PII tmp = getFarthest(i);
        VI point = getCentroid(tmp.ss);
        maxOnePoint = max(maxOnePoint, len);
        if (point[1] < point[2]) swap(point[1], point[2]);
         // cout << '\t';for (int i = 0; i < 3; i++) cout << point[i] << " \n"[i==2];
        pts[0] = (max(point[1], point[2]));
        sort(ALL(pts));
        // cout << i-1 << ": " << point[0]-1 << ' ' << point[1] << ' ' << point[2] << '\n';
        // cout << i << ": " << point[0] << ' ' << point[1] << ' ' << point[2] << '\n';
    }
    if (pts[2] > -1) res = max(res, pts[2] + pts[3] + L);
    if (pts[1] > -1) res = max(res, pts[1] + pts[2] + 2*L);
 
    res = max(res, maxOnePoint);
    return res;
}