#include <bits/stdc++.h>
#include "friend.h"
#define LINE "------------\n"
#define ALL(x) x.begin(),x.end()
#define LLA(x) x.rbegin(),x.rend()
#define pb push_back
using namespace std;
using PII = pair <int, int>;
using ll = long long;
// Find out best sample
/*  
0 for IAmYourFriend,
1 for MyFriendsAreYourFriends,
2 for WeAreYourFriends.
*/
const int N = 1e3 + 5;
bool isFriend[N][N];
vector <int> paths[N];
int dp[N][2], nums[N];
void IAmYourFriend(int x, int y) {
	if (x > y) swap(x, y);
	paths[x].pb(y);
	isFriend[x][y] = isFriend[y][x] = 1;
}
void MyFriendsAreYourFriends(int x, int y) {
	for (int i = 0; i < N; i++) 
		if (isFriend[y][i]) IAmYourFriend(i, x);
}
void WeAreYourFriends(int x, int y) {
	MyFriendsAreYourFriends(x, y);
	IAmYourFriend(x, y);
}
int solve(int pos, int state) {
	int& res = dp[pos][state];
	if (res != -1) return res;
	res = state * nums[pos];
	for (auto el : paths[pos]) {
		if (state) res += solve(el, state^1);
		else res += max(solve(el, state), solve(el, state^1));
	}
	// cout << pos << " " << state << ": " << res << '\n';
	return res;
}
int findSample(int n, int _nums[], int host[], int type[]){
	int ans = 0;
	for (int i = 0; i < n; i++) nums[i] = _nums[i];
	if (n <= 10) {
		for (int i = 1; i < n; i++) {
			if (type[i] == 0) IAmYourFriend(i, host[i]);
			else if (type[i] == 1) MyFriendsAreYourFriends(i, host[i]);
			else WeAreYourFriends(i, host[i]);
		}
		for (int i = 0; i < (1<<n); i++) {
			vector <int> ids;
			for (int j = 0; j < n; j++) {
				if (i & (1<<j)) ids.pb(j);
			}
			bool no = 0;
			for (auto el : ids)
			for (auto el1 : ids) {
				no |= isFriend[el][el1];
			}
			if (no) continue;
			int sum = 0;
			for (auto el : ids) {
				// cout << el <<" ";
				sum += nums[el];
			}
			// cout << ": " << sum << '\n';
			ans = max(ans, sum);
		}
	} else if (type[1] == 0) {
		// cout << "HERE\n";
		for (int i = 1; i < n; i++) {
			// cout << i << " " << host[i] << '\n';
			IAmYourFriend(i, host[i]);
		}

		// for (int i = 0; i < n; i++)
		// for (int j = 0; j < n; j++) 
		// 	cout << isFriend[i][j] << " \n"[j==n-1];
		memset(dp, -1, sizeof(dp));
		solve(0, 0), solve(0, 1);
		ans = max(dp[0][0], dp[0][1]);
	} else if (type[1] == 1) {
		// cout << "B\n";
		for (int i = 0; i < n; i++) ans += nums[i];
	} else if (type[1] == 2) {
		// cout << "C\n";
		for (int i = 0; i < n; i++) ans = max(ans, nums[i]);
	}

	return ans;
}
