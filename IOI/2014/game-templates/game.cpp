#include "game.h"
#include <bits/stdc++.h>
#define lb lower_bound
#define pb push_back
#define LINE "------------\n"
#define ALL(x) x.begin(),x.end()
using namespace std;
using PII = pair <int, int>;
const int N = 1500;
int cnt[N], n;
void initialize(int _n) {
    n = _n;
}
int hasEdge(int u, int v) {
    if (u > v) swap(u, v);
    cnt[v]++;
    return cnt[v] == v;
}
