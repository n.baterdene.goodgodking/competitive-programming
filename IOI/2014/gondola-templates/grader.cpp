#include <bits/stdc++.h>
#include "gondola.h"
using namespace std;
int gondolaSequence[100001];
int replacementSequence[250001];

int main() {
  freopen("0.out", "w", stdout);
  int st = 7, ed = 10;
  // st = ed = 6;
  for (int test = st; test <= ed; test++) {
    string input = "sample-"+to_string(test)+".in";
    // string output = "sample-"+to_string(test)+".out";
    ifstream in(input.c_str());
    // ofstream out(output.c_str());
    int i, n, subtask;
    int nr; 
    assert(in >> subtask);
    assert(in >> n);
    for(i=0;i<n;i++) in >> gondolaSequence[i];
    cout << "testCase " << test << "," << subtask << ": ";
    switch (subtask){
    case 1: case 2: case 3:
      cout << valid(n, gondolaSequence) << "\n";
      break;
    case 4: case 5: case 6:
      nr = replacement(n, gondolaSequence, replacementSequence);
      cout << nr << " ";
      if (nr > 0) {
      	for (i=0; i<nr-1; i++)
          cout << replacementSequence[i] << " ";
        cout << replacementSequence[nr-1] << "\n";
      } else cout << '\n';
      break;

    case 7: case 8: case 9: case 10:
      cout << countReplacement(n, gondolaSequence) << "\n";
      break;
    }
  }

  return 0;
}
