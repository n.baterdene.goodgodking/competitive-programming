#include<bits/stdc++.h>
#include"holiday.h"
using namespace std;
int main() {
    freopen("0.out", "w", stdout);
    int st = 1, ed = 3;
    st = ed = 1;
    for (int test = st; test <= ed; test++) {
        string input = "sample-"+to_string(test)+".in";
        string output = "sample-"+to_string(test)+".out";
        ifstream in(input.c_str());
        ifstream out(output.c_str());
        long long n, start, d, expectedValue;
        in >> n >> start >> d;
        out >> expectedValue;
        int attraction[n];
        for (int i = 0; i < n; ++i) in >> attraction[i];
        cout << "testCase " << test << ": " << expectedValue << " " << findMaxAttraction(n, start, d,  attraction) << '\n';
    }

    return 0;
}
