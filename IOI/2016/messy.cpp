#include "messy.h"
#include <bits/stdc++.h>
using namespace std;
 
int n;
vector <int> ans;
string defaultStr;
void add(vector <int> ids) {
    string tmp = defaultStr;
    for (auto a : ids) tmp[a] = '1';
    add_element(tmp);
}
int val;
bool check(vector <int> ids) {
    string tmp = defaultStr;
    for (auto a : ids) tmp[a] = '1';
    val = check_element(tmp);
    // cout << "ask " << tmp << ": " << val << '\n';
    return val;
}
void init(int st, int end, vector<int>&use) {
    if (st == end) return;
    int mid = (st+end)/2;
    for (int i = mid+1; i <= end; i++) {
        vector <int> tmp = use;
        tmp.push_back(i);
        add(tmp);
    }
    //lowHalf
    for (int i = st; i <= mid; i++) {
        use.push_back(i);
    }
    init(mid+1, end, use);
    for (int i = st; i <= mid; i++) {
        use.pop_back();
    }
    //upperHalf
    for (int i = mid+1; i <= end; i++) {
        use.push_back(i);
    }
    init(st, mid, use);
    for (int i = mid+1; i <= end; i++) {
        use.pop_back();
    }
}
void solve(int st, int end, vector <int>&use) {
    // cout << st << ' ' << end << ": ";
    // for (auto el : use) cout << el << ' ';
    // cout << '\n';
    vector <bool> seen(n, 0);
    for (auto el : use) seen[el] = 1;
    if (st == end) {
        val = 0;
        for (int i = 0; i < n; i++) {
            if (!seen[i]) val = i;
        }
        // cout << "ANS: " << val << ": " << st << '\n';
        ans[val] = st;
        return;
    }
    int mid = (st+end)/2;
    vector <int> lower, upper;
    for (int i = 0; i < n; i++) {
        if (seen[i]) continue;
        vector <int> tmp = use;
        tmp.push_back(i);
        bool cur = check(tmp);
        if (cur) upper.push_back(i);
        else lower.push_back(i);
    }
    // cout << "lower: ";
    // for (auto el : lower) cout << el << ' ';
    // cout << '\n';
    // cout << "upper: ";
    // for (auto el : upper) cout << el << ' ';
    // cout << '\n';
    //lowHalf
    for (int i = 0; i < lower.size(); i++) {
        use.push_back(lower[i]);
    }
    solve(mid+1, end, use);
    for (int i = 0; i < lower.size(); i++) {
        use.pop_back();
    }
    //upperHalf
    for (int i = 0; i < upper.size(); i++) {
        use.push_back(upper[i]);
    }
    solve(st, mid, use);
    for (int i = 0; i < upper.size(); i++) {
        use.pop_back();
    }
}
vector<int> restore_permutation(int n1, int w, int r) {
    n = n1;
    for (int i = 0; i < n; i++) ans.push_back(-i);
    for (int i = 0; i < n; i++) defaultStr.push_back('0');
    vector <int> use;
    init(0, n-1, use);
    compile_set();
    // cout << "DONE\n";
    // set <int> posVals;
    // for (int i = 0; i < n; i++) posVals.insert(i);
    solve(0, n-1, use);
    // vector <int> test1 = {2, 1, 3, 0};
    // check_element("0");
    return ans;
}