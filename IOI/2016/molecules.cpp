#include "molecules.h"
#include <bits/stdc++.h>
typedef long long ll;
#define ff first
#define ss second
using namespace std;
vector<int> find_subset(int l, int u, vector<int> w) {
    int n = w.size();
    vector <pair <int, int> > nums;
    for (int i = 0; i < n; i++) {
        nums.push_back({w[i], i});
    }
    sort(nums.begin(), nums.end());
    queue <pair <int, int> > cur; // val, idx
    vector <int> ans;
    ll sum = 0, pos = 0; 
    bool answ = 0;
    while(pos < n) {
        sum += nums[pos].ff;
        cur.push({nums[pos++]});
        if (l <= sum && sum <= u) {
            answ = 1;
            break;
        }
        while(sum > u) {
            auto num = cur.front();
            cur.pop();
            sum -= num.ff;
        }
        if (l <= sum && sum <= u) {
            answ = 1;
            break;
        }
    }
    if(answ) {
        while(!cur.empty()) {
            ans.push_back(cur.front().ss);
            cur.pop();
        }
    }
    return ans;
}