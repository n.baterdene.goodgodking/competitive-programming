#include "wiring.h"
#include <bits/stdc++.h>
 
typedef long long ll;
#define ff first
#define ss second
 
using namespace std;
vector <pair <ll, int> > all;
const int BLUE = 0;
const int RED = 1;
const ll INF = 1e12;
ll dist(ll a, ll b) {
    return abs(a - b);
}
const int N = 2e5 + 5;
vector <ll> pre(N), dp(N);
ll sum(int a, int b) {
    // cout << "SUM: " << a << ' ' << b << '\n';
    return pre[a] - (b >= 0 ? pre[b] : 0);
}
long long min_total_length(vector<int> r, vector<int> b) {
    int n = r.size() + b.size();
    int color = RED;
    if (b[0] < r[0]) color = BLUE;
    all.push_back({-INF, 1-color});
    color = RED;
    if (b.back() > r.back()) color = BLUE;
    all.push_back({INF, 1-color});
    for (auto el : r) all.push_back({el, RED});
    for (auto el : b) all.push_back({el, BLUE});
    sort(all.begin(), all.end());
    for (int i = 1; i <= n; i++) {
        pre[i] = pre[i - 1] + all[i].ff;
        // cout << pre[i] << ' ';
    }
    // cout << "\n";
    dp[0] = 0;
    int pastSt = 1;
    for (int i = 1; i <= n; i++) {
        int j = i; 
        while(j + 1 <= n && all[j+1].ss == all[i].ss) j++;
        for (int k = pastSt; k < i; k++) {
            dp[k] = min(dp[k], dp[k-1] + dist(all[i].ff, all[k].ff));
        }
		ll tmp = dp[i-1], add = 0;
		for (int k = 1; i+k-1 <= j; k++) {
			int a = i+k-1, b = i-k;
			add += dist(all[a].ff, all[i-1].ff);
			if (pastSt <= b) {
				tmp = min(tmp, dp[b-1] + (k * all[i-1].ff) - sum(i-1, b-1));
			}
			dp[a] = tmp + add;
		}
        pastSt = i;
        i = j;
    }
	// for (int i = 1; i <= n; i++) cout << dp[i] << ' ';
	// cout << '\n';
    return dp[n];