#include "combo.h"
#include <bits/stdc++.h>
#define pb push_back
using namespace std;

// max call (N+2)  int press(string p) -> returns max Prefix in p; sz(p) <= 4 * sz(n);
// N <= 2000
// characters: A, B, X, Y
string guess_sequence(int n) {
    string p = "";
    set <char> tmp = {'A', 'B', 'X', 'Y'};
    vector <char> c;
    { // first character;
        int x = press("AB");
        if (x) {
            if (x == 2) p = "AB";
            else if (press("A")) p = "A";
            else p = "B";
        } else {
            if (press("X")) p = "X";
            else p = "Y";
        }
        tmp.erase(p[0]);
        for (auto el : tmp) c.pb(el);
    }
    while (p.size()+2 <= n) {
        string A = p+c[0]+c[0];
        string B = p+c[0]+c[1];
        string C = p+c[0]+c[2];
        string D = p+c[1];
        int cur = press(A+B+C+D);
        if (cur == p.size()) {
            p.pb(c[2]);
        } else if (cur == p.size()+1) {
            p.pb(c[1]);
        } else if (cur == p.size()+2) {
            p.pb(c[0]);
        }
    }
    if (p.size() < n) {
        if ( press(p+c[0]) == n) {
            p.pb(c[0]);
        } else if (press(p+c[1]) == n) {
            p.pb(c[1]);
        } else {
            p.pb(c[2]);
        }
    }
    return p;
}
