#include "doll.h"
#include <bits/stdc++.h>
#define sz(x) (int)x.size()
#define ALL(x) x.begin(), x.end()
#define pb push_back
#define ff first
#define ss second
using namespace std;
using VI = vector <int>;
using PII = pair <int, int>;
int n, m, p = 1;
const int N = 4e5;
VI c, x(N), y(N), state(N);
int switchNo;
int generate(int l, int r) {
    if (r < p-n-1) {
        return -1;
    }
    if (l == r) {
        return 0; // up or down
    }
    
    int mid = (l+r)>>1, curNode = ++switchNo;
    x[curNode] = generate(l, mid);
    y[curNode] = generate(mid+1, r);
    return -curNode;
}
void addPos(int pos, int v) { // removes n zero
    int& nx = (state[-pos] ? y[-pos] : x[-pos]); state[-pos] ^= 1;
    if (nx == 0) {
        nx = v;
        return;
    }
    return addPos(nx, v);
}
void create_circuit(int M, vector<int> nums) {
    m = M;
    n = sz(nums);
    c = VI(m+1, -1);
    while(p < n+1) p *= 2;
    switchNo = 0;
    generate(0, p-1);
    for (int i = 0; i < n; i++) addPos(-1, nums[i]);
    addPos(-1, 0);
    for (int i = 0; i < switchNo; i++) {
        x[i] = x[i+1];
        y[i] = y[i+1];
    }
    x.resize(switchNo);
    y.resize(switchNo);
    // for (int i = 0; i < sz(x); i++) cout << i+1 << ": " << x[i] << ',' << y[i] << '\n';
    answer(c, x, y);
}
