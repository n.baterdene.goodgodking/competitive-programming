#include "shoes.h"
#include <bits/stdc++.h>
typedef long long ll;
using namespace std;
ll ans = 0, n;
void countInv(vector <int>&s, int st, int end) {
	if (st == end) return;
	int mid = (st + end) / 2;
	countInv(s, st, mid);
	countInv(s, mid+1, end);
	vector <int> tmp, A, B;
	for (int i = st; i <= mid; i++) A.push_back(s[i]);
	for (int i = mid+1; i <= end; i++) B.push_back(s[i]);
	ll inv = A.size();
	A.push_back(INT_MAX);
	B.push_back(INT_MAX);
	ll a = 0, b = 0;
	while(a + b + 2 < A.size() + B.size()) {
		if (A[a] <= B[b]) {
			tmp.push_back(A[a++]);
			inv--;
		} else {
			ans += inv;
			tmp.push_back(B[b++]);
		}
	}
	for (int i = 0; i < tmp.size(); i++) s[i+st] = tmp[i];
}
long long count_swaps(vector<int> s) {
	ans = 0, n = s.size();
	map <int, vector <int> > l, r;
	set <int> vals;
	for (int i = 0; i < n; i++) {
		if (s[i] < 0) 
			l[-s[i]].push_back(i);
		else
			r[s[i]].push_back(i);
		vals.insert(abs(s[i]));
	}
	vector <pair <int, int> > lines;
	for (auto val : vals) {
		vector <int> a = l[val], b = r[val];
		for (int i = 0; i < a.size(); i++) {
			int x = a[i], y = b[i];
			if (x > y) swap(x, y), ans++;
 
			lines.push_back({x, y});
		}
	}
	ll cur = 1;
	sort(lines.begin(), lines.end());
	for (auto [a, b] : lines) {
		s[a] = s[b] = cur++;
		// cout << a << ' ' << b << '\n';
	}
	// cout << ans << '\n';
	// for (int i = 0; i < n; i++) cout << s[i] << ' ';
	// cout << '\n';
	countInv(s, 0, n - 1);
	return ans;
}