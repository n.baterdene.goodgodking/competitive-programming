#include "biscuits.h"
#include <bits/stdc++.h>
typedef long long ll;
using namespace std;
map<ll, ll> m;
int firstBit(ll a) {
	for (int i = 60; i > 0; i--) {
		if (1ll<<i & a) return i;
	}
	return 0;
}
ll f(vector<ll> &s, ll x, ll n) {
    if(n <= 0) return 0;
    if(m.count(n)) return m[n];
    ll a = firstBit(n-1);
    return m[n] = f(s,x,1LL<<a) + f(s,x,min(n,1+s[a]/x)-(1LL<<a));
}
long long count_tastiness(long long x, vector<long long> a) {
    m.clear();
	a.resize(63, 0);
    for(int i=1; i<(int)a.size(); i++) {
        a[i] = a[i-1] + (a[i]<<i);
    }
	m[1] = 1;
	return f(a, x, 1+a.back());
}
