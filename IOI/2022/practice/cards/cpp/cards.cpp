#include <bits/stdc++.h>
using namespace std;
int n1, k1;
void init_assistant(int N, int K) {
    n1 = N, k1 = K;
}

vector<int> choose_cards(vector<int> cards) {
    vector <int> res;
    int s = 1;
    for (int i = 2; i < k1; i++) s *= i;
    int skip, val;
    if (cards[k1-2] + s >= cards[k1-1]) {
        skip = k1-1;
        val = cards[k1-1]-cards[k1-2];
    } else {
        skip = 0;
        val = n1-cards[k1-1] + cards[0];
    }
    for (int i = 0; i < k1; i++) {
        if (i == skip) continue;
        res.push_back(cards[i]);
    }
    for (int i = 1; i < val; i++) next_permutation(res.begin(), res.end());
    return res;
}