
#include <bits/stdc++.h>
using namespace std;
int n, k;
void init_magician(int N, int K) {
    n = N, k = K;
}


int find_discarded_card(vector<int> cards) {
    int value = 1;
    vector <int> tmp = cards;
    sort(tmp.begin(), tmp.end());
    int bg = tmp.back();
    while (tmp != cards) {
        value++;
        next_permutation(tmp.begin(), tmp.end());
    }
    if (bg+value <= n) return bg+value;
    return value+bg-n;
}
