#include "hoax.h"
#include <bits/stdc++.h>

#define mp make_pair
#define pb push_back
using namespace std;
using VI = vector <int>;
int n, s;
VI par, sz;
int find(int a) {
    return par[a] = (par[a] == a ? a : find(par[a]));
}
void merge(int a, int b) {
    a = find(a);
    b = find(b);
    if (a == b) return;
    sz[a] += sz[b];
    par[b] = a;
}
void init(int N, int S, vector<int> c, vector<vector<int>> a, vector<vector<int>> b) {
    n = N;
    s = S;
    par = VI(n+1, 0);
    sz = VI(n+1, 1);
    for (int i = 0; i <= n; i++) par[i] = i;
    vector <VI> segs; // time, action, id
    for (int i = 0; i < n; i++) 
    for (int j = 0; j < c[i]; j++) {
        segs.pb({a[i][j], 1, i});
        segs.pb({b[i][j]+1, 0, i});
    }
    sort(segs.begin(), segs.end());
    set <int> lst;
    for (auto& seg : segs) {
        if (seg[1]) { // add
            if (!lst.empty()) merge(*lst.begin(), seg[2]);
            lst.insert(seg[2]);
        } else { // remove
            auto it = lst.lower_bound(seg[2]);
            lst.erase(it);
        }
    }
    
}

int count_users(int P) {
    return sz[find(P)];
}
