#include "team.h"

#include <cassert>
#include <cstdio>

#include <vector>

int main() {
  int N, K;
  freopen("0.in", "r", stdin);
  freopen("0.out", "w", stdout);
  assert(2 == scanf("%d %d", &N, &K));

  std::vector<int> L(N);
  for (int i = 0; i < N; ++i) {
    assert(1 == scanf("%d", &L[i]));
  }

  int result = maximum_teams(N, K, L);
  printf("%d\n", result);
  return 0;
}
