#include "team.h"
#include <bits/stdc++.h>
using namespace std;
int maximum_teams(int n, int k, vector <int> nums) {
    sort(nums.begin(), nums.end());
    int ans = 0, l = 0, r = n-1;
    while (l+1 < r) {
        if (nums[r] + nums[l] <= k) {
            l++;
            continue;
        }
        ans++, r--, l+=2;
    }
    return ans;
}
