#include "towns.h"
#include <bits/stdc++.h>
using namespace std;
int n;
// bool check_road(int A, int B)
const int N = 2e3;
bool dp[N][N];
bool get(int a, int b) {
    // cout << a << ' ' << b << "\n";
    dp[a][b] = dp[b][a] = 1;
    return check_road(a, b);
}
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
int find_town(int _n) {
    n = _n;
    int out[n]; memset(out, 0, sizeof(out));
    set <int> vals, vals1;
    for (int i = 0; i < 3; i++) vals.insert(i);
    for (int i = 0; i < n; i++) dp[i][i] = 1;
    for (int i = 3; i < n; i++) {
        vals.insert(i);
        for (auto el : vals) 
        for (auto el1 : vals) {
            if (!dp[el][el1]) {
                if (get(el, el1)) {
                    out[el]++;
                } else {
                    out[el1]++;
                }
            }
        }
        vals1.clear();
        for (auto el : vals) {
            if (out[el] > 1) continue;
            vals1.insert(el);
        }
        swap(vals, vals1);
    }
    int order[n];
    for (int i = 0; i < n; i++) order[i] = i;
    shuffle(order, order+n, rng);
    for (int i = 0; i < n; i++) {
        int x = order[i];
        for (auto el : vals) {
            if (!dp[x][el] && out[el] < 2) {
                if (get(x, el)) {
                    out[x]++;
                } else {
                    out[el]++;
                }
            }
        }
    }
    for (auto el : vals) {
        if (out[el] < 2) return el;
    }
    return -1;
}
