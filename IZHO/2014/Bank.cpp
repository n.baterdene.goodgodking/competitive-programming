#include <bits/stdc++.h>
#define ff first
#define ss second
using namespace std;
using VI = vector <int>;
using PII = pair <int, int>;
using sint = short int;
const int N = 20;
const int M = 1<<N;
sint n, m;
sint a[N], b[N];
PII dp[M];
 
signed main() {
	ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
	cin >> n >> m;
	for (sint i = 0; i < n; i++) cin >> a[i];
	for (sint i = 0; i < m; i++) cin >> b[i];
	sort(a, a+n);
	sort(b, b+m);
	int full = (1<<m)-1;
	dp[0] = {0, 0};
	bool done = 0;
	for (int i = 1; i <= full && !done; i++) {
		dp[i] = {0, 0};
		for (sint j = 0; j < m; j++) {
			if (!(i & (1<<j)) || dp[i].ff > dp[i^(1<<j)].ff) continue;
			PII& tmp = dp[i^(1<<j)];
			if (tmp.ff > dp[i].ff) dp[i] = tmp;
			if (a[tmp.ff] >= b[j]+tmp.ss) dp[i].ss = max(dp[i].ss, b[j]+tmp.ss);
		}
		if (dp[i].ss == a[dp[i].ff]) dp[i] = {dp[i].ff+1, 0};
		done |= dp[i].ff == n;
	}
	cout << (done ? "YES" : "NO") << "\n";
}