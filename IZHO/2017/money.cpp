#include <bits/stdc++.h>
#define pb push_back
#define LINE "-----------------\n"
#define ALL(x) x.begin(),x.end()
using namespace std;
int n, m;
const int N = 1e6 + 5;
const int M = 1e6 + 1;
int cnt[N], nx[N], pv[N];
int nn, mm, x, i, ans, j, curCnt, a, b;

signed main() {
	ios::sync_with_stdio(0);
	cin.tie(0);
	cout.tie(0);
	cin >> n;
	int nums[n+1], sorted[n+1];
	for (i = 0; i < n; i++) {
		cin >> x;
		cnt[x]++;
		nums[nn++] = x;
	}
	for (i = 1; i < M; i++) 
		if (cnt[i] > 0) sorted[mm++] = i;
	
	nx[0] = sorted[0];
	pv[sorted[0]] = 0;
	nx[sorted[mm-1]] = M;
	pv[M] = sorted[mm-1];
	for (i = 0; i < mm-1; i++) {
		nx[sorted[i]] = sorted[i+1];
		pv[sorted[i+1]] = sorted[i];
	}
	for (i = n - 1; i >= 0; i--) {
		curCnt = 1;
		bool border = 1;
		for (j = i-1; j >= 0; j--) {
			if (nums[j] == nums[j+1]) {
				curCnt++;
				continue;
			} else {
				if ((nx[nums[j]] != nums[j+1]) || (!border && curCnt != cnt[nums[j+1]])) break;
				cnt[nums[j+1]] -= curCnt;
				if (cnt[nums[j+1]] == 0) {
					a = pv[nums[j+1]], b = nx[nums[j+1]];
					nx[a] = b;
					pv[b] = a;
				}
				border = 0;
				curCnt = 1;
			}
		}
		cnt[nums[j+1]] -= curCnt;
		if (cnt[nums[j+1]] == 0) {
			a = pv[nums[j+1]], b = nx[nums[j+1]];
			nx[a] = b;
			pv[b] = a;
		}
		ans++;
		i = j+1;
	}
	cout << ans << '\n';
}







