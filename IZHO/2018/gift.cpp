#include <bits/stdc++.h>
#define pb push_back
#define ff first
#define ss second
#define LLA(x) x.rbegin(),x.rend()
#define print(x) for(auto el:x)cout<<el<<' ';cout<<"\n";
#define printPII(x) for(auto [a,b]:x)cout<<a<<","<<b<<' ';cout<<'\n';
using namespace std;
using ll = long long;
using PII = pair <ll, int>;
int n, k, id;
bool impossible;
// set <PII> bfs;
priority_queue <PII> bfs;
vector <ll> ansW;
vector <vector <int> > ans;
signed main() {
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	cout.tie(0);
	cin >> n >> k;
	ll sum = 0, val;
	for (int i = 1; i <= n; i++) {
		ll x; cin >> x;
		sum += x;
		bfs.push({x, i});
	}
	bfs.push({0, 0});
	ll packs = sum / k;
	if (sum % k != 0 || bfs.top().ff > packs) {
		cout << "-1\n";
		return 0;
	}
	while(packs > 0) {
		vector <PII> add;
		for (int i = 0; i < k; i++) {
			tie(val, id) = bfs.top();
			bfs.pop();
			add.pb({val, id});
		}
		ll pick = min(add.back().ff, packs-bfs.top().ff);
		packs -= pick;
		vector <int> cur;
		for (int i = 0; i < k; i++) {
			tie(val, id) = add.back();
			add.pop_back();
			val -= pick;
			cur.pb(id);
			bfs.push({val, id});
		}
		ansW.pb(pick);
		ans.pb(cur);
	}
	cout << ans.size() << '\n';
	for (int i = 0; i < ans.size(); i++){
		cout << ansW[i] << ' ';
		print(ans[i]);
	}
}