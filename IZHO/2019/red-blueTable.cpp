#include <bits/stdc++.h> 
#define pb push_back
#define ALL(x) x.begin(),x.end()
using namespace std;
int n, m;
const int N = 1e3+1;
bool ans[N][N];
void go() {
	bool flip = 0;
	cin >> n >> m;
	char a = '+', b = '-';
	if (n > m) {
		flip = 1;
		swap(n, m);
	}
	for (int i = 0; i < n; i++)
	for (int j = 0; j < m; j++) ans[i][j] = 1;

	int changeCnt = m * ((n-1)/2), curLine = 0, curPick = 0;
	for (int i = 0; changeCnt>0; i++) {
		ans[curLine][i%m] = 0;
		changeCnt--, curPick++;
		if (curPick*2 > m) {
			curLine++;
			curPick = 0;
		}
	}
	cout << m + curLine <<'\n';
	if (flip) swap(n, m);
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) cout << (flip^(flip?ans[j][i]:ans[i][j])? b:a);
		cout << '\n';
	}
}
signed main() {
	ios::sync_with_stdio(0);
	cin.tie(0);
	cout.tie(0);
	// freopen("in.txt", "r", stdin);
	// freopen("out.txt", "w", stdout);
	int T; cin >> T;
	while(T--) go();
}