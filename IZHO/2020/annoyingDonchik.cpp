#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define ff first
#define ss second
#define LINE "------------\n"
#define ALL(x) x.begin(),x.end()
using PII = pair <int, int>;
using VI = vector <int>;
const int INF = 1.07e9;
const int N = 1e6+1;
string s1[N], s2[N];

int changed[2 * N];
VI change[2];
int n, m, t;
void impossible() {
	cout << "-1\n";
	exit(0);
}
bool check(int a, int b, int a1, int b1) {
	return 
		s1[a][b] == s2[a][b] && 
		s1[a][b1] == s2[a][b1] && 
		s1[a1][b] == s2[a1][b] && 
		s1[a1][b1] == s2[a1][b1];
}
vector <vector <pair <int, int> > >paths(N);
void add(int a, int b, int c) {
	paths[a].pb({b, c});
	paths[b].pb({a, c});
	// c: 1 -> color[a] == color[b]
	// c: 0 -> color[a] != color[b]
}
VI last;
bool lastRes;
void dfs(int pos, int expect, VI&change) {
	if (changed[pos] != -1) {
		if (changed[pos] != expect) lastRes = 0;
		return;
	}
	changed[pos] = expect;
	last.pb(pos);
	if (expect == 1 && pos > 0) change.pb(pos);
	for (auto&[a, b] : paths[pos]) {
		if (b == 0) dfs(a, expect^1, change);
		else dfs(a, expect, change);
	}
}
signed main() {
	// freopen("in.txt", "r", stdin);
	// freopen("out.txt", "w", stdout);
	cin >> n >> m >> t;

	for (int i = 1; i <= n; i++) cin >> s1[i];
	for (int i = 1; i <= n; i++) cin >> s2[i];
	for (int i = 1; i <= n; i++) s1[i] = '0' + s1[i], s2[i] = '0' + s2[i];
	for (int i = 1, i1 = n; i <= i1; i++, i1--) 
	for (int j = 1, j1 = m; j <= j1; j++, j1--) {
		if ((i == i1 || j == j1) && !check(i, j, i1, j1)) impossible();
		/* no change */
		bool c0 = check(i, j, i1, j1);
		swap(s1[i][j], s1[i][j1]);
		swap(s1[i1][j], s1[i1][j1]);
		/* row  */
		bool c1 = check(i, j, i1, j1);
		swap(s1[i][j], s1[i1][j]);
		swap(s1[i][j1], s1[i1][j1]);
		/* row col */
		bool c3 = check(i, j, i1, j1);
		swap(s1[i][j], s1[i][j1]);
		swap(s1[i1][j], s1[i1][j1]);
		/* col */
		bool c2 = check(i, j, i1, j1);
		int cnt = c0 + c1 + c2 + c3;
		if (cnt == 0) impossible();
		if (cnt == 1) {
			add(0, i, (c1 || c3 ? 1 : 0));
			add(0, n+j, (c2 || c3 ? 1 : 0));
		} else if (cnt == 2) {
			if (c0 && c1) add(0, n+j, 0);
			if (c0 && c2) add(0, i, 0);
			if (c0 && c3) add(i, n+j, 1);
			if (c1 && c2) add(i, n+j, 0);
			if (c1 && c3) add(0, i, 1);
			if (c2 && c3) add(0, n+j, 1);
		}
	}
	vector <pair <char, int> > ans;
	memset(changed, -1, sizeof(changed));
	for (int i = 0; i <= n+m; i++) {
		if (changed[i] != -1) continue;
		for (int j = 0; j < 2; j++) change[j].clear();
		last.clear();
		lastRes = 1;
		dfs(i, 1, change[0]);
		bool dfs1 = lastRes, dfs2 = 0;
		if (i) {
			lastRes = 1;
			for (auto& el : last) changed[el] = -1;
			dfs(i, 0, change[1]);
			dfs2 = lastRes;
			if ((change[1].size() < change[0].size() || !dfs1) && dfs2) change[0] = change[1], dfs1 = dfs2;
		}
		if (!dfs1) impossible();
		for (auto& el : change[0]) {
			ans.pb({el > n ? 'C' : 'R', el > n ? el-n : el});
		}
	}
	cout << ans.size() << '\n';
	for (auto& [a, b] : ans) {
		cout << a << ' ' << b << "\n";
	}
}











