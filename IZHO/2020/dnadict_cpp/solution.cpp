#include "grader.h"
#include <bits/stdc++.h>
using namespace std;
#define pb push_back
#define ff first
#define ss second
int N;
// get_min_max
string chars = "ACGT_";
map <char, int> id;
string find_next(string s) {
    N = s.size();
    string ans = "", prefix = "";
    for (int i = 0; i < 5; i++) id[chars[i]] = i;
    pair <string, string> res;
    {
        res = get_min_max(s);
        if (!res.ff.empty()) return s;
    }
    bool found = 0;
    for (int i = N-1; i >= 0 && !found; i--) {
    	if (id[s[i]] == 3) continue;
    	// generate
        string q = s.substr(0, i); q.pb('?');
        while(q.size() < N) q.pb('_');
    	if (id[s[i]] == 2) {
    		q[i] = 'T';
			res = get_min_max(q);
            if (!res.ff.empty()) {
            	found = 1;
                prefix = q;
            }
    		continue;
    	}
    	if (id[s[i]] == 1) {
    		q[i] = 'G';
			res = get_min_max(q);
            if (!res.ff.empty()) {
            	found = 1;
                prefix = q;
                continue;
            }
    		q[i] = 'T';
			res = get_min_max(q);
            if (!res.ff.empty()) {
            	found = 1;
                prefix = q;
            }
    		continue;
    	}
		// check 
		res = get_min_max(q);
		q[i] = s[i];
		if (res.ss <= q) continue;
		if (q < res.ff) {
			found = 1;
			prefix = res.ff;
			continue;
		}
		// findAns
		found = 1;
		prefix = res.ss;
		int tmp = id[res.ss[i]];
		for (int I = id[s[i]]+1; I < tmp; I++) {
            q[i] = chars[I];
            res = get_min_max(q);
            if (!res.ff.empty()) {
                prefix = q;
                break;
            }
        }
    }
    if (!found) return ans;
    for (auto& el : prefix) 
        if (el == '_') el = '?';
    ans = get_min_max(prefix).ff;
    return ans;
}