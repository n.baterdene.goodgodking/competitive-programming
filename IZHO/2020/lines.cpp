#include <bits/stdc++.h>
using namespace std;
using VI = vector <int>;
using VVI = vector <VI>;
#define pb push_back
int n, ans;
const int N = 1e6+1;
VVI nums(2);
signed main() {
	ios::sync_with_stdio(0);
	cin.tie(0);
	cout.tie(0);
	// freopen("in.txt", "r", stdin);
	// freopen("out.txt", "w", stdout);
	cin >> n;
	multiset <int> a, b;
	multiset <int> :: iterator ita, itb;
	for (int i = 0; i < n; i++) {
		int x; cin >> x;
		a.insert(x);
	}
	for (int i = 0; i < n; i++) {
		int x; cin >> x;
		b.insert(x);
	}
	while(!a.empty()) {
		ita = a.begin();
		itb = b.begin();
		int A = *ita, B = *itb;
		a.erase(ita);
		b.erase(itb);
		if (A > B) {
			ans++;
			a.insert(B);
			b.insert(A);
		}
	}
	cout << ans << '\n';
}