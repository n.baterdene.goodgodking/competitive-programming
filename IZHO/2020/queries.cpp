#include <bits/stdc++.h>
using namespace std;

#define ff first
#define ss second
#define pb push_back
#define lb lower_bound
#define ALL(x) x.begin(),x.end()

using ll = long long;
using VI = vector <ll>;
using VVI = vector <VI>;

int n, m; 
const int N = 2e5 + 5;
const int BLOCK = 450;
ll ans[N], dp[BLOCK][BLOCK];
vector <pair <int, int> > L, R;
vector <int> pos[N];

int nums[N], nx[N], pv[N], qL[N], qR[N], lastQ[N], first[BLOCK][BLOCK], last[BLOCK][BLOCK];

void printList(int pos = 0) {
	cout << nums[pos] << ' ';
	do {
		pos = nx[pos];
		cout << nums[pos] << ' ';
	} while(pos != n+1);
	cout << '\n';
}
void calc(int l, int r) {
	// compress values
	vector <ll> curNums;
	for (int i = l; i <= r; i++)
		curNums.pb(nums[i]);
	sort(ALL(curNums));
	curNums.erase( unique(ALL(curNums)), curNums.end() );
	int curSz = curNums.size();

	// reset values
	for (int i = 0; i <= curSz; i++) {
		// cout << "CLEAR: " << i << '\n';
		pos[i].clear();
	}
	memset(dp, 0, sizeof(dp));
	memset(last, 0, sizeof(last));
	memset(first, 0, sizeof(first));

	// generate compressed values
	for (int i = l; i <= r; i++) {
		int id = (lb(ALL(curNums), nums[i]) - curNums.begin());
		// cout << "PUSH: " << id << '\n';
		pos[id].pb(i);
	}
	for (int i = 0, j = 0; i < m; i++) {
		while(j < curSz && L[i].ff > curNums[j]) j++;
		qL[ L[i].ss ] = j;
	}
	for (int i = m-1, j = curSz-1; i >= 0; i--) {
		// cout << curNums[j] << ' ' << R[i].ff << " ->";
		while(j >= 0 && curNums[j] > R[i].ff) j--;
		// cout << curNums[j] << ' ' << R[i].ff << '\n';
		qR[ R[i].ss ] = j;
	}
 	// calculate dp
	for (int mini = 0; mini < curSz; mini++) {
		int maxi = curSz - 1, lastPos = 0;
		ll curAns = 0;
		// generate linked list
		for (int i = l; i <= r; i++) {
			if (nums[i] < curNums[mini] || nums[i] > curNums[maxi]) continue;
			if (lastPos > 0) curAns += abs(nums[lastPos] - nums[i]);
			nx[lastPos] = i;
			pv[i] = lastPos;
			lastPos = i;
		}
		nx[lastPos] = n+1;
		pv[n+1] = lastPos;
		// generate dp[mini][mini...maxi]
		dp[mini][maxi] = curAns;
		first[mini][maxi] = nx[0];
		last[mini][maxi] = pv[n+1];
		while(mini < maxi) {
			for (auto& y : pos[maxi]) {
				int x = pv[y], z = nx[y];
				if (x != 0) curAns -= nums[y] - nums[x];
				if (z != n+1) curAns -= nums[y] - nums[z];
				if (x != 0 && z != n+1) curAns += abs(nums[x] - nums[z]);
				nx[x] = z;
				pv[z] = x;
			}
			maxi--;
			dp[mini][maxi] = curAns;
			first[mini][maxi] = nx[0];
			last[mini][maxi] = pv[n+1];
		}
	}
	// calculate for each query
	for (int i = 1; i <= m; i++) {
		int x = qL[i], y = qR[i];
		if (x == curSz || y < 0) continue;
		ans[i] += dp[x][y];
		if (lastQ[i] != 0 && first[x][y] != 0) ans[i] += abs(nums[lastQ[i]] - nums[first[x][y]]);
		if (last[x][y] != 0) lastQ[i] = last[x][y];
	}
}
signed main() {
	// freopen("in.txt", "r", stdin);
	// freopen("out.txt", "w", stdout);
	ios::sync_with_stdio(0);
	cin.tie(0);
	cout.tie(0);
	cin >> n >> m;
	for (int i = 1; i <= n; i++) cin >> nums[i];
	for (int i = 1; i <= m; i++) {
		int a, b; cin >> a >> b;
		L.pb({a, i});
		R.pb({b, i});
	}
	sort(ALL(L));
	sort(ALL(R));
	for (int l = 1, r; l <= n; l+=BLOCK) 
		calc(l, min(l+BLOCK-1, n));
	
	for (int i = 1; i <= n; i++) cout << ans[i] << '\n';
}