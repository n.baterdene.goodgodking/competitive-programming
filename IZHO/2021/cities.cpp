#include <bits/stdc++.h>
#define int long long
using namespace std; 

int n, st, ed, dist, curMin, pos, l, r;
const int N = 1e6 + 5;
const int INF = 1e17;
vector <int> nums(N);
vector <int> minVal(N, INF), dp(N, INF);
int len(int a, int b) {
    return abs(a - b);
}
signed main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    // freopen("in.txt", "r", stdin);
    // freopen("out.txt", "w", stdout);
    cin >> n >> st >> ed;
    for (int i = 1; i <= n; i++) cin >> nums[i];
    set <vector <int> > bfs;
    int L = st, R = st;
    while(L > 1 && nums[L-1] >= nums[st]) L--;
    while(R < n && nums[R+1] >= nums[st]) R++;
    
    bfs.insert({0, nums[st], st, L, R}); // 
    int dx[] = {-1, 1}, cnt = 0;
    minVal[ed] = len(st, ed) * nums[st];
    while(!bfs.empty()) {
        auto it = bfs.begin();
        vector <int> cur = *it;
        bfs.erase(it);
        dist = cur[0], curMin = cur[1], pos = cur[2], l = cur[3], r = cur[4];
        minVal[pos] = min(minVal[pos], dist);
        if (pos == ed) break;
        if (curMin >= dp[pos]) continue;
        while(l > 1 && nums[l-1] >= nums[pos]) l--;
        while(r < n && nums[r+1] >= nums[pos]) r++;
        dp[pos] = curMin;
        if (l != 1) {
            int pos1 = l-1, newMin = min(curMin, nums[pos1]);
            if (newMin < dp[pos1]) bfs.insert({dist+curMin*len(pos, pos1), newMin, pos1, l-1, r});
        }
        if (r != n) {
            int pos1 = r+1, newMin = min(curMin, nums[pos1]);
            if (newMin < dp[pos1]) bfs.insert({dist+curMin*len(pos, pos1), newMin, pos1, l, r+1});
        }
        minVal[ed] = min(minVal[ed], dist+len(ed,pos)*curMin);
    }
    cout << minVal[ed] << '\n';
}