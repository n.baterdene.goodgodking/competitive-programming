#include <bits/stdc++.h>
using namespace std; 
#define pb push_back
typedef long long ll;

ll n, x, y, q;
ll solve(vector <int>&a) {
    if (a.empty()) return 0;
    set <int> vals;
    bool X = 1;
    for (auto el : a) {
        if (vals.count(el)) X = 0;
        vals.insert(el);
    }
    if (X) return x;
    int m = a.size();
    vector <int> next[2];
    for (int i = 0; i < m; i++) next[i&1].pb(a[i]);

    return y + solve(next[0]) + solve(next[1]);
}
signed main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    freopen("in.txt", "r", stdin);
    freopen("out1.txt", "w", stdout);
    cin >> n >> x >> y;
    vector <int> nums(n);
    for (int i = 0; i < n; i++) cin >> nums[i];
    cin >> q;
    while(q--) {
        int l, r;
        cin >> l >> r;
        l--, r--;
        vector <int> a;
        for (int i = l; i <= r; i++) a.pb(nums[i]);
        cout << solve(a) << '\n';
    }
}