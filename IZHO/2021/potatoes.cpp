#include <bits/stdc++.h>
// #pragma GCC optimize("Os")
#pragma GCC optimize("Ofast,no-stack-protector,unroll-loops,fast-math")
#pragma GCC target("sse,sse2,sse3,ssse3,sse4.1,sse4.2,avx,avx2,popcnt,tune=native")
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
#define pb push_back
#define ff first
#define ss second
#define ALL(A) A.begin(),A.end()
#define LINE "----------------\n";
using namespace std; 
using ll = long long;
using VI = vector <int>;
const int N = 2e5 + 1;
const int M = 3*N;
int n, q, nums[N], bit[N], suffixPos[M] = {-1}, pos[N];
ll ans[N];
gp_hash_table <int, int> position[M];
ll x, y, xy;

struct Query {
    int l, r, i;
    bool operator< (const Query&a) const {
        return r < a.r;
    }
};
vector <Query> queries;
int sum(int r) {
    int ret = 0;
    for (; r >= 0; r = (r & (r + 1)) - 1)
        ret += bit[r];
    return ret;
}
int sum(int l, int r) {
    return sum(r) - sum(l - 1);
}
void add(int idx, int delta) {
    for (; idx < n; idx = idx | (idx + 1))
        bit[idx] += delta;
}
void addVal(int lst, int num, int curPos) { // logN
    if (position[lst].find(num) != position[lst].end()) {
        int& val = position[lst][num];
        if (suffixPos[lst] < val) {  // logN
            if (suffixPos[lst] != -1) add(suffixPos[lst], -1);
            suffixPos[lst] = val;
            add(suffixPos[lst], 1);
        }
        val = curPos;
    } else {
        position[lst][num] = curPos; // logN
    }
}
vector <int> lhs, rhs;
void generatePos(VI&tmp, int l, int r) { // NlogN
    for (int i = l; i <= r; i++) pos[tmp[i]]++;
    if (l == r) return;
    for (int i = l; i <= r; i++) { // N
        if ((i & 1) == (l & 1)) lhs.pb(tmp[i]);
        else rhs.pb(tmp[i]);
    }
    for (int i = l; i < l + lhs.size(); i++) tmp[i] = lhs[i-l];
    for (int i = l+lhs.size(); i <= r; i++) tmp[i] = rhs[i-l-lhs.size()];
    int Lhs = lhs.size();
    lhs.clear(), rhs.clear();
    generatePos(tmp, l, l+Lhs-1); // NlogN
    generatePos(tmp, l+Lhs, r); // NlogN
}
signed main() {
    // testing
    // freopen("in.txt", "r", stdin);
    // freopen("out.txt", "w", stdout);
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int i, idQ = 0, j, minL=n, maxR=1, a, b;
    cin >> n >> x >> y;
    xy=x+y;
    for (i = 1; i <= n; i++) cin >> nums[i];
    cin >> q;
    for (i = 1; i <= q; i++) {
        cin >> a >> b;
        minL = min(minL, a);
        maxR = max(maxR, b);
        queries.pb({a, b, i});
    }
    sort(ALL(queries));
    // generate positions of every element in binary tree.
    vector <int> cur;
    for (i = 1; i <= n; i++) cur.pb(i);
    generatePos(cur, 0, n-1); // NlogN
    // for (int i = 1; i <= n; i++) cout << pos[i] << " \n"[i==n];

    for (i = minL; i <= maxR && idQ < q; i++) { // NLog^2_N
        // add I-th element to the tree.
        int x = i, head = 0;
        for (j = 0; j < pos[i]; j++) {
            addVal(head, nums[i], i); // logN
            if (x & 1) head = head * 2 + 1;
            else head = head * 2 + 2;
            x = (x + 1) / 2;
        }
        while(idQ < q && queries[idQ].r == i) { // logN ? 
            ans[queries[idQ].i] = sum(queries[idQ].l, n); // logN
            idQ++;
        }
    }
    for (i = 1; i <= q; i++) cout << ans[i]*xy+x << "\n";
}



















