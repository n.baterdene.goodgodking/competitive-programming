#include <bits/stdc++.h>
using namespace std;
using ll = long long;
const int N = 1e6 + 5;
const int INF = 1e9+1;
vector <int> nums(N), l(N), r(N), doors(N);
vector <ll> pre(N), dp(N, -1);
int n;
ll sum(int a, int b) {
    return pre[b] - (a ? pre[a-1] : 0);
}
ll solve(int pos) {
    ll& ans = dp[pos];
    if (ans != -1) return ans;
    if (l[pos] == 0 || r[pos] == n+2) return ans = 0;
    ll curSum = sum(l[pos], r[pos]-1);
    ans = min(
        max(max(0ll, doors[l[pos]] - curSum), solve(l[pos])),
        max(max(0ll, doors[r[pos]] - curSum), solve(r[pos]))
    );
    return ans;
}
signed main() {
    // input
    // freopen("in.txt", "r", stdin);
    // freopen("out.txt", "w", stdout);
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    cin >> n;
    for (int i = 1; i <= n; i++) cin >> nums[i];
    for (int i = 1; i <= n+1; i++) cin >> doors[i];
    // init
    for (int i = 0; i <= n+1; i++) 
        pre[i] = (i ? pre[i-1] : 0) + nums[i];
    doors[0] = doors[n+2] = INF;
    {
        vector <int> vals(1, 0);
        l[0] = l[n+2] = 0;
        for (int i = 1; i <= n+1; i++) {
            while(doors[vals.back()] <= doors[i]) vals.pop_back();
            l[i] = vals.back();
            vals.push_back(i);
        }
    }
    {
        vector <int> vals(1, n+2);
        r[0] = r[n+2] = n+2;
        for (int i = n+1; i > 0; i--) {
            while(doors[vals.back()] <= doors[i]) vals.pop_back();
            r[i] = vals.back();
            vals.push_back(i);
        }
    }
    // solution
    vector <ll> ans(n+5, INF);
    for (int i = 1; i <= n+1; i++) {
        ll cur = solve(i);
        if (i < n+1) ans[i] = min(ans[i], max(cur, max(0ll, ll(doors[i] - nums[i]))));
        if (i > 1) ans[i-1] = min(ans[i-1], max(cur, max(0ll, ll(doors[i] - nums[i-1]))));
    }
    for (int i = 1; i <= n; i++) 
        cout << ans[i] << " \n"[i==n];
}