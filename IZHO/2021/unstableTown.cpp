#include <bits/stdc++.h>
using namespace std;
#define pb push_back
using ll = long long;
const int N = 1e6 + 5;
const int MOD = 1e9 + 7;
int n;
int nxt[N];
ll fact[N], invFact[N];
bool vis[N];

ll pow(ll a, ll b, ll p) {
    a %= p;
    ll res = 1;
    while(b > 0) {
        if (b & 1) res = res * a % MOD;
        a = a * a % MOD;
        b /= 2;
    }
    return res;
}
ll inv(ll a, ll p) {
    return pow(a, p-2, p);
}
ll nCr(ll a, ll b) {
    return fact[a] * invFact[b] % MOD * invFact[a - b] % MOD;
}


signed main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    cin >> n;
    vector <int> sizes;
    for (int i = 1; i <= n; i++) cin >> nxt[i];
    for (int i = 1; i <= n; i++) {
        if (vis[i]) continue;
        int curSize = 0, a = i;
        while(!vis[a]) {
            curSize++;
            vis[a] = 1;
            a = nxt[a];
        }
        sizes.pb(curSize);
    }
    fact[0] = fact[1] = 1;
    for (int i = 2; i <= n+1; i++) fact[i] = fact[i-1] * i % MOD;
    invFact[n+1] = inv(fact[n+1], MOD);
    for (int i = n; i >= 0; i--) invFact[i] = invFact[i+1] * ll(i+1) % MOD;

    ll ans = 1ll * n * n % MOD * fact[n] % MOD, ans1 = 0, ans2 = 0, div3 = inv(3, MOD);
    for (auto el : sizes) {
        if (el == 1) continue;
        ll cur = (el == 2 ? 1 : el);
        ans1 = (ans1 + cur * fact[n+1] % MOD * div3 % MOD) % MOD;
    }
    for (auto el : sizes) {
        if (el < 4 || el % 2 == 1) continue;
        ll cur = el / 2, extra = n - el;
        ll tmp = nCr(n+1, el+1), tmp1 = fact[cur] * fact[cur] % MOD;
        tmp1 = tmp1 * fact[extra] % MOD;
        ans2 = (ans2 + 2 * tmp * tmp1 % MOD) % MOD;
    }
    ans = (ans - ans1 + ans2 + MOD) % MOD;
    cout << ans << '\n';
    return 0;
}