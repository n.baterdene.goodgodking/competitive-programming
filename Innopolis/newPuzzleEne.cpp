#include<bits/stdc++.h>
using namespace std;
struct node{
	int ff, ss;
};
node euclid(int x, int y){
	node q;
	if (x==1){
		q.ff=1;
		q.ss=0;
		return q;
	}
	int r=y%x;
	q=euclid(r, x);
	// r * q.ff - x * q.ss == 1
	// x * A - y * B == 1

	// r : (y - (y / x) * x)  * q.ff
	// x : -q.ss
	// --------------
	// x : -q.ss - (y/x)*q.ff
	// y : -q.ff
	int f=-q.ss-q.ff*(y/x);
	int s=-q.ff;
	q.ff=f;
	q.ss=s;
	return q;
}
int main(){
	int n, k, s, x;
	cin>>n>>k>>s>>x;
	int dif=x-s, gcdd=__gcd(n, k);
	if (dif<0)dif+=n;
	if (dif%gcdd!=0){
		cout<<"-1\n";
		return 0;
	}
	dif/=gcdd;
	n/=gcdd;
	k/=gcdd;
	node w=euclid(k, n);
	if (w.ff<0)w.ff%=n;
	cout<<((w.ff+n)*dif)%n;
}