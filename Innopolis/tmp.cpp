#include <iostream>
#include <vector>
#include <climits>
#include <iomanip>
using namespace std;
 

struct Edge {
    int src, dest, weight;
};
 

class Graph
{
public:
    
    vector<vector<Edge>> adjList;
 
    
    Graph(vector<Edge> const &edges, int n)
    {
        
        adjList.resize(n);
 
        
        for (Edge const &edge: edges) {
            adjList[edge.src].push_back(edge);
        }
    }
};
 


int DFS(Graph const &graph, int v, vector<bool> &discovered, vector<int> &departure, int &time) {
    discovered[v] = true;
    for (Edge e: graph.adjList[v]) {
        int u = e.dest;
        if (!discovered[u]) {
            DFS(graph, u, discovered, departure, time);
        }
    }
 
    
    
    departure[time] = v;
    time++;
}
 



void findLongestDistance(Graph const &graph, int source, int n) {
    vector<int> departure(n, -1);
    vector<bool> discovered(n);
    int time = 0;
    for (int i = 0; i < n; i++) {
        if (!discovered[i]) {
            DFS(graph, i, discovered, departure, time);
        }
    }
 
    vector<int> cost(n, INT_MAX);
    cost[source] = 0;
    for (int i = n - 1; i >= 0; i--) {
        int v = departure[i];
        for (Edge e: graph.adjList[v]) {
            int u = e.dest;
            int w = e.weight * -1;        
            if (cost[v] != INT_MAX && cost[v] + w < cost[u]) {
                cost[u] = cost[v] + w;
            }
        }
    }
    for (int i = 0; i < n; i++) {
        cout << "dist(" << source << ", " << i << ") = " << setw(2) << cost[i] * -1;
        cout << endl;
    }
}
 

int main() {
    vector<Edge> edges =
    {
        {0, 6, 2}, {1, 2, -4}, {1, 4, 1}, {1, 6, 8}, {3, 0, 3}, {3, 4, 5},
        {5, 1, 2}, {7, 0, 6}, {7, 1, -1}, {7, 3, 4}, {7, 5, -4}
    };
 
    int n = 8;
 
    Graph graph(edges, n);
 
    int source = 7;
 
    findLongestDistance(graph, source, n);
 
    return 0;
}