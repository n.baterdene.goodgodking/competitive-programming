#include <bits/stdc++.h>
#define ff first
#define ss second
#define ALL(x) x.begin(),x.end()
using namespace std;
using PII = pair <int, int>;
int n;
const int N = 5e5 + 5;
const int M = 2 * N;
int a[M], b[M], ans[M]; // 0 ? A : B
const int INF = 1.07e9;
PII dp[M][2];
const PII zero = {-1, -1};
void merge(PII&a, PII b, int add) {
	if (a == zero) {
		if (b != zero) a = {b.ff+add, b.ss+add};
		return;
	}
	if (b == zero) return;
	a.ff = min(b.ff+add, a.ff);
	a.ss = max(b.ss+add, a.ss);
}
signed main() {
	// freopen("0.in", "r", stdin);
	// freopen("0.out", "w", stdout);
	ios::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	cin >> n;
	for (int i = 1; i <= 2 * n; i++) cin >> a[i];
	for (int i = 1; i <= 2 * n; i++) cin >> b[i];
	dp[1][0] = {1, 1};
	dp[1][1] = {0, 0};

	for (int i = 2; i <= 2 * n; i++) {
		dp[i][0] = dp[i][1] = zero;
		if (a[i-1] <= a[i]) merge(dp[i][0], dp[i-1][0], 1);
		if (b[i-1] <= a[i]) merge(dp[i][0], dp[i-1][1], 1);
		if (a[i-1] <= b[i]) merge(dp[i][1], dp[i-1][0], 0);
		if (b[i-1] <= b[i]) merge(dp[i][1], dp[i-1][1], 0);
	}
	// merge(dp[2*n][0], dp[2*n][1], 0);
	if ((dp[2*n][0].ss < n || n < dp[2*n][0].ff) && (dp[2*n][1].ss < n || n < dp[2*n][1].ff)) {
		cout << "-1\n";
		return 0;
	}
	string ans = "";
	int last = 0;
	if (dp[2*n][0].ff <= n && n <= dp[2*n][0].ss) last = 0;
	else last = 1;
	int target = n;

	for (int i = 2 * n; i > 1; i--) {
		ans.push_back('A'+last);
		target -= last == 0;
		if (last == 0) {
			if (a[i-1] <= a[i] && dp[i-1][0] != zero && dp[i-1][0].ff <= target && target <= dp[i-1][0].ss) last = 0;
			else last = 1;
		} else {
			if (a[i-1] <= b[i] && dp[i-1][0] != zero && dp[i-1][0].ff <= target && target <= dp[i-1][0].ss) last = 0;
			else last = 1;
		}
	}
	ans.push_back('A'+last);
	target -= last == 0;
	reverse(ALL(ans));
	cout << ans << '\n';
}