#include <bits/stdc++.h>
#define int double
using namespace std;
// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))

#define _upgrade ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define II <int, int>
#define Q queue
#define ff first
#define bk back()
#define ss second
#define rs resize
#define ins insert 
#define fr front() 
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define PQ priority_queue
#define umap unordered_map
#define sz(x) (int)x.size()

typedef long long ll;
typedef double db;
typedef unsigned uint;
typedef unsigned long long ull;
typedef unordered_map<int, int> umapII;
// PQ going up <int, VI, greater<int> >
typedef vector<int> VI;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<bool> VB;
typedef vector<VB> VVB;
typedef vector<VVB> VVVB;
typedef vector<umapII> VumapII;
typedef vector<ll> VLL;
typedef vector<VLL> VVLL;
typedef vector<VVLL> VVVLL;
typedef vector<VVVLL> VVVVLL;
typedef vector<db> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VPI> VVPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
typedef vector<VVVI> VVVVI;
typedef vector<VVVVI> VVVVVI;
// mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
 

int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
void readI(int& a) {
    cin >> a;
}
void readI(int& a, int&b) {
    cin >> a >> b;
}
void readI(int& a, int&b, int&c) {
    cin >> a >> b >> c;
}
VI readVI(int n) {
    VI a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVI readVVI(int n, int m) {
    VVI a(n, VI(m));
    REP(i, n) a[i] = readVI(m);
    return a;
}
VLL readVLL(ll n) {
    VLL a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVLL readVVLL(ll n, ll m) {
    VVLL a(n, VLL(m));
    REP(i, n) a[i] = readVLL(m);
    return a;
}



void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}

void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
const int MOD = 1e9 + 7;
const int INF = INT_MAX;
const ll INFF = INT64_MAX;
const db EPS = 1e-9;
const db PI = acos(-1.0); //M_PI;
const int moveX[] = {-1, 0, 1, 0};
const int moveY[] = {0, 1, 0, -1};
/*
|      ⣠⣶⡾⠏⠉⠙⠳⢦⡀⠀  ⠀⢠⠞⠉⠙⠲⡀
|  ⠀⠀⣴⠿⠏⠀⠀⠀⠀⠀⠀⢳⡀⠀  ⡏⠀⠀ ⠀⠀ ⢷
|⠀⠀⢠⣟⣋⡀⢀⣀⣀⡀⠀⣀⡀⣧⠀  ⢸⠀⠀⠀⠀   ⡇ ⠀
| ⠀⢸⣯⡭⠁⠸⣛⣟⠆⡴⣻⡲⣿   ⣸⠀⠀OK.  ⡇
|⠀⠀⣟⣿⡭⠀⠀⠀⠀⠀⢱⠀⠀⣿   ⢹⠀⠀⠀⠀⠀ ⡇
|⠀⠀⠙⢿⣯⠄⠀⠀⠀⢀⡀⠀⠀⡿⠀  ⡇⠀⠀ ⠀ ⡼
|⠀⠀⠀⠀⠹⣶⠆⠀⠀⠀⠀⠀⡴⠃ ⠀ ⠘⠤⣄⣠⠞⠀ ⠀
|⠀⠀⠀⠀⢸⣷⡦⢤⡤⢤⣞⣁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ ⠀
|⠀⢀⣤⣴⣿⣏⠁⠀⠀⠸⣏⢯⣷⣖⣦⡀⠀⠀⠀⠀⠀⠀
|⢀⣾⣽⣿⣿⣿⣿⠛⢲⣶⣾⢉⡷⣿⣿⠵⣿⠀⠀⠀⠀⠀
|⣼⣿⠍⠉⣿⡭⠉⠙⢺⣇⣼⡏⠀ ⠀⣄⢸⣿
|⣿⣿⣧⣀⣿.........⣀⣰⣏⣘⣆⣀
      
|                                                   _         _   _
|                                                   (_)       | | | |
|_ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_
| '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|
| |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_
| .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|
| |               __/ |
|_|              |___/
 _   _      _ _    __        __         _     _ 
| | | | ___| | | __\ \      / /__  _ __| | __| |
| |_| |/ _ \ | |/ _ \ \ /\ / / _ \| '__| |/ _` |
|  _  |  __/ | | (_) \ V  V / (_) | |  | | (_| |
|_| |_|\___|_|_|\___/ \_/\_/ \___/|_|  |_|\__,_|                                  


 
 
 


*/







int AA(PII&a, PII&b) {
    return (a.ff - b.ff) * (a.ff - b.ff) + (a.ss - b.ss) * (a.ss - b.ss);
}

int dist(PII& a, PII& b) {
    PII x = {(((ll)(a.ff + b.ff)) / 2), 1};
    PII y = {(((ll)(a.ff + b.ff)) / 2), 2};
    return max(
        min(AA(a, x), AA(b, x)),
        min(AA(a, y), AA(b, y))
    );
}

void go () {
    int n, m, curIdx = 1;
    cin >> n >> m;
    char x;
    map <int, PII> enter;
    set <PII> els;
    cin >> x;
    enter[curIdx++] = {1, 1};
    cout << "1 1\n";
    els.insert({1, 1});
    set <VD> dists;
    set <PII> left;
    REP1(i, n+1) {
        left.insert({i, 1});
        left.insert({i, 2});
    }
    REP(i, m - 1) {
        cin >> x;
        if (x == 'L') {
            curIdx++;
            int idx, add = 0;
            PII a, b;
            cin >> idx;
            PII cur = enter[idx];
            auto it = els.find(cur);
            auto it1 = it;
            bool tmp = 1;
            if (it != els.begin()) {
                auto it2 = --it;
                a = *it2;
                add += it->ff - cur.ff;
                PII temp = *it;
                dists.erase({-(dist(temp, cur)), it->ff, it->ss, cur.ff, cur.ss});
            } else {
                tmp = 0;
            }
            if ((++it1) != els.end()) {
                add += it1->ff - cur.ff;
                PII temp = *it;
                dists.erase({-(dist(cur, temp)), cur.ff, cur.ss, it1->ff, it1->ss});
                b = *it1;
            } else {
                tmp = 0;
            }

            if (tmp) {
                add = (dist(b, a));
                dists.insert({-(add), a.ff, a.ss, b.ff, b.ss});
            }
            els.erase(cur);
            left.insert(cur);

        } else {
            if (els.size() == 0) {
                PII cur = mp(1, 1);
                els.insert(cur);
                cout << cur.ff << ' ' << cur.ss << '\n';
                left.erase(cur);
                enter[curIdx++] = cur;
                continue;
            }
            if (els.size() == 1) {
                PII cur = (*els.begin() == mp(1.0, 1.0) ? mp(n, 2.0) : mp(1.0, 1.0));
                els.insert(cur);
                cout << cur.ff << ' ' << cur.ss << '\n';
                left.erase(cur);
                enter[curIdx++] = cur;
                PII A = {1, 1}, B = {n, 2};
                int add = dist(A, B);
                dists.insert({-(add), 1, 1, n, 2});
                continue;
            } 
            auto IT = (dists.begin());
            VI a = *IT;
            dists.erase(IT);
            PII cur;
            if (a[0] == 0) {
                cur = *(left.begin());
            } else {
                cur.ff = ((ll)(a[1]+a[3])/2);
                if (a[2] == a[4]) 
                    cur.ss = 3 - a[2];
                else cur.ss = 1;
                if (((ll)a[3] - (ll)a[1]) % 2 == 1 && a[2] != a[4]) cur.ss = a[4];
            }
            if ((a[3] - a[1] == 1) || (a[3] - a[1] == 2 && a[2] != a[4])) {
                cur = {a[1], 3 - a[2]};
            }
            if ((--els.end())->ff != n) {
                cur = *(--els.end());
                cur = {n, 3 - cur.ss};
            }
            els.insert(cur);
            cout << cur.ff << ' ' << cur.ss << '\n';
            left.erase(cur);
            enter[curIdx++] = cur;
            auto it = els.find(cur);
            cur = *it;
            auto it1 = it;
            if (it != els.begin()) {
                auto it2 = --it;
                PII b = *it2;
                int add = dist(cur, b);
                dists.insert({-(add), b.ff, b.ss, cur.ff, cur.ss});
            }
            if ((++it1) != els.end()) {
                PII b = *it1;
                VI temp = {b.ff - cur.ff, cur.ff, cur.ss, b.ff, b.ss};
                int add = dist(cur, b);
                dists.insert({-(add), cur.ff, cur.ss, b.ff, b.ss});
            } 
        } 
    }
}



signed main () {
    freopen("in.txt", "r", stdin);
    _upgrade
    int T = 1;
    while(T--) go();
    return 0;
}
