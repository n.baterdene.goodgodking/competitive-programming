#include <bits/stdc++.h>
using namespace std;

class Solution {
private:
	int n;
	map <int, int> bitmaskDp;
	vector <int> nums1;
public:
int score(int curMult, int bitmask) {
	if (curMult > n / 2) return 0;
	if (bitmaskDp.find(bitmask) != bitmaskDp.end()) return bitmaskDp[bitmask];
	int curMax = 0;
	for (int i = 0; i < n; i++) {
		if ((1<<i) & bitmask) continue;
		for (int j = i + 1; j < n; j++) {
			if ((1<<j) & bitmask) continue;
			bitmask += (1<<i) + (1<<j);
			int res = score(curMult+1, bitmask);
			bitmask -= (1<<i) + (1<<j);
			curMax = max(curMax, gcd(nums1[i], nums1[j]) * curMult + res);
		}
	}
	return bitmaskDp[bitmask] = curMax;
}
    int maxScore(vector<int>& nums) {
        sort(nums.begin(), nums.end());
		n = nums.size();
		nums1 = nums;
		vector <bool> used(n, 0);
		return score(1, 0);
    }
};