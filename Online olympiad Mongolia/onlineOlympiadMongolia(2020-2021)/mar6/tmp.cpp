
  
#include <bits/stdc++.h> 
using namespace std; 
  
vector<int> children[100000]; 
void addEdge(int frm, int to) { 
    children[frm].push_back(to); 
} 
void print(int dist[], int V) { 
	int ans = 0;
    for (int i = 1; i <= V; i++) { 
		ans += dist[i];
    } 
	cout << ans << '\n';
} 
void shortestPathFaster(int S, int V) { 
    int dist[V + 1]; 
    vector <bool> vis(V+1, 0);
    for (int i = 0; i <= V; i++) { 
        dist[i] = INT_MAX; 
    } 
    dist[S] = 0; 
    queue<int> bfs; 
    bfs.push(S); 
    vis[S] = true; 
    while (!bfs.empty()) { 
        int u = bfs.front(); 
        bfs.pop(); 
        vis[u] = false; 
        for (int i = 0; i < children[u].size(); i++) { 
            int v = children[u][i];
            int weight = 1; 
            if (dist[v] > dist[u] + weight) { 
                dist[v] = dist[u] + weight; 
                if (!vis[v]) { 
                    bfs.push(v); 
                    vis[v] = true; 
                } 
            } 
        } 
    } 
    print(dist, V); 
} 
  
int main() { 
    int V = 5; 
    int S = 1; 
    addEdge(1, 2);
    addEdge(2, 3);
    addEdge(2, 4);
    addEdge(1, 3);
    addEdge(1, 4);
    addEdge(3, 4);
    addEdge(2, 5);
    addEdge(4, 5);
    shortestPathFaster(S, V); 
  
    return 0; 
} 