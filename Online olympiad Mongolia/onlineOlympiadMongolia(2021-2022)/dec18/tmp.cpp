#include <string>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

typedef unsigned int hash_type;

int n;
string a, b;

const long long MOD = 1e9+7 ;
vector<vector<hash_type> > ha, hb;
vector<bool> mem;

bool can(int i) {
    if (i == n) return true;
    if (mem[i]) return false;
    mem[i] = true;

    for (int j = i + 1; j <= n; j++) {
        int l = j - i - 1;
        if (ha[i][l] == hb[n - j][l]) {
            if (can(j))
                return true;
        }
    }
    return false;
}

int main() {

    // freopen("in.txt", "r", stdin);
    // freopen("out.txt", "w", stdout);
    cin >> n >> a >> b;

    mem.resize(n);
    ha.resize(n);
    hb.resize(n);

    for (int i = 0; i < n; i++) {
        a[i] -= 'a'; b[i] -= 'a';
        ha[i].resize(n - i + 1);
        hb[i].resize(n - i + 1);
        ha[i][0] = a[i];
        hb[i][0] = b[i];
    }

    hash_type power = 1;

    for (int l = 1; l < n; l++) {
        power = power * (1e9+7);
        for (int i = 0; i + l <= n; i++) {
            ha[i][l] = ha[i][l - 1] + power * a[i + l];
            hb[i][l] = hb[i][l - 1] + power * b[i + l];
        }
    }

    if (can(0)) {
        cout << "Yes\n";
    } else {
        cout << "No\n";
    }
    return 0;
}