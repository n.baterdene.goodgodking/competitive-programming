#include <bits/stdc++.h>
#define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define sz(x) (int)x.size()

using ll = long long;
using db = double;
using uint = unsigned;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VS = vector<string>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VVVVB = vector<VVVB>;
using VVVVVB = vector<VVVVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using VVVLL = vector<VVLL>;
using VVVVLL = vector<VVVLL>;
using VD = vector<db>;
using PII = pair<int, int>;
using PDD = pair<db, db>;
using PLL = pair<ll, ll>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
using VVVVI = vector<VVVI>;
using VVVVVI = vector<VVVVI>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int bitCnt(int a) {
    bitset <64> b(a);
    return b.count();
}
int bitCnt(string a) {
    bitset <64> b(a);
    return b.count();
}
VI readVI(int n) {
    VI a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVI readVVI(int n, int m) {
    VVI a(n, VI(m));
    REP(i, n) a[i] = readVI(m);
    return a;
}
VLL readVLL(ll n) {
    VLL a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVLL readVVLL(ll n, ll m) {
    VVLL a(n, VLL(m));
    REP(i, n) a[i] = readVLL(m);
    return a;
}


int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}

void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VPI& a) {
    for (auto el : a) {
        cout << el.ff << ',' << el.ss << ' ';
    }
    cout << '\n';
}

void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VVI& a) {
    for (auto el : a) {
        print(el);
    }
}
const int MOD = 1e9 + 7;
const int MOD1 = 998244353;//7*17*2^23 +1;
const int INF = 1.07e9;
const ll INFF = INT64_MAX;
const db EPS = 1e-9;
const db PI = acos(-1.0); //M_PI;
const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0, 1, 0, -1};
/*
|      ⣠⣶⡾⠏⠉⠙⠳⢦⡀⠀  ⠀⢠⠞⠉⠙⠲⡀
|    ⣴⠿⠏⠀⠀⠀⠀⠀⠀⢳⡀⠀  ⡏⠀⠀ ⠀⠀ ⢷
|⠀⠀⢠⣟⣋⡀⢀⣀⣀⡀⠀⣀⡀⣧⠀  ⢸⠀⠀⠀⠀   ⡇ ⠀
| ⠀⢸⣯⡭⠁⠸⣛⣟⠆⡴⣻⡲⣿   ⣸⠀⠀OK.  ⡇
|⠀⠀⣟⣿⡭⠀⠀⠀⠀⠀⢱⠀⠀⣿   ⢹⠀⠀⠀⠀⠀ ⡇
|⠀⠀⠙⢿⣯⠄⠀⠀⠀⢀⡀⠀⠀⡿⠀  ⡇⠀⠀ ⠀ ⡼
|⠀⠀⠀⠀⠹⣶⠆⠀⠀⠀⠀⠀⡴⠃ ⠀ ⠘⠤⣄⣠⠞⠀ ⠀
|⠀⠀⠀⠀⢸⣷⡦⢤⡤⢤⣞⣁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ ⠀
|⠀⢀⣤⣴⣿⣏⠁⠀⠀⠸⣏⢯⣷⣖⣦⡀⠀⠀⠀⠀⠀⠀
|⢀⣾⣽⣿⣿⣿⣿⠛⢲⣶⣾⢉⡷⣿⣿⠵⣿⠀⠀⠀⠀⠀
|⣼⣿⠍⠉⣿⡭⠉⠙⢺⣇⣼⡏⠀ ⠀⣄⢸⣿
|⣿⣿⣧⣀⣿.........⣀⣰⣏⣘⣆⣀
      
|                                                    _         _   _
|                                                   (_)       | | | |
 _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_
| '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|
| |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_
| .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|
| |               __/ |
|_|              |___/
 _   _      _ _    __        __         _     _ 
| | | | ___| | | __\ \      / /__  _ __| | __| |
| |_| |/ _ \ | |/ _ \ \ /\ / / _ \| '__| |/ _` |
|  _  |  __/ | | (_) \ V  V / (_) | |  | | (_| |
|_| |_|\___|_|_|\___/ \_/\_/ \___/|_|  |_|\__,_|                                  
 ____     __    _______    _______   __________
|     \  |  |  |  _____|  /  _____) |____  ____|
|  |\  \ |  |  | |__     (  (_____      |  |
|  | \  \|  |  |  __|     \_____  \     |  | 
|  |  \     |  | |_____    _____)  )    |  | 
|__|   \____|  |_______|  (_______/     |__| 
 
 
 


*/










const int N = 1e5+5;

ll solve(int l, int r, vector <int> pos) {
    if (pos.size() == 1) return 0;
    int n = pos.size();
    ll res = 0, res1 = 0, cur;
    ll lhs[n], rhs[n], rrhs[n];

    cur = pos[0] - l + 1;
    lhs[0] = cur;
    cur = (cur + cur * (pos[1] - pos[0] - 1) + pos[1] - l + 1) % MOD;
    for (int i = 1; i < n; i++) {
        lhs[i] = cur;
        if (i + 1 < n) 
            cur = (cur + cur * (pos[i+1] - pos[i] - 1) + pos[i+1] - l + 1) % MOD;
    }
    cur = r - pos[n-1] + 1;
    rrhs[n-1] = cur;
    cur = (cur + cur * (pos[n-1] - pos[n-2] - 1)) % MOD;
    rhs[n-1] = cur;
    cur = (cur + r - pos[n-2] + 1) % MOD;
    for (int i = n-2; i > 0; i--) {
        rrhs[i] = cur;
        cur = (cur + (r - pos[i] + 1) * (pos[i] - pos[i-1] - 1)) % MOD;
        rhs[i] = cur;
        if (i > 0) 
            cur = (cur + r - pos[i-1] + 1) % MOD;
    }
    ll a = lhs[0];
    // cout << "rhs: ";
    rhs[0] = 0;
    // for (int i = 0; i < n; i++) cout << rhs[i] << " \n"[i==n-1];
    // cout << a << ' ' << rhs[1] << '\n';
    res = a * rhs[1];
    for (int i = 1; i < n-1; i++) {
        a = pos[i] - l + 1;
        // cout << a << ' ' << rhs[i+1] << '\n';
        res = (res + a * rhs[i+1])%MOD;
    }
    for (int i = 0, j = 1; j < n; i++, j++) {
        ll x = pos[i] - l + 1;
        ll y = r - pos[j] + 1;
        ll z = pos[j] - pos[i] - 2;
        if (z > 0) {
            ll a = z, b = z+1, c = z+2;
            if (a % 3 == 0) a /= 3;
            if (b % 3 == 0) b /= 3;
            if (c % 3 == 0) c /= 3;
            if (a % 2 == 0) a /= 2;
            if (b % 2 == 0) b /= 2;
            ll tmp = a * b % MOD * c;
            tmp %= MOD;
            res1 = (res1 + x * y % MOD * tmp) % MOD;
        }
        y = pos[j] - pos[i] - 1;
        res1 = (res1 + (x) * y * rrhs[j]%MOD)%MOD;
    }
    // cout << res << ' ' << res1 << '\n';
    return (res+res1)%MOD;
}
ll len(ll a) {
    return a * (a + 1) / 2;
}
ll calc(int l, int r, vector <int> pos) {
    int L = pos[0], R = pos.back();
    ll res = len(r - l + 1) - len(L - l) - len(r - R);
    for (int i = 1; i < pos.size(); i++) {
        res -= len(pos[i] - pos[i-1]-1);
    }
    return res;
}

int n, m, k;
int nums[N], l[N], r[N];
string s;
map <int, int> Val;
vector <int> pos[N];
void go () {
    cin >> n;
    set <int> vals;
    for (int i = 1; i <= n; i++) {
        cin >> nums[i];
        vals.insert(nums[i]);
    }
    if (n < 40) {
        int ans = 0;
        for (int a = 1; a <= n; a++)
        for (int b = a; b <= n; b++)
        for (int c = b+1; c <= n; c++)
        for (int d = c; d <= n; d++) {
            int p = a, x = nums[a];
            int q = c, y = nums[c];
            while(p <= b) x = max(x, nums[p++]);
            while(q <= d) y = max(y, nums[q++]);
            if (x == y) ans++;
        }
        cout << ans << '\n';
        return;
    }
    int tmp = -1;
    for (auto& el : vals) 
        Val[el] = ++tmp;

    for (int i = 1; i <= n; i++) {
        nums[i] = Val[nums[i]];
        pos[nums[i]].pb(i);
    }
    nums[0] = nums[n+1] = tmp+1;
    { // generate l[]
        vector <int> ids(1, 0);
        for (int i = 1; i <= n; i++) {
            while(nums[ids.back()] <= nums[i]) ids.pop_back();
            l[i] = ids.back();
            ids.push_back(i);
        }
    }
    { // generate r[]
        vector <int> ids(1, n+1);
        for (int i = n; i > 0; i--) {
            while(nums[ids.back()] <= nums[i]) ids.pop_back();
            r[i] = ids.back();
            ids.push_back(i);
        }
    }

    // for (int i = 0; i <= n+1; i++) cout << nums[i] << " \n"[i==n+1];
    // for (int i = 1; i <= n; i++) cout << l[i] << " \n"[i==n];
    // for (int i = 1; i <= n; i++) cout << r[i] << " \n"[i==n];
    ll ans = 0;
    // tmp = 3;
    while(tmp >= 0) {
        VI& Pos = pos[tmp];

        m = Pos.size();
        vector <ll> sizes;
        int st = 0, ed = 0;
        ll curAns = 0, curAns1 = 0;
        while(ed < m) {
            vector <int> cur(1, Pos[st]);
            while(ed + 1 < m && Pos[ed+1] < r[Pos[st]]) 
                cur.pb(Pos[++ed]);
            
            curAns = (curAns + solve(l[Pos[st]]+1, r[Pos[st]]-1, cur)) % MOD;
            sizes.pb(calc(l[Pos[st]]+1, r[Pos[st]]-1, cur));
            st = ed = ed+1;
        }
        ll sum = 0;
        for (auto& el : sizes) sum += el;
        for (auto& el : sizes) {
            sum -= el;
            curAns1 = (curAns1 + el * sum) % MOD;
        }
        tmp--;
        // cout << LINE;
        // cout << tmp << "\n\n";
        // cout << curAns << ' ' << curAns1 << ' ' << curAns+curAns1 << '\n';
        ans = (ans + curAns+curAns1)%MOD;
    }
    cout << ans << '\n';
}



signed main () {

#ifndef ONLINE_JUDGE
    // freopen("in.txt", "r", stdin);
    // freopen("out.txt", "w", stdout);
#endif
    _upgrade
    int T = 1;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














