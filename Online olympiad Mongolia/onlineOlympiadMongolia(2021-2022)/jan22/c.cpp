#include <bits/stdc++.h>
using namespace std;
const long long MOD = 1000000007;
signed main () {
    long long n, m, k;
    cin >> n;
    long long ans = 0;
    for (int i = 0; i < 30 && (1<<i) <= n; i++) {
        long long cur = (1<<i);
        long long other = 0;
        for (int j = 29; j >= 0; j--) {
            if (i == j) continue;
            other <<= 1;
            if (cur + (1<<j) <= n) {
                cur += (1<<j);
                other |= 1;
            }
        }
        ans = (ans + (1ll<<i) * (other+1) % MOD * (n-other-1) % MOD) % MOD;
    }
    cout << ans << '\n';
}










