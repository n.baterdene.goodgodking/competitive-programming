#include <bits/stdc++.h>
// #define int long long
using namespace std;
 
#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define rs resize
#define ins insert 
#define fr front() 
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define umap unordered_map
#define sz(x) (int)x.size()
 
typedef long long ll;
typedef double db;
typedef unsigned uint;
typedef unsigned long long ull;
typedef unordered_map<int, int> umapII;
// PQ going up <int, VI, greater<int> >
typedef vector<int> VI;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<bool> VB;
typedef vector<VB> VVB;
typedef vector<VVB> VVVB;
typedef vector<ll> VLL;
typedef vector<VLL> VVLL;
typedef vector<VVLL> VVVLL;
typedef vector<VVVLL> VVVVLL;
typedef vector<db> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef pair<int, int> PII;
typedef pair<db, db> PDD;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VPI> VVPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
typedef vector<VVVI> VVVVI;
typedef vector<VVVVI> VVVVVI;
// mt19937 rng(chrono::high_resolution_clock::now().time_since_epoch().count() + reinterpret_cast<unsigned long>(new int) + *(new unsigned long));
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int bitCnt(int a) {
    bitset <64> b(a);
    return b.count();
}
int bitCnt(string a) {
    bitset <64> b(a);
    return b.count();
}
VI readVI(int n) {
    VI a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVI readVVI(int n, int m) {
    VVI a(n, VI(m));
    REP(i, n) a[i] = readVI(m);
    return a;
}
VLL readVLL(ll n) {
    VLL a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVLL readVVLL(ll n, ll m) {
    VVLL a(n, VLL(m));
    REP(i, n) a[i] = readVLL(m);
    return a;
}
 
 
int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}
 
void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VPI& a) {
    for (auto el : a) {
        cout << el.ss << ',' << el.ff << ' ';
    }
    cout << '\n';
}
 
void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VVI& a) {
    for (auto el : a) {
        print(el);
    }
}
const int MOD = 1e9 + 7;
const int MOD1 = 998244353;//119*2^23 +1;
const int INF = 2e9;
const ll INFF = INT64_MAX;
const db EPS = 1e-9;
const db PI = acos(-1.0); //M_PI;
const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0, 1, 0, -1};
/*
|      â£ â£¶â¡¾â �â �â �â ³â¢¦â¡�â �  â �â¢ â �â �â �â ²â¡�
|    â£´â ¿â �â �â �â �â �â �â �â¢³â¡�â �  â¡�â �â � â �â � â¢·
|â �â �â¢ â£�â£�â¡�â¢�â£�â£�â¡�â �â£�â¡�â£§â �  â¢¸â �â �â �â �   â¡� â �
| â �â¢¸â£¯â¡­â �â ¸â£�â£�â �â¡´â£»â¡²â£¿   â£¸â �â �OK.  â¡�
|â �â �â£�â£¿â¡­â �â �â �â �â �â¢±â �â �â£¿   â¢¹â �â �â �â �â � â¡�
|â �â �â �â¢¿â£¯â �â �â �â �â¢�â¡�â �â �â¡¿â �  â¡�â �â � â � â¡¼
|â �â �â �â �â ¹â£¶â �â �â �â �â �â �â¡´â � â � â �â ¤â£�â£ â �â � â �
|â �â �â �â �â¢¸â£·â¡¦â¢¤â¡¤â¢¤â£�â£�â �â �â �â �â �â �â �â �â �â � â �
|â �â¢�â£¤â£´â£¿â£�â �â �â �â ¸â£�â¢¯â£·â£�â£¦â¡�â �â �â �â �â �â �
|â¢�â£¾â£½â£¿â£¿â£¿â£¿â �â¢²â£¶â£¾â¢�â¡·â£¿â£¿â µâ£¿â �â �â �â �â �
|â£¼â£¿â �â �â£¿â¡­â �â �â¢ºâ£�â£¼â¡�â � â �â£�â¢¸â£¿
|â£¿â£¿â£§â£�â£¿.........â£�â£°â£�â£�â£�â£�
      
|                                                    _         _   _
|                                                   (_)       | | | |
 _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_
| '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|
| |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_
| .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|
| |               __/ |
|_|              |___/
 _   _      _ _    __        __         _     _ 
| | | | ___| | | __\ \      / /__  _ __| | __| |
| |_| |/ _ \ | |/ _ \ \ /\ / / _ \| '__| |/ _` |
|  _  |  __/ | | (_) \ V  V / (_) | |  | | (_| |
|_| |_|\___|_|_|\___/ \_/\_/ \___/|_|  |_|\__,_|                                  
 ____     __    _______    _______   __________
|     \  |  |  |  _____|  /  _____) |____  ____|
|  |\  \ |  |  | |__     (  (_____      |  |
|  | \  \|  |  |  __|     \_____  \     |  | 
|  |  \     |  | |_____    _____)  )    |  | 
|__|   \____|  |_______|  (_______/     |__| 
 
 
 
 
*/
 
 
 
 
 
 
 
 
 
 
 
class BipartiteMatching {
private:
    int n, m;
    VVI paths1;
    VI greedy1, fix1, connected1, connected2;
    bool connect(int pos, int par = -1) {
        while (greedy1[pos] < paths1[pos].size()) {
            if (connected2[ paths1[pos][greedy1[pos]] ] == 0) {
                PII connecting = {pos, paths1[pos][greedy1[pos]]};
                // cout << "Connect: " << connecting.ff << ' ' << connecting.ss << '\n';
                greedy1[pos]++;
                connected1[ connecting.ff ] = connecting.ss;
                connected2[ connecting.ss ] = connecting.ff;
                return 1;
            }
            greedy1[pos]++;
        }
        while (fix1[pos] < paths1[pos].size()) {
            PII cutting = {connected2[ paths1[pos][fix1[pos]] ], paths1[pos][fix1[pos]]};
            PII connecting = {pos, paths1[pos][fix1[pos]]};
            if (connecting.ss == par) {
                fix1[pos]++;
                continue;
            }
            // cout << "Cut: " << cutting.ff << ' ' << cutting.ss << '\n';
            // cout << "Connect: " << connecting.ff << ' ' << connecting.ss << '\n';
            connected1[ cutting.ff ] = connected2[ cutting.ss ] = 0;
            connected1[ connecting.ff ] = connecting.ss;
            connected2[ connecting.ss ] = connecting.ff;
            fix1[pos]++;
            if (connect(cutting.ff, cutting.ss)) {
                return 1;
            }
 
            // cout << "Cut: " << connecting.ff << ' ' << connecting.ss << '\n';
            // cout << "Connect: " << cutting.ff << ' ' << cutting.ss << '\n';
            connected1[ connecting.ff ] = connected2[ connecting.ss ] = 0;
            connected1[ cutting.ff ] = cutting.ss;
            connected2[ cutting.ss ] = cutting.ff;
        }
        return 0;
    }
    void match() {
        fix1.resize(n+5, 0);
        greedy1.resize(n+5, 0);
        bool T = 1;
        while(T) {
            T = 0;
            for (int i = 0; i < n+5; i++) fix1[i] = greedy1[i] = 0;
            for (int i = 1; i <= n; i++) {
                if (connected1[i] == 0) 
                    T |= connect(i);
            }
        }
    }
public:
    VPI matching;
    BipartiteMatching(int _n, int _m, VPI&paths) {
        n = _n;
        m = _m;
        paths1.resize(n+5);
        connected1.resize(n+5, 0);
        connected2.resize(m+5, 0);
        for (auto&[a, b] : paths) 
            paths1[a].pb(b);
        match();
        for (int i = 1; i <= n; i++) {
            if (connected1[i] > 0)
                matching.pb({i, connected1[i]});
        }
    }
};
void go () {
    int n, x;
    cin >> n;
    VVI nums = readVVI(n, n);
    VI cnt(11, 1);
    VVPI paths(11); // paths[i] => i and i+1;
    VVI id(n, VI(n, 0));

    for (int i = 0; i < n; i++)
    for (int j = 0; j < n; j++) {
        id[i][j] = cnt[nums[i][j]]++;
        // cout << id[i][j] << " \n"[j==n-1];
    }
    
    for (int i = 0; i < n; i++)
    for (int j = 0; j < n; j++) {
        for (int k = 0; k < 4; k++) {
            int ii = i + dx[k];
            int jj = j + dy[k];
            if (ii < 0 || ii >= n || jj < 0 || jj >= n || nums[ii][jj] != nums[i][j]+1) continue;
            paths[nums[i][j]].pb({id[i][j], id[ii][jj]});
        }
    }
    bool ans = 1;
    int i;
    for (i = 1; cnt[i+1]-1 > 0 && ans; i++) {
        if (cnt[i] < cnt[i+1]) {
            ans = 0;
            continue;
        }
        // cout << i << ": " << cnt[i] << ' ' << cnt[i+1] << " -> ";
        // for (auto [a, b] : paths[i]) cout << a << ',' << b << ' ';
        //     cout << "\n";
        BipartiteMatching g(cnt[i], cnt[i+1], paths[i]);
        // cout << "SZ: " << g.matching.size() << ' ' << cnt[i+1] << '\n';
        ans &= (g.matching.size() == cnt[i+1]-1);
    }
    for (i=i+1; i < 11; i++) {
        ans &= (cnt[i] == 1);
    }
    cout << (ans ? "YES" : "NO") << '\n';
}
 
 
 
signed main () {
 
#ifndef ONLINE_JUDGE
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
#endif
    _upgrade
    int T = 1;
    cin >> T;
    while(T--) go();
    return (0-0); //<3
}
 
/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/
 