#include <bits/stdc++.h>
// #define int long long
using namespace std;
// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))

#define _upgrade ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define II <int, int>
#define Q queue
#define ff first
#define ss second
#define rs resize
#define ins insert 
#define fr front() 
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define PQ priority_queue
#define umap unordered_map
#define sz(x) (int)x.size()

typedef long long ll;
typedef double db;
typedef unsigned uint;
typedef unsigned long long ull;
typedef unordered_map<int, int> umapII;
// PQ going up <int, VI, greater<int> >
typedef vector<int> VI;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<bool> VB;
typedef vector<VB> VVB;
typedef vector<VVB> VVVB;
typedef vector<umapII> VumapII;
typedef vector<ll> VLL;
typedef vector<VLL> VVLL;
typedef vector<VVLL> VVVLL;
typedef vector<VVVLL> VVVVLL;
typedef vector<db> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VPI> VVPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
typedef vector<VVVI> VVVVI;
typedef vector<VVVVI> VVVVVI;
mt19937 rng(chrono::high_resolution_clock::now().time_since_epoch().count() + reinterpret_cast<unsigned long>(new int) + *(new unsigned long));
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int bitCnt(int a) {
    bitset <64> b(a);
    return b.count();
}
int bitCnt(string a) {
    bitset <64> b(a);
    return b.count();
}
VI readVI(int n) {
    VI a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVI readVVI(int n, int m) {
    VVI a(n, VI(m));
    REP(i, n) a[i] = readVI(m);
    return a;
}
VLL readVLL(ll n) {
    VLL a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVLL readVVLL(ll n, ll m) {
    VVLL a(n, VLL(m));
    REP(i, n) a[i] = readVLL(m);
    return a;
}


int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}

void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VPI& a) {
    for (auto el : a) {
        cout << el.ff << ',' << el.ss << ' ';
    }
    cout << '\n';
}

void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VVI& a) {
    for (auto el : a) {
        print(el);
    }
}
const int MOD = 1e9 + 7;
const int MOD1 = 998244353;//119*2^23 +1;
const int INF = 2e9;
const ll INFF = INT64_MAX;
const db EPS = 1e-9;
const db PI = acos(-1.0); //M_PI;
const int moveX[] = {-1, 0, 1, 0};
const int moveY[] = {0, 1, 0, -1};
/*
|      ⣠⣶⡾⠏⠉⠙⠳⢦⡀⠀  ⠀⢠⠞⠉⠙⠲⡀
|    ⣴⠿⠏⠀⠀⠀⠀⠀⠀⢳⡀⠀  ⡏⠀⠀ ⠀⠀ ⢷
|⠀⠀⢠⣟⣋⡀⢀⣀⣀⡀⠀⣀⡀⣧⠀  ⢸⠀⠀⠀⠀   ⡇ ⠀
| ⠀⢸⣯⡭⠁⠸⣛⣟⠆⡴⣻⡲⣿   ⣸⠀⠀OK.  ⡇
|⠀⠀⣟⣿⡭⠀⠀⠀⠀⠀⢱⠀⠀⣿   ⢹⠀⠀⠀⠀⠀ ⡇
|⠀⠀⠙⢿⣯⠄⠀⠀⠀⢀⡀⠀⠀⡿⠀  ⡇⠀⠀ ⠀ ⡼
|⠀⠀⠀⠀⠹⣶⠆⠀⠀⠀⠀⠀⡴⠃ ⠀ ⠘⠤⣄⣠⠞⠀ ⠀
|⠀⠀⠀⠀⢸⣷⡦⢤⡤⢤⣞⣁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ ⠀
|⠀⢀⣤⣴⣿⣏⠁⠀⠀⠸⣏⢯⣷⣖⣦⡀⠀⠀⠀⠀⠀⠀
|⢀⣾⣽⣿⣿⣿⣿⠛⢲⣶⣾⢉⡷⣿⣿⠵⣿⠀⠀⠀⠀⠀
|⣼⣿⠍⠉⣿⡭⠉⠙⢺⣇⣼⡏⠀ ⠀⣄⢸⣿
|⣿⣿⣧⣀⣿.........⣀⣰⣏⣘⣆⣀
      
|                                                    _         _   _
|                                                   (_)       | | | |
 _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_
| '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|
| |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_
| .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|
| |               __/ |
|_|              |___/
 _   _      _ _    __        __         _     _ 
| | | | ___| | | __\ \      / /__  _ __| | __| |
| |_| |/ _ \ | |/ _ \ \ /\ / / _ \| '__| |/ _` |
|  _  |  __/ | | (_) \ V  V / (_) | |  | | (_| |
|_| |_|\___|_|_|\___/ \_/\_/ \___/|_|  |_|\__,_|                                  
 ____     __    _______    _______   __________
|     \  |  |  |  _____|  /  _____) |____  ____|
|  |\  \ |  |  | |__     (  (_____      |  |
|  | \  \|  |  |  __|     \_____  \     |  | 
|  |  \     |  | |_____    _____)  )    |  | 
|__|   \____|  |_______|  (_______/     |__| 
 
 
 


*/











namespace RadixSort {
    template <typename T>
    void sort(vector<T> &arr, const int max_element, int (*get_key)(T &), int begin = 0) {
        const int n = arr.size();
        vector<T> new_order(n);
        vector<int> count(max_element + 1, 0);

        for (int i = begin; i < n; i++)
            count[get_key(arr[i])]++;

        for (int i = 1; i <= max_element; i++)
            count[i] += count[i - 1];

        for (int i = n - 1; i >= begin; i--) {
            new_order[count[get_key(arr[i])] - (begin == 0)] = arr[i];
            count[get_key(arr[i])]--;
        }

        arr.swap(new_order);
    }

    template <typename T> void sort_pairs(vector<T> &arr, const int rank_size) {
    RadixSort::sort<T>(arr, rank_size, [](T &item) { return item.first.second; }, 0ll);
    RadixSort::sort<T>(arr, rank_size, [](T &item) { return item.first.first; }, 0ll);
    }
}
class SuffixArray {
private:
    string s;
    int n;
    typedef pair<int, int> Rank;
    void build_ranks(const vector<pair<Rank, int>> &ranks, vector<int> &ret) {
        ret[ranks[0].second] = 1;
        for (int i = 1; i < n; i++) {
            if (ranks[i - 1].first == ranks[i].first)
                ret[ranks[i].second] = ret[ranks[i - 1].second];
            else
                ret[ranks[i].second] = ret[ranks[i - 1].second] + 1;
        }
    }
    vector<int> build_suffix_array() {
        vector<pair<Rank, int>> ranks(this->n);
        vector<int> arr(this->n);

        for (int i = 0; i < n; i++)
        ranks[i] = pair<Rank, int>(Rank(s[i], 0), i);

        RadixSort::sort_pairs(ranks, 256);
        build_ranks(ranks, arr);

        {
        int jump = 1;
        int max_rank = arr[ranks.back().second];
        while (max_rank != this->n) {
            for (int i = 0; i < this->n; i++) {
            ranks[i].first.first = arr[i];
            ranks[i].first.second = (i + jump < this->n ? arr[i + jump] : 0);
            ranks[i].second = i;
            }

            RadixSort::sort_pairs(ranks, n);
            build_ranks(ranks, arr);

            max_rank = arr[ranks.back().second];
            jump *= 2;
        }
        }

        vector<int> sa(this->n);
        for (int i = 0; i < this->n; i++)
        sa[arr[i] - 1] = i;
        return sa;
    }
    vector<int> build_lcp() {
        lcp.resize(n, 0);
        vector<int> inverse_suffix(n);

        for (int i = 0; i < n; i++)
        inverse_suffix[sa[i]] = i;

        int k = 0;

        for (int i = 0; i < n; i++) {
        if (inverse_suffix[i] == n - 1) {
            k = 0;
            continue;
        }

        int j = sa[inverse_suffix[i] + 1];

        while (i + k < n && j + k < n && s[i + k] == s[j + k])
            k++;

        lcp[inverse_suffix[i]] = k;

        if (k > 0)
            k--;
        }

        return lcp;
    }
public:
    vector<int> sa;
    vector<int> lcp;
    SuffixArray(string &s) {
        this->n = s.size();
        this->s = s;
        this->sa = build_suffix_array();
        this->lcp = build_lcp();
    }
};
int n, N = 5e5+50, M = 25;
VVI sparse(M, VI(N));
string s = "", s1;
int query(int l, int r) {
    int pow = 0, len = r - l;
    while((1<<(pow+1)) <= len) pow++;
    return min(sparse[pow][l], sparse[pow][r-(1<<pow)]);
}
void go () {
    VI seg, suffix, lcp;
    VS tmp;
    int x = 0;
    for (int i = 0; i < 2; i++) {
        cin >> s1;
        s1.pb('Z' - x++);
        tmp.pb(s1);
    }
    if (tmp.empty()) {
        cout << "0\n";
        return;
    }
    if (tmp.size() == 1) {
        cout << s1.size()-1 << '\n';
        return;
    }
    int pos = 0, strCnt = tmp.size(), maxAns = tmp.back().size()-1, len;
    for (int i = 0; i < tmp.size(); i++) {
        maxAns = min(maxAns, (int)tmp[i].size()-1);
        for (auto& el : tmp[i]) s.pb(el);
        while (pos < s.size()) {
            seg.pb(i);
            pos++;
        }
    }
    len = n = s.size();
    SuffixArray sa(s);
    suffix = sa.sa;
    lcp = sa.lcp;
    for (int i = 0; i < n; i++) {
        sparse[0][i] = len - suffix[i];
        sparse[1][i] = (i+1<n ? lcp[i] : 0);
    }
    for (int j = 2; j < M; j++) 
    for (int i = 0; i < n; i++) {
        int st1 = i, st2 = min(n-1, i + (1<<(j-1)));
        sparse[j][i] = min(sparse[j-1][st1], sparse[j-1][st2]);
    }
    int l = strCnt, r = l, segCnt = 0;
    VI cnts(M, 0);
    auto add = [&](int a) {
        cnts[a]++;
        segCnt+=(cnts[a]==1);
    };
    auto remove = [&](int a) {
        cnts[a]--;
        segCnt-=(cnts[a]==0);
    };
    int ans = 0;
    while(r < n) {
        while(r < n && segCnt < strCnt) {
            add(seg[suffix[r++]]);
        }
        if (segCnt == strCnt) {
            ans = max(ans, query(l, r));
        }
        while(segCnt == strCnt) {
            remove(seg[suffix[l++]]);
            if (segCnt == strCnt) 
                ans = max(ans, query(l, r));
        }
    }
    ans = min(ans, maxAns);
    // cout << s << ": " << ans << '\n';
    cout << ans << '\n';
}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
#endif
    _upgrade
    int T = 1;
    // cin >> T;
    while(T--) go();
    return 0;
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














