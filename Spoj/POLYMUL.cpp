#include <bits/stdc++.h>
// #pragma GCC optimize("O3, unroll-loops")
// #pragma GCC target("avx2")
// #define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define ub upper_bound 
#define umap unordered_map
#define sz(x) (int)x.size()
#define chmin(a,b) a=min(a,b)
#define chmax(a,b) a=max(a,b)

using db = double;
using ld = long double;
using sint = short int;
using ll = long long;
using uint = unsigned int;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using PII = pair<int, int>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using PLL = pair<ll, ll>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}
/*

|                    _                                                     
|  __   _           | |                                                    
| |   \| | ___  ___ | |_                                                   
| | |\ | |/ _ \/ __/| __|                                                  
| | | \  |  __/\__ \| |_                                                   
| |_|  \_|\___ /___/ \__|                                                  
|                                                      _         _   _     
|                                                     (_)       | | | |    
|  _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_   
| | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|  
| | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_   
| | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|  
| | |               __/ |                                                  
| |_|              |___/                   _                               
|                                  _      (_)                              
|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                               
| | '_ ` _ \ / _` |_   / _` |/ _` | |/ _` | |                              
| | | | | | | (_|  /  / (_| | (_| | | (_| | |                              
| |_| |_| |_|\__,_|____\__,_|\__,_|_|\__,_|_|                              
*/

const db PI = acos(-1.0); //M_PI;
const ll INFF = 4e18;
const int INF = 1.07e9;
const int MOD = 1e9 + 7; // 100663319, 53, 97 good hash values
const int MOD1 = 998244353; //7*19*2^23 +1;
const int dx[] = {-1, 0, 1, 0, -1, -1, 1, 1};
const int dy[] = {0, 1, 0, -1, -1, 1, -1, 1};



namespace FFT {
    using C = complex<double>;
    const double PI = acos(-1);

    void fft(vector<C>& a, const bool& invert) {
        ll n = a.size();
        for (ll i = 1, j = 0; i < n; ++i) {
            ll bit = n >> 1;
            for (; j & bit; bit >>= 1)
                j ^= bit;
            j ^= bit;
            if (i < j)
            swap(a[i], a[j]);
        }

        for (ll len = 2; len <= n; len <<= 1) {
            const double ang = 2 * PI / len * (invert ? -1 : 1);
            C wlen(cos(ang), sin(ang));
            for (ll i = 0; i < n; i += len) {
                C w(1);
                for (ll j = 0; j < len / 2; ++j) {
                    C u = a[i+j], v = a[i+j+len/2] * w;
                    a[i+j] = u+v;
                    a[i+j+len/2] = u-v;
                    w *= wlen;
                }
            }
        }
        if (invert) { // inverse matrix has a 1/n multiplication
            for (auto& x : a) x /= n;
        }
    }

    vector<ll> multiply(const vector<ll>& a, const vector<ll>& b) {
        vector<C> fa(a.begin(),a.end()), fb(b.begin(), b.end());
        ll n = 1;
        while (n < a.size() + b.size())
            n <<= 1;
        fa.resize(n);
        fb.resize(n);

        fft(fa, false);
        fft(fb, false);

        for (ll i = 0; i < n; ++i)
            fa[i] *= fb[i];

        fft(fa, true);

        vector<ll> result(n);
        for (ll i = 0; i < n; ++i)
            result[i] = llround(fa[i].real());

        return result;
    }

    /* if it's numbers and not polynomials, we have to normalise */
    void normalise(vector<ll>& result) {
        for (ll i = 0, carry = 0; i < result.size(); ++i) {
            result[i] += carry;
            carry = result[i] / 10;
            result[i] %= 10;
        }
    }
};

const int N = 2e5 + 5;
const int M = 2e5 + 5;

int nums[N];

int n, m, k, q;
string s;

void go () {
    ll n, m; cin >> n;  m = n = n+1;
    vector<ll> a(n), b(m);
    for (auto& e : a) cin >> e;
    for (auto& e : b) cin >> e;

    vector<ll> res = FFT::multiply(a, b);
    for (ll i = 0; i < n + m - 1; ++i)
        cout << res[i] << " ";
    cout << '\n';

}

signed main () {

#ifndef ONLINE_JUDGE
    // freopen("0.in", "r", stdin);
    // freopen("0.out", "w", stdout);
#endif
    _upgrade
    int T = 1;
    cin >> T;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/
























