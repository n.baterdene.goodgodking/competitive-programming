#include <iostream>
#include <iomanip>
using namespace std;

int main() {
	int n, q = 0;
	cin >> n;	
	int num[n * n];
	for(int i = 0; i < n; i++){
		for(int j = 0; j < n; j++){
            if(i % 2 != 0)
			    num[q] = i * n + j + 1;
            else
                num[q] = i * n - j + n;
            q++;
		}
	}
	for(int i = 0; i < n * n; i++){
		cout << ' ' << setw(2) << num[i];
		if(i % n == n - 1) 
			cout <<"\n";
	}
	

	return 0;
}