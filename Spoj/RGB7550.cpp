#include <iostream>
#include <cstring>
#include <iomanip>
using namespace std;
int main () {
	int n;
	cin >> n;
	int h = n;
	n = n * 2 - 1;
	int husnegt1[h + 2][n + 2], husnegt2[h + 2][n + 2];
	memset(husnegt1, 0, sizeof(husnegt1));
	int st = h, end = h;
	for(int i = 0; i <= h + 1; i++){
		for(int j = 0; j <= n + 1; j++){
			if(i == 0 || i == h + 1 || j == 0 || j == n + 1)
				husnegt2[i][j] = 1;
		}
	}
	for(int i = 1; i <= h; i++){
		for(int j = 1; j <= n; j++){
			if(j < st || j > end)
				husnegt2[i][j] = 1;
			else {
				husnegt2[i][j] = 0;
			}
		}
		st--;
		end++;
	}
	int num = 1, y = 1, x = h, mv = 1;
	while(num <= h * h){
		husnegt1[y][x] = num;
		husnegt2[y][x] = 1;
		if(mv == 1){
			if(husnegt2[y + 1][x - 1] != 1){
				y++;
				x--;
			} else {
				x++;
				num++;
                if(num > h * h)
                    break;
				husnegt1[y][x] = num;
				husnegt2[y][x] = 1;
				mv = 2;
			}
		}
		if(mv == 2){
			if(husnegt2[y][x + 1] != 1){
				x++;
			} else {
				x--;
				y--;
				num++;
                if(num > h * h)
                    break;
				husnegt1[y][x] = num;
				husnegt2[y][x] = 1;
				mv = 3;
			}
		}
		if(mv == 3){
			if(husnegt2[y - 1][x - 1] != 1){
				y--;
				x--;
			} else {
				x--;
				num++;
                if(num > h * h)
                    break;
				husnegt1[y][x] = num;
				husnegt2[y][x] = 1;
				mv = 1;
				x--;
				y++;
			}
		}
		num++;
	}
	for(int i = 0; i <= h + 1; i++){
		for (int j = 0; j <= n + 1; j++){
			if(husnegt1[i][j] != 0)
				cout << setw(3) <<  husnegt1[i][j];
			else
				cout <<"   ";
		}
		cout << "\n";
	}
	return 0;
}