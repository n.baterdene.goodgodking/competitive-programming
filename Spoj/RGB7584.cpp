#include <bits/stdc++.h>
using namespace std;
using ll = long long;
ll solve(ll n, int digit) {
    ll ans = 0;
    for (ll i = 1; i * max(1, digit) <= n; i*=10) {
        ll lastDigits = (n % (10 * i));
        ll curDigit = n / (10 * i) * i;
        if (lastDigits >= digit * i)
            curDigit += min(lastDigits, i * (digit+1) - 1) - i * digit + 1;
        ans += curDigit;
    }
    return ans;
}
vector <ll> get(ll n) {
    vector <ll> ans(10);
    ll allDigit = 0, curDigitCount = 1;
    for (ll i = 1; i <= n; i *= 10, curDigitCount++) {
        if (i * 10 - 1 <= n) allDigit += curDigitCount * i * 9;       
        else allDigit += curDigitCount * (n - i + 1);
    }
    for (ll i = 1; i < 10; i++) {
        ans[i] = solve(n, i);
        allDigit -= ans[i];
    }
    ans[0] = allDigit;
    return ans;
}
int main() {
    long long n, m, k;
    cin >> n >> m >> k;
    vector <ll> a, b, cnts(10);
    a = get(m);
    b = get(n-1);
    for (int i = 0; i < 10; i++) cnts[i] = a[i]-b[i];
    int ans = -1;
    for (int i = 9; i >= 0; i--) {
        if (k - cnts[i] <= 0) {
            ans = i;
            break;
        }
        k -= cnts[i];
    }
    cout << ans << '\n';
    return 0;
}