#include <iostream>
#include <algorithm>
#include <cstring>
using namespace std;
int main() {
	int n, m, niit = 0;
	cin >> n >> m;
	int ornuud[n + 1][n + 1];
	memset(ornuud, 0, sizeof(ornuud));
	for(int i = 1; i <= m; i++){
		int x, y, z;
		cin >> x >> y >> z;
		ornuud[x][y] = z;
		niit += z;
	}
	for(int i = 1; i <= n; i++){
		for(int j = 1; j <= n; j++){
			for(int q = 1; q <= n; q++){
                if(ornuud[i][j] != 0 && ornuud[j][q] != 0 && ornuud[q][i] != 0){
                    int mn = min(min(ornuud[i][j], ornuud[j][q]), ornuud[q][i]);
                    niit -= 3 * mn;
                    ornuud[i][j] -= mn;
                    ornuud[j][q] -= mn;
                    ornuud[q][i] -= mn;
                }
				if(ornuud[i][j] != 0 && ornuud[j][q] != 0){
					if(ornuud[i][j] < ornuud[j][q]){
						niit -= ornuud[i][j];
						ornuud[i][q] += ornuud[i][j];
						ornuud[j][q] = ornuud[j][q] - ornuud[i][j];
						ornuud[i][j] = 0;
					} else {
                        if(ornuud[i][j] > ornuud[j][q]){
                            niit -= ornuud[j][q];
                            ornuud[i][q] += ornuud[j][q];
                            ornuud[i][j] = ornuud[i][j] - ornuud[j][q];
                            ornuud[j][q] = 0;
                        } else {
                            niit -= ornuud[i][j];
                            ornuud[i][q] = ornuud[i][j];
                            ornuud[i][j] = 0;
                            ornuud[j][q] = 0;
                        }
					}
				}
			}

		}
	}
	for(int i = 1; i <= n; i++){
		for(int j = i + 1; j <= n; j++){
            niit -= min(ornuud[i][j], ornuud[j][i]);
        }
    }
	cout << niit << "\n";

	return 0;
}