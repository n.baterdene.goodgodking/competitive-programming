#include <iostream>
#include <string>
using namespace std;
int str_to_int(char a){
    int n;
    if(a != '.'){
        n = a - '0';
        return n;
    }
    return -1;    
}
int main () {
    int n, q = 0, count[15];
    memset(count, 0, sizeof(count));
    cin >> n;
    n *= 2;
    string toonuud[4];
    for(int i = 0; i < 4; i++){
        cin >> toonuud[i];
    }
    char too[16];
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++){
            too[q] = toonuud[i][j];
            q++;
        }
    }
    int numbers[16];
    for(int i = 0; i < 16; i++){
        numbers[i] = str_to_int( too[i] );
    }
    for(int i = 0; i < 16; i++){
        if(numbers[i] == -1){
            count[0]++;
        } else {
            count[numbers[i]]++;
        }
    }

    for(int i = 1; i <= 10; i++){
        if(count[i] > n){
            cout << "NO"<< "\n";
            return 0;
        }
    }
    cout << "YES" << "\n";

    return 0;   
}