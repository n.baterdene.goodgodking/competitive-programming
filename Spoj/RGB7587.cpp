#include <iostream>
#include <cstring>
using namespace std;
int main () {
    int n, k, ans = 0;
    cin >> n >> k;
    int times = n / k;
    int massives[times][k];
    int count1[k], count0[k];
    memset(count1, 0, sizeof(count1));
    memset(count0, 0, sizeof(count0));
    for(int i = 0; i < times; i++){
        for(int j = 0; j < k; j++){
            cin >> massives[i][j];
            if(massives[i][j] == 1)
                count1[j] += 1;
        }
    }
    for(int i = 0; i < k; i++){
        count0[i] = times - count1[i];
        if(count0[i] < count1[i])
            ans += count0[i];
        else
            ans += count1[i];
    }
    cout << ans << "\n";



    return 0;
}