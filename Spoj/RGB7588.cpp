#include <iostream>
using namespace std;
int main () {
    int n, ones = 0, mx = 0;
    cin >> n;
    char num[n];
    for(int i = 0; i < n; i++){
        cin >> num[i];
        if(num[i] == '1')
            ones++;
    }
    for(int i = 0; i < n; i++){
        int ans = 0;
        if(num[i] == '0'){
            for(int j = i; j < n ; j++){
                if(num[j] == '0'){
                    ans++;
                } else {
                    ans--;
                }
                if(mx < ans)
                    mx = ans;
            }
            
        }
    }
    if(ones == n){
        cout << n - 1 << "\n";
        return 0;
    }
    cout << ones + mx << "\n";
    return 0;
}