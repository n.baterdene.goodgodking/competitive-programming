#include <iostream>
#include <algorithm>
using namespace std;
#define mp make_pair
bool myfunc(pair<int , int > x, pair <int , int > y){
    if(x.first < y.first)
        return true;
    return false;    
}
int main () {
    int power, dragons, n, m;
    cin >> power >> dragons;
    pair<int, int> no[dragons];
    for(int i = 0; i < dragons; i++){
        cin >> n >> m;
        no[i] = mp(n, m);
    }
    sort(no, no + dragons, myfunc);
    for(int i = 0; i < dragons; i++){
        //cout << no[i].first << " luu ";
        if(no[i].first >= power){
            cout << "NO"<<  endl;
            return 0;
        }
        power += no[i].second;
    }
    cout << "YES"<< endl;



    return 0;
}