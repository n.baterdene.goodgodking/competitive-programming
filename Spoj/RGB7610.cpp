
#include <iostream>
#include <cstring>

#define int long long

using namespace std;

const int MAXN = 60;

int dp[MAXN][3];
int n;

int solve(int idx, int bef) {
	if (idx == n) return 1;
	if (dp[idx][bef] != -1) return dp[idx][bef];
    int ans = 0;
	if (bef == 0) ans += solve(idx + 1, 1);
	ans += solve(idx + 1, 0);
	return dp[idx][bef] = ans;
}

signed main() {
    cin >> n;
    memset(dp, -1, sizeof dp);
    cout << solve(0, 0) << "\n";
    return 0;
}   