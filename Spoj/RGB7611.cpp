#include <iostream>
#include <cstring>
using namespace std;                
int dp[100][3];
int n;
int solve(int i, int bef){
    /*
    0 00
    1 01 
    2 11
    0101000010
       */
    if(i == n)
        return 1;
    if(dp[i][bef] != -1)
        return dp[i][bef];
        int ans = 0;
    if(bef == 0){
        ans += solve(i + 1, 1);
    }
    if(bef == 1)
        ans += solve(i + 1, 2);
    ans += solve(i + 1, 0);
    return dp[i][bef] = ans;
}
int main () {
    cin >> n;
    memset(dp, -1, sizeof(dp));
    cout << solve(0, 0) << '\n' ;
    return 0;
}