#include <iostream>
#include <vector>
#include <cmath>

using namespace std;
int main () {
    int n, tmp;
    cin >> n;
    vector <int> boys, girls;
    for(int i = 0; i < n; i ++){
        cin >> tmp;
        boys.push_back(tmp);
    }
    cin >> n;
    for(int i = 0; i < n; i ++){
        cin >> tmp;
        girls.push_back(tmp);
    }
    int ans = 0;
    sort(boys.begin(), boys.end());
    sort(girls.begin(), girls.end());

    while(girls.size() != 0 && boys.size() != 0){
        if(abs(girls.back() - boys.back()) <= 1){
            ans++;
            girls.pop_back();
            boys.pop_back();
            
        } else {
            if(boys.back() > girls.back())
                boys.pop_back();
            else  
                girls.pop_back();
        }
    }
    cout << ans << '\n';
    return 0;
}