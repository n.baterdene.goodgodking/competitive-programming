#include <iostream>
using namespace std;
int main () {
    int a, b, m, n;
    cin >> a >> b;
    int num[a + 1];
    for(int i = 1; i <= a; i++)
        num[i] = i;
    while(cin >> n >> m){
        swap(num[n], num[m]);
    }
    for(int i = 1; i <= a; i++)
        cout << num[i] << ' ';
    cout << "\n";

    return 0;
}