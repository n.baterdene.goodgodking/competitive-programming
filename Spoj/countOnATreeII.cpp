#include <bits/stdc++.h>
// #define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define sz(x) (int)x.size()

using ll = long long;
using db = double;
using ld = long double;
using uint = unsigned;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VS = vector<string>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using VD = vector<db>;
using PII = pair<int, int>;
using PLL = pair<ll, ll>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int bitCnt(int a) {
    bitset <64> b(a);
    return b.count();
}
int bitCnt(string a) {
    bitset <64> b(a);
    return b.count();
}

int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}

void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VPI& a) {
    for (auto el : a) {
        cout << el.ff << ',' << el.ss << ' ';
    }
    cout << '\n';
}

void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VVI& a) {
    for (auto el : a) {
        print(el);
    }
}

/*

|                    _                                                     
|  __   _           | |                                                    
| |   \| | ___  ___ | |_                                                   
| | |\ | |/ _ \/ __/| __|                                                  
| | | \  |  __/\__ \| |_                                                   
| |_|  \_|\___ /___/ \__|                                                  
|                                                      _         _   _     
|                                                     (_)       | | | |    
|  _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_   
| | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|  
| | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_   
| | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|  
| | |               __/ |                                                  
| |_|              |___/                   _                               
|                                  _      (_)                              
|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                               
| | '_ ` _ \ / _` |_   / _` |/ _` | |/ _` | |                              
| | | | | | | (_|  /  / (_| | (_| | | (_| | |                              
| |_| |_| |_|\__,_|____\__,_|\__,_|_|\__,_|_|                              
*/

const db PI = acos(-1.0); //M_PI;
const ll INFF = 4e18;
const int INF = 1.07e9;
const int MOD = 1e9 + 7;
const int MOD1 = 998244353; //7*17*2^23 +1;
const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0, 1, 0, -1};





const int N = 4e4 + 5;
const int M = 1e5 + 5;
const int K = 16;
const int BLOCK = 200;

int nums[N], ans[M], ed[N], st[N], cnt[N], cntID[N], lvl[N];
vector <int> paths[N];
int jump[N][K];
int LCA(int a, int b) {
	if (lvl[a] < lvl[b]) swap(a, b);
	int dif = lvl[a] - lvl[b];
	for (int i = 0; i < K; i++) 
		if (dif & (1<<i)) a = jump[a][i];

	if (a == b) return a;
	for (int i = K-1; i >= 0; i--) {
		if (jump[a][i] != jump[b][i]) {
			a = jump[a][i];
			b = jump[b][i];
		}
	}
	return jump[a][0];
}
struct Query {
	int l, r, id, L, lca;
	bool operator < (const Query& tmp) const {
		return L == tmp.L ? (L & 1 ? r > tmp.r : r < tmp.r) : (L < tmp.L);
	}
};
int n, m, k, curTime, curAns;
vector <int> vals;
void dfs(int pos, int par = 1) {
	jump[pos][0] = par;
	lvl[pos] = lvl[par] + 1;
	vals.pb(pos);
	st[pos] = curTime++;
	for (auto& el : paths[pos]) {
		if (el == par) continue;
		dfs(el, pos);
	}
	vals.pb(pos);
	ed[pos] = curTime++;
}
void add(int val) {
	cntID[val]^=1;
	int curChange = (cntID[val] == 1 ? 1 : -1);
	cnt[nums[val]] += curChange;
	curAns += (cnt[nums[val]] == 1 && curChange > 0);
	curAns -= (cnt[nums[val]] == 0);
}
void go () {
    cin >> n >> m;
    for (int i = 1; i <= n; i++) cin >> nums[i];
   	if (m == 0) return;
    { // compress
    	set <int> vals;
    	for (int i = 1; i <= n; i++) vals.insert(nums[i]);
    	int cur = 0;
    	map <int, int> comp;
    	for (auto& el : vals) comp[el] = cur++;
    	for (int i = 1; i <= n; i++) nums[i] = comp[nums[i]];
    }
   	for (int i = 1; i < n; i++) {
   		int a, b; cin >> a >> b;
   		paths[a].pb(b);
   		paths[b].pb(a);
   	}
   	dfs(1);
   	for (int j = 1; j < K; j++)
   	for (int i = 1; i <= n; i++) 
   		jump[i][j] = jump[jump[i][j-1]][j-1];

	vector <Query> queries;
   	for (int i = 1; i <= m; i++) {
   		int a, b; cin >> a >> b;
   		int lca = LCA(a, b);
   		if (st[a] > st[b]) swap(a, b);
   		if (lca == a || lca == b) {
   			queries.pb({st[a], st[b], i, st[a]/BLOCK, -1});
   		} else {
   			queries.pb({ed[a], st[b], i, st[a]/BLOCK, lca});
   		}
   	}
   		
   	sort(ALL(queries));
   	int l = queries[0].l, r = l - 1;
   	for (auto& query : queries) {
   		while (l < query.l) add(vals[l++]);
   		while (r > query.r) add(vals[r--]);
   		while (l > query.l) add(vals[--l]);
   		while (r < query.r) add(vals[++r]);
   		ans[query.id] = curAns;
   		if (query.lca != -1) ans[query.id] += cnt[nums[query.lca]] == 0;
   	}
   	for (int i = 1; i <= m; i++) cout << ans[i] << '\n';
}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("0.in", "r", stdin);
    freopen("0.out", "w", stdout);
#endif
    _upgrade
    int T = 1;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














