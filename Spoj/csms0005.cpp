#include <bits/stdc++.h>
#define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define sz(x) (int)x.size()

using ll = long long;
using db = double;
using ld = long double;
using uint = unsigned;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VS = vector<string>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VVVVB = vector<VVVB>;
using VVVVVB = vector<VVVVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using VVVLL = vector<VVLL>;
using VVVVLL = vector<VVVLL>;
using VD = vector<db>;
using PII = pair<int, int>;
using PDD = pair<db, db>;
using PLL = pair<ll, ll>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
using VVVVI = vector<VVVI>;
using VVVVVI = vector<VVVVI>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int bitCnt(int a) {
    bitset <64> b(a);
    return b.count();
}
int bitCnt(string a) {
    bitset <64> b(a);
    return b.count();
}
VI readVI(int n) {
    VI a(n);
    REP(i, n) cin >> a[i];
    return a;
}
int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}

void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VPI& a) {
    for (auto el : a) {
        cout << el.ff << ',' << el.ss << ' ';
    }
    cout << '\n';
}

void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VVI& a) {
    for (auto el : a) {
        print(el);
    }
}
const int MOD = 1e9 + 7;
const int MOD1 = 998244353;//7*17*2^23 +1;
const int INF = 1.07e9;
const ll INFF = INT64_MAX;
const db EPS = 1e-9;
const db PI = acos(-1.0); //M_PI;
const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0, 1, 0, -1};
/*
|      ⣠⣶⡾⠏⠉⠙⠳⢦⡀⠀  ⠀⢠⠞⠉⠙⠲⡀
|    ⣴⠿⠏⠀⠀⠀⠀⠀⠀⢳⡀⠀  ⡏⠀⠀ ⠀⠀ ⢷
|⠀⠀⢠⣟⣋⡀⢀⣀⣀⡀⠀⣀⡀⣧⠀  ⢸⠀⠀⠀⠀   ⡇ ⠀
| ⠀⢸⣯⡭⠁⠸⣛⣟⠆⡴⣻⡲⣿   ⣸⠀⠀OK.  ⡇
|⠀⠀⣟⣿⡭⠀⠀⠀⠀⠀⢱⠀⠀⣿   ⢹⠀⠀⠀⠀⠀ ⡇
|⠀⠀⠙⢿⣯⠄⠀⠀⠀⢀⡀⠀⠀⡿⠀  ⡇⠀⠀ ⠀ ⡼
|⠀⠀⠀⠀⠹⣶⠆⠀⠀⠀⠀⠀⡴⠃ ⠀ ⠘⠤⣄⣠⠞⠀ ⠀
|⠀⠀⠀⠀⢸⣷⡦⢤⡤⢤⣞⣁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ ⠀
|⠀⢀⣤⣴⣿⣏⠁⠀⠀⠸⣏⢯⣷⣖⣦⡀⠀⠀⠀⠀⠀⠀
|⢀⣾⣽⣿⣿⣿⣿⠛⢲⣶⣾⢉⡷⣿⣿⠵⣿⠀⠀⠀⠀⠀
|⣼⣿⠍⠉⣿⡭⠉⠙⢺⣇⣼⡏⠀ ⠀⣄⢸⣿
|⣿⣿⣧⣀⣿.........⣀⣰⣏⣘⣆⣀
      
|                                                    _         _   _
|                                                   (_)       | | | |
 _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_
| '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|
| |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_
| .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|
| |               __/ |
|_|              |___/
 _   _      _ _    __        __         _     _ 
| | | | ___| | | __\ \      / /__  _ __| | __| |
| |_| |/ _ \ | |/ _ \ \ /\ / / _ \| '__| |/ _` |
|  _  |  __/ | | (_) \ V  V / (_) | |  | | (_| |
|_| |_|\___|_|_|\___/ \_/\_/ \___/|_|  |_|\__,_|                                  
 ____     __    _______    _______   __________
|     \  |  |  |  _____|  /  _____) |____  ____|
|  |\  \ |  |  | |__     (  (_____      |  |
|  | \  \|  |  |  __|     \_____  \     |  | 
|  |  \     |  | |_____    _____)  )    |  | 
|__|   \____|  |_______|  (_______/     |__| 
 
 
 


*/







const int N = 5e4 + 5;
const int M = 2 * N;

int ans[N], nx[M], pv[M];
map <int, PII> comp;
map <int, int> pmoc;

int n, m, k, p;
string s;
int st = 0, ed = M-1;
void print() {
    int x = nx[st];
    while(x != ed) {
        cout << comp[x].ff << ',' << comp[x].ss << ' ';
        x = nx[x];
    }
    cout << '\n';
}
void go () {
    // input
    VPI L;
    set <PII> P;
    set <int> ids;
    vector <int> pos;
    vector <PII> ops, nums;
    {
        cin >> n;
        for (int i = 1; i <= n; i++) {
            int a, b; cin >> a >> b;
            ops.pb({a, b});
            ids.insert(a);
            ids.insert(b);
        }
        ids.insert(1);
        ids.insert(1e9);
        cin >> m;
        for (int i = 1; i <= m; i++) {
            int a; char c; cin >> c >> a;
            if (c == 'L') L.pb({a, i});
            else P.insert({a, i});
        }
        sort(ALL(L));
    }
    // compress
    {
        p = 1;
        for (auto& el : ids) pos.pb(el);
        pmoc[pos[0]] = p;
        comp[p++] = {pos[0], pos[0]};
        // return;
        for (int i = 1; i < pos.size(); i++) {
            int l = pos[i-1] + 1;
            int r = pos[i] - 1;
            if (l <= r) comp[p++] = {l, r};
            pmoc[pos[i]] = p;
            comp[p++] = {pos[i], pos[i]};
        }
    }
    // generate linked list & do the operations
    {
        nx[st] = 1;
        pv[ed] = p-1;
        for (int i = 1; i < p; i++) {
            nx[i] = (i == p-1 ? ed : i+1);
            pv[i] = i-1;
        }
        // print();
        for (auto& [a, b] : ops) {
            if (a == b) continue;
            a = pmoc[a];
            b = pmoc[b];
            int l = pv[a], r = nx[a];
            nx[l] = r, pv[r] = l;
            r = b;
            l = pv[r];
            nx[l] = a, pv[a] = l;
            pv[r] = a, nx[a] = r;
            // print();
        }
        int x = nx[st];
        while(x != ed) {
            nums.pb({comp[x]});
            x = nx[x];
        }
    }
    int lp = 0, ln = L.size(), pre = 0;
    n = nums.size();
    for (auto& [l, r] : nums) {
        int len = r - l + 1;
        // P:  Position of person with id: x
        while (!P.empty()) {
            auto it = P.lb({l, 0});
            if (it == P.end() || it->ff > r) break;
            int x, id; tie(x, id) = *it;
            P.erase(it);
            ans[id] = pre+x-l+1;
        }
        // L: id of the x-th person in the queue
        pre += len;
        while (lp < ln && L[lp].ff <= pre) {
            ans[L[lp].ss] = r - (pre - L[lp].ff);
            lp++;
        }
    }
    for (int i = 1; i <= m; i++) cout << ans[i] << "\n";
}



signed main () {

#ifndef ONLINE_JUDGE
    // freopen("in.txt", "r", stdin);
    // freopen("out.txt", "w", stdout);
#endif
    _upgrade
    int T = 1;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














