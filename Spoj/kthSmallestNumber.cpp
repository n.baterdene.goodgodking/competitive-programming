
#include <bits/stdc++.h>
using namespace std;

#define pb push_back
#define ff first
#define ss second
#define mp make_pair
#define memo(a,b) memset(a,b,sizeof(a))
#define INF 1e9
#define EPS 1e-8
#define PI 3.14159265358979323846

typedef long long ll ;
typedef unsigned long long ull ;

const int N = 5e6;
unsigned a , b , mod , k ;
vector <unsigned> A(N);
int main()
{
    // freopen("in.txt","r",stdin);
    // freopen("out.txt","w",stdout);
    cin >> a >> b >> mod >> k ;

    for( int i=0 ; i<N ; i++ )
    {
        a = 31014 * (a & 65535) + (a >> 16);
        b = 17508 * (b & 65535) + (b >> 16);
        A[i] = ((a << 16) + b) % mod;
    }

    nth_element(A.begin(), A.begin()+(k-1), A.end());
    
    cout << A[k-1] << endl ;

    return 0;
}