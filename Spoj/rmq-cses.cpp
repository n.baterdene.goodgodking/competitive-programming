#include<iostream>
using namespace std;

const int N = 200005;

int n, queries, a[N], tree[1<<19];

void build(int l, int r, int x){
    if( l == r ){
        tree[x] = a[l];
        return;
    }


    int mid = (l+r) >> 1;
    build( l, mid, x*2 );
    build( mid+1, r, x*2+1);

    tree[x] = min( tree[x*2], tree[x*2+1] );
}

int query(int l, int r, int p, int q, int x){
    if(r < p || l > q) return 0;
    if(p < l && r < q) return tree[x];
    if(l == r) return tree[x];
    int mid = l + (r - l) / 2;
    return min(
            query(l, mid, p, q, x * 2),
            query(mid + 1, r, p, q, x * 2 + 1)
        );
}

int main(){
    scanf("%d %d", &n, &queries);
    for(int i = 0 ; i < n ; i++){
        scanf("%d", &a[i]);
    }
    build(0, n-1, 1);
    int p, q;
    for(int i = 0; i < queries; i++){
        cin >> p >> q;
        cout << query(1, n, p, q, 1) << '\n';
    }   
    return 0;
}