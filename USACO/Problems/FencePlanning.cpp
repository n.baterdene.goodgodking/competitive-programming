#include <bits/stdc++.h>
// #define int long long
using namespace std;
// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))

#define _upgrade ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define rs resize
#define ins insert 
#define fr front() 
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define umap unordered_map
#define sz(x) (int)x.size()

typedef long long ll;
typedef double db;
typedef unsigned uint;
typedef unsigned long long ull;
typedef unordered_map<int, int> umapII;
// PQ going up <int, VI, greater<int> >
typedef vector<int> VI;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<bool> VB;
typedef vector<VB> VVB;
typedef vector<VVB> VVVB;
typedef vector<ll> VLL;
typedef vector<VLL> VVLL;
typedef vector<VVLL> VVVLL;
typedef vector<VVVLL> VVVVLL;
typedef vector<db> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef pair<int, int> PII;
typedef pair<db, db> PDD;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VPI> VVPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
typedef vector<VVVI> VVVVI;
typedef vector<VVVVI> VVVVVI;

const int MOD = 1e9 + 7;
const int MOD1 = 998244353;//119*2^23 +1;
const int INF = 2e8;
int n, m;
const int N = 1e5+5;
VPI cows;
VI par(N);
VVI ans(N, {-INF, INF, -INF, INF});


signed main () {
    freopen("fenceplan.in", "r", stdin);
    freopen("fenceplan.out", "w", stdout);

    function <int (int)> find = [&](int a){
        if (par[a] == a) return a;
        return par[a] = find(par[a]);
    };
    auto merge = [&](int a, int b) {
        a = find(a), b = find(b);
        if (a == b) return;
        par[b] = a;
    };
    auto update = [&](int a, int x, int y) {
        ans[a][0] = max(ans[a][0], x);
        ans[a][1] = min(ans[a][1], x);
        ans[a][2] = max(ans[a][2], y);
        ans[a][3] = min(ans[a][3], y);
    };
    auto val = [&](int a) {
        if (ans[a][0] == -INF) return (int)2e9;
        return 2 * (ans[a][0] - ans[a][1] + ans[a][2] - ans[a][3]);
    };
    cin >> n >> m;
    for (int i = 0; i < n; i++) {
        int a, b;
        par[i] = i;
        cin >> a >> b;
        cows.pb({a, b});
    }
    for (int i = 0; i < m; i++) {
        int a, b;
        cin >> a >> b;
        merge(a-1, b-1);
    }
    int answ = 2e9;
    for (int i = 0; i < n; i++) {
        int a = find(i);
        update(a, cows[i].ff, cows[i].ss);
    }
    for (int i = 0; i < n; i++) {
        answ = min(answ, val(i));
    }
    cout << answ << '\n';
    return 0;
}