#include <bits/stdc++.h>
// #pragma GCC optimize("O3, unroll-loops")
// #pragma GCC target("avx2")
// #define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define sz(x) (int)x.size()

using ll = long long;
using db = double;
using ld = long double;
using uint = unsigned;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VS = vector<string>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using VD = vector<db>;
using PII = pair<int, int>;
using PLL = pair<ll, ll>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int bitCnt(int a) {
    bitset <64> b(a);
    return b.count();
}
int bitCnt(string a) {
    bitset <64> b(a);
    return b.count();
}

int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}

void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VPI& a) {
    for (auto el : a) {
        cout << el.ff << ',' << el.ss << ' ';
    }
    cout << '\n';
}

void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VVI& a) {
    for (auto el : a) {
        print(el);
    }
}

/*

|                    _                                                     
|  __   _           | |                                                    
| |   \| | ___  ___ | |_                                                   
| | |\ | |/ _ \/ __/| __|                                                  
| | | \  |  __/\__ \| |_                                                   
| |_|  \_|\___ /___/ \__|                                                  
|                                                      _         _   _     
|                                                     (_)       | | | |    
|  _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_   
| | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|  
| | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_   
| | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|  
| | |               __/ |                                                  
| |_|              |___/                   _                               
|                                  _      (_)                              
|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                               
| | '_ ` _ \ / _` |_   / _` |/ _` | |/ _` | |                              
| | | | | | | (_|  /  / (_| | (_| | | (_| | |                              
| |_| |_| |_|\__,_|____\__,_|\__,_|_|\__,_|_|                              
*/

const db PI = acos(-1.0); //M_PI;
const ll INFF = 4e18;
const int INF = 1.07e9;
const int MOD = 1e9 + 7;
const int MOD1 = 998244353; //7*19*2^23 +1;
const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0, 1, 0, -1};




const int N = 2e3 + 5;
const int M = 2e5 + 5;

int nums[N];

int n, m, k, q;
string s1, s2;
ll dp[N][N];
int val(char x) {
    if (x == '+') return 1;
    return 2;
}
ll solve(int i, int j) {
    if (i == n || j == m) {
        // cout << i << "," << j << ": " << 1 << " with return 0" << '\n';
        return 1;
    }
    ll& res = dp[i][j];
    if (res != -1) {
        // cout << i << "," << j << ": " << res << " with return 1" << '\n';
        return res;
    }
    res = 0;
    if (val(s1[i]) == val(s2[j])) {
        int i1 = i; while(i1+1 < n && val(s1[i1+1]) == val(s1[i1])) i1++;
        int j1 = j; while(j1+1 < m && val(s2[j1+1]) == val(s2[j1])) j1++;
        if (i1 == n-1 && j1 == m-1) {
            // cout << i << "," << j << ": " << res << " with return 2" << '\n';
            return res = 1;
        }
        res = solve(i1+1, j1+1);
        for (int st = i; st <= i1; st++) 
            res = (res + solve(st, j1+2)) % MOD;
        for (int st = j; st <= j1; st++) 
            res = (res + solve(i1+2, st)) % MOD;
        
        // cout << i << "," << j << ": " << res << " with return 3" << '\n';
        return res;
    }
    // cout << i << "," << j << ": " << res << " with return 4" << '\n';
    return res = (solve(i, j+1) + solve(i+1, j)) % MOD;
}
void go () {
    s1.clear();
    s2.clear();
    int z1 = 0, z2 = 0;
    cin >> n;
    for (int i = 1; i <= n; i++) {
        char c; cin >> c;
        if (c == '0') z1 = s1.size();
        if (c != '1') s1.pb(c);
    }
    for (int i = 1; i <= n; i++) {
        char c; cin >> c;
        if (c == '0') z2 = s2.size();
        if (c != '1') s2.pb(c);
    }
    n = s1.size(), m = s2.size();
    for (int i = 0; i < n; i++)
    for (int j = 0; j < m; j++) dp[i][j] = -1;
        
    cout << solve(z1, z2) << '\n';
}



signed main () {

#ifndef ONLINE_JUDGE
    // freopen("0.in", "r", stdin);
    // freopen("0.out", "w", stdout);
#endif
    _upgrade
    int T = 1;
    cin >> T;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














