#include <bits/stdc++.h>
#define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define sz(x) (int)x.size()

using ll = long long;
using db = double;
using ld = long double;
using uint = unsigned;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VS = vector<string>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using VD = vector<db>;
using PII = pair<int, int>;
using PLL = pair<ll, ll>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int bitCnt(int a) {
    bitset <64> b(a);
    return b.count();
}
int bitCnt(string a) {
    bitset <64> b(a);
    return b.count();
}

int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}

void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VPI& a) {
    for (auto el : a) {
        cout << el.ff << ',' << el.ss << ' ';
    }
    cout << '\n';
}

void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VVI& a) {
    for (auto el : a) {
        print(el);
    }
}

/*         
|                    _                         
|  __   _           | |                         
| |   \| | ___  ___ | |_                                   
| | |\ | |/ _ \/ __/| __|                                   
| | | \  |  __/\__ \| |_                                   
| |_|  \_|\___ /___/ \__|                                   
|                                          _                                       
|                                  _      (_)                            
|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                                 
| | '_ ` _ \ / _` |_   / _` |/ _` | |/ _` | |                                  
| | | | | | | (_|  /  / (_| | (_| | | (_| | |                                 
| |_| |_| |_|\__,_|____\__,_|\__,_|_|\__,_|_|                                  
|                                                      _         _   _
|                                                     (_)       | | | |
|  _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_
| | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|
| | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_
| | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|
| | |               __/ |                                 
| |_|              |___/                                                         
*/

const db PI = acos(-1.0); //M_PI;
const ll INFF = 4e18;
const int INF = 1.07e9;
const int MOD = 1e9 + 7;
const int MOD1 = 998244353; //7*17*2^23 +1;
const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0, 1, 0, -1};





const int N = 1e5 + 5;
const int M = 2 * N;
struct Board {
    int x, y, id;
    bool jump;
    bool operator < (const Board& a) const {
        if (x != a.x) return x < a.x;
        if (y != a.y) return y < a.y;
        return jump > a.jump;
    }
};
int n, m, k, Y = 1;
int dp[N], node[M];
string s;
map <int, int> compY;
int query(int x) {
    int res = 0;
    for (; x >= 0; x = (x & (x + 1)) - 1) res = max(res, node[x]);
    return res;
}
void update(int x, int val) {
    for (; x < M; x = x | (x + 1)) node[x] = max(node[x], val);
}
void go () {
    cin >> n >> m;
    set <int> ys;
    vector <Board> boards;
    for (int i = 1; i <= m; i++) {
        int x1, y1, x2, y2; cin >> x1 >> y1 >> x2 >> y2;
        boards.pb({x1, y1, i, 1}); // st
        boards.pb({x2, y2, i, 0}); // end
        ys.insert(y1);
        ys.insert(y2);
    }
    {
        ys.insert(0);
        ys.insert(n);
        for (auto& el : ys) compY[el] = Y++;
    }
    Y--;
    sort(ALL(boards));
    for (auto& board : boards) {
        if (board.jump) {
            dp[board.id] = board.x+board.y-query(compY[board.y]);
        } else {
            update(compY[board.y], board.x+board.y-dp[board.id]);
        }
    }
    cout << n*2 - query(Y) << '\n';
}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("boards.in", "r", stdin);
    freopen("boards.out", "w", stdout);
#endif
    _upgrade
    int T = 1;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/


