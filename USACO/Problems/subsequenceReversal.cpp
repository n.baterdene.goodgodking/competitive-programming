#include <bits/stdc++.h>
// #pragma GCC optimize("O3, unroll-loops")
// #pragma GCC target("avx2")
// #define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define ub upper_bound 
#define sz(x) (int)x.size()
#define chmin(a,b) a=min(a,b)
#define chmax(a,b) a=max(a,b)

using ll = long long;
using db = double;
using ld = long double;
using uint = unsigned;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using PII = pair<int, int>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using PLL = pair<ll, ll>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}
/*

|                    _                                                     
|  __   _           | |                                                    
| |   \| | ___  ___ | |_                                                   
| | |\ | |/ _ \/ __/| __|                                                  
| | | \  |  __/\__ \| |_                                                   
| |_|  \_|\___ /___/ \__|                                                  
|                                                      _         _   _     
|                                                     (_)       | | | |    
|  _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_   
| | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|  
| | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_   
| | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|  
| | |               __/ |                                                  
| |_|              |___/                   _                               
|                                  _      (_)                              
|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                               
| | '_ ` _ \ / _` |_   / _` |/ _` | |/ _` | |                              
| | | | | | | (_|  /  / (_| | (_| | | (_| | |                              
| |_| |_| |_|\__,_|____\__,_|\__,_|_|\__,_|_|                              
*/

const db PI = acos(-1.0); //M_PI;
const ll INFF = 4e18;
const int INF = 1.07e9;
const int MOD = 1e9 + 7;
const int MOD1 = 998244353; //7*19*2^23 +1;
const int dx[] = {-1, 0, 1, 0, -1, -1, 1, 1};
const int dy[] = {0, 1, 0, -1, -1, 1, -1, 1};




const int N = 50;
const int M = 52;

int nums[M];
int dp[M][M][M][M]; // l, r, minimumNumber, maximumNumber
int n, m, k, q;
string s;
void go () {
    cin >> n;
    for (int i = 1; i <= n; i++) {
        cin >> nums[i];
        dp[i][i][nums[i]][nums[i]] = 1;
    }
    for (int i = 1; i < n; i++) {
        int a = nums[i];
        int b = nums[i+1];
        if (a > b) swap(a, b);
        dp[i][i+1][a][b] = 2;
    }
    nums[0] = nums[n+1] = N+1;

    for (int d1 = 0; d1 < n; d1++)
    for (int l = 1, r = l+d1; r <= n; l++, r++) {
        for (int d = 0; d < N; d++) 
        for (int mn=1, mx=mn+d; mx <= N; mn++, mx++) {
            chmax(dp[l][r][mn][mx], dp[l][r][mn+1][mx]);
            chmax(dp[l][r][mn][mx], dp[l][r][mn][mx-1]);
        }
        int a = nums[l-1], b = nums[r+1];
        for (int d = 0; d < N; d++) 
        for (int mn=1, mx=mn+d; mx <= N; mn++, mx++) {
            chmax(dp[l-1][r][mn][mx], dp[l][r][mn][mx]);
            chmax(dp[l][r+1][mn][mx], dp[l][r][mn][mx]);
            chmax(dp[l-1][r][min(a, mn)][mx], dp[l][r][mn][mx]+(a <= mn?1:0));
            chmax(dp[l][r+1][mn][max(b, mx)], dp[l][r][mn][mx]+(mx <= b?1:0));
        }
        if (l > 0 && r < n) {
            swap(a, b);
            for (int d = 0; d < N; d++) 
            for (int mn=1, mx=mn+d; mx <= N; mn++, mx++) 
                chmax(dp[l-1][r+1][min(a, mn)][max(b, mx)], dp[l][r][mn][mx]+(a <= mn?1:0)+(mx <= b?1:0));
        }
        
    }
    cout << dp[1][n][1][N] << '\n';
}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("subrev.in", "r", stdin);
    freopen("subrev.out", "w", stdout);
#endif
    _upgrade
    int T = 1;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














