/*
ID: n.bater1
TASK: wormhole
LANG: C++14                 
*/
#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
using namespace std;
int n, ans = 0, m;
vector <pair <int, int> > portals;
map <pair <int, int>, int> idx; 
map <int, vector <int> > lines;
int checkNext(pair <int, int> spaceShip, vector <int>& cur) {
    vector <int> curLine = lines[spaceShip.first];
    sort (curLine.begin(), curLine.end());
    for (int i = 0; i < curLine.size(); i++) 
        if (curLine[i] > spaceShip.second) 
            return idx[mp(spaceShip.first, curLine[i])];
    return -1;
}
void check(vector <int>& cur) {
    for (int i = 0; i < n; i++) {
        int st = i;
        map <int, bool> used;
        while(st != -1) {
            if(used[st] == true) {
                ans++;
                return;
            }
            used[st] = true;
            st = checkNext(portals[cur[st]], cur);
        }
    }
}
void print(vector <int> cur) {
    for (int i = 0; i < cur.size(); i++) 
        cout << cur[i] << ' ';
    cout << '\n';
}
void choose(int used, vector <int>& cur) {
    if (used == n) {
        // print(cur);
        return check(cur);
    }
    int st = 0;
    while (cur[st] != -1) st++;
    for (int i = st + 1; i < n; i++) 
        if (cur[i] == -1) {
            cur[st] = i, cur[i] = st;
            choose(used + 2, cur);
            cur[st] = -1, cur[i] = -1;
        }
}
int main () {
    ofstream out ("wormhole.out");
    ifstream in ("wormhole.in");
    in >> n;
    int x, y;
    for (int i = 0; i < n; i++) {
        in >> x >> y;
        portals.pb(mp(y, x));
        idx[mp(y, x)] = i;
        lines[y].push_back(x);
    }
    map <int, vector <int> > :: iterator it;
    for (auto el : lines) 
        el.second.pb(-1);
    
    vector <int> cur(n, -1);
    choose(0, cur);
    out << ans << '\n';
    return 0;   
}