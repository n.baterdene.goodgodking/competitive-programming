/*
ID: n.bater1
TASK: ariprog
LANG: C++14                 
*/
#include <bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define maxVal 125001
using namespace std;
vector <int> numbers;
vector <bool> seen(maxVal, false);
vector <pair <int, int> > answers;
int n, m;   
void check(int a, int b) {
    int firstA = a;
    for (int i = 0; i < n; i++) {
        if (!seen[a]) return;
        a += b;
    } 
    answers.pb(mp(b, firstA));
}
// vector< pair<int, int> > ans(vector<pair<int, int> > &collection, vector<int> &path, int diff){

//     if (path.size() == m){
//         coll
//     }

// }
int main () {
    ifstream in ("ariprog.in");
    ofstream out ("ariprog.out");
    in >> n >> m;
    for (int i = 0; i <= m; i++) {
        for (int j = i; j <= m; j++) {
            int tmp = i * i + j * j;
            if (!seen[tmp]) numbers.pb(tmp);
            seen[tmp] = true;
        }
    }
    
    int maxB = m * m * 2 / (n - 1);
    sort(numbers.begin(), numbers.end());
    for (int j = 1; j <= maxB; j++) 
        for (int i = 0; i <= numbers.size() - n; i++) 
            check(numbers[i], j);
 
    if (answers.size() == 0) {
        out << "NONE\n";
        return 0;
    }
    // sort(answers.begin(), answers.end());
    for (int i = 0; i < answers.size(); i++) 
        out << answers[i].second << ' ' << answers[i].first << '\n';

    return 0;   
}