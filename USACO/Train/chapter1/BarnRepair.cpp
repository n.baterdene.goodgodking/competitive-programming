/*
ID: n.bater1
TASK: barn1
LANG: C++14                 
*/
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>

#define pb push_back
#define mp make_pair
using namespace std;
int main () {
    ifstream in ("barn1.in");
    ofstream out ("barn1.out");
    int board, n, m, tmp;
    in >> board >> n >> m;
    vector <int> broken(m);
    for (int i = 0; i < m; i++) 
        in >> broken[i];

    sort (broken.begin(), broken.end());
    vector <pair <int, int> > dif;
    for (int i = 0; i < broken.size() - 1; i++) 
        dif.pb(mp(broken[i + 1] - broken[i] + 1, i));


    vector <int> ends;
    int size = dif.size();
    sort(dif.begin(), dif.end());
    for (int i = 0; i < board - 1; i++) {
        int idx = size - i - 1;
        if (idx >= 0) ends.pb(dif[idx].second);
    }
    sort(ends.begin(), ends.end());
    ends.pb(broken.size() - 1);
    int st = 0, ans = 0;
    for (int i = 0; i < ends.size(); i++) {
        int cur = broken[ends[i]] - broken[st] + 1;
        ans += cur;
        st = ends[i] + 1;
    }
    out << ans << '\n';
    return 0;   
}