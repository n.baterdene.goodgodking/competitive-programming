/*
ID: n.bater1
TASK: beads
LANG: C++14                 
*/
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <unordered_map>

using namespace std;
int main () {
    ofstream out ("beads.out");
    ifstream in ("beads.in");
    int n, max = 0, current, state, i, j;
    string s;
    char c;
    in >> n >> s;
    s += s;
    for (i = 0; i < n; i++) {
        c = (char) s[i];
        if (c == 'w') state = 0;
        else state = 1;
        j = i;
        current = 0;
        while (state <= 2) {
            while (j < n + i && (s[j] == c || s[j] == 'w')) {
                current++;
                j++;
            }
            state++;
            c = s[j];
        }
        if (current > max) max = current;
    }
    out << max << '\n';
    return 0;   
}