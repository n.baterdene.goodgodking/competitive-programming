/*
 ID: n.bater.1
 PROG: combo
 LANG: C++11
 */


#include <algorithm>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>
#include <vector>
#include <set>

using namespace std;

bool close (int a, int b, int n) {
    if (abs (a - b) <= 2) return true;
    if (abs (a - b) >= n - 2) return true;
    return false;
}

bool close_enough (int a1, int a2, int a3, int b1, int b2, int b3, int n) {
    return close (a1, b1, n) && close (a2, b2, n) && close (a3, b3, n);
}

int main() {
    ofstream out("combo.out");
    ifstream in("combo.in");
    int n;
    in >> n;
    vector <int> key(3), master(3);
    for (int i = 0; i < 3; i++) 
        in >> key[i];

    for (int i = 0; i < 3; i++) 
        in >> master[i];

    // what I did originally
    set <vector <int> > s;
    for (int i = -2; i <= 2; i++) {
        for (int j = -2; j <= 2; j++) {
            for (int k = -2; k <= 2; k++) {
                int a = (key[0] + i) % n, b = (key[1] + j) % n, c = (key[2] + k) % n;
                if (a <= 0) a += n;
                if (b <= 0) b += n; 
                if (c <= 0) c += n;
                vector <int> tmp;
                tmp.push_back(a);
                tmp.push_back(b);
                tmp.push_back(c);
                s.insert(tmp);

                a = (master[0] + i) % n, b = (master[1] +j ) % n, c = (master[2] + k) % n;
                if (a <= 0) a += n; 
                if (b <= 0) b += n; 
                if (c <= 0) c += n; 
                vector <int> tmp1;
                tmp1.push_back(a);
                tmp1.push_back(b);
                tmp1.push_back(c);
                s.insert(tmp1);
            }
        }
    }

    // simpler idea is, official solution
    int total = 0;
    for (int n1 = 1; n1 <= n; n1++) {
        for (int n2 = 1; n2 <= n; n2++) {
            for (int n3 = 1; n3 <= n; n3++) {
                if (close_enough(n1, n2, n3, key[0], key[1], key[2], n) || close_enough(n1, n2, n3, master[0], master[1], master[2], n))
                    total++;
            }
        }
    }
    out << total << '\n';
    return 0;
}