/*
ID: n.bater1
TASK: dualpal
LANG: C++14                 
*/
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>

#define pb push_back
#define mp make_pair
using namespace std;
bool checkPalindrome (string str) {
    int st = 0, end = str.size() - 1;
    while (st < end) {
        if(str[st] != str[end]) return false;
        st++;
        end--;
    }
    return true;
}
string changeBase(int num, int BaseNum) {
    string ans = "";
    while (num > 0) {
        int rem = num % BaseNum;
        num /= BaseNum;
        ans += to_string(rem);
    }
    return ans;
}
int main () {
    ofstream out ("dualpal.out");
    ifstream in ("dualpal.in");
    int testAns = 0;
    // for (int i = 2; i <= )
    int n, m, ans = 0;
    in >> n >> m;
    for (int num = m + 1; ans < n; num++) {
        int cur = 0;
        for (int BaseNum = 2; BaseNum <= 10; BaseNum++) {
            if (checkPalindrome(changeBase(num, BaseNum))) cur++;
            if (cur >= 2){
                ans++;
                out << num << '\n';
                break;
            }
        }
    }

    // cout << checkPalindrome("12021") << endl;

    return 0;   
}
/*
    48 -> 0
    57 -> 9
    65 -> A
    90 -> Z
*/