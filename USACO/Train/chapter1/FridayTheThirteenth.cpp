
/*
ID: n.bater1
PROG: friday
LANG: C++14
*/

#include <string>
#include <vector>
#include <fstream>
#include <iostream>
using namespace std;

bool isLeap(int year) {
    if (year % 400 == 0)
        return true;
    if (year % 100 == 0)
        return false;
    if (year % 4 == 0)
        return true;
    return false;
}

int main () {

    ofstream out("friday.out");
    ifstream in("friday.in");
    int n;
    in >> n;
    vector <int> date(7, 0);
    int months[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    int curDay = 0;

    for (int year = 1900; year < 1900 + n; year++) {
        for (int month = 0; month < 12; month++) {
            int day = (curDay + 12) % 7;
            date[(day + 2) % 7]++;
            curDay += months[month];
            if (month == 1 && isLeap(year)) curDay++;
        }
    }

    for (int i = 0; i < 6; i++)
        out << date[i] << ' ';
    out << date[6] << '\n';
    return 0;
}