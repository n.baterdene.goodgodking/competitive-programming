/*
ID: n.bater1
TASK: gift1
LANG: C++14                 
*/
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <unordered_map>
using namespace std;
int main () {
    unordered_map <string, int> people;
    unordered_map <string, int> :: iterator it;
    ofstream out ("gift1.out");
    ifstream in ("gift1.in");
    int n;
    in >> n;
    string name, getting;
    vector <string> human(n);
    for (int i = 0; i < n; i++) {
        in >> human[i];
        people[human[i]] = 0;
    }
    int money, who;
    for (int i = 0; i < n; i++) {
        in >> name;
        in >> money >> who;
        for (int j = 0; j < who; j++) {
            in >> getting;
            people[getting] += money / who;
            people[name] -= money / who;
        }
    }
    for (int i = 0; i < n; i++) 
        out << human[i] << ' ' << people[human[i]] << '\n';
    
    
    return 0;
}