/*
ID: n.bater1
TASK: milk2
LANG: C++14                 
*/
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>

#define pb push_back
#define mp make_pair
using namespace std;


int main () {
    ofstream out ("milk2.out");
    ifstream in ("milk2.in");
    int n, st, end;
    in >> n;
    vector < pair <int, int> > workers;
    for (int i = 0; i < n; i++) {
        in >> st >> end;
        pair <int, int>  cur = mp(st, end);
        workers.pb(cur);
    }
    sort(workers.begin(), workers.end());
    st = workers[0].first;
    end = workers[0].second;
    int ansCont = end - st, ansNonCont = 0;
    
    for (int i = 1; i < n; i++) {
        if (end >= workers[i].first) end = max(end, workers[i].second);
        if (end < workers[i].first || i == n - 1) {
            ansCont = max(ansCont, end - st);
            ansNonCont = max(ansNonCont, workers[i].first - end);
            st = workers[i].first;
            end = workers[i].second;
        }
    }
    ansCont = max(ansCont, end - st);
    ansNonCont = max(ansNonCont, workers.back().first - end);
    st = workers.back().first;
    end = workers.back().second;
    out << ansCont << ' ' << ansNonCont << '\n';

    return 0;   
}