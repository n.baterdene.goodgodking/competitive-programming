/*
ID: n.bater1
TASK: milk
LANG: C++14                 
*/
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>

#define pb push_back
#define mp make_pair
using namespace std;
int main () {
    ofstream out ("milk.out");
    ifstream in ("milk.in");
    int n, m;
    in >> m >> n;
    pair <int, int> empty;
    vector < pair <int, int> > farmers(n, empty);
    for(int i = 0; i < n; i++) 
        in >> farmers[i].first >> farmers[i].second;

    sort (farmers.begin(), farmers.end());
    int moneySpend = 0;
    for (int i = 0; m > 0 && i < n; i++) {
        int willBuy = min(m, farmers[i].second);
        moneySpend += (willBuy * farmers[i].first);
        m -= willBuy;
    }
    out << moneySpend << '\n';
    return 0;   
}