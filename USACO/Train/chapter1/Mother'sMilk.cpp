/*
ID: n.bater1
TASK: milk3
LANG: C++14                 
*/
#include <bits/stdc++.h>
#define pb push_back
#define mt make_tuple
#define mp make_pair
using namespace std;
map <vector <int>, bool> seen;
map <int, bool> used;
vector <int> answers;
queue < vector <pair <int, int> > > bfs;
void pour(vector <pair <int, int> > buckets) {
    vector <int> key;
    key.pb(buckets[0].first);
    key.pb(buckets[1].first);
    key.pb(buckets[2].first);
    if (seen.find(key) != seen.end() && seen[key]) return;
    seen[key] = true;
    if (buckets[0].first == 0 && used.find(buckets[2].first) == used.end()) {
        used[buckets[2].first] = true;
        answers.pb(buckets[2].first);
    }
    for (int choose = 0; choose < 3; choose++) {
        for (int to = 0; to < 3; to++) {
            if (choose == to) continue;
            int canPour = min(buckets[choose].first, buckets[to].second - buckets[to].first);
            buckets[choose].first -= canPour;
            buckets[to].first += canPour;
            bfs.push(buckets);
            buckets[choose].first += canPour;
            buckets[to].first -= canPour;
        }
    }
}
int main () {
    ifstream in ("milk3.in");
    ofstream out ("milk3.out");
    vector <pair <int, int> > buckets(3);
    for (int i = 0; i < 3; i++) {
        in >> buckets[i].second;
        buckets[i].first = 0;
    }
    buckets[2].first = buckets[2].second;
    bfs.push(buckets);
    while (bfs.size() > 0) {
        buckets = bfs.front();
        bfs.pop();
        pour(buckets);
    }
    sort (answers.begin(), answers.end());
    for (int i = 0; i < answers.size() - 1; i++) 
        out << answers[i] << ' ';
    out << answers.back() << '\n';
    return 0;   
}