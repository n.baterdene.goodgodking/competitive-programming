#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
    ofstream out ("namenum.out");
    ifstream in ("namenum.in");
    ifstream dictionary ("dict.txt");
    int nsolutions = 0;
    int numlen;
    char word[80], num[80], *p, *q, map[256];
    int i, j;
    if(true) {
        map['A'] = map['B'] = map['C'] = '2';
        map['D'] = map['E'] = map['F'] = '3';
        map['G'] = map['H'] = map['I'] = '4';
        map['J'] = map['K'] = map['L'] = '5';
        map['M'] = map['N'] = map['O'] = '6';
        map['P'] = map['R'] = map['S'] = '7';
        map['T'] = map['U'] = map['V'] = '8';
        map['W'] = map['X'] = map['Y'] = '9';
    }
    in >> num;
    numlen = strlen(num);
    while (cin >> word) {
        for (p=word, q=num; *p && *q; p++, q++) {
            if (map[*p] != *q)
                break;
        }
        if (*p == '\0' && *q == '\0') {
            cout << word << '\n';
            nsolutions++;
        }
    }
    if (nsolutions == 0) cout << "NONE\n";
    return 0;
}
