/*
ID: n.bater1
TASK: numtri
LANG: C++14                 
*/
#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
using namespace std;
vector <vector <int> > numbers;
vector <vector <int> > dp;
int n, tmp, ans;
int rec (int i, int j) {
    if (i == n - 1) return numbers[i][j];
    if (dp[i][j] != -1) return dp[i][j]; //ali hediin oltson

    return dp[i][j] = (max(rec(i + 1, j), rec(i + 1, j + 1)) + numbers[i][j]);
}
int main () {
    ifstream in ("numtri.in");
    ofstream out ("numtri.out");
    in >> n;
    for (int i = 0; i < n; i++) {
        vector <int> tmp(i + 1, -1);
        dp.pb(tmp);
    }
    for (int i = 0; i < n; i++) {
        vector <int> tmp1(i + 1);
        for (int j = 0; j <= i; j++)
            in >> tmp1[j];
        numbers.pb(tmp1);
    }
    out << rec(0, 0) << '\n';
    return 0;   
}