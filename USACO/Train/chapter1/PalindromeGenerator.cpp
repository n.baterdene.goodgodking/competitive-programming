#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
using namespace std;
int n;
vector <vector <string> > pal(10);
void mkPal (int len) {
    vector <string> current = pal[len - 2];
    for (int i = 0; i < current.size(); i++) 
        for (int j = 0; j < 10; j++) 
            pal[len].pb(to_string(j) + current[i] + to_string(j));
}
int main () {
    cin >> n;
    for (int i = 0; i < 10; i++) {
        pal[1].pb(to_string(i));
        pal[2].pb(to_string(i) + to_string(i));
    }
    for (int i = 3; i <= n; i++) 
        mkPal(i);

    for (int i = 1; i <= n; i++) 
        for (int j = 0; j < pal[i].size(); j++) 
            cout << pal[i][j] << '\n';
    return 0;
}