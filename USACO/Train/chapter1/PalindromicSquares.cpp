/*
ID: n.bater1
TASK: palsquare
LANG: C++14                 
*/
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>

#define pb push_back
#define mp make_pair
using namespace std;
ofstream out ("palsquare.out");
ifstream in ("palsquare.in");
char Base[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'};
int BaseNum;
bool checkPalindrome (string str) {
    int st = 0, end = str.size() - 1;
    while (st < end) {
        if(str[st] != str[end]) return false;
        st++;
        end--;
    }
    return true;
}
string changeBase(int num) {
    string ans = "";
    while (num > 0) {
        int rem = num % BaseNum;
        num /= BaseNum;
        ans += Base[rem];
    }
    reverse(ans.begin(), ans.end());
    return ans;
}
void transform (int num) {
    int square = num * num;
    string ans, number;
    ans = changeBase(square);
    number = changeBase(num);
    if (checkPalindrome(ans)) out << number << ' ' << ans << '\n';
}
int main () {
    in >> BaseNum;
    for (int i = 1; i <= 300; i++) 
        transform(i);
    return 0;   
}
/*
    48 -> 0
    57 -> 9
    65 -> A
    90 -> Z
*/