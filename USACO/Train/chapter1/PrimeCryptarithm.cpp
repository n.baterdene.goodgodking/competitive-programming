/*
ID: n.bater1
TASK: barn1
LANG: C++14                 
*/
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>

#define pb push_back
#define mp make_pair
using namespace std;
int n;
vector <int> numbers, tops, bots;
unordered_map <int, int> canBeUsed;
void make_num(int cur, int maxLong, int idx) {
    if (idx == 0) {
        for (int i = 0; i < n; i++) 
            make_num(numbers[i], maxLong, idx + 1);
        return;
    }
    if (idx == maxLong) {
        if (maxLong == 3) tops.pb(cur);
        else bots.pb(cur);
    }
    for (int i = 0; i < n; i++) 
        make_num(numbers[i] + cur * 10, maxLong, idx + 1);
}
int main () {
    ifstream in ("barn1.in");
    ofstream out ("barn1.out");
    cin >> n;
    numbers.resize(n);
    for (int i = 0; i < n; i++) {
        cin >> numbers[i];
        canBeUsed[numbers[i]]++;
    }
    make_num(0, 3, 0);
    make_num(0, 2, 0);
    cout << "tops: ";
    for (int i = 0; i < tops.size(); i++) 
        cout << tops[i] << ' ';
    cout << '\n';
    cout << "bots: ";
    for (int i = 0; i < bots.size(); i++) 
        cout << bots[i] << ' ';
    cout << '\n';
    return 0;   
}