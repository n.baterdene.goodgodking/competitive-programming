/*
ID: n.bater1
TASK: pprime
LANG: C++14                 
*/
#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
using namespace std;
vector <vector <string> > pal(10);
void mkPal (int len) {
    vector <string> current = pal[len - 2];
    for (int i = 0; i < current.size(); i++) 
        for (int j = 0; j < 10; j++) 
            pal[len].pb(to_string(j) + current[i] + to_string(j));
}
bool checkPrime(int number) {
    int max = sqrt(number);
    for (int i = 2; i <= max + 1; i++) 
        if(number % i == 0) return false;

    return true;
}
int main () {
    ifstream in ("pprime.in");
    ofstream out ("pprime.out");
    int st, end;
    in >> st >> end;
    int minLen = (to_string(st)).size(),maxLen = (to_string(end)).size();
    for (int i = 0; i < 10; i++) {
        pal[1].pb(to_string(i));
        pal[2].pb(to_string(i) + to_string(i));
    }
    for (int i = 3; i <= maxLen; i++) 
        mkPal(i);
    
    vector <int> ans;
    for (int i = minLen; i <= maxLen; i++) {
        for (int j = 0; j < pal[i].size(); j++) {
            stringstream  strstm(pal[i][j]);
            int number;
            strstm >> number;
            int firstNum = number % 10, len = (to_string(number)).size();
            if (number % 2 == 0 || len != i || number < st || number > end) continue;
            if (checkPrime(number)) ans.pb(number);
        }
    }
    sort(ans.begin(), ans.end());
    for (int i = 0; i < ans.size(); i++) 
        out << ans[i] << '\n';
    return 0;
}