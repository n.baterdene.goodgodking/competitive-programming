/*
ID: n.bater1
TASK: skidesign
LANG: C++14                 
*/
#include <bits/stdc++.h>

#define pb push_back
#define mp make_pair
using namespace std;
int main () {
    ifstream in ("skidesign.in");
    ofstream out ("skidesign.out");
    int sum = 0, n; 
    in >> n;
    vector <int> numbers(n);
    int minimum = numbers[0], maximum = numbers[0];
    for (int i = 0; i < n; i++) {
        in >> numbers[i];
        maximum = max(maximum, numbers[i]);
        minimum = min(minimum, numbers[i]);
    }
    int dif = maximum - minimum, ans = INT_MIN;
    if (dif <= 17) {
        cout << "0\n";
        return 0;
    }
    maximum -= 17;
    for (int i = minimum; i < maximum; i++) {
        int mini = i;
        int cur = 0;
        for (int j = 0; j < n; j++) {
            int add = 0;
            dif = numbers[j] - mini;
            if (dif < 0) add = dif;
            if (dif > 17) add = dif - 17;
            cur += (add * add);
        }
        ans = min(ans, cur);
    }
    out << ans << '\n';
    return 0;   
}