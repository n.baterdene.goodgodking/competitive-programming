/*
ID: n.bater1
TASK: sprime
LANG: C++14                 
*/
#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
using namespace std;
bool checkPrime(int number) {
    int max = sqrt(number);
    for (int i = 2; i <= max + 1; i++) 
        if(number % i == 0) return false;

    return true;
}
int main () {
    ifstream in ("sprime.in");
    ofstream out ("sprime.out");
    int len;
    in >> len;
    queue <int> numbers;
    numbers.push(2);
    numbers.push(3);
    numbers.push(5);
    numbers.push(7);
    int nextDigit[] = {1, 3, 7, 9};
    for (int cur = 1; cur < len; cur++) {
        int times = numbers.size();
        for (int j = 0; j < times; j++) {
            int number = numbers.front();
            numbers.pop();
            for (int k = 0; k < 4; k++) {
                int tmp = number * 10 + nextDigit[k];
                if (checkPrime(tmp)) {
                    if(cur == len - 1) out << tmp << '\n'; 
                    numbers.push(tmp);
                }
            }
        }
    }
    return 0;
}