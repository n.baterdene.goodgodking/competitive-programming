/*
ID: n.bater1
TASK: transform
LANG: C++14                 
*/
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>

#define pb push_back
using namespace std;

vector <vector <string> > table;
int n;
ofstream out ("transform.out");
ifstream in ("transform.in");
bool check90() {
    for (int i = 0; i < n; i++) 
        for (int j = 0; j < n; j++) 
            if (table[0][i][j] != table[1][j][n - i - 1]) 
                return false;
    return true;
}
bool check180() {
    for (int i = 0; i < n; i++) 
        for (int j = 0; j < n; j++) 
            if (table[0][i][j] != table[1][n - i - 1][n - j - 1]) 
                return false;
    return true;
}
bool check270() {
    for (int i = 0; i < n; i++) 
        for (int j = 0; j < n; j++) 
            if (table[0][i][j] != table[1][n - j - 1][i]) 
                return false;
    return true;
}
bool horizontal() { 
    for (int i = 0; i < n; i++) 
        for (int j = 0; j < n; j++) 
            if (table[0][i][j] != table[1][i][n - j - 1]) 
                return false;
    return true;
}
bool reflected() {
    for (int i = 0; i < n; i++) 
        reverse(table[1][i].begin(), table[1][i].end());
    if (horizontal() || check270() || check180() || check90()) return true;
    return false;
}
int main () {
    in >> n;
    string str;
    for (int idx; idx < 2; idx++) {
        vector <string> shape;
        for(int i = 0; i < n; i++) {
            in >> str;
            shape.pb(str);
        }
        table.pb(shape);
    }
    if (check90()) {
        out << 1 << '\n';
        return 0;
    }
    if (check180()) {
        out << 2 << '\n';
        return 0;
    }
    if (check270()) {
        out << 3 << '\n';
        return 0;
    }
    if (horizontal()) {
        out << 4 << '\n';
        return 0;
    }
    if (reflected()) {
        out << 5 << '\n';
        return 0;
    }
    if (table[0] == table[1]) {
        out << 6 << '\n';
        return 0;
    }
    out << 7 << '\n';
    return 0;   
}