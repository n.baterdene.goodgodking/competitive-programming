/*
ID: n.bater1
TASK: ride
LANG: C++14                 
*/
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int check(string str) {
    int num1 = 1;
    for(int i = 0; i < a.size(); i ++) {
        int ascii = a[i] - 64;
        num1 = (num1 * ascii) % 47;
    }
    return num1;
}
int main() {
    ofstream out ("ride.out");
    ifstream in ("ride.in");
    string a, b;
    in >> a >> b;
    if (check(a) == check(b)) out << "GO\n";
    else out << "STAY\n";
    return 0;
}