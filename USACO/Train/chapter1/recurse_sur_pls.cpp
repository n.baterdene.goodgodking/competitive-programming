/*
ID: n.bater1
TASK: numtri
LANG: C++14                 
*/
#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
using namespace std;
vector <vector <int> > numbers;
int n, tmp, ans;

int rec(int i, int j) {
    // if (i == n)
    //     return 0;
    return max(rec(i + 1, j), rec(i + 1, j + 1)) + numbers[i][j];
}
int main () {
    ifstream in ("numtri.in");
    ofstream out ("numtri.out");
    cin >> n;
    for (int i = 0; i < n; i++) {
        vector <int> tmp(i + 1);
        for (int j = 0; j <= i; j++) 
            cin >> tmp[j];
        numbers.pb(tmp);
    }
    cout << rec(0,0) << '\n';
    return 0;   
}re