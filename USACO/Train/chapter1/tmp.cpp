/*
ID: n.bater1
TASK: tmp
LANG: C++14                 
*/
#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
using namespace std;
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    ifstream fin("tmp.in");
    ofstream out("tmp.out");
	return 0;
}
