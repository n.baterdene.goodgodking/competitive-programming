/*
ID: n.bater1
TASK: comehome
LANG: C++14                 
*/
#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
using namespace std;
int n, tax;
vector <vector <pair <int, int> > > paths(100);
pair <int, pair <int, int> > cur;
set <pair <int, pair <int, int> > > :: iterator it;
int main () {
    ifstream in ("comehome.in");
    ofstream out ("comehome.out");
    in >> n;
    int st, end;
    char tmp1, tmp2;
    set <pair <int, pair <int, int> > > bfs;
    for (int i = 0; i < n; i++) {
        in >> tmp1 >> tmp2 >> tax;
        st = tmp1 - 'A';
        end = tmp2 - 'A'; 
        int starting = min(st, end);
        int ending = max(st, end);
        // if (ending + 'A' < 'Z') continue;
        paths[st].pb(mp(end, tax));
        paths[end].pb(mp(st, tax));
        if (st + 'A' >= 'A' && st + 'A' < 'Z') bfs.insert(mp(0, mp(st, st)));
    }
    // for (int i = 0; i < 100; i++) {
    //     if (paths[i].size() != 0) {
    //         cout << char(i + 'A') << " : ";
    //         for (int j = 0; j < paths[i].size(); j++) 
    //             cout << char(paths[i][j].first + 'A') << ',' << paths[i][j].second << ' ';
    //         cout << '\n';
    //     }
    // }
    vector <bool> seen(100, false);
    while (bfs.size() > 0) {
        it = bfs.begin();
        bfs.erase(it);
        cur = *it;
        if (seen[cur.second.first]) continue;
        seen[cur.second.second] = true;
        // cout << char(cur.second.second + 'A') << ' ' << char(cur.second.first + 'A') << ' ' << cur.first << '\n';
        if (cur.second.first == 'Z' - 'A') {
            out << char(cur.second.second + 'A') << ' ' << cur.first << '\n';
            return 0;
        }
        for (int i = 0; i < paths[cur.second.first].size(); i++) 
            bfs.insert(mp(cur.first + paths[cur.second.first][i].second, mp(paths[cur.second.first][i].first, cur.second.second)));
    }
    cout << "notReached\n";
    return 0;
}