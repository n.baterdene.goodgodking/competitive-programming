/*
ID: n.bater1
LANG: C++14
TASK: concom
*/
#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
using namespace std;
int n;
vector <vector <pair <int, int> > > companies(100);
bool mySort(pair <int, int>& a, pair <int, int>& b) {
    if (a.first == b.first) return a.second < b.second;
    return a.first < b.first; 
}
vector <int> myFunc(int cur) {
    vector <bool> visited(100, false);
    vector <int> used(100, 0);
    queue <int> bfs;
    bfs.push(cur);
    used[cur] = 100;
    while (bfs.size() > 0) {
        int pos = bfs.front();
        bfs.pop();
        if (visited[pos] || used[pos] <= 50) continue;
        visited[pos] = true;
        for (int i = 0; i < companies[pos].size(); i++) {
            if (companies[pos][i].second == 0) continue;
            used[companies[pos][i].first] += companies[pos][i].second;
            bfs.push(companies[pos][i].first);
        }
    }
    return used;
}
int main () {
    ofstream out ("concom.out");
    ifstream in ("concom.in");
    int a, b, c;
    in >> n;
    for (int i = 0; i < n; i++) {
        in >> a >> b >> c;
        companies[a - 1].pb(mp(b - 1, c));
    }
    vector <pair <int, int> > answers;
    for (int i = 0; i < 100; i++) {
        vector <int> cur = myFunc(i);
        for (int j = 0; j < 100; j++) 
            if (cur[j] > 50 && i != j) answers.pb(mp(i + 1, j + 1));
    }
    sort(answers.begin(), answers.end(), mySort);
    for (int i = 0; i < answers.size(); i++) 
        out << answers[i].first << ' ' << answers[i].second << '\n';
    return 0;
}