/*
ID: n.bater1
TASK: nocows
LANG: C++14                 
*/
#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
#define limit 9901
using namespace std;
int height, cows;
vector <vector <vector <int> > > seen(100, vector <vector <int> >(200, vector <int>(100, -1)));
int choose(int a, int b) {
    if (b == 1) return a;
    int cur = a;
    for (int i = 1; i < b; i++) 
        cur = (cur * (a - b + 1) / b) % limit;
    
    return cur;
}
int calc(int curHeight, int curCows, int parent) {
    if (height < curHeight || curCows > cows) return 0;
    if (curHeight == height && curCows == cows) return 1;
    if (seen[curHeight][curCows][parent] != -1) return seen[curHeight][curCows][parent];
    
    int cur = 0;
    for (int i = 2; i <= parent * 2; i+=2) 
        cur += (choose(parent, min(i / 2, parent - i / 2)) * (calc(curHeight + 1, curCows + i, i) % limit));

    return seen[curHeight][curCows][parent] = cur % limit;
}
signed main() {
    ifstream in("nocows.in");
    ofstream out("nocows.out");
    in >> cows >> height;
    out << calc(1, 1, 1) << '\n';
    return 0;
}