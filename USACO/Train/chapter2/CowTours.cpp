/*
ID: n.bater1
TASK: cowtour
LANG: C++14                 
*/
#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
#define limit 150
using namespace std;
int n, x, y, field = 0;
pair <double, int> cur;
vector <double> dp(limit);
vector <pair <int, int> > points;
vector <vector <int> > paths(limit);
vector <vector <int> > fields(limit);
vector <int> discovered(limit, false);
set <pair <double, int> > :: iterator it;

void makeFields(int st) {
    queue <int> bfs;
    bfs.push(st);
    while(bfs.size() > 0) {
        int cur = bfs.front();
        bfs.pop();
        if (discovered[cur]) continue;
        discovered[cur] = true;
        fields[field].pb(cur);
        for (int j = 0; j < paths[cur].size(); j++) 
            bfs.push(paths[cur][j]);
    }
    return;
}
double findDist(int a, int b) {
    double x = points[a].second - points[b].second;
    double y = points[a].first - points[b].first;
    return sqrt(x * x + y * y);
}
double Max(double a, double b) {
    if (a > b) return a;
    return b;
}
double Min(double a, double b) {
    if (a < b) return a;
    return b;
}
double findMax(int st) {
    set <pair <double, int> > bfs;
    bfs.insert(mp(0, st));
    vector <bool> seen(limit, 0);
    double max = 0;
    while (bfs.size() > 0) {
        it = bfs.begin();
        bfs.erase(it);
        cur = *it;
        if (seen[cur.second]) continue;
        seen[cur.second] = true;
        max = Max(max, cur.first);
        for (int i = 0; i < paths[cur.second].size(); i++) 
            bfs.insert(mp(cur.first + findDist(paths[cur.second][i], cur.second), paths[cur.second][i]));
    }
    return max;
}
int main () {
    ifstream in ("cowtour.in");
    freopen("cowtour.out", "w", stdout);
    in >> n;
    for (int i = 0; i < n; i++) {
        in >> y >> x;
        points.pb(mp(y, x));
    }
    char tmp;
    for (int i = 0; i < n; i++) 
    for (int j = 0; j < n; j++) {
        in >> tmp;
        if (tmp == '1') paths[i].pb(j);
    }
    for (int i = 0; i < n; i++) {
        if (discovered[i]) continue;
        makeFields(i);
        field++;
    }
    if (field == 1 || field > 3) {
        cout << "1.000000\n";
        return 0;
    }
    for (int i = 0; i < n; i++) 
        dp[i] = findMax(i);
    double ans = dp[fields[0][0]] + dp[fields[1][0]] + findDist(fields[0][0], fields[1][0]);
    for (int i = 0; i < fields[0].size(); i++) 
    for (int j = 0; j < fields[1].size(); j++) {
        double cur = dp[fields[0][i]] + dp[fields[1][j]] + findDist(fields[0][i], fields[1][j]);
        ans = Min(cur, ans);
    }
    printf("%.6f\n", ans);
    return 0;
}