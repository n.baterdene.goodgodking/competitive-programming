/*
ID: n.bater1
TASK: fracdec
LANG: C++14                 
*/
#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
using namespace std;
string ans;
int num, divider, times;
map <pair <int, int>, int> seen;
void solve() {
    num *= 10;
    times = num / divider;
    if (num % divider == 0) {
        ans += to_string(times);
        return;
    }
    if (seen.find(mp(num, divider)) != seen.end()) {
        string add = ans.substr(seen[mp(num, divider)]);
        ans = ans.substr(0, seen[mp(num, divider)]);
        ans = ans + '(' + add + ')';
        return;
    }
    seen[mp(num, divider)] = ans.size();
    ans += to_string(times);
    num -= times * divider;
    return solve();
}
int main () {
    ifstream in ("fracdec.in");
    ofstream out ("fracdec.out");
    in >> num >> divider;
    times = num / divider;
    num -= divider * times;
    ans = to_string(times) + '.';
    solve();
    string tmp;
    while (ans.size() > 0) {
        if (ans.size() >= 76) {
            tmp = ans.substr(0, 76);
            ans = ans.substr(76);
            out << tmp << '\n';
        } else {
            out << ans << '\n';
            break;
        }
    }
    return 0;
}