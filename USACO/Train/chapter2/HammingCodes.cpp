
/*
ID: n.bater1
PROG: hamming
LANG: C++14
*/

#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
using namespace std;
vector <int> answers;
int myCount(int a) {
    int ans = 0;
    while (a != 0) {
        if (a&1) ans++;
        a = a>>1;
    }
    return ans;
}
int main () {
    ofstream out("hamming.out");
    ifstream in("hamming.in");
    int n, length, hammingDistance;
    in >> n >> length >> hammingDistance;
    answers.pb(0);
    out << "0 ";
    int cur = 1, limit = 1 << 8;
    for (int i = 1; cur != n && i < limit; i++) {
        for (int j = 0; j < cur; j++) {
            if (myCount(int(answers[j]^i)) < hammingDistance) break;
            if (j == cur - 1) {
                out << i;
                cur++;
                if (cur != n && cur % 10 != 0) out << ' ';
                if (cur % 10 == 0 && cur != n) out << '\n';
                answers.pb(i);
            }
        }
    }
    out << '\n';
    return 0;
}