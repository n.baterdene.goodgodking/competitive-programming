/*
ID: n.bater1
TASK: prefix
LANG: C++14                 
*/
#include <bits/stdc++.h>
#define pb push_back
using namespace std;
 
vector <string> keys;
string quiz = "";
vector <int> dp(200010);
 
int main() {
    ifstream in("prefix.in");
    ofstream out("prefix.out");
    string str;
    while(in >> str) {
        if (str == ".") break;
        keys.pb(str);
    }
    while (in >> str) 
        quiz += str;

    dp[0] = 0;
    for (int i = 1; i <= quiz.size(); i++) {
        if (i > 0) dp[i] = dp[i - 1];
        for (int j = 0; j < keys.size(); j++) {
            string curKey = keys[j];        
            int keyLen = curKey.size(); 
            if (i >= keyLen && quiz.substr(dp[i - curKey.size()], curKey.size()) == curKey)
                if (i - curKey.size() == 0 || dp[i - curKey.size()] != 0) {
                    int newSize = dp[i - curKey.size()] + curKey.size();
                    dp[i] = max(dp[i], newSize);
                }
        }
    }
    out << dp[quiz.size()] << endl;
    return 0;
}