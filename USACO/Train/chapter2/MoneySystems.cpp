/*
ID: n.bater1
LANG: C++14
TASK: money
*/
// #include <iostream>
// #include <vector>
// #include <map>
// #include <fstream>
#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
using namespace std;
int n;
vector <int> coins;
map <pair <int, int>, long long> seen;
long long divide(int money, int curCoin) {
    if (money < 0) return 0;
    if (money == 0) return 1;
    if (curCoin < 0) return 0;
    if (seen.find(mp(money, curCoin)) != seen.end()) return seen[mp(money, curCoin)];

    long long not_used = divide(money, curCoin - 1);
    long long used = divide(money - coins[curCoin], curCoin);

    return seen[mp(money, curCoin)] = not_used + used;
}
int main () {
    ofstream out ("money.out");
    ifstream in ("money.in");
    int money, tmp;
    in >> n >> money;
    for (int i = 0; i < n; i++) {
        in >> tmp;
        coins.pb(tmp);
    }
    out << divide(money, n - 1) << '\n';
    return 0;
}