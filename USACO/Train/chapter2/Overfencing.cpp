/*
ID: n.bater1
LANG: C++14
TASK: maze1
*/
#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
using namespace std;
vector <pair <int, int> > gates;
vector <vector <bool> > been;
vector <vector <int> > maze;
vector <string> inst;
int n, m;
int moveX[] = {0, 1, 0, -1};
int moveY[] = {-1, 0, 1, 0};
int moves[] = {1, 2, 4, 8};
pair <int, int> cur;

void findGates() {
    for (int j = 0; j < 2 * n + 1; j+=(2*n)) 
    for (int i = 1; i < 2 * m + 1; i+=2) 
        if (inst[i][j] == ' ') gates.pb(mp(i / 2, (j - 1) / 2));

    for (int j = 1; j < 2 * n + 1; j+=2) 
    for (int i = 0; i < 2 * m + 1; i+=(2*m)) 
        if (inst[i][j] == ' ') gates.pb(mp((i - 1) / 2, j / 2));

}
int main () {
    ifstream in ("maze1.in");
    ofstream out ("maze1.out");
    in >> n >> m;
    if (n == 1 && m == 1) {
        out << 1 << '\n';
        return 0;
    }
    inst.resize(2 * m + 1);
    been.resize(m + 1, vector <bool>(n + 1, false));
    maze.resize(m, vector <int>(n, 0));
    getline(in, inst[0]);
    for (int i = 0; i < 2 * m + 1; i++) 
        getline(in, inst[i]);

    findGates();
    for (int i = 0; i < gates.size(); i++) 
        cout << gates[i].first << ' ' << gates[i].second << '\n';
    for (int i = 0; i < m; i++) 
    for (int j = 0; j < n; j++) 
    for (int move = 0; move < 4; move++) {
        int x = j * 2 + 1, y = i * 2 + 1;
        x += moveX[move];
        y += moveY[move];
        if (inst[y][x] != ' ') maze[i][j] += moves[move];
    }
    queue <pair <pair <int, int>, int> > bfs;
    bfs.push(mp(gates[0], 1));
    bfs.push(mp(gates[1], 1));
    int ans = 0;
    while (bfs.size() > 0) {
        cur = bfs.front().first;
        int value = bfs.front().second;
        bfs.pop();
        if (been[cur.first][cur.second]) continue;
        ans = max(ans, value);
        been[cur.first][cur.second] = true;
        int x, y;
        for (int move = 0; move < 4; move++) {
            x = cur.second + moveX[move];
            y = cur.first + moveY[move];
            if (x < 0 || y < 0 || x >= n || y >= m) continue;
            if ((maze[cur.first][cur.second] & moves[move]) != 0) continue;
            bfs.push(mp(mp(y, x), value + 1));
        }
    }
    out << ans << '\n';
    return 0;
}