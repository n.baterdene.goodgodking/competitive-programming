/*
ID: n.bater1
TASK: lamps
LANG: C++14                 
*/
#include <bits/stdc++.h>

#define pb push_back
#define mp make_pair
using namespace std;
int n, maxCount;
vector <int> last, lastUsed;
vector <vector <int> > buttons(4);
map <pair <vector <int>, int>, bool> seen;
bool check(vector <int>& lights) {
    for (int i = 0; i < n; i++) {
        if (last[i] == -1) continue;
        if (last[i] != lights[i]) return false;
    }
    lastUsed = lights;
    return true;
}
vector <vector <int> > turnLights(vector <int>& lights, int curCount) {
    vector <vector <int> > cur;
    if (seen.find(mp(lights, curCount)) != seen.end() && seen[mp(lights, curCount)] == true) return cur;
    seen[mp(lights, curCount)] = true;
    if (curCount == maxCount) {
        if (check(lights)) cur.pb(lastUsed);
        return cur;
    }
    vector <int> next;
    for (int move = 0; move < 4; move++) {
        next = lights;
        for (int i = 0; i < buttons[move].size(); i++) 
            next[buttons[move][i]] = abs(next[buttons[move][i]] - 1);

        vector <vector <int> > res = turnLights(next, curCount + 1);
        for (int i = 0; i < res.size(); i++) 
            cur.pb(res[i]);
    }
    return cur;
}
int main () {
    ifstream in ("lamps.in");
    ofstream out ("lamps.out");
    in >> n >> maxCount;
    maxCount %= 12;
    last.resize(n, -1);
    int number;
    while (in >> number) {
        number--;
        if (number == -2) break;
        last[number] = 1;
    }
    while (in >> number) {
        number--;
        if (number == -2) break;
        last[number] = 0;
    }
    for (int i = 0; i < n; i++) {
        buttons[0].pb(i);
        if (i % 2 == 1) buttons[1].pb(i);
        else buttons[2].pb(i);
        if (i % 3 == 0) buttons[3].pb(i);
    }
    vector <int> lights(n, 1);
    vector <vector <int> >res;
    res = turnLights(lights, 0); 
    if (res.size() == 0) {
        out << "IMPOSSIBLE\n";
        return 0;
    } 
    sort(res.begin(), res.end());
    for (int i = 0; i < res.size(); i++) {
        for (int j = 0; j < res[i].size(); j++) 
            out << res[i][j];
        out << '\n';
    }
    return 0;
}