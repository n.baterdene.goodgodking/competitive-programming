/*
ID: n.bater1
TASK: preface
LANG: C++14                 
*/
#include <bits/stdc++.h>

#define pb push_back
#define mp make_pair
using namespace std;
vector <int> counts(7, 0);
char numbers[] = {'I', 'V', 'X', 'L', 'C', 'D', 'M'};
void add(int number, int idx) {
    if (number == 0) return ;
    int lastDigit = number % 10;
    int times = lastDigit % 5;
    if (times <= 3) counts[idx * 2] += times;
    else counts[idx * 2] += abs(times - 5);
    if (lastDigit >= 4 && lastDigit <= 8) counts[idx * 2 + 1]++;
    if (lastDigit >= 9) counts[(idx + 1) * 2]++;
    add(number / 10, idx + 1);
}
int main () {
    ifstream in ("preface.in");
    ofstream out ("preface.out");
    int n;
    in >> n;

    for (int i = 1; i <= n; i++) 
        add(i, 0);
    
    int end = 0;
    for (int i = 0; i < 7; i++)
        if (counts[i] != 0) end = i + 1;

    for (int i = 0; i < end; i++) 
        out << numbers[i] << ' ' << counts[i] << '\n';
    return 0;   
}