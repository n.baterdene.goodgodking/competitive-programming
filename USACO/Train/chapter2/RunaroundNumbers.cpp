/*
ID: n.bater1
TASK: runround
LANG: C++14                 
*/
#include <bits/stdc++.h>

#define pb push_back
#define mp make_pair
using namespace std;
int number;
bool checkUnique(int cur) {
    string seq = to_string(cur);
    unordered_map <char, int> seen;
    for (int i = 0; i < seq.size(); i++) {
        seen[seq[i]]++;
        if (seen[seq[i]] == 2 || seq[i] == '0') return true;
    }
    return false;
}
bool notRunAround(int cur) {
    if (checkUnique(cur)) return true;
    string seq = to_string(cur);
    int time, i = 0, len = seq.size();
    vector <bool> seen(len, false);
    for (time = 0; seen[i] == false; time++) {
        seen[i] = true;
        int go = (seq[i] - '0');
        i = (i + go) % len;
    }
    if (time == len && i == 0) return false;
    return true;
}
int main () {
    ifstream in ("runround.in");
    ofstream out ("runround.out");
    in >> number;
    number++;
    while (notRunAround(number)) 
        number++;
    out << number << '\n';
    return 0;
}