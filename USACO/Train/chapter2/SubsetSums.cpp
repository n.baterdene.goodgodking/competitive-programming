/*
ID: n.bater1
TASK: subset
LANG: C++14                 
*/

#include <bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define int long long
using namespace std;
vector <vector <int> > memo(800, vector <int>(40, -1));
int start(int sum, int end){
    if (sum == 0) return 1;
    if (end == 0 || sum < 0) return 0;
    if (memo[sum][end] != -1) return memo[sum][end];
    return memo[sum][end] = start(sum - end, end - 1) + start(sum, end - 1);
}

signed main () {
    ifstream in ("subset.in");
    ofstream out ("subset.out");
    int n, ans = 0;
    in >> n;
    int sum = n * (n + 1) / 2;
    if (sum % 2 == 0) ans = start(sum / 2, n) / 2;
    out << ans << '\n';
    return 0;   
}