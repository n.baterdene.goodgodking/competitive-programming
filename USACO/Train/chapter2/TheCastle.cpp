/*
ID: n.bater1
TASK: castle
LANG: C++14                 
*/
#include <bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define limit 2505
#define p0 first.first
#define p1 first.second
#define p2 second.first
#define p3 second.second
using namespace std;

vector <vector <int> > rooms, colors;
vector <int> roomColor(limit);
int color = 0;
int moveY[] = {0, -1, 0, 1};
int moveX[] = {-1, 0, 1, 0};
int moves[] = {1, 2, 4, 8};
string directions[] = {"W", "N", "E", "S"};
int n, m; 
// 1 zuun, 2 deesh, 4 baruun, 8 doosh
//    0        1        2        3
int discover (int i, int j) {
    if (i < 0 || j < 0 || i >= n || j >= m) return 0;
    if (colors[i][j] != -1) return 0;
    colors[i][j] = color;
    int cur = 1;
    for (int move = 0; move < 4; move++) {
        if ((rooms[i][j] & moves[move]) != 0) continue;
        cur += discover(i + moveY[move], j + moveX[move]);
    }
    return cur;
}
pair <pair <int, int>, pair <int, int> > check (int i, int j) {
    int curColor = colors[i][j];
    int firstSize = roomColor[curColor];
    int maxAns = firstSize;
    int x, y, curSize;
    pair <pair <int, int>, pair <int, int> > res = mp(mp(0, 0), mp(0, 0));
    for (int move = 1; move <= 2; move++) {
        y = i + moveY[move];
        x = j + moveX[move];
        if ((rooms[i][j] & moves[move]) == 0) continue;
        if (y < 0 || x < 0 || y >= n || x >= m) continue;
        color = colors[y][x];
        curSize = roomColor[color];
        if (color != curColor) curSize += firstSize;
        if (curSize > maxAns) res = mp(mp(curSize, move), mp(i, j));
        maxAns = max(maxAns, curSize);
    }
    return res;
}
int main () {
    ifstream in ("castle.in");
    ofstream out ("castle.out");
    in >> m >> n;
    rooms.resize(n, vector <int>(m));
    colors.resize(n, vector <int>(m, -1));
    for (int i = 0; i < n; i++) 
        for (int j = 0; j < m; j++) 
            in >> rooms[i][j];

    int maxRoom = 0;
    for (int i = 0; i < n; i++) 
    for (int j = 0; j < m; j++) {
        if (colors[i][j] != -1) continue;
        color++;
        int cur = discover(i, j);
        maxRoom = max(maxRoom, cur);
        roomColor[color] = cur;
    }
    out << color << '\n';
    out << maxRoom << '\n';
    // size direction posy posx
    pair <pair <int, int>, pair <int, int> > pos = mp(mp(0, 0),mp(0, 0)), cur;
    for (int j = 0; j < m; j++) 
    for (int i = n - 1; i >= 0; i--) {
        cur = check(i, j);
        if (cur.p0 > pos.p0) pos = cur; 
    }

    out << pos.p0 << '\n' << pos.p2 + 1 << ' ' << pos.p3 + 1 << ' ' << directions[pos.p1] << '\n';
    return 0;
}