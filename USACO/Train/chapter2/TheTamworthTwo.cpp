/*
ID: n.bater1
LANG: C++14
TASK: ttwo
*/
#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
using namespace std;
int n = 10, m = 10, myCount = 0;
bool ended = false;
vector <vector <int> > terrain(10, vector <int>(10));
// 0 empty, 1, farmer, 2 cows, obs -1
vector <vector <vector <bool> > > seenFarmer(4, vector <vector <bool> >(100, vector <bool>(100, false)));
vector <vector <vector <bool> > > seenCows(4, vector <vector <bool> >(100, vector <bool>(100, false)));
vector <pair <int, int> > pos(2);
// 0 farmer, 1 cows
int moveX[] = {0, 1, 0, -1};
int moveY[] = {-1, 0, 1, 0};
int direction[] = {0, 0};
ofstream out ("ttwo.out");
ifstream in ("ttwo.in");
int startMove() {
    if (pos[0] == pos[1]) {
        ended = true;
        return 0;
    }
    if (seenFarmer[direction[0]][pos[0].first][pos[0].second] && seenCows[direction[1]][pos[1].first][pos[1].second]) {
        myCount++;
        if (myCount == 100) return 0;
    }
    seenFarmer[direction[0]][pos[0].first][pos[0].second] = true;
    seenCows[direction[1]][pos[1].first][pos[1].second] = true;
    for (int i = 0; i < 2; i++) {
        int nextX = pos[i].second + moveX[direction[i]];
        int nextY = pos[i].first + moveY[direction[i]];
        if (nextY >= m || nextX >= n || nextY < 0 || nextX < 0 || 
            terrain[nextY][nextX] == -1) {
            direction[i] = (direction[i] + 1) % 4;
            if (i == 0) seenFarmer[direction[i]][pos[i].first][pos[i].second] = false;
            else seenCows[direction[i]][pos[i].first][pos[i].second] = false;
            continue;
        }
        terrain[pos[i].first][pos[i].second] = 0;
        terrain[nextY][nextX] = i + 1;
        pos[i] = mp(nextY, nextX);
    }
    return 1 + startMove();
}
int main () {
    char tmp;

    for (int i = 0; i < n; i++) 
    for (int j = 0; j < m; j++) {
        in >> tmp;
        if (tmp == '.') terrain[i][j] = 0;
        if (tmp == '*') terrain[i][j] = -1;
        if (tmp == 'F') {
            pos[0] = mp(i, j);
            terrain[i][j] = 1;
        }
        if (tmp == 'C') {
            pos[1] = mp(i, j);
            terrain[i][j] = 2;
        }
    }
    int ans = startMove();
    if (!ended) ans = 0;
    out << ans << '\n';
    return 0;
}