/*
ID: n.bater1
TASK: zerosum
LANG: C++14                 
*/
#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
using namespace std;
int n;
ofstream out("zerosum.out");
void calc(int sum, int lastNum, bool add, int cur, string curStr) {
    if (cur == n + 1) {
        if (add) sum += lastNum;
        else sum -= lastNum;
        if (sum == 0) out << curStr << '\n';
        return;
    }
    calc(sum, lastNum * 10 + cur, add, cur + 1, curStr + ' ' + to_string(cur));
    if (add) sum += lastNum;
    else sum -= lastNum;
    calc(sum, cur, true, cur + 1, curStr + "+" + to_string(cur));   
    calc(sum, cur, false, cur + 1, curStr + "-" + to_string(cur));   
}
int main() {
    ifstream in("zerosum.in");
    in >> n;
    calc(0, 1, true, 2, "1");
    return 0;
}