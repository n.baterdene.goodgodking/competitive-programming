/*
ID: n.bater1
TASK: holstein
LANG: C++14                 
*/
#include <bits/stdc++.h>

#define pb push_back
#define mp make_pair
using namespace std;
ifstream in ("holstein.in");
ofstream out ("holstein.out");
vector <vector <int> > foods;
vector <int> cows;
int n, m;
pair <pair <vector <int>, vector <int> >, int> cur;
bool check() {
    int enough = 0;
    for (int i = 0; i < n; i++) {
        if (cur.first.second[i] >= cows[i]) enough++;
        else break;
    }
    if (enough != n) return false;
    out << cur.first.first[0];
    for (int i = 1; i < cur.first.first.size(); i++) 
        out << ' ' << cur.first.first[i];
    out << '\n';
    return true;
}
int main () {
    in >> n;
    cows.resize(n);
    for (int i = 0; i < n; i++) 
        in >> cows[i];

    in >> m;
    for (int i = 0; i < m; i++) {
        vector <int> curFood(n);
        for (int j = 0; j < n; j++) 
            in >> curFood[j];
        foods.pb(curFood);
    }
    set <pair <pair <vector <int>, vector <int> >, int> > bfs;
    set <pair <pair <vector <int>, vector <int> >, int> > :: iterator it;
    vector <int> used, empty(n, 0);
    used.pb(0);
    cur = mp(mp(used, empty), 0);
    bfs.insert(cur);
    while (bfs.size() != 0) {
        it = bfs.begin();
        cur = *it;
        bfs.erase(it);
        int idx = cur.second;
        if (check()) return 0;
        if (idx == m) continue;
        cur.second++;
        bfs.insert(cur);
        for (int i = 0; i < n; i++) 
            cur.first.second[i] += foods[idx][i];
        cur.first.first[0]++;
        cur.first.first.pb(idx + 1);
        bfs.insert(cur);
    }
    return 0;   
}