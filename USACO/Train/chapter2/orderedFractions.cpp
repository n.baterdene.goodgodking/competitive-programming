/*
ID: n.bater1
TASK: frac1
LANG: C++14                 
*/
#include <bits/stdc++.h>

#define pb push_back
#define mp make_pair
using namespace std;
vector <pair <int, int> > numbers;
vector <vector <bool> > seen(200, vector <bool>(200, false));
bool mySort(pair <int, int> a, pair <int, int> b) {
    return a.first * b.second < a.second * b.first;
}
int main () {
    ifstream in ("frac1.in");
    ofstream out ("frac1.out");
    int n;
    in >> n;
    for (int i = 1; i <= n; i++) 
    for (int j = i; j <= n; j++) {
        if (seen[i][j]) continue;
        int times = n / j;
        for (int time = 1; time <= times; time++) 
            seen[i * time][j * time] = true;

        numbers.pb(mp(i, j));
    }
        
    sort(numbers.begin(), numbers.end(), mySort);
    if (n >= 1) out << "0/1\n";
    for (int i = 0; i < numbers.size(); i++) 
        out << numbers[i].first << '/' << numbers[i].second << '\n';
    return 0;   
}