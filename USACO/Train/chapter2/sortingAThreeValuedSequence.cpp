/*
ID: n.bater1
TASK: sort3
LANG: C++14                 
*/
#include <bits/stdc++.h>

#define pb push_back
#define mp make_pair
using namespace std;
int main () {
    ifstream in ("sort3.in");
    ofstream out ("sort3.out");
    int n, tmp; 
    in >> n;
    vector <int> numbers(n);
    vector <int> count(4, 0);
    for (int i = 0; i < n; i++) {
        in >> numbers[i];
        count[numbers[i]]++;
    }
    vector <vector <int> > sectorCounts(4, vector <int>(4));
    int idx = 0;
    for (int st = 1; st <= 3; st++) 
        for(int i = 0; i < count[st]; i++) {
            sectorCounts[st][numbers[idx]]++;
            idx++;
        }
    
    int moved = 0;
    // for (int st = 1; st <= 3; st++)
    //     for (int number = 1; number <= 3; number++) 
    //         cout << st << ' ' << number << ' ' << sectorCounts[st][number] << '\n';
        
    for (int st = 1; st <= 3; st++)
        for (int number = st; number <= 3; number++) {
            if (st == number) sectorCounts[st][number] = 0;
            int maxOneMove = min(sectorCounts[st][number], sectorCounts[number][st]);
            moved += maxOneMove;
            // cout << st << ' ' << number << " -> " << sectorCounts[st][number] << ' ' << sectorCounts[number][st] << " : " << maxOneMove << ' ' << moved << '\n';
            sectorCounts[st][number] -= maxOneMove;
            sectorCounts[number][st] -= maxOneMove;
        }
    
    // for (int st = 1; st <= 3; st++)
    //     for (int number = 1; number <= 3; number++) 
    //         cout << st << ' ' << number << " -> " << sectorCounts[st][number] << '\n';
    // cout << moved << '\n';
    int extra = 0;
    for (int st = 1; st <= 3; st++) 
        for (int number = 1; number <= 3; number++) {
            extra += sectorCounts[st][number];
        }

    extra = extra / 3 * 2;
    out << moved + extra << '\n';
    // cout << extra << ' ' << moved << '\n';
    return 0;   
}