#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define limit 101
int owns[limit][limit];
int controls[limit][limit];
void addcontroller(int i, int j) {
    int k;
    if (controls[i][j]) return;
    controls[i][j] = 1;
    for (k=0; k<limit; k++)
        owns[i][k] += owns[j][k];

    for (k=0; k<limit; k++)
        if (controls[k][i])
            addcontroller(k, j);

    for (k=0; k<limit; k++)
        if (owns[i][k] > 50)
            addcontroller(i, k);
}
void addowner(int i, int j, int p) {
    int k;
    for (k=0; k<limit; k++)
        if (controls[k][i])
            owns[k][j] += p;

    for (k=0; k<limit; k++)
        if (owns[k][j] > 50)
            addcontroller(k, j);
}
void main(void) {
    FILE *fin, *fout;
    int i, j, n, a, b, p;
    fin = fopen("concom.in", "r");
    fout = fopen("concom.out", "w");
    assert(fin != NULL && fout != NULL);
    for (i = 0; i<limit; i++)
        controls[i][i] = 1;

    fscanf(fin, "%d", &n);
    for (i = 0; i<n; i++) {
        fscanf(fin, "%d %d %d", &a, &b, &p);
        addowner(a, b, p);
    }
    for (i = 0; i<limit; i++)
    for (j = 0; j<limit; j++)
        if (i != j && controls[i][j])
            fprintf(fout, "%d %d\n", i, j);
    exit(0);
}
