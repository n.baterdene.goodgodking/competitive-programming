/*
ID: n.bater1
LANG: C++14
TASK: agrinet
*/
#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
using namespace std;
vector <pair <int, pair <int, int> > > paths;
vector <int> parents;
int myFind(int i) {
    if (parents[i] == i) return i;
    return parents[i] = myFind(parents[i]);
}
void merge(int a, int b) {
    int from = myFind(a);
    int to = myFind(b);
    parents[from] = parents[to];
}
int main () {
    ifstream in ("agrinet.in");
    ofstream out ("agrinet.out");
    int n, tmp;
    in >> n;
    for (int i = 0; i < n; i++) 
        parents.pb(i);

    for (int i = 0; i < n; i++) 
    for (int j = 0; j < n; j++) {
        in >> tmp;
        if (i < j) continue;
        paths.pb(mp(tmp, mp(i, j)));
    }
    sort(paths.begin(), paths.end());
    int used = 0;
    for (int i = 0; i < paths.size(); i++) {
        if (myFind(paths[i].second.first) == myFind(paths[i].second.second)) continue;
        used += paths[i].first;
        merge(paths[i].second.first, paths[i].second.second);
    }
    out << used << '\n';
    return 0;
}