/*
ID: n.bater1
LANG: C++14
TASK: camelot
*/
#include <bits/stdc++.h>
using namespace std;

// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))
#define FOR(i, a, b) for (int i = a; i < b; i++)
#define FORB(i, a, b) for (int i = a - 1; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i <= a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define pb push_back
#define mp make_pair
#define umap unordered_map
#define ff first
#define ss second
 
typedef long long ll;
typedef double d;
typedef unsigned uint;
typedef unsigned long long ull;
 
typedef vector<int> VI;
typedef vector<bool> VB;
typedef vector<VB> VVB;
typedef vector<VVB> VVVB;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<ll> VLL;
typedef vector<d> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<int, int> MIB;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VPI> VVPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
 
const int MOD = int(1e9) + 7;
const int INF = 10400;
const ll INFF = 0x3f3f3f3f3f3f3f3fLL;
const d EPS = 1e-9;
const d PI = acos(-1.0); //M_PI;
const int moveX[] = {-2,  2, -1,  1, -2, 2, -1, 1};
const int moveY[] = {-1, -1, -2, -2,  1, 1,  2, 2};
#ifdef ONLINE_JUDGE
#define cerr if (false) cerr
#endif 
#define debug(x) cerr << "Line Number:" <<__LINE__ << ": " << #x << " = " << (x) << endl



int n, m;
VVVI dist(30, VVI(30, VI(2)));
VVI kingDist(30, VI(30)), gather(30, VI(30, 0));
PII king;
set <pair <PII, PII> > bfs;
// queue <PII> bfs;
void travel(int a, int b, int status) {
    int dis = dist[a][b][status], res = 0;
    REP(i, 8) {
        int newX = a + moveX[i];
        int newY = b + moveY[i];
        if (newX < 0 || newY < 0 || newX >= m || newY >= n) continue;
        if (dist[newX][newY][status] > dis + 1) {
            res = 1;
            dist[newX][newY][status] = dis + 1;
            pair <PII, PII> tmp = mp(mp(dist[newX][newY][status], status), mp(newX, newY));
            bfs.insert(tmp);
        }
    }
    int distToKing = max(abs(king.ff - a), abs(king.ss - b));

    if (!status && dist[a][b][1] > dis + distToKing) {
        dist[a][b][1] = dis + distToKing;
        pair <PII, PII> tmp = mp(mp(dist[a][b][1], 1), mp(a, b));
        bfs.insert(tmp);
    }
} 

void calcAllDist(int a, int b) {
    REP(i, m) 
    REP(j, n) {
        dist[i][j][0] = INF;
        dist[i][j][1] = INF;
    }

    dist[a][b][0] = 0;
    dist[a][b][1] = max(abs(king.ff - a), abs(king.ss - b));
    bfs.insert(mp(mp(0, 0), mp(a, b)));
    bfs.insert(mp(mp(dist[a][b][1], 1), mp(a, b)));
    VVVI vis(m, VVI(n, VI(2, 0)));
    // cout << dist[0][2][1] << '\n';
    while(!bfs.empty()) {
        pair <PII, PII>  tmp = *(bfs.begin());
        PII cur = tmp.ss, cur1 = tmp.ff;
        bfs.erase(bfs.begin());
        if (vis[cur.ff][cur.ss][cur1.ss]) continue;
        // cout << cur.ff << ' ' << cur.ss << ' ' << cur1.ss << '\n';
        vis[cur.ff][cur.ss][cur1.ss] = 1;
        travel(cur.ff, cur.ss, cur1.ss);
    }
}
int main () {
    ifstream cin("camelot.in");
    ofstream cout("camelot.out");
    char tmp;
    int a, b;
    cin >> n >> m >> tmp >> b;
    a = tmp - 'A';
    b--;
    king = mp(a, b);
    REP(i, m) 
    REP(j, n) 
        kingDist[i][j] = max(abs(king.ff - i), abs(king.ss - j));

    while (cin >> tmp >> b) {
        a = tmp - 'A';
        b--;
        calcAllDist(a, b);
        REP(i, m) 
        REP(j, n) {
            gather[i][j] += dist[i][j][0];
            kingDist[i][j] = min(kingDist[i][j], dist[i][j][1] - dist[i][j][0]);
        }
    }
    int ans = INF;
    REP(i, m) 
    REP(j, n) 
        ans = min(ans, gather[i][j] + kingDist[i][j]);
    cout << ans << '\n';
    return 0;
}