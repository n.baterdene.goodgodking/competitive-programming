/*
ID: n.bater1
LANG: C++14
TASK: ratios
*/
#include <bits/stdc++.h>
using namespace std;

// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))
#define FOR(i, a, b) for (int i = a; i < b; i++)
#define FORB(i, a, b) for (int i = a - 1; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define pb push_back
#define mp make_pair
#define umap unordered_map
#define ff first
#define ss second
 
typedef long long ll;
typedef double d;
typedef unsigned uint;
typedef unsigned long long ull;
 
typedef vector<int> VI;
typedef vector<bool> VB;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<ll> VLL;
typedef vector<d> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<int, int> MIB;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
 
const int MOD = int(1e9) + 7;
const int INF = 0x3f3f3f3f;
const ll INFF = 0x3f3f3f3f3f3f3f3fLL;
const d EPS = 1e-9;
const d PI = acos(-1.0); //M_PI;
const int moveX[] = {-1, 0, 1, 0};
const int moveY[] = {0, 1, 0, -1};






int mini;
int mloc[4]; 
int ratios[3][3];
int goal[3], sum[3];



int main () {
    ifstream in ("ratios.in");
    ofstream out ("ratios.out");
    int gsum, t, s; 
    int lv, lv2, lv3;
    cin >> goal[0] >> goal[1] >> goal[2];
    REP(i, 3) cin >> ratios[i][0] >> ratios[i][1] >> ratios[i][2];
    gsum = goal[0] + goal[1] + goal[2];
    mini = 301; 

    if (gsum == 0) {
        cout << "0 0 0 0\n";
        return 0;
    }

    for (lv = 0; lv < 100; lv++)
    for (lv2 = 0; lv2 < 100; lv2++) {
        sum[0] = lv*ratios[0][0] + lv2*ratios[1][0];
        sum[1] = lv*ratios[0][1] + lv2*ratios[1][1];
        sum[2] = lv*ratios[0][2] + lv2*ratios[1][2];

        if (lv + lv2 > mini) break;
        for (lv3 = 0; lv3 < 100; lv3++) {
            s = lv + lv2 + lv3;
            if (s >= mini) break;
            t = (sum[0] + sum[1] + sum[2]) / gsum;

            if (t != 0 && sum[0] == t*goal[0] && 
                sum[1] == t*goal[1] && sum[2] == t*goal[2]) {
                mini = s;
                mloc[0] = lv;
                mloc[1] = lv2;
                mloc[2] = lv3;
                mloc[3] = t;
            }
            sum[0] += ratios[2][0];
            sum[1] += ratios[2][1];
            sum[2] += ratios[2][2];
        }
    }
    if (mini == 301) cout << "NONE\n";
    else cout << mloc[0] << ' ' << mloc[1] << ' ' << mloc[2] << ' ' << mloc[3] << '\n';
    return 0;

}