/*
ID: n.bater1
TASK: humble
LANG: C++14                 
*/
#include <bits/stdc++.h>
#define PII pair <int, int>
#define pb push_back
#define mp make_pair
#define ll long long
#define ff first
#define ss second
using namespace std;
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int n = 0, m = 0;
    ifstream fin("humble.in");
    ofstream out("humble.out");
    fin >> n >> m;
    vector <ll> prime(n), humble;
    for (int i = 0; i < n; i++)
        fin >> prime[i];
    
    vector <int> sortedIndex(m, 0);
    humble.pb(1);
    
    while (humble.size() <= m) {
        ll min = INT_MAX;
        int m = -1;
        for (int i = 0; i < n; i++) {
            while (prime[i] * humble[sortedIndex[i]] <= humble.back())
                sortedIndex[i]++;

            if (prime[i] * humble[sortedIndex[i]] < min) {
                min = prime[i] * humble[sortedIndex[i]];
                m = i;
            }
        }
        humble.pb(min);
    }
    out << humble[m] << endl;

    return 0;
}
