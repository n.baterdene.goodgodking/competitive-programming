/*
ID: n.bater1
LANG: C++14
TASK: fence
*/
#include <bits/stdc++.h>
using namespace std;

// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))
#define FOR(i, a, b) for (int i = a; i < b; i++)
#define FORB(i, a, b) for (int i = a - 1; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i <= a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define pb push_back
#define mp make_pair
#define umap unordered_map
#define ff first
#define ss second
 
typedef long long ll;
typedef double d;
typedef unsigned uint;
typedef unsigned long long ull;
 
typedef vector<int> VI;
typedef list<int> LI;
typedef vector<bool> VB;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<ll> VLL;
typedef vector<d> VD;
typedef vector<int> SI;
typedef vector<SI> VSI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<int, int> MIB;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VPI> VVPI;
typedef vector<VI> VVI;
typedef vector<LI> VLI;
typedef vector<VVI> VVVI;
 
const int MOD = int(1e9) + 7;
const int INF = INT32_MAX;
const ll INFF = INT64_MAX;
const d EPS = 1e-9;
const d PI = acos(-1.0); //M_PI;
const int moveX[] = {-1, 0, 1, 0};
const int moveY[] = {0, 1, 0, -1};





int n, limit = 1025;
VLI paths(limit);
VI ans;
LI :: iterator it;
void addPath(int a, int b) {
    cout << a << ' ' << b << '\n';
    paths[a].pb(b);
    paths[b].pb(a);
}
void delPath(int a, int b) {
  paths[b].erase(find(paths[b].begin(), paths[b].end(), a)); 
  paths[a].erase(find(paths[a].begin(), paths[a].end(), b)); 
}
int reach(int a, VB& seen) {
    seen[a] = true; 
    int count = 1; 
    for (auto el : paths[a]) 
        if (el != -1 && !seen[el]) 
            count += reach(el, seen);
  
  return count; 

}
bool isBridge(int a, int b) {
    int cnt = 0;
    for (auto el : paths[a]) 
        if (el != -1) cnt++;

    if (cnt == 1) return 1;
    VB seen(limit, 0);
    int cnt1 = reach(a, seen);
    delPath(a, b);
    REP(i, limit) seen[i] = 0;
    int cnt2 = reach(b, seen);
    addPath(a, b);
    return cnt1 == cnt2;
}
void printPath(int st) {
    ans.pb(st);
    paths[st].sort();
    for (auto next : paths[st]) {
        if (next != -1 && isBridge(st, next)) {

            delPath(st, next);
            printPath(next);
        }
    }
}
int main () {
    ifstream cin("fence.in");
    ofstream cout("fence.out");
    cin >> n;
    int a, b;
    VPI tmp;
    REP(i, n) {
        cin >> a >> b;
        addPath(a, b);
    }
    REP(i, limit) {
        paths[i].sort();
    }
    int st = 1;
    REP1(i, limit)
        if (paths[i].size() & 1) {
            st = i;
            break;
        }
    printPath(st);
    for (auto num : ans) cout << num << '\n';
    return 0;
}