/*
ID: n.bater1
LANG: C++14
TASK: inflate
*/
#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
using namespace std;
vector <int> dp(10005, 0);
int main () {
    int n, m, time, score;
    ifstream in ("inflate.in");
    ofstream out ("inflate.out");
    in >> m >> n;
    for (int i = 0; i < n; i++) {
        in >> score >> time;
        dp[time] = max(dp[time], score);
        for (int j = time; j <= m; j++) 
            dp[j] = max(dp[j], dp[j - time] + dp[time]);
    }
    out << dp[m] << '\n';
    return 0;
}