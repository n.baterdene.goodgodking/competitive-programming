/*
ID: n.bater1
LANG: C++14
TASK: shopping
*/
#include <bits/stdc++.h>
using namespace std;

// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))
#define FOR(i, a, b) for (int i = a; i < b; i++)
#define FORB(i, a, b) for (int i = a - 1; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i <= a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define pb push_back
#define mp make_pair
#define umap unordered_map
#define ff first
#define ss second
 
typedef long long ll;
typedef double d;
typedef unsigned uint;
typedef unsigned long long ull;
 
typedef vector<int> VI;
typedef vector<bool> VB;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<ll> VLL;
typedef vector<d> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<int, int> MIB;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VPI> VVPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
 
const int MOD = int(1e9) + 7;
const int INF = 0x3f3f3f3f;
const ll INFF = 0x3f3f3f3f3f3f3f3fLL;
const d EPS = 1e-9;
const d PI = acos(-1.0); //M_PI;
const int moveX[] = {-1, 0, 1, 0};
const int moveY[] = {0, 1, 0, -1};





int n, m, a, b, c, ans;
map <int, int> sale;
map <int, int> cost, pos;
VVPI offers;
VPI shopList;
int determineCost(VPI& a) {
    int curAns = 0;
    REP(i, a.size()) 
        curAns += cost[a[i].ff] * a[i].ss;

    return curAns;
}
int findSum(VPI& cur) {
    int sum = 0;
    REP(i, cur.size()) sum += cur[i].ss;
    return sum;
}
bool possible(VPI& cur, int idx) {
    REP(i, offers[idx].size()) {
        if (cur[pos[offers[idx][i].ff]].ss < offers[idx][i].ss) return 0;
    }
    return 1;
}
void print(VPI& a) {

    for (auto el : a) cout << el.ff << ' ' << el.ss << '\n';
}
pair <int, VPI> takeOffer(VPI cur, int idx, int curCost) {
    REP(i, offers[idx].size()) {
        cur[pos[offers[idx][i].ff]].ss -= offers[idx][i].ss;
    }
    pair <int, VPI> res;
    // cout << "this offer:\n";
    // print(offers[idx]);
    // cout << "has cost: " << sale[idx] << '\n';
    res.ff = curCost + sale[idx];
    res.ss = cur;
    return res;
}
int main () {
    ifstream cin("shopping.in");
    // ofstream cout("shopping.out");
    freopen("shopping.out", "w", stdout);
    cin >> n;
    REP (i, n) {
        VPI tmp;
        cin >> m;
        REP (j, m) {
            cin >> a >> b;
            tmp.pb(mp(a, b));
        }
        cin >> m;
        offers.pb(tmp);
        sale[i] = m;
    }
    cin >> m;
    REP(i, m) {
        cin >> a >> b >> c;
        PII tmp = mp(a, b);
        cost[a] = c;
        pos[a] = i;
        shopList.pb(tmp);
    }
    set  <pair <int, VPI> > bfs;
    set  <pair <int, VPI> > :: iterator it;
    bfs.insert(mp(0, shopList));
    ans = determineCost(shopList);
    // int temp = 0;
    while(!bfs.empty()) {
        // temp++;
        // if (temp == 100) break;
        cout << "---------------\n";
        it = bfs.begin();
        VPI cur = (*it).ss;
        int curCost = (*it).ff;
        int sum = findSum(cur);
        bfs.erase(it);
        print(cur);
        cout << curCost << '\n';
        if (curCost >= ans) continue;
        if (sum == 0) {
            ans = min(ans, curCost);
            continue;
        }
        REP(i, n) 
            if (possible(cur, i)) bfs.push(takeOffer(cur, i, curCost));
        ans = min(determineCost(cur) + curCost, ans);
    }
    cout << ans << '\n';
    return 0;
}