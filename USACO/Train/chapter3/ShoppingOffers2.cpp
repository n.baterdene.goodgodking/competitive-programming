/*
ID: n.bater1
LANG: C++14
TASK: shopping
*/
#include <bits/stdc++.h>
using namespace std;

// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))
#define FOR(i, a, b) for (int i = a; i < b; i++)
#define FORB(i, a, b) for (int i = a - 1; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i <= a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define pb push_back
#define mp make_pair
#define umap unordered_map
#define ff first
#define ss second
 
typedef long long ll;
typedef double d;
typedef unsigned uint;
typedef unsigned long long ull;
 
typedef vector<int> VI;
typedef vector<bool> VB;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<ll> VLL;
typedef vector<d> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<int, int> MIB;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VPI> VVPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
 
const int MOD = int(1e9) + 7;
const int INF = 0x3f3f3f3f;
const ll INFF = 0x3f3f3f3f3f3f3f3fLL;
const d EPS = 1e-9;
const d PI = acos(-1.0); //M_PI;
const int moveX[] = {-1, 0, 1, 0};
const int moveY[] = {0, 1, 0, -1};





int ans[10][10][10][10][10];
int n, m, a, b, c;
map <int, int> sale;
map <int, int> cost, pos;
VVPI offers;
VPI shopList;
VI limits(5, 0);
int determineCost(VPI& a) {
    int curAns = 0;
    REP(i, a.size()) 
        curAns += cost[a[i].ff] * a[i].ss;

    return curAns;
}
int buyAll(int i0, int i1, int i2, int i3, int i4) {
    VPI tmp;
    tmp.pb(mp(shopList[0].ff, i0));
    tmp.pb(mp(shopList[1].ff, i1));
    tmp.pb(mp(shopList[2].ff, i2));
    tmp.pb(mp(shopList[3].ff, i3));
    tmp.pb(mp(shopList[4].ff, i4));
    return determineCost(tmp);
}
void addAll(int j0, int j1, int j2, int j3, int j4) {
    REP(i, n) {
        VI idx(5);
        idx[0] = j0, idx[1] = j1, idx[2] = j2, idx[3] = j3, idx[4] = j4;
        REP(j, offers[i].size()) {
            PII tmp = offers[i][j];
            idx[pos[tmp.ff]] += tmp.ss;
        }
        if (idx[0] > limits[0] || idx[1] > limits[1] || idx[2] > limits[2] || idx[3] > limits[3] || idx[4] > limits[4]) continue;
        ans[idx[0]][idx[1]][idx[2]][idx[3]][idx[4]] = min(ans[idx[0]][idx[1]][idx[2]][idx[3]][idx[4]], ans[j0][j1][j2][j3][j4] + sale[i]);
    }
}
int main () {
    ifstream cin("shopping.in");
    ofstream cout("shopping.out");
    cin >> n;
    REP (i, n) {
        VPI tmp;
        cin >> m;
        REP (j, m) {
            cin >> a >> b;
            tmp.pb(mp(a, b));
        }
        cin >> m;
        offers.pb(tmp);
        sale[i] = m;
    }
    cin >> m;
    REP(i, m) {
        cin >> a >> b >> c;
        limits[i] = b;
        cost[a] = c;
        pos[a] = i;
        shopList.pb(mp(a, b));
    }
    while(shopList.size() < 5) shopList.pb(mp(0, 0));
    REP(i0, 6) 
    REP(i1, 6) 
    REP(i2, 6) 
    REP(i3, 6) 
    REP(i4, 6) 
        ans[i0][i1][i2][i3][i4] = INF;
    ans[0][0][0][0][0] = 0;
    REP(i0, 6) 
    REP(i1, 6) 
    REP(i2, 6) 
    REP(i3, 6) 
    REP(i4, 6) {
        if (i0 > limits[0] || i1 > limits[1] || i2 > limits[2] || i3 > limits[3] || i4 > limits[4]) continue;
        if (ans[i0][i1][i2][i3][i4] == INF) ans[i0][i1][i2][i3][i4] = buyAll(i0, i1, i2, i3, i4);
        addAll(i0, i1, i2, i3, i4);
    }
    cout << ans[shopList[0].ss ][shopList[1].ss ][shopList[2].ss ][shopList[3].ss ][shopList[4].ss ] << '\n';
    return 0;
}