/*
ID: n.bater1
LANG: C++14
TASK: spin
*/
#include <bits/stdc++.h>
using namespace std;

// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))
#define FOR(i, a, b) for (int i = a; i < b; i++)
#define FORB(i, a, b) for (int i = a - 1; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define pb push_back
#define mp make_pair
#define umap unordered_map
#define ff first
#define ss second
 
typedef long long ll;
typedef double d;
typedef unsigned uint;
typedef unsigned long long ull;
 
typedef vector<int> VI;
typedef vector<bool> VB;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<ll> VLL;
typedef vector<d> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<int, int> MIB;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
 
const int MOD = int(1e9) + 7;
const int INF = 0x3f3f3f3f;
const ll INFF = 0x3f3f3f3f3f3f3f3fLL;
const d EPS = 1e-9;
const d PI = acos(-1.0); //M_PI;
const int moveX[] = {-1, 0, 1, 0};
const int moveY[] = {0, 1, 0, -1};


int n = 5, m, a, b;
struct wheel {
    int speed;
    VPI holes;
};
vector <wheel> wheels(5);
bool go (int time) {
    REP(i, 360) {
        int count = 0;
        REP(j, 5) {
            REP(l, wheels[j].holes.size()) {
                int st = wheels[j].speed * time;
                a = (wheels[j].holes[l].ff + st) % 360;
                b = (wheels[j].holes[l].ff + st + (wheels[j].holes[l].ss % 360)) % 360;
                if (a > b) b += 360;
                int tmp = i;
                while (tmp < a) tmp += 360;
                if (a <= tmp && b >= tmp) {
                    count++;
                    break;
                }
            }
        }
        if (count == 5) return true;
    }
    return false;
}
int main () {
    ifstream in ("spin.in");
    ofstream out ("spin.out");
    REP(i, n) {
        in >> wheels[i].speed;
        in >> m;
        REP(j, m) {
            in >> a >> b;
            wheels[i].holes.pb(mp(a, b));
        }
    }
    REP(i, 360) {
        if(go(i)) {
            out << i << '\n';
            return 0;
        }
    }
    out << "none\n";
    return 0;
}