/*
ID: n.bater1
LANG: C++14
TASK: heritage
*/
#include <bits/stdc++.h>
using namespace std;

// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))
#define FOR(i, a, b) for (int i = a; i < b; i++)
#define FORB(i, a, b) for (int i = a - 1; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i <= a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define pb push_back
#define mp make_pair
#define umap unordered_map
#define ff first
#define ss second
 
typedef long long ll;
typedef double d;
typedef unsigned uint;
typedef unsigned long long ull;
 
typedef vector<int> VI;
typedef vector<bool> VB;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<ll> VLL;
typedef vector<d> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<int, int> MIB;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VPI> VVPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
 
const int MOD = int(1e9) + 7;
const int INF = 0x3f3f3f3f;
const ll INFF = 0x3f3f3f3f3f3f3f3fLL;
const d EPS = 1e-9;
const d PI = acos(-1.0); //M_PI;
const int moveX[] = {-1, 0, 1, 0};
const int moveY[] = {0, 1, 0, -1};
#ifdef ONLINE_JUDGE
#define cerr if (false) cerr
#endif 
#define debug(x) cerr << "Line Number:" <<__LINE__ << ": " << #x << " = " << (x) << endl

ifstream in("heritage.in");
ofstream out("heritage.out");

struct node {
    char val;
    node *l, *r;
    node(char x) {
        val = x;
        l = r = NULL;
    }
} *root;

void solve(string a, string b, node* head) {
    if (a.size() <= 1) return;
    string st1, st2, end1, end2;
    int len = a.find(b[0]);
    st1 = a.substr(0, len);
    st2 = b.substr(1, len);
    if (len + 1 < a.size()) {
        end1 = a.substr(len + 1);
        end2 = b.substr(len + 1);
    }

    node *left = NULL, *right = NULL;
    if (st2.size() > 0)       
        left = new node(st2[0]);

    if (end2.size() > 0)
        right = new node(end2[0]);
    head->l = left;
    head->r = right;
    solve(st1, st2, left);
    solve(end1, end2, right);
}
void print(node* head) {
    // out << "a\n";
    if (head->l != NULL) print(head->l);
    if (head->r != NULL) print(head->r);
    // out << "b\n";
    out << head->val;
}
int main() {
    string a, b;
    in >> a >> b;
    root = new node(b[0]);
    solve(a, b, root);
    print(root);
    out << '\n';
    return 0;
}