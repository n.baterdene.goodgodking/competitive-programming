/*
ID: n.bater1
LANG: C++14
TASK: contact
*/
#include <bits/stdc++.h>
#define pb push_back
#define mp make_pair
#define ff first
#define ss second
using namespace std;
bool myFunc(pair <int, string>& a, pair <int, string>& b ) {
    return a.ff > b.ff;
}
bool mySort(string&a, string&b) {
    if (a.size() != b.size()) return a.size() < b.size();
    return a < b;
}
int main () {
    ifstream in ("contact.in");
    ofstream out ("contact.out");
    int n, m, k, len;
    string a = "", b;
    in >> n >> m >> k;
    while(in >> b) a += b;
    len = a.size();
    map <string, int> count;
    for (int i = 0; i < len; i++) 
    for (int j = n; j <= m; j++) {
        if (j + i > len) break;
        count[a.substr(i, j)]++;
    }
    vector <pair <int, string> > res;
    for (auto el : count) 
        res.pb(mp(el.ss, el.ff));

    sort(res.begin(), res.end(), myFunc);
    map <int, vector <string> > ans;
    set <int> answer;
    for (int i = 0; ans.size() <= k && i < res.size(); i++) {
    	
        if (ans.size() == k && ans.find(res[i].ff) == ans.end()) break;
        ans[res[i].ff].pb(res[i].ss);
        answer.insert(res[i].ff);
    }
    if(answer.size() == 0) return 0;
    for (auto it = answer.rbegin(); it != answer.rend(); it++) {
        vector <string> cur = ans[*it];
        sort(cur.begin(), cur.end(), mySort);
        out << *it << endl;
        for (int i = 0; i < cur.size() - 1; i++) 
            if (i % 6 == 5) out << cur[i] << endl;
            else out << cur[i] << ' ';
        out << cur.back() << endl;
    }
    return 0;
}