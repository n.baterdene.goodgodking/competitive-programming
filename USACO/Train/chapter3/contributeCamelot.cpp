#include <bits/stdc++.h>
using namespace std;
#define mp make_pair
#define ff first
#define ss second
/* enough big number */
const int INF = 1e5;

// knight's movement
const int moveX[] = {-2,  2, -1,  1, -2, 2, -1, 1};
const int moveY[] = {-1, -1, -2, -2,  1, 1,  2, 2};

// rows ans columns
int n, m;

// distance to get for current knight to get to this square 
// first and second index: position
// third index : if it has king or not
vector <vector <vector <int> > > dist(30, vector <vector <int> >(30, vector <int>(2)));

// distance of a knight to meet the king 
vector <vector <int> > kingDist(30, vector <int>(30));

// total distance of all knights gathering here
vector <vector <int> > gather(30, vector <int>(30, 0));

//king's position
pair <int, int> king;

//set for bfs algorithm
set <pair <pair <int, int>, pair <int, int> > > bfs;

// knight's all possible movement from position (a, b)

void travel(int a, int b, int status) {
    int dis = dist[a][b][status], res = 0;
    for (int i = 0; i < 8; i++) {
        //new position
        int newX = a + moveX[i];
        int newY = b + moveY[i];

        //we shouldn't go out board
        if (newX < 0 || newY < 0 || newX >= m || newY >= n) continue;

        //if it's useful
        if (dist[newX][newY][status] > dis + 1) {
            res = 1;
            dist[newX][newY][status] = dis + 1;
            // add this position to bfs so we can go again
            bfs.insert(mp(mp(dist[newX][newY][status], status), mp(newX, newY)));
        }
    }
    // distance from king's position
    int distToKing = max(abs(king.ff - a), abs(king.ss - b));

    //  checking if we can meet the king here
    if (!status && dist[a][b][1] > dis + distToKing) {
        dist[a][b][1] = dis + distToKing;
        //add this position to bfs (pickerd king)
        pair <pair <int, int>, pair <int, int> > tmp = mp(mp(dist[a][b][1], 1), mp(a, b));
        bfs.insert(tmp);
    }
} 

void calcAllDistFromKnight(int a, int b) {

// initiate all positions to be enough big
    for (int i = 0; i < m; i++)
    for (int j = 0; j < n; j++) {
        dist[i][j][0] = INF;
        dist[i][j][1] = INF;
    }
    //distance to itself is zero
    dist[a][b][0] = 0;

    int distToKing = max(abs(king.ff - a), abs(king.ss - b));
    dist[a][b][1] = distToKing;

    //add this to bfs
    bfs.insert(mp(mp(0, 0), mp(a, b)));
    bfs.insert(mp(mp(dist[a][b][1], 1), mp(a, b)));

    // we don't need to go same place 2 time
    vector <vector <vector <int> > > vis(m, vector <vector <int> >(n, vector <int>(2, 0)));

    //bfs
    while(!bfs.empty()) {
        //we need to pick shortest one first so we used set
        pair <pair <int, int>, pair <int, int> >  tmp = *(bfs.begin());
        // first element is distance
        // second element is status(if we have king or not)
        //last 2 element is location
        pair <int, int> cur = tmp.ss, cur1 = tmp.ff;

        bfs.erase(bfs.begin());

        if (vis[cur.ff][cur.ss][cur1.ss]) continue;
        vis[cur.ff][cur.ss][cur1.ss] = 1;
        travel(cur.ff, cur.ss, cur1.ss);
    }
}
int main () {
    ifstream cin("camelot.in");
    ofstream cout("camelot.out");
    char tmp;
    int a, b;
    cin >> n >> m >> tmp >> b;
    //king position
    a = tmp - 'A';
    b--;
    //let's store king's position in a pair
    king = mp(a, b);
    // let's find distance from king  to all other position
    for (int i = 0; i < m; i++)
    for (int j = 0; j < n; j++)
        kingDist[i][j] = max(abs(king.ff - i), abs(king.ss - j));

    while (cin >> tmp >> b) {
        a = tmp - 'A';
        b--;
        // this function calculates all distances 
        calcAllDistFromKnight(a, b);
        for (int i = 0; i < m; i++)
        for (int j = 0; j < n; j++) {
            //if we chooose to gather all knights and king here, we have to add it's distance
            gather[i][j] += dist[i][j][0];
            
            // we need minimum cost to get king
            kingDist[i][j] = min(kingDist[i][j], dist[i][j][1] - dist[i][j][0]);
        }
    }
    //asnwer is lower than 1e5
    int ans = INF;

    // check all square if it's the best square to choose
    for (int i = 0; i < m; i++)
    for (int j = 0; j < n; j++)
        ans = min(ans, gather[i][j] + kingDist[i][j]);
    cout << ans << '\n';
    return 0;
}