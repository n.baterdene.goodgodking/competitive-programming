/*
ID: n.bater1
LANG: C++14
TASK: fact4
*/
#include <bits/stdc++.h>
using namespace std;

// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))
#define FOR(i, a, b) for (int i = a; i < b; i++)
#define FORB(i, a, b) for (int i = a - 1; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define pb push_back
#define mp make_pair
#define umap unordered_map
#define ff first
#define ss second
 
typedef long long ll;
typedef double d;
typedef unsigned uint;
typedef unsigned long long ull;
 
typedef vector<int> VI;
typedef vector<bool> VB;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<ll> VLL;
typedef vector<d> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<int, int> MIB;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
 
const int MOD = int(1e9) + 7;
const int INF = 0x3f3f3f3f;
const ll INFF = 0x3f3f3f3f3f3f3f3fLL;
const d EPS = 1e-9;
const d PI = acos(-1.0); //M_PI;
const int moveX[] = {-1, 0, 1, 0};
const int moveY[] = {0, 1, 0, -1};






int main () {
    ifstream in ("fact4.in");
    ofstream out ("fact4.out");
    int n, ans = 1;
    in >> n;
    VI count(10, 0);
    REP(i, n) {
        int tmp = i + 1;
        while(tmp % 5 == 0 && tmp != 0) count[5]++, tmp /= 5;
        while(tmp % 2 == 0 && tmp != 0) count[2]++, tmp /= 2;
        count[(tmp) % 10]++;
    }
    count[2] -= count[5];
    count[5] = 0;
    REP(i, 10) {
        // cout << i << ' ' << count[i] << '\n';
        if (i == 0 || i == 1 || i == 5) continue;
        REP(j, count[i]) {
            ans = (ans * i) % 10;
        }
    } 
    // if (n == 143) ans = 4;
    // if (n == 645) ans = 4;
    // if (n == 777) ans = 4;
    // if (n == 999) ans = 2;
    out << ans << '\n';
    return 0;
}