/*
ID: n.bater1
LANG: C++14
TASK: range
*/
#include <bits/stdc++.h>
using namespace std;

// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))
#define FOR(i, a, b) for (int i = a; i < b; i++)
#define FORB(i, a, b) for (int i = a - 1; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i <= a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define pb push_back
#define mp make_pair
#define umap unordered_map
#define ff first
#define ss second
 
typedef long long ll;
typedef double d;
typedef unsigned uint;
typedef unsigned long long ull;
 
typedef vector<int> VI;
typedef vector<bool> VB;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<ll> VLL;
typedef vector<d> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<int, int> MIB;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VPI> VVPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
 
const int MOD = int(1e9) + 7;
const int INF = 0x3f3f3f3f;
const ll INFF = 0x3f3f3f3f3f3f3f3fLL;
const d EPS = 1e-9;
const d PI = acos(-1.0); //M_PI;
const int moveX[] = {-1, 0, 1, 0};
const int moveY[] = {0, 1, 0, -1};
#ifdef ONLINE_JUDGE
#define cerr if (false) cerr
#endif 
#define debug(x) cerr << "Line Number:" <<__LINE__ << ": " << #x << " = " << (x) << endl




VVI land(255, VI(255)), sums(255, VI(255, 0));
VI ans(255, 0);
int n;
void add(int range) {
    int cur = 1;
    while(range) {
        ans[range] += (cur * cur);
        range--;
    }
}
bool check(int i, int j, int range) {
    if (range > min(n - 1 - i, n - 1 - j)) return 0;
    int sum = sums[i + range][j + range];

    if (i > 0) sum -= sums[i - 1][j + range];
    if (j > 0) sum -= sums[i + range][j - 1];
    if (i > 0 && j > 0) sum += sums[i - 1][j - 1];
    // cout << sum << '\n';
    range++;
    if (sum == range * range) {
        // cout << i << ' ' << j << ' ' << range << '\n';
        ans[range]++;
        // add(range);
        return 1;
    }
    return 0;
}
int main () {
    ifstream cin("range.in");
    ofstream cout("range.out");
    cin >> n;
    char a;
    REP(i, n)
    REP(j, n) {
        cin >> a;
        land[i][j] = a - '0';
    }

    REP(i, n) {
        int sum = 0;
        REP(j, n) {
            sum += land[i][j];
            if (i - 1 >= 0) sums[i][j] = sum + sums[i - 1][j];
            else sums[i][j] = sum;
        }
    }
    
    REP(i, n) {
        REP(j, n) {
            REP1(range, n) {
                if (!check(i, j, range)) break;
            }
        }
    }
    FOR(i, 2, n + 1) {
        if (ans[i] == 0) {
            break;
        }
        cout << i << ' ' << ans[i] << '\n';
    }
    return 0;
}