/*
ID: n.bater1
LANG: C++14
TASK: msquare
*/
#include <bits/stdc++.h>
using namespace std;

// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))
#define FOR(i, a, b) for (int i = a; i < b; i++)
#define FORB(i, a, b) for (int i = a - 1; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define pb push_back
#define mp make_pair
#define umap unordered_map
#define ff first
#define ss second
 
typedef long long ll;
typedef double d;
typedef unsigned uint;
typedef unsigned long long ull;
 
typedef vector<int> VI;
typedef vector<bool> VB;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<ll> VLL;
typedef vector<d> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<int, int> MIB;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
 
const int MOD = int(1e9) + 7;
const int INF = 0x3f3f3f3f;
const ll INFF = 0x3f3f3f3f3f3f3f3fLL;
const d EPS = 1e-9;
const d PI = acos(-1.0); //M_PI;
const int moveX[] = {-1, 0, 1, 0};
const int moveY[] = {0, 1, 0, -1};





int n = 8;
VI res(n), cur;
queue <pair <pair <int, VI>, VC > > bfs;
void A(VI cur, int&a, VC last) {
    last.pb('A');
    swap(cur[0], cur[7]);
    swap(cur[1], cur[6]);
    swap(cur[2], cur[5]);
    swap(cur[3], cur[4]);
    bfs.push(mp(mp(a + 1, cur), last));
}
void B(VI cur, int&a, VC last) {
    last.pb('B');
    int x = cur[3], y = cur[4];
    cur[3] = cur[2], cur[4] = cur[5];
    cur[2] = cur[1], cur[5] = cur[6];
    cur[1] = cur[0], cur[6] = cur[7];
    cur[0] = x, cur[7] = y;
    bfs.push(mp(mp(a + 1, cur), last));
}
void C(VI cur, int&a, VC last) {
    last.pb('C');
    int x = cur[1];
    cur[1] = cur[6];
    cur[6] = cur[5];
    cur[5] = cur[2];
    cur[2] = x;
    bfs.push(mp(mp(a + 1, cur), last));
}
int main () {
    ifstream in ("msquare.in");
    ofstream out ("msquare.out");
    REP(i, n) {
        in >> res[i];
        cur.pb(i + 1);
    }
    VC emp;
    bfs.push(mp(mp(0, cur), emp));
    map <VI, bool> seen;
    while(1) {
        int a = bfs.front().ff.ff;
        VI tmp = bfs.front().ff.ss;
        VC last = bfs.front().ss;
        bfs.pop();
        if (seen.find(tmp) != seen.end()) continue;
        seen[tmp] = true;
        if (tmp == res) {
            out << a << '\n';
            REP(i, a) out << last[i];
            out << '\n';
            return 0;
        }
        A(tmp, a, last);
        B(tmp, a, last);
        C(tmp, a, last);
    }

    return 0;
}