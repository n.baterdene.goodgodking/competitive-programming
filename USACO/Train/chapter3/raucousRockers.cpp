/*
ID: n.bater1
LANG: C++14
TASK: rockers
*/
#include <bits/stdc++.h>
using namespace std;

// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))
#define FOR(i, a, b) for (int i = a; i < b; i++)
#define FORB(i, a, b) for (int i = a - 1; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i <= a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define pb push_back
#define mp make_pair
#define umap unordered_map
#define ff first
#define ss second
 
typedef long long ll;
typedef double d;
typedef unsigned uint;
typedef unsigned long long ull;
 
typedef vector<int> VI;
typedef vector<bool> VB;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<ll> VLL;
typedef vector<d> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<int, int> MIB;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VPI> VVPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
 
const int MOD = int(1e9) + 7;
const int INF = 0x3f3f3f3f;
const ll INFF = 0x3f3f3f3f3f3f3f3fLL;
const d EPS = 1e-9;
const d PI = acos(-1.0); //M_PI;
const int moveX[] = {-1, 0, 1, 0};
const int moveY[] = {0, 1, 0, -1};
#ifdef ONLINE_JUDGE
#define cerr if (false) cerr
#endif 
#define debug(x) cerr << "Line Number:" <<__LINE__ << ": " << #x << " = " << (x) << endl


VI songs;
int n, m, k, tmp;
ofstream out("rockers.out");

int solve(int cur, int ans, int used, int left) {
    if (used > m) return ans - 1;   
    if (used == m || cur >= songs.size()) {
        if (left > 0 && used == m) return ans - 1;
        return ans;
    }
    int tmp = ans;
    if (left >= songs[cur])
        tmp = max(tmp, solve(cur + 1, ans + 1, used, left - songs[cur]));
    else 
        tmp = max(tmp, solve(cur + 1, ans + 1, used + 1, k - songs[cur]));
    return max(tmp, solve(cur + 1, ans, used, left));
}
int main() {
    ifstream cin("rockers.in");
    ofstream cout("rockers.out");
    cin >> n >> k >> m;
    REP(i, n) {
        cin >> tmp;
        if (tmp > k) continue;
        songs.pb(tmp);
    }
    cout << solve(0, 0, 0, k) << '\n';
	return 0;
}