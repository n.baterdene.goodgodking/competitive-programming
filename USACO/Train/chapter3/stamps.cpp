/*
ID: n.bater1
LANG: C++14
TASK: stamps
*/
#include <bits/stdc++.h>
using namespace std;

// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))
#define FOR(i, a, b) for (int i = a; i < b; i++)
#define FORB(i, a, b) for (int i = a - 1; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define pb push_back
#define mp make_pair
#define umap unordered_map
#define ff first
#define ss second
 
typedef long long ll;
typedef double d;
typedef unsigned uint;
typedef unsigned long long ull;
 
typedef vector<int> VI;
typedef vector<bool> VB;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<ll> VLL;
typedef vector<d> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<int, bool> MIB;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
 
const int MOD = int(1e9) + 7;
const int INF = 0x3f3f3f3f;
const ll INFF = 0x3f3f3f3f3f3f3f3fLL;
const d EPS = 1e-9;
const d PI = acos(-1.0); //M_PI;
const int moveX[] = {-1, 0, 1, 0};
const int moveY[] = {0, 1, 0, -1};






int main () {
    ifstream in ("stamps.in");
    ofstream out ("stamps.out");
    int n, m;
    in >> m >> n;
    VI stamps(n), pos(2e6 + 5, 0);
    REP(i, n) {
        in >> stamps[i];
        pos[stamps[i]] = 1;
    }
    sort(ALL(stamps));
    int cur = 1;
    REPB(i, n) {
        REP(j, 2e6 + 5) {
            if (pos[j] && pos[j] < m) {
                if (!pos[j + stamps[i]]) {
                    pos[j + stamps[i]] = pos[j] + 1;
                } else {
                    pos[j + stamps[i]] = min(pos[j + stamps[i]], pos[j] + 1);
                }
            }
        }
    }
    REP(i, 2e6 + 5) {
        if (!pos[i + 1]) {
            out << i << '\n';
            break;
        }
    }
    return 0;
}