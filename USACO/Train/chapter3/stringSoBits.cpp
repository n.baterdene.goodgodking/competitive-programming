/*
ID: n.bater1
LANG: C++14
TASK: kimbits
*/
#include <bits/stdc++.h>
using namespace std;

// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))
#define FOR(i, a, b) for (int i = a; i < b; i++)
#define FORB(i, a, b) for (int i = a - 1; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define pb push_back
#define mp make_pair
#define umap unordered_map
#define ff first
#define ss second
 
typedef long long ll;
typedef double d;
typedef unsigned uint;
typedef unsigned long long ull;
 
typedef vector<int> VI;
typedef vector<bool> VB;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<ll> VLL;
typedef vector<VLL> VVLL;
typedef vector<d> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<int, int> MIB;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
 
const int MOD = int(1e9) + 7;
const int INF = 0x3f3f3f3f;
const ll INFF = 0x3f3f3f3f3f3f3f3fLL;
const d EPS = 1e-9;
const d PI = acos(-1.0); //M_PI;
const int moveX[] = {-1, 0, 1, 0};
const int moveY[] = {0, 1, 0, -1};

VVLL counts(33, VLL(33, 0));
void myFunc(ll n, ll m) {
    REP(i, 32) counts[i][0] = counts[0][i] = 1;
    REP(i, 32) 
    REP(j, 33) {
        counts[i + 1][j] = counts[i][j];
        if (j != 0) counts[i + 1][j] += counts[i][j - 1];
    }
}
int main () {
    ifstream in ("kimbits.in");
    ofstream out ("kimbits.out");
    ll n, m, k;
    in >> n >> m >> k;
    myFunc(32, 32);
    k--;
    REP(i, n) {
        // cout << n - i << ' ' << m << ' ' << k << '\n';
        ll sum = counts[n - i - 1][m];
        if (sum <= k) {
            out << 1;
            m--, k -= sum;
        } else 
            out << 0;
    }
    out << '\n';
    return 0;
}