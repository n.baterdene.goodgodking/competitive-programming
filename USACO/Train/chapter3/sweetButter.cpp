/*
ID: n.bater1
LANG: C++14
TASK: butter
*/
#include <bits/stdc++.h>
using namespace std;

// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))
#define FOR(i, a, b) for (int i = a; i < b; i++)
#define FORB(i, a, b) for (int i = a - 1; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i <= a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define pb push_back
#define mp make_pair
#define umap unordered_map
#define ff first
#define ss second
 
typedef long long ll;
typedef double d;
typedef unsigned uint;
typedef unsigned long long ull;
 
typedef vector<int> VI;
typedef vector<bool> VB;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<ll> VLL;
typedef vector<d> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<int, int> MIB;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VPI> VVPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
 
const int MOD = int(1e9) + 7;
const int INF = 0x3f3f3f3f;
const ll INFF = 0x3f3f3f3f3f3f3f3fLL;
const d EPS = 1e-9;
const d PI = acos(-1.0); //M_PI;
const int moveX[] = {-1, 0, 1, 0};
const int moveY[] = {0, 1, 0, -1};





const int limit = 801;
int n, p, m;
VI cow(limit);
VVPI paths(limit);
VVI dist(limit, VI(limit, INF));
 
void myFunc(int st) {
    queue <int> bfs;
    VB vis(limit, 0);
    bfs.push(st);
    while(!bfs.empty()) {
        int cur = bfs.front();
        bfs.pop();
        REP(i, paths[cur].size()) {
            PII tmp = paths[cur][i];
            if (dist[st][cur] + paths[cur][i].ss < dist[st][tmp.ff]) {
                dist[st][tmp.ff] = dist[st][cur] + paths[cur][i].ss;
                if (!vis[tmp.ff]) {
                    bfs.push(tmp.ff);
                    vis[tmp.ff] = 1;
                }
            }
        }
        vis[cur] = 0;
    }
}
 
int main()
{
#ifndef LOCAL
    ifstream cin("butter.in");
    ofstream cout("butter.out");
#endif  
    cin >> n >> p >> m;

    REP(i, n) cin >> cow[i];

    int a, b, c;
    REP(i, m){
        cin >> a >> b >> c;
        paths[a].pb(mp(b, c));
        paths[b].pb(mp(a, c));
    }
    REP1(i, p) dist[i][i] = 0;
    REP1(i, p) myFunc(i);

    int ans = INF;
    REP1(i, p){
        int curAns = 0;
        REP(j, n) curAns += dist[i][cow[j]];
        ans = min(ans, curAns);
    }
    cout << ans << endl;
    return 0;
}