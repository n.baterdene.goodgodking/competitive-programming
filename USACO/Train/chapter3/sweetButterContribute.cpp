/*
ID: n.bater1
LANG: C++14
TASK: butter
*/
#include <bits/stdc++.h>

#define pb push_back
#define mp make_pair
#define ff first
#define ss second

using namespace std;

const int INF = INT_MAX;
const int limit = 805; // maximum is 800 so I used 805 with extra 4 space just in case
int cows, pastures, roads;
vector <int> cow(limit); // position of the cows
vector <vector <pair <int, int> > > paths(limit); // all paths of i-th pasture
vector <vector <int> > dist(limit, vector <int>(limit, INF)); // massive for storing shortest path from a to b (dist[a][b])
 
void travel(int st) {
    queue <int> bfs; // lets use bfs from pasture st (start)
    vector<bool> vis(limit, 0); // we don't have to go already visited pasture so let's memorize it using vis
    bfs.push(st); // starting at st

    while(!bfs.empty()) { // bfs
        int cur = bfs.front();
        bfs.pop();
        for (int i = 0; i < paths[cur].size(); i++) {
            pair <int, int> tmp = paths[cur][i]; // cur-th pasture's I-th path,  first -> pastureNumber, second -> distance
            int stToNext = dist[st][cur] + paths[cur][i].ss; // total distance of st to next pasture
            if (stToNext < dist[st][tmp.ff]) { // if it isn't shortcut we don't need it
                dist[st][tmp.ff] = stToNext; 
                if (!vis[tmp.ff]) { 
                    bfs.push(tmp.ff); 
                    vis[tmp.ff] = 1;
                }
            }
        }
        vis[cur] = 0;
    }
}
 
int main() {
    ifstream cin("butter.in");
    ofstream cout("butter.out");
    cin >> cows >> pastures >> roads;
    for (int i = 0; i < cows; i++)
        cin >> cow[i];

    int a, b, cost;
    // inializing paths
    for (int i = 0; i < roads; i++) {
        cin >> a >> b >> cost;
        paths[a].pb(mp(b, cost)); 
        paths[b].pb(mp(a, cost));
    }
    for (int i = 1; i <= pastures; i++) dist[i][i] = 0; // distance from a to a is 0

    for (int i = 1; i <= pastures; i++) 
        travel(i); // finding all distances of I-th pasture

    int ans = INF; // big number
    for (int i = 1; i <= pastures; i++) {
        int curAns = 0;
        for (int j = 0; j < cows; j++)   //finding total distance
            curAns += dist[i][cow[j]];
        ans = min(ans, curAns); // lowest distance is answer
    }
    cout << ans << endl;
    return 0;
}