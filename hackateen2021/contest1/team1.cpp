#include<bits/stdc++.h>

#define ll long long
#define pb push_back
#define f first
#define s second

using namespace std;
const int N = 17;
ll dp[1<<N];
ll a[N][N];
int n;
int main() {
    ios::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    cin >> n;
    for(int i = 0; i < n; i++)
    for(int j = 0;j < n; j++)
        cin >> a[i][j];

    for (int i = 0; i < (1 << n); i++) 
    for (int j = 0; j < n; j++) {
        if (i & (1 << j)) {
            for (int k = j + 1 ; k < n; k++) {
                if (i & (1 << k)) dp[i] += a[j][k];
            }
        }
    }
    for(int i = 0; i < (1 << n); i++)
    for(int sub = i; sub > 0; sub = (sub-1) & i)
        dp[i] = max(dp[i], dp[i^sub] + dp[sub]);
    
} 