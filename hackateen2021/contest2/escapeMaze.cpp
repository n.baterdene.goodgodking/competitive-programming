#include <bits/stdc++.h>
#define int long long
using namespace std;
// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))

#define _upgrade ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define II <int, int>
#define Q queue
#define ff first
#define bk back()
#define ss second
#define rs resize
#define ins insert 
#define fr front() 
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define PQ priority_queue
#define umap unordered_map
#define sz(x) (int)x.size()

typedef long long ll;
typedef double db;
typedef unsigned uint;
typedef unsigned long long ull;
typedef unordered_map<int, int> umapII;
// PQ going up <int, VI, greater<int> >
typedef vector<int> VI;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<bool> VB;
typedef vector<VB> VVB;
typedef vector<VVB> VVVB;
typedef vector<umapII> VumapII;
typedef vector<ll> VLL;
typedef vector<VLL> VVLL;
typedef vector<VVLL> VVVLL;
typedef vector<VVVLL> VVVVLL;
typedef vector<db> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VPI> VVPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
typedef vector<VVVI> VVVVI;
typedef vector<VVVVI> VVVVVI;
// mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
 
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
void readI(int& a) {
    cin >> a;
}
void readI(int& a, int&b) {
    cin >> a >> b;
}
void readI(int& a, int&b, int&c) {
    cin >> a >> b >> c;
}
VI readVI(int n) {
    VI a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVI readVVI(int n, int m) {
    VVI a(n, VI(m));
    REP(i, n) a[i] = readVI(m);
    return a;
}
VLL readVLL(ll n) {
    VLL a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVLL readVVLL(ll n, ll m) {
    VVLL a(n, VLL(m));
    REP(i, n) a[i] = readVLL(m);
    return a;
}


int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}

void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}

void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
const int MOD = 1e9 + 7;
const int INF = 2e9;
const ll INFF = INT64_MAX;
const db EPS = 1e-9;
const db PI = acos(-1.0); //M_PI;
const int moveX[] = {-1, 0, 1, 0};
const int moveY[] = {0, 1, 0, -1};
/*
|      ⣠⣶⡾⠏⠉⠙⠳⢦⡀⠀  ⠀⢠⠞⠉⠙⠲⡀
|  ⠀⠀⣴⠿⠏⠀⠀⠀⠀⠀⠀⢳⡀⠀  ⡏⠀⠀ ⠀⠀ ⢷
|⠀⠀⢠⣟⣋⡀⢀⣀⣀⡀⠀⣀⡀⣧⠀  ⢸⠀⠀⠀⠀   ⡇ ⠀
| ⠀⢸⣯⡭⠁⠸⣛⣟⠆⡴⣻⡲⣿   ⣸⠀⠀OK.  ⡇
|⠀⠀⣟⣿⡭⠀⠀⠀⠀⠀⢱⠀⠀⣿   ⢹⠀⠀⠀⠀⠀ ⡇
|⠀⠀⠙⢿⣯⠄⠀⠀⠀⢀⡀⠀⠀⡿⠀  ⡇⠀⠀ ⠀ ⡼
|⠀⠀⠀⠀⠹⣶⠆⠀⠀⠀⠀⠀⡴⠃ ⠀ ⠘⠤⣄⣠⠞⠀ ⠀
|⠀⠀⠀⠀⢸⣷⡦⢤⡤⢤⣞⣁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ ⠀
|⠀⢀⣤⣴⣿⣏⠁⠀⠀⠸⣏⢯⣷⣖⣦⡀⠀⠀⠀⠀⠀⠀
|⢀⣾⣽⣿⣿⣿⣿⠛⢲⣶⣾⢉⡷⣿⣿⠵⣿⠀⠀⠀⠀⠀
|⣼⣿⠍⠉⣿⡭⠉⠙⢺⣇⣼⡏⠀ ⠀⣄⢸⣿
|⣿⣿⣧⣀⣿.........⣀⣰⣏⣘⣆⣀
      
|                                                   _         _   _
|                                                   (_)       | | | |
|_ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_
| '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|
| |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_
| .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|
| |               __/ |
|_|              |___/
 _   _      _ _    __        __         _     _ 
| | | | ___| | | __\ \      / /__  _ __| | __| |
| |_| |/ _ \ | |/ _ \ \ /\ / / _ \| '__| |/ _` |
|  _  |  __/ | | (_) \ V  V / (_) | |  | | (_| |
|_| |_|\___|_|_|\___/ \_/\_/ \___/|_|  |_|\__,_|                                  
 ____     __    _______    _______   __________
|     \  |  |  |  _____|  /  _____) |____  ____|
|  |\  \ |  |  | |__     (  (_____      |  |
|  | \  \|  |  |  __|     \_____  \     |  | 
|  |  \     |  | |_____    _____)  )    |  | 
|__|   \____|  |_______|  (_______/     |__| 
 
 
 


*/






const int N = 100;
int n, m;
void print(VS& x) {
    int y = 0;
    for (auto el : x) {
        if (++y > n) break;
        cout << el << '\n';
    }
    cout << '\n';
}
VS maze(N+5), tmp;
VVB vis(N+5, VB(N+5, 0));
int solve(PII st) {
    int ans;
    REP(i, n+5)
    REP(j, m+5) vis[i][j] = 0;

    set <VI> bfs;
    bfs.insert({0, st.ff, st.ss});
    PII last;
    while(!bfs.empty()) {
        VI cur = *bfs.begin();
        bfs.erase(cur);
        int x = cur[1], y = cur[2], z = cur[0];
        if (vis[x][y]) continue;
        last = {x, y};
        vis[x][y] = 1;
        if (x == 0 || x == n - 1 || y == 0 || y == m - 1) {
            ans = z;
            break;
        }
        vis[x][y] = 1;
        REP(i, 4) {
            int xx = x + moveX[i];
            int yy = y + moveY[i];
            int zz = z;
            if (xx < 0 || xx >= n || yy < 0 || yy >= m || maze[xx][yy] == '*' || vis[xx][yy]) continue;
            if (maze[xx][yy] == '#') zz++;
            bfs.insert({zz, xx, yy});
        }
    }
    return ans;
}
VVVI out(N+5, VVI(N+5, VI(3, INF))); // [i][j] -> 
void preProcess(VPI& ends, int idx) {
    REP(i, n+5)
    REP(j, m+5) vis[i][j] = 0;
    set <VI> bfs;
    for (auto st : ends) 
        bfs.insert({(maze[st.ff][st.ss] == '#' ? 1 : 0), st.ff, st.ss});
    
    while(!bfs.empty()) {
        VI cur = *bfs.begin();
        bfs.erase(cur);
        int x = cur[1], y = cur[2], z = cur[0];
        if (vis[x][y]) continue;
        vis[x][y] = 1;
        out[x][y][idx] = z;
        // cout << x << ' ' << y << ' ' << idx << ": " << z << '\n';
        REP(i, 4) {
            int xx = x + moveX[i];
            int yy = y + moveY[i];
            int zz = z;
            if (xx < 0 || xx >= n || yy < 0 || yy >= m || maze[xx][yy] == '*' || vis[xx][yy]) continue;
            if (maze[xx][yy] == '#') zz++;
            bfs.insert({zz, xx, yy});
        }
    }
}
void go () {
    cin >> n >> m;
    REP(i, n) cin >> maze[i];
    PII st = {-1, -1}, st1 = {-1, -1};
    VPI ends;
    REP(i, n)
    REP(j, m) {
        if ((i == 0 || i == n - 1 || j == 0 || j == m - 1) && maze[i][j] != '*') {
            ends.pb({i, j});
        }
        if (maze[i][j] == '$') {
            if (st.ff == -1) st = {i, j};
            else st1 = {i, j};
        }
    }
    ll ans = INF;
    if (st1.ff != -1) {
        preProcess(ends, 0);
        ends.clear();

        ends.pb(st);
        preProcess(ends, 1);
        ends.clear();

        ends.pb(st1);
        preProcess(ends, 2);

        ans = out[st.ff][st.ss][0] + out[st1.ff][st1.ss][0];
        REP(i, n)
        REP(j, m) {
            if (maze[i][j] == '*') continue;
            ll curAns = (maze[i][j] == '#' ? -2 : 0); 
            REP(k, 3) curAns += out[i][j][k];
            ans = min(ans, curAns);
        }
    } else {
        ans = solve(st);
    }
    cout << ans << '\n';
}



signed main () {

#ifdef ONLINE_JUDGE
#else
    // freopen("in.txt", "r", stdin);
//	freopen("out.txt", "w", stdout);
#endif
    _upgrade
    int T = 1;
    while(T--) go();
    return 0;
}

/* stuff you should look for
	* int overflow, array bounds
	* special cases (n=1?)
    
*/