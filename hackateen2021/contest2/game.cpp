#include <bits/stdc++.h>
using namespace std;
// #define FOR(i, begin, end) for (__typeof(end) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))

#define _upgrade ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define II <int, int>
#define Q queue
#define ff first
#define bk back()
#define ss second
#define rs resize
#define ins insert 
#define fr front() 
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define PQ priority_queue
#define umap unordered_map
#define sz(x) (int)x.size()

typedef long long ll;
typedef double db;
typedef unsigned uint;
typedef unsigned long long ull;
typedef unordered_map<int, int> umapII;
// PQ going up <int, VI, greater<int> >
typedef vector<int> VI;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<bool> VB;
typedef vector<VB> VVB;
typedef vector<VVB> VVVB;
typedef vector<umapII> VumapII;
typedef vector<ll> VLL;
typedef vector<VLL> VVLL;
typedef vector<VVLL> VVVLL;
typedef vector<VVVLL> VVVVLL;
typedef vector<db> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef map<int, int> MII;
typedef map<string, int> MSI;
typedef pair<int, int> PII;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VPI> VVPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
typedef vector<VVVI> VVVVI;
typedef vector<VVVVI> VVVVVI;
// mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
 
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
void readI(int& a) {
    cin >> a;
}
void readI(int& a, int&b) {
    cin >> a >> b;
}
void readI(int& a, int&b, int&c) {
    cin >> a >> b >> c;
}
VI readVI(int n) {
    VI a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVI readVVI(int n, int m) {
    VVI a(n, VI(m));
    REP(i, n) a[i] = readVI(m);
    return a;
}
VLL readVLL(ll n) {
    VLL a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVLL readVVLL(ll n, ll m) {
    VVLL a(n, VLL(m));
    REP(i, n) a[i] = readVLL(m);
    return a;
}


int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}

void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}

void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
const int MOD = 1e9 + 7;
const int INF = 2e9;
const ll INFF = INT64_MAX;
const db EPS = 1e-9;
const db PI = acos(-1.0); //M_PI;
const int moveX[] = {-1, 0, 1, 0};
const int moveY[] = {0, 1, 0, -1};
/*
|      ⣠⣶⡾⠏⠉⠙⠳⢦⡀⠀  ⠀⢠⠞⠉⠙⠲⡀
|  ⠀⠀⣴⠿⠏⠀⠀⠀⠀⠀⠀⢳⡀⠀  ⡏⠀⠀ ⠀⠀ ⢷
|⠀⠀⢠⣟⣋⡀⢀⣀⣀⡀⠀⣀⡀⣧⠀  ⢸⠀⠀⠀⠀   ⡇ ⠀
| ⠀⢸⣯⡭⠁⠸⣛⣟⠆⡴⣻⡲⣿   ⣸⠀⠀OK.  ⡇
|⠀⠀⣟⣿⡭⠀⠀⠀⠀⠀⢱⠀⠀⣿   ⢹⠀⠀⠀⠀⠀ ⡇
|⠀⠀⠙⢿⣯⠄⠀⠀⠀⢀⡀⠀⠀⡿⠀  ⡇⠀⠀ ⠀ ⡼
|⠀⠀⠀⠀⠹⣶⠆⠀⠀⠀⠀⠀⡴⠃ ⠀ ⠘⠤⣄⣠⠞⠀ ⠀
|⠀⠀⠀⠀⢸⣷⡦⢤⡤⢤⣞⣁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ ⠀
|⠀⢀⣤⣴⣿⣏⠁⠀⠀⠸⣏⢯⣷⣖⣦⡀⠀⠀⠀⠀⠀⠀
|⢀⣾⣽⣿⣿⣿⣿⠛⢲⣶⣾⢉⡷⣿⣿⠵⣿⠀⠀⠀⠀⠀
|⣼⣿⠍⠉⣿⡭⠉⠙⢺⣇⣼⡏⠀ ⠀⣄⢸⣿
|⣿⣿⣧⣀⣿.........⣀⣰⣏⣘⣆⣀
      
|                                                   _         _   _
|                                                   (_)       | | | |
|_ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_
| '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|
| |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_
| .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|
| |               __/ |
|_|              |___/
 _   _      _ _    __        __         _     _ 
| | | | ___| | | __\ \      / /__  _ __| | __| |
| |_| |/ _ \ | |/ _ \ \ /\ / / _ \| '__| |/ _` |
|  _  |  __/ | | (_) \ V  V / (_) | |  | | (_| |
|_| |_|\___|_|_|\___/ \_/\_/ \___/|_|  |_|\__,_|                                  
 ____     __    _______    _______   __________
|     \  |  |  |  _____|  /  _____) |____  ____|
|  |\  \ |  |  | |__     (  (_____      |  |
|  | \  \|  |  |  __|     \_____  \     |  | 
|  |  \     |  | |_____    _____)  )    |  | 
|__|   \____|  |_______|  (_______/     |__| 
 
 
 


*/









int n, m;
const int N = 55;
const int M = 15;
vector <pair <int, string> > queries;
string answer;
VVI tmp;
void sub(VI possible, int need, int st, VI& cur) {
    if (need == 0){
        tmp.pb(cur);
        return;
    }
    for (int i = st; i < possible.size(); i++) {
        cur.push_back(possible[i]);
        sub(possible, need - 1, i + 1, cur);
        cur.pop_back();
    }
}
VI left(VI&a, VI&b) {
    map <int, bool> seen;
    for (auto el : b) seen[el] = 1;
    VI res;
    for (auto el : a) {
        if (seen.find(el) == seen.end()) res.pb(el);
    }
    return res;
}
int solve(VI&correct, VVB&incorrect, int idx) {
    if (idx == n) {
        ll res = 1;
        string cur = "";
        REP(i, m) {
            if (correct[i] != -1) {
                cur += char(correct[i] + '0');
                continue;
            }
            int cnt = 0;
            for (int j = 0; j < 10; j++) {
                if (!incorrect[i][j]) {
                    if (cnt == 0) cur += char(j + '0');
                    cnt++;
                }
            }
            res *= cnt;
            if (res == 0) break;
        }
        if (res > 0) answer = cur;
        // cout << answer << ": " << res << '\n';
        return res;

    }
    int bulls = 0;
    VI possible;
    REP(i, m)  {
        if (correct[i] >= 0 && correct[i] == queries[idx].ss[i] - '0') bulls++;
        if (correct[i] == -1 && !incorrect[i][queries[idx].ss[i] - '0']) possible.pb(i);
    }
    int need = queries[idx].ff - bulls;
    if (bulls > queries[idx].ff || need > possible.size()) return 0;
    int res = 0;
    if (need == 0) {
        VI tmpChange;
        for (auto el : possible) {
            if (!incorrect[el][queries[idx].ss[el] - '0']) {
                incorrect[el][queries[idx].ss[el] - '0'] = 1;
                tmpChange.pb(el);
            }
        }
        res += solve(correct, incorrect, idx+1);
        for (auto el : tmpChange) {
            incorrect[el][queries[idx].ss[el] - '0'] = 0;
        }
    } else {
        VI gege;
        sub(possible, need, 0, gege);
        VVI subsets = tmp;
        tmp.clear();
        for (auto el : subsets) {
            VI tmp = left(possible, el);
            for (auto el1 : el) {
                correct[el1] = queries[idx].ss[el1] - '0';
            }
            VI tmpChange;
            for (auto el1 : tmp) {
                if (!incorrect[el1][queries[idx].ss[el1] - '0']) {
                    incorrect[el1][queries[idx].ss[el1] - '0'] = 1;
                    tmpChange.pb(el1);
                }
            }
            res += solve(correct, incorrect, idx+1);
            for (auto el1 : el) {
                correct[el1] = -1;
            }
            for (auto el : tmpChange) {
                incorrect[el][queries[idx].ss[el] - '0'] = 0;
            }
        }
    }
    return res;
}
void go () {
    cin >> n >> m;
    REP(i, n) {
        string x;
        int y;
        cin >> x >> y;
        queries.pb({y, x});
    }
    sort(ALL(queries));
    VI correct(m, -1);
    VVB incorrect(m, VB(10, 0));
    int res = solve(correct, incorrect, 0);
    // cout << res << ": ";
    if (res == 0) cout << "Liar" << '\n';
    if (res == 1) cout << answer << '\n';
    if (res > 1) cout << "Ambiguity" << '\n';
}
// 9 -> 10 -> 100


signed main () {

#ifdef ONLINE_JUDGE
#else
    // freopen("in.txt", "r", stdin);
//	freopen("out.txt", "w", stdout);
#endif
    _upgrade
    int T = 1;
    // cin >> T;
    while(T--) go();
    return 0;
}