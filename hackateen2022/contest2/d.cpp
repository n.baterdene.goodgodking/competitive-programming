#include <bits/stdc++.h>
// #define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define sz(x) (int)x.size()

using ll = long long;
using db = double;
using ld = long double;
using uint = unsigned;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VS = vector<string>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using VD = vector<db>;
using PII = pair<int, int>;
using PLL = pair<ll, ll>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int bitCnt(int a) {
    bitset <64> b(a);
    return b.count();
}
int bitCnt(string a) {
    bitset <64> b(a);
    return b.count();
}

int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}

void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VPI& a) {
    for (auto el : a) {
        cout << el.ff << ',' << el.ss << ' ';
    }
    cout << '\n';
}

void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VVI& a) {
    for (auto el : a) {
        print(el);
    }
}

/*
|      ⣠⣶⡾⠏⠉⠙⠳⢦⡀⠀  ⠀⢠⠞⠉⠙⠲⡀
|    ⣴⠿⠏⠀⠀⠀⠀⠀⠀⢳⡀⠀  ⡏⠀⠀ ⠀⠀ ⢷
|⠀⠀⢠⣟⣋⡀⢀⣀⣀⡀⠀⣀⡀⣧⠀  ⢸⠀⠀⠀⠀   ⡇ ⠀
| ⠀⢸⣯⡭⠁⠸⣛⣟⠆⡴⣻⡲⣿   ⣸⠀⠀OK.  ⡇
|⠀⠀⣟⣿⡭⠀⠀⠀⠀⠀⢱⠀⠀⣿   ⢹⠀⠀⠀⠀⠀ ⡇
|⠀⠀⠙⢿⣯⠄⠀⠀⠀⢀⡀⠀⠀⡿⠀  ⡇⠀⠀ ⠀ ⡼
|⠀⠀⠀⠀⠹⣶⠆⠀⠀⠀⠀⠀⡴⠃ ⠀ ⠘⠤⣄⣠⠞⠀ ⠀
|⠀⠀⠀⠀⢸⣷⡦⢤⡤⢤⣞⣁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ ⠀
|⠀⢀⣤⣴⣿⣏⠁⠀⠀⠸⣏⢯⣷⣖⣦⡀⠀⠀⠀⠀⠀⠀
|⢀⣾⣽⣿⣿⣿⣿⠛⢲⣶⣾⢉⡷⣿⣿⠵⣿⠀⠀⠀⠀⠀
|⣼⣿⠍⠉⣿⡭⠉⠙⢺⣇⣼⡏⠀ ⠀⣄⢸⣿
|⣿⣿⣧⣀⣿.........⣀⣰⣏⣘⣆⣀
                           
 ____     __ _______ _______ __________
|     \  |  |  _____/  _____|____  ____|
|  |\  \ |  | |__  (  (_____    |  |
|  | \  \|  |  __|  \_____  \   |  | 
|  |  \     | |_____ _____)  )  |  | 
|__|   \____|_______(_______/   |__|                        
                   _                         
 __   _           | |                         
|   \| | ___  ___ | |_                                   
| |\ | |/ _ \/ __/| __|                                   
| | \  |  __/\__ \| |_                                   
|_|  \_|\___ /___/ \__|                                   
|                                                      _         _   _
|                                                     (_)       | | | |
|  _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_
| | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|
| | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_
| | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|
| | |               __/ |                                 
| |_|              |___/                   _                                       
|                                  _      (_)                            
|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                                 
| | '_ ` _ \ / _` |_   / _` |/ _` | |/ _` | |                                  
| | | | | | | (_|  /  / (_| | (_| | | (_| | |                                 
| |_| |_| |_|\__,_|____\__,_|\__,_|_|\__,_|_|                                  
*/

const db PI = acos(-1.0); //M_PI;
const ll INFF = 4e18;
const int INF = 1.07e9;
const int MOD = 1e9 + 7;
const int MOD1 = 998244353; //7*17*2^23 +1;
const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0, 1, 0, -1};





const int N = 1e5 + 5;
const int M = 18;

int n, m, k;
VI paths[N];
int jump[N][M];
int nums[N], lvl[N];
void dfs(int pos, int par, int l) {
    jump[pos][0] = par;
    lvl[pos] = l;
    for (auto& el : paths[pos]) {
        if (el == par) continue;
        dfs(el, pos, l+1);
    }
}
int Jump(int a, int b) {
    // cout << a << " " << b << " -> ";
    for (int j = M-1; j >= 0; j--) {
        if (b & (1<<j)) a = jump[a][j];
    }
    // cout << a << '\n';
    return a;
}
int lca(int a, int b) {
    if (lvl[a] < lvl[b]) swap(a, b);
    // cout << a << ' ' << b << ' ' << lvl[a] << " " << lvl[b] << " to\n";
    a = Jump(a, lvl[a] - lvl[b]);
    if (a == b) return a;
    // cout << a << ' ' << b <<" to ";
    for (int j = M-1; j >= 0; j--) {
        if (jump[a][j] != jump[b][j]) {
            a = jump[a][j];
            b = jump[b][j];
        }
    }
    // cout << a << ' ' << b << '\n';
    return jump[a][0];
}
bool ans;
bool check(int x, int a, int b) {
    if (ans) return 1;
    if (lca(x, a) == a && lca(x, b) == b) {
        return 1;
    }
    return 0;
}
void go () {
    cin >> n >> m;
    for (int i = 1; i < n; i++) {
        int a, b; cin >> a >> b;
        paths[a].pb(b);
        paths[b].pb(a);
    }
    dfs(1, 1, 0);
    for (int i = 1; i < M; i++)
    for (int j = 1; j <= n; j++) 
        jump[j][i] = jump[jump[j][i-1]][i-1];
    for (int i = 1; i <= m; i++) {
        int a, b, c, d; cin >> a >> b >> c >> d;
        if (lvl[a] < lvl[b]) swap(a, b);
        if (lvl[c] < lvl[d]) swap(c, d);
        int A = lca(a, b);
        int D = lca(c, d);
        int AC = lca(a, c);
        int BD = lca(b, d);
        int AD = lca(b, d);
        int BC = lca(b, d);
        ans = 0;
        ans |= check(AC, A, D);
        ans |= check(BD, A, D);
        ans |= check(AD, A, D);
        ans |= check(BC, A, D);
        cout << (ans ? "yes" : "no") << '\n';
    }
}



signed main () {

#ifndef ONLINE_JUDGE
    // freopen("0.in", "r", stdin);
    // freopen("0.out", "w", stdout);
#endif
    _upgrade
    int T = 1;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














