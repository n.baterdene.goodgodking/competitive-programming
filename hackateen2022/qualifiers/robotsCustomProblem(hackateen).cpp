#include <bits/stdc++.h>
#define pb push_back
#define ff first
#define ss second
#define ALL(x) x.begin(), x.end()
using namespace std;
using ll = long long;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());

string s;

const int N = 505;
ll nums[N][N];
ll dp[N][N];
bool done[N][N], vis[N][N];
int dx[] = {0, 1, 0, -1};
int dy[] = {1, 0, -1, 0};
void create(string input, string output) {
    // { // generate test case

    //     ofstream in(input.c_str());
    //     int limit = 400;
    //     int n = 500, half = n / 2;
    //     int robot = n;
    //     int valueLimit = 1e6;
    //     in << n << '\n';
    //     for (int i = 1; i <= n; i++)
    //     for (int j = 1; j <= n; j++) 
    //         in << rng() % valueLimit + 1 << " \n"[j==n];
        
    //     in << robot << '\n';
    //     for (int i = 1; i <= robot; i++) {
    //         int x = rng()%half+1 + half/2;
    //         int y = rng()%half+1 + half/2;
    //         in << x << ' ' << y << '\n';
    //     }

    //     in.close();
    // }
    { // solve test case

        // ofstream out(output.c_str());
        // ifstream in(input.c_str());
        int n, robot; cin >> n;
        for (int i = 1; i <= n; i++) 
        for (int j = 1; j <= n; j++) 
            cin >> nums[i][j];
        cin >> robot;
        vector <pair <int, int> > robots;
        int minX = n, minY = n;
        robots.pb({0, 0});
        for (int i = 1; i <= robot; i++) {
            int x, y; cin >> x >> y;
            minX = min(x, minX);
            minY = min(y, minY);
            robots.pb({x, y});
        }
        for (int i = 1; i <= robot; i++) {
            int a, b; tie(a, b) = robots[i];
            a -= minX, b -= minY;
            robots[i] = {a, b};
        }
        for (int i = 0; i <= n+1; i++)
        for (int j = 0; j <= n+1; j++) {
            bool gg = 0;
            vis[i][j] = 0;
            dp[i][j] = 0;
            // cout << i << "," << j << ": ";
            for (int p = 1; p <= robot; p++) {
                int x = i+robots[p].ff;
                int y = j+robots[p].ss;
                // cout << x <<',' << y << ' ';
                if (x > n || y > n || x == 0 || y == 0) {
                    gg = 1;
                    continue;
                }
                dp[i][j] += nums[x][y];
            }
            done[i][j] = gg;
        }
        set <vector <ll> > bfs;
        bfs.insert({0, minX, minY});
        while(!bfs.empty()) {
            auto it = bfs.begin();
            vector <ll> pt = *it;
            bfs.erase(it);
            if (vis[pt[1]][pt[2]]) continue;
            vis[pt[1]][pt[2]] = 1;
            if (done[pt[1]][pt[2]]) {
                cout << pt[0] << '\n';
                break;
            }
            for (int i = 0; i < 4; i++) {
                int x = pt[1] + dx[i];
                int y = pt[2] + dy[i];
                bfs.insert({pt[0] + dp[x][y], x, y});
            }
        }
        // in.close();
        // out.close();
    }
}
string getStrDigit(int x) {
    string val = to_string(x);
    if (val.size() == 1) val = "0"+val;
    return val;
}
int main () {
    // for (int i = 15; i <= 15; i++) {
        // string input = "../../hackateen2022/qualifiers/inputs/input"+getStrDigit(i)+".txt";
        // string output = "../../hackateen2022/qualifiers/outputs/output"+getStrDigit(i)+".txt";
        create("input", "output");
    // }
    return 0;
}