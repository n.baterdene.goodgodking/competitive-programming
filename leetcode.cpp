class Solution {
public:
    vector<vector<string>> solveNQueens(int n) {
        vector <int> perm;
        for (int i = 0; i < n; i++) perm.push_back(i);
        vector <vector <string> > ans;
        string tmp = string(n, '.');
        vector <string> cur(n, tmp);
        // ans.push_back(cur);
        do {
            vector <int> x(3*n, 0);
            vector <int> y(3*n, 0);
            for (int i = 0; i < n; i++) {
                x[perm[i]-i+n]++;
                y[perm[i]+i+n]++;
            }
            bool ans = 1;
            for (int i = 0; i < 3 * n && ans; i++) {
                ans &= x[i] < 2;
                ans &= y[i] < 2;
            }
            if (ans) {
                for (int i = 0; i < n; i++) {
                    cur[i][perm[i]] = 'Q';
                }
                ans.pb(cur);
                for (int i = 0; i < n; i++) {
                    cur[i][perm[i]] = '.';
                }
            }
        } while(perm.begin(), perm.end());
        return ans;
    }
};