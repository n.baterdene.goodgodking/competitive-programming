#include <bits/stdc++.h>
// #pragma GCC optimize("O3, unroll-loops")
// #pragma GCC target("avx2")
// #define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define ub upper_bound 
#define sz(x) (int)x.size()
#define chmin(a,b) a=min(a,b)
#define chmax(a,b) a=max(a,b)

using ll = long long;
using db = double;
using ld = long double;
using uint = unsigned;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using PII = pair<int, int>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using PLL = pair<ll, ll>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}
/*

|                    _                                                     
|  __   _           | |                                                    
| |   \| | ___  ___ | |_                                                   
| | |\ | |/ _ \/ __/| __|                                                  
| | | \  |  __/\__ \| |_                                                   
| |_|  \_|\___ /___/ \__|                                                  
|                                                      _         _   _     
|                                                     (_)       | | | |    
|  _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_   
| | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|  
| | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_   
| | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|  
| | |               __/ |                                                  
| |_|              |___/                   _                               
|                                  _      (_)                              
|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                               
| | '_ ` _ \ / _` |_   / _` |/ _` | |/ _` | |                              
| | | | | | | (_|  /  / (_| | (_| | | (_| | |                              
| |_| |_| |_|\__,_|____\__,_|\__,_|_|\__,_|_|                              
*/

const db PI = acos(-1.0); //M_PI;
const ll INFF = 4e18;
const int INF = 1.07e9;
const int MOD = 1e9 + 7;
const int MOD1 = 998244353; //7*19*2^23 +1;
const int dx[] = {-1, 0, 1, 0, -1, -1, 1, 1};
const int dy[] = {0, 1, 0, -1, -1, 1, -1, 1};




const int N = 2500 + 5;
const int M = 2500 + 5;

int s[N], t[N];

int n, m, k, q;
int testcase;
void answer(ll res) {
    cout << "Case #" << ++testcase << ": " << res << "\n";
}
vector <int> pos[M];
void go () {
    cin >> n; for (int i = 0; i < n; i++) cin >> s[i];
    cin >> m; for (int i = 0; i < m; i++) cin >> t[i];
    int dp[m]; memset(dp, 0, sizeof(dp));
    int newDp[m]; 
    for (int i = 0; i < N; i++) pos[i].clear();
    for (int i = 0; i < m; i++) pos[t[i]].pb(i);
    for (int i = 0; i < n; i++) {
        set <PII> bfs;
        if (i > 0)
            for (auto p : pos[s[i-1]]) 
                bfs.insert({dp[p], p});
        else 
            for (int i = 0; i < m; i++)
                bfs.insert({0, i});
        memset(newDp, -1, sizeof(newDp));
        while(!bfs.empty()) {
            int d, p; tie(d, p) = *bfs.begin();
            bfs.erase(bfs.begin());
            if (newDp[p] != -1) continue;
            newDp[p] = d;
            if (p-1 >= 0 && newDp[p-1]==-1) bfs.insert({d+1, p-1});
            if (p+1 < m && newDp[p+1]==-1) bfs.insert({d+1, p+1});
        }
        memcpy(dp, newDp, sizeof(dp));
    }
    int ans = 1e9;
    for (auto p : pos[s[n-1]]) ans = min(ans, dp[p]);
    answer(ans);

}



signed main () {

    // freopen("0.in", "r", stdin);
    // freopen("0.out", "w", stdout);
    int T = 1;
    cin >> T;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














