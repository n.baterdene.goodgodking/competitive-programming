#include <bits/stdc++.h>
// #pragma GCC optimize("O3, unroll-loops")
// #pragma GCC target("avx2")
#define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define ub upper_bound 
#define umap unordered_map
#define sz(x) (int)x.size()
#define chmin(a,b) a=min(a,b)
#define chmax(a,b) a=max(a,b)

using db = double;
using ld = long double;
// using sint = short int;
using ll = long long;
using uint = unsigned int;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using PII = pair<int, int>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using PLL = pair<ll, ll>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}
/*

|                    _                                                     
|  __   _           | |                                                    
| |   \| | ___  ___ | |_                                                   
| | |\ | |/ _ \/ __/| __|                                                  
| | | \  |  __/\__ \| |_                                                   
| |_|  \_|\___ /___/ \__|                                                  
|                                                      _         _   _     
|                                                     (_)       | | | |    
|  _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_   
| | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|  
| | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_   
| | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|  
| | |               __/ |                                                  
| |_|              |___/                   _                               
|                                  _      (_)                              
|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                               
| | '_ ` _ \ / _` |_   / _` |/ _` | |/ _` | |                              
| | | | | | | (_|  /  / (_| | (_| | | (_| | |                              
| |_| |_| |_|\__,_|____\__,_|\__,_|_|\__,_|_|                              
*/

const db PI = acos(-1.0); //M_PI;
const ll INFF = 4e18;
const int INF = 1.07e9;
const int P1 = 53;
const int P2 = 10;
const ll MOD = 1e9+7; // 100663319, 53, 97 good hash values
const int MOD1 = 998244353; //7*19*2^23 +1;
const int dx[] = {-1, 0, 1, 0, -1, -1, 1, 1};
const int dy[] = {0, 1, 0, -1, -1, 1, -1, 1};




const int N = 9e6+1;
const int M = 3001;
int n, m, k, q;
string s;
int nums[N];
int test = 1;
void out(int ans) {
    cout << "Case #" << test++ << ": ";
    cout << ans;
    cout << '\n';
}


ll fact[N+5], invFact[N+5];
ll power(ll a, int b) {
    ll ans = 1; 
    while (b > 0) {
        if (b & 1) ans = (ans * a) % MOD;
        b >>= 1; 
        a = (a * a) % MOD;
    }
    return ans;
}
ll inv(ll a, ll p) {
    return power(a, p-2);
}
ll nCr(ll a, ll b) {
    if (a < b || b < 0) return 0;
    if (b == 0 || a == b) return 1;
    return fact[a] * invFact[b] % MOD * invFact[a - b] % MOD;
}
void init() {
    fact[0] = fact[1] = 1;
    for (int i = 2; i <= N; i++) fact[i] = fact[i-1] * i % MOD;
    invFact[N] = inv(fact[N], MOD);
    for (int i = N-1; i >= 0; i--) invFact[i] = invFact[i+1] * ll(i+1) % MOD;
}
int x[M], y[M]; // weigth, count
void go () {
    cin >> n >> k;
    int w, c, same = 0, small = 0, big = 0, total = 0;
    for (int i = 1; i <= n; i++) {
        int a, b; cin >> b >> a;
        x[i] = a, y[i] = b;
        if (i == 1) {
            w = a;
            c = b;
        } else {
            if (w == a) {
                same += b;
            } else if (w < a) {
                big += b;
            } else {
                small += b;
            }
        }
        total += b;
    }
    int dp[k+5]; memset(dp, 0, sizeof(dp));
    dp[0] = 1;
    k++;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= min(k, y[i]); j++) {
            for (int p = 0; p+j <= k; p++) {
                dp[p+j] = (dp[p+j] + dp[p]*nCr(k-p, j)) % MOD;
            }
        }
    }
    // cout << n << ' ' << k << '\n';
    // cout << c << ' ' << small << ' ' << same << ' ' << big << ' ' << total << '\n';
    cout << k << ' ' << dp[k-1] << ' ' << dp[k] << '\n';
    total = inv(dp[k], MOD);
    int ans = 0;
    for (int r = 0; r <= min(k, small); r++) {
        int l = k - r;
        int tmp = small-r+c-1+same;
        // cout << small << "! / " << (small-r) << "! ";
        int cur = fact[small]*invFact[small-r]%MOD;
        cur = cur * (fact[tmp]*invFact[tmp-l]%MOD)%MOD;
        // cout << " * " << tmp << "! / " << (tmp-l) << "!  => ";
        ans = (ans + cur) % MOD;
        // cout << r << ": " << cur << '\n';
    }
    ans = ans * total % MOD;
    out(ans);
}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("0.in", "r", stdin);
    freopen("0.out", "w", stdout);
#endif
    _upgrade
    int T = 1;
    init();
    cin >> T;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














