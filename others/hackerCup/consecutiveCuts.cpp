#include <bits/stdc++.h>
// #pragma GCC optimize("O3, unroll-loops")
// #pragma GCC target("avx2")
// #define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define ub upper_bound 
#define umap unordered_map
#define sz(x) (int)x.size()
#define chmin(a,b) a=min(a,b)
#define chmax(a,b) a=max(a,b)

using db = double;
using ld = long double;
using sint = short int;
using ll = long long;
using uint = unsigned int;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using PII = pair<int, int>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using PLL = pair<ll, ll>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}
/*

|                    _                                                     
|  __   _           | |                                                    
| |   \| | ___  ___ | |_                                                   
| | |\ | |/ _ \/ __/| __|                                                  
| | | \  |  __/\__ \| |_                                                   
| |_|  \_|\___ /___/ \__|                                                  
|                                                      _         _   _     
|                                                     (_)       | | | |    
|  _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_   
| | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|  
| | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_   
| | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|  
| | |               __/ |                                                  
| |_|              |___/                   _                               
|                                  _      (_)                              
|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                               
| | '_ ` _ \ / _` |_   / _` |/ _` | |/ _` | |                              
| | | | | | | (_|  /  / (_| | (_| | | (_| | |                              
| |_| |_| |_|\__,_|____\__,_|\__,_|_|\__,_|_|                              
*/

const db PI = acos(-1.0); //M_PI;
const ll INFF = 4e18;
const int INF = 1.07e9;
const int P1 = 53;
const int P2 = 10;
const ll MOD = 1e9+7; // 100663319, 53, 97 good hash values
const int MOD1 = 998244353; //7*19*2^23 +1;
const int dx[] = {-1, 0, 1, 0, -1, -1, 1, 1};
const int dy[] = {0, 1, 0, -1, -1, 1, -1, 1};




const int N = 5e5 + 5;
const int M = 20;

int nums[N];

int n, m, k, q;
string s;

int test = 1;
ll a[2*N], b[2*N], p1[2*N], p2[2*N], A[2*N], B[2*N];
void out(string ans) {
    cout << "Case #" << test++ << ": ";
    cout << ans;
    cout << '\n';
}
void init() {
    p1[0] = p2[0] = 1;
    for (int i = 1; i < 2*N; i++) p1[i] = p1[i-1] * P1 % MOD;
    for (int i = 1; i < 2*N; i++) p2[i] = p2[i-1] * P2 % MOD;
}
ll pw(ll a, ll b, ll p) {
    ll res = 1;
    while (b) {
        if (b & 1) res = res * a % p;
        a = a * a % p;
        b >>= 1;
    }
    return res;
}
ll inv(ll a, ll p) {
    return pw(a, p-2, p);
}
void go () {
    cin >> n >> k;
    for (int i = 0; i < n; i++) {cin >> a[i];A[i] = a[i];}
    for (int i = 0; i < n; i++) {cin >> b[i];B[i] = b[i];}
    for (int i = n; i < 2*n; i++) {B[i] = b[i] = b[i-n];}
    for (int i = 1; i < n; i++) { // a,A  b,B
        A[i] = (A[i-1] + A[i] * p1[i] % MOD) % MOD;
        a[i] = (a[i-1] + a[i] * p2[i] % MOD) % MOD;
    }
    for (int i = 1; i < 2*n; i++) { // a,A  b,B
        B[i] = (B[i-1] + B[i] * p1[i] % MOD) % MOD;
        b[i] = (b[i-1] + b[i] * p2[i] % MOD) % MOD;
    }
    bool same = 0, sameSt = 0, notSameSt = 0;
    for (int i = 0; i < n; i++) {
        int r = i+n-1;
        ll y = (B[r] - (i ? B[i-1] : 0) + MOD) % MOD * inv(p1[i], MOD) % MOD;
        ll x = (b[r] - (i ? b[i-1] : 0) + MOD) % MOD * inv(p2[i], MOD) % MOD;
        if (mp(x, y) == mp(a[n-1], A[n-1])) {
            same = 1, sameSt |= (i==0), notSameSt |= (i!=0);
        }
    }
    if (n == 2) k %= 2;
    if (same && notSameSt && k == 1) out("YES");
    else if (same && sameSt && k == 0) out("YES");
    else if (same && k > 1) out("YES");
    else out("NO");
}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("0.in", "r", stdin);
    freopen("0.out", "w", stdout);
#endif
    _upgrade
    int T = 1;
    cin >> T;
    init();
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














