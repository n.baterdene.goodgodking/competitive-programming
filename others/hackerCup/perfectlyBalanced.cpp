#include <bits/stdc++.h>
// #pragma GCC optimize("O3, unroll-loops")
// #pragma GCC target("avx2")
// #define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define ub upper_bound 
#define umap unordered_map
#define sz(x) (int)x.size()
#define chmin(a,b) a=min(a,b)
#define chmax(a,b) a=max(a,b)

using db = double;
using ld = long double;
using sint = short int;
using ll = long long;
using uint = unsigned int;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using PII = pair<int, int>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using PLL = pair<ll, ll>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}
/*

|                    _                                                     
|  __   _           | |                                                    
| |   \| | ___  ___ | |_                                                   
| | |\ | |/ _ \/ __/| __|                                                  
| | | \  |  __/\__ \| |_                                                   
| |_|  \_|\___ /___/ \__|                                                  
|                                                      _         _   _     
|                                                     (_)       | | | |    
|  _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_   
| | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|  
| | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_   
| | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|  
| | |               __/ |                                                  
| |_|              |___/                   _                               
|                                  _      (_)                              
|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                               
| | '_ ` _ \ / _` |_   / _` |/ _` | |/ _` | |                              
| | | | | | | (_|  /  / (_| | (_| | | (_| | |                              
| |_| |_| |_|\__,_|____\__,_|\__,_|_|\__,_|_|                              
*/

const db PI = acos(-1.0); //M_PI;
const ll INFF = 4e18;
const int INF = 1.07e9;
const int P1 = 53;
const int P2 = 10;
const ll MOD = 1e9+7; // 100663319, 53, 97 good hash values
const int MOD1 = 998244353; //7*19*2^23 +1;
const int dx[] = {-1, 0, 1, 0, -1, -1, 1, 1};
const int dy[] = {0, 1, 0, -1, -1, 1, -1, 1};




const int N = 1e6+1;
const int M = 20;
int n, m, k, q;
string s;
int values[N];

const int blockSize = 1000;
struct Query {
    int l, r, block, id;
    Query(int _l, int _r, int _id) {
        l = _l, r = _r, id = _id;
        block = l / blockSize;
    }
    bool operator< (const Query&a) const{
        if (block != a.block)
            return block < a.block;
        return block & 1 ? (r < a.r) : (a.r < r);
    }
};

int l, r, dif;
int lCnt[30], rCnt[30];
void Add(int val) {
    int old = abs(lCnt[val] - rCnt[val]);
    lCnt[val]++;
    int cur = abs(lCnt[val] - rCnt[val]);
    dif += cur - old;
}
void Rem(int val) {
    int old = abs(lCnt[val] - rCnt[val]);
    rCnt[val]++;
    int cur = abs(lCnt[val] - rCnt[val]);
    dif += cur - old;
}
void add(bool isRight) {
    assert(0 <= l && l < n);
    assert(0 <= r && r < n);
    if (isRight) {
        int m = (l+r)/2;
        Add(values[m]);
        Rem(values[r]);
    } else {
        int m = (l+r+1)/2;
        Add(values[l]);
        Rem(values[m]);
    }
}
void rem(bool isRight) {
    if (!(0 <= l && l < n)) {
        // cout << l << ' ' << r << ' ' << isRight << '\n';
        exit(0);
    }
    assert(0 <= l && l < n);
    assert(0 <= r && r < n);
    if (isRight) {
        int m = (l+r)/2;
        Rem(values[m]);
        Add(values[r]);
    } else {
        int m = (l+r+1)/2;
        Rem(values[l]);
        Add(values[m]);
    }
}

int test = 1;
void out(int ans) {
    cout << "Case #" << test++ << ": ";
    cout << ans;
    cout << '\n';
}
void go () {
    cin >> n;
    for (int i = 0; i < n; i++) cin >> values[i];
    cin >> q;
    vector <Query> queries;
    queries.reserve(q);
    for (int i = 1; i <= q; i++) {
        int a, b; cin >> a >> b;
        a--, b--;
        if ((b - a + 1) % 2 == 0) continue;
        queries.pb({a, b, i});
    }
    memset(lCnt, 0, sizeof(lCnt));
    memset(rCnt, 0, sizeof(rCnt));
    sort(ALL(queries));
    l = queries[0].l, r = queries[0].l, dif = 0;
    int ans = 0;
    for (auto& query : queries) {
        while (r < query.r) {r++, add(1);}
        while (l > query.l) {l--, add(0);}
        while (r > query.r) {rem(1), r--;}
        while (l < query.l) {rem(0), l++;}
        cout << l << ' ' << r << '\n';
        bool cur = 0;
        if (dif == 0) cur = 1;
        if (dif == 2) {
            int mid = (l+r)/2, val = values[mid];
            int curDif = abs(lCnt[val] - rCnt[val]);
            if (curDif == 1) cur = 1;
        }
        ans += cur;
    }
    out(ans);
}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("0.in", "r", stdin);
    freopen("0.out", "w", stdout);
#endif
    _upgrade
    int T = 1;
    cin >> T;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














