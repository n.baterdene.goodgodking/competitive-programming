#include <bits/stdc++.h>
// #pragma GCC optimize("O3, unroll-loops")
// #pragma GCC target("avx2")
#define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define ub upper_bound 
#define umap unordered_map
#define sz(x) (int)x.size()
#define chmin(a,b) a=min(a,b)
#define chmax(a,b) a=max(a,b)

using db = double;
using ld = long double;
// using sint = short int;
using ll = long long;
using uint = unsigned int;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using PII = pair<int, int>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using PLL = pair<ll, ll>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}
/*

|                    _                                                     
|  __   _           | |                                                    
| |   \| | ___  ___ | |_                                                   
| | |\ | |/ _ \/ __/| __|                                                  
| | | \  |  __/\__ \| |_                                                   
| |_|  \_|\___ /___/ \__|                                                  
|                                                      _         _   _     
|                                                     (_)       | | | |    
|  _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_   
| | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|  
| | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_   
| | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|  
| | |               __/ |                                                  
| |_|              |___/                   _                               
|                                  _      (_)                              
|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                               
| | '_ ` _ \ / _` |_   / _` |/ _` | |/ _` | |                              
| | | | | | | (_|  /  / (_| | (_| | | (_| | |                              
| |_| |_| |_|\__,_|____\__,_|\__,_|_|\__,_|_|                              
*/

const db PI = acos(-1.0); //M_PI;
const ll INFF = 4e18;
const int INF = 1.07e9;
const int P1 = 53;
const int P2 = 10;
const ll MOD = 1e9+7; // 100663319, 53, 97 good hash values
const int MOD1 = 998244353; //7*19*2^23 +1;
const int dx[] = {-1, 0, 1, 0, -1, -1, 1, 1};
const int dy[] = {0, 1, 0, -1, -1, 1, -1, 1};




const int N = 5e5 + 5;
const int M = 20;

int nums[N];
PII queries[N];

int n, m, k, q;
string s;

int test = 1;
void out(ll ans) {
    cout << "Case #" << test++ << ": ";
    cout << ans;
    cout << '\n';
}
void go () {
    ll ans = 0;
    cin >> n;
    map <ll, ll> cntX, cntY;
    for (int i = 1; i <= n; i++) {
        ll x, y; cin >> x >> y;
        cntX[x]++; cntY[y]++;
    }
    ll CxX = 0, CxY = 0, CxX2 = 0, CxY2 = 0;
    for (auto [a, b] : cntX) {
        CxX = (CxX + a * b) % MOD;
        CxX2 = (CxX2 + a * a % MOD * b % MOD) % MOD; 
    }
    for (auto [a, b] : cntY) {
        CxY = (CxY + a * b) % MOD;
        CxY2 = (CxY2 + a * a % MOD * b % MOD) % MOD; 
    }
    cin >> q;
    // cout << CxX << ' ' << CxX2 << '\n';
    for (int i = 1; i <= q; i++) {
        ll x, y; cin >> x >> y;
        ll cur = (n * (x*x%MOD + y*y%MOD)%MOD + CxX2 + CxY2 - 2 * x * CxX%MOD - 2 * y * CxY%MOD)%MOD;
        // cout << cur << '\n';
        ans = (ans + cur) % MOD;
    }
    ans = (ans + MOD) % MOD;
    out(ans);
    // cout << ans << '\n';
}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("0.in", "r", stdin);
    freopen("0.out", "w", stdout);
#endif
    _upgrade
    int T = 1;
    cin >> T;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














