#include <bits/stdc++.h>
#define lb lower_bound
#define ub upper_bound
using namespace std;
const int N = 1e5+5;
int n, m, k;
signed main() {
	ios::sync_with_stdio();
	cin.tie(0);
	cout.tie(0);
	freopen("0.in", "r", stdin);
	freopen("0.out", "w", stdout);
	cin >> n;
	set <int> vals;
	for (int i = 1; i <= n; i++) {
		int x; cin >> x;
		if (i == 1) {
			vals.insert(x);
			continue;
		}
		if (*vals.begin() > x) {
			vals.insert(x);
		} else {
			auto it = vals.ub(x);
			if (it != vals.end()) vals.erase(it);
			vals.insert(x);
		}
		// for (auto& el : vals) cout << el << ' '; cout << "\n";
	}
	cout << vals.size() << ' ' << '\n';
};