#include <bits/stdc++.h>
#define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define rs resize
#define ins insert 
#define fr front() 
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define umap unordered_map
#define sz(x) (int)x.size()

typedef long long ll;
typedef double db;
typedef unsigned uint;
typedef unsigned long long ull;
typedef unordered_map<int, int> umapII;
// PQ going up <int, VI, greater<int> >
typedef vector<int> VI;
typedef vector<char> VC;
typedef vector<string> VS;
typedef vector<bool> VB;
typedef vector<VB> VVB;
typedef vector<VVB> VVVB;
typedef vector<ll> VLL;
typedef vector<VLL> VVLL;
typedef vector<VVLL> VVVLL;
typedef vector<VVVLL> VVVVLL;
typedef vector<db> VD;
typedef set<int> SI;
typedef set<string> SS;
typedef pair<int, int> PII;
typedef pair<db, db> PDD;
typedef pair<ll, ll> PLL;
typedef vector<PII> VPI;
typedef vector<VPI> VVPI;
typedef vector<VI> VVI;
typedef vector<VVI> VVVI;
typedef vector<VVVI> VVVVI;
typedef vector<VVVVI> VVVVVI;
// mt19937 rng(chrono::high_resolution_clock::now().time_since_epoch().count() + reinterpret_cast<unsigned long>(new int) + *(new unsigned long));
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int bitCnt(int a) {
    bitset <64> b(a);
    return b.count();
}
int bitCnt(string a) {
    bitset <64> b(a);
    return b.count();
}
VI readVI(int n) {
    VI a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVI readVVI(int n, int m) {
    VVI a(n, VI(m));
    REP(i, n) a[i] = readVI(m);
    return a;
}
VLL readVLL(ll n) {
    VLL a(n);
    REP(i, n) cin >> a[i];
    return a;
}
VVLL readVVLL(ll n, ll m) {
    VVLL a(n, VLL(m));
    REP(i, n) a[i] = readVLL(m);
    return a;
}


int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}

void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VPI& a) {
    for (auto el : a) {
        cout << el.ss << ',' << el.ff << ' ';
    }
    cout << '\n';
}

void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VVI& a) {
    for (auto el : a) {
        print(el);
    }
}
const int MOD = 1e9 + 7;
const int MOD1 = 998244353;//119*2^23 +1;
const int INF = 2e9;
const ll INFF = INT64_MAX;
const db EPS = 1e-9;
const db PI = acos(-1.0); //M_PI;
const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0, 1, 0, -1};
/*
|      ⣠⣶⡾⠏⠉⠙⠳⢦⡀⠀  ⠀⢠⠞⠉⠙⠲⡀
|    ⣴⠿⠏⠀⠀⠀⠀⠀⠀⢳⡀⠀  ⡏⠀⠀ ⠀⠀ ⢷
|⠀⠀⢠⣟⣋⡀⢀⣀⣀⡀⠀⣀⡀⣧⠀  ⢸⠀⠀⠀⠀   ⡇ ⠀
| ⠀⢸⣯⡭⠁⠸⣛⣟⠆⡴⣻⡲⣿   ⣸⠀⠀OK.  ⡇
|⠀⠀⣟⣿⡭⠀⠀⠀⠀⠀⢱⠀⠀⣿   ⢹⠀⠀⠀⠀⠀ ⡇
|⠀⠀⠙⢿⣯⠄⠀⠀⠀⢀⡀⠀⠀⡿⠀  ⡇⠀⠀ ⠀ ⡼
|⠀⠀⠀⠀⠹⣶⠆⠀⠀⠀⠀⠀⡴⠃ ⠀ ⠘⠤⣄⣠⠞⠀ ⠀
|⠀⠀⠀⠀⢸⣷⡦⢤⡤⢤⣞⣁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ ⠀
|⠀⢀⣤⣴⣿⣏⠁⠀⠀⠸⣏⢯⣷⣖⣦⡀⠀⠀⠀⠀⠀⠀
|⢀⣾⣽⣿⣿⣿⣿⠛⢲⣶⣾⢉⡷⣿⣿⠵⣿⠀⠀⠀⠀⠀
|⣼⣿⠍⠉⣿⡭⠉⠙⢺⣇⣼⡏⠀ ⠀⣄⢸⣿
|⣿⣿⣧⣀⣿.........⣀⣰⣏⣘⣆⣀
      
|                                                    _         _   _
|                                                   (_)       | | | |
 _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_
| '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|
| |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_
| .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|
| |               __/ |
|_|              |___/
 _   _      _ _    __        __         _     _ 
| | | | ___| | | __\ \      / /__  _ __| | __| |
| |_| |/ _ \ | |/ _ \ \ /\ / / _ \| '__| |/ _` |
|  _  |  __/ | | (_) \ V  V / (_) | |  | | (_| |
|_| |_|\___|_|_|\___/ \_/\_/ \___/|_|  |_|\__,_|                                  
 ____     __    _______    _______   __________
|     \  |  |  |  _____|  /  _____) |____  ____|
|  |\  \ |  |  | |__     (  (_____      |  |
|  | \  \|  |  |  __|     \_____  \     |  | 
|  |  \     |  | |_____    _____)  )    |  | 
|__|   \____|  |_______|  (_______/     |__| 
 
 
 


*/











class ConvexHull {
private:
    int n;
    VPI points; // x, y;
    VPI lowerHalf, upperHalf;
    PII lowPoint, highPoint;
    static bool compBySlope(VI&a, VI&b) {
        if ((a[0] * b[1] == a[1] * b[0])) return (a[0] == b[0] ? a[1] < b[1] : a[0] < b[0]);
        return (a[0] * b[1] < a[1] * b[0]);
    };
    bool isOnLine(PII a, PII b, PII c) { // c in (a, b)
        VPI tmp = {a, b, c};
        sort(ALL(tmp));
        a = tmp[0], b = tmp[1], c = tmp[2];
        b = {a.ss - b.ss, a.ff - b.ff};
        c = {a.ss - c.ss, a.ff - c.ff};
        return (b.ff * c.ss == c.ff * b.ss);
    }
    void findLowestAndHighestPoints() {
        int id = 0, id1 = 0;
        for (int i = 1; i < n; i++) {
            PII pt = points[i];
            if (pt < points[id]) id = i;
            if (pt > points[id1]) id1 = i;
        }
        lowPoint = points[id];
        highPoint = points[id1];
    }
    void dividePoints() {
        VPI line = {lowPoint, highPoint};
        long long X = highPoint.ss - lowPoint.ss, Y = highPoint.ff - lowPoint.ff;
        for (auto &pt : points) {
            if (pt == highPoint || pt == lowPoint) continue;
            long long x = pt.ss - lowPoint.ss, y = pt.ff - lowPoint.ff;
            if (y * X >= Y * x) upperHalf.pb(pt);
            else lowerHalf.pb(pt);
        }
        lowerHalf.pb(lowPoint);
        upperHalf.pb(highPoint);
        swap(lowerHalf[0], lowerHalf.back());
        swap(upperHalf[0], upperHalf.back());
    }
    void sortPoints() {
        VVI lowerPoints, upperPoints; // (k = A, B), x, y
        for (auto &pt : lowerHalf) {
            int x = pt.ss - lowPoint.ss, y = pt.ff - lowPoint.ff; // slope: y / x
            lowerPoints.pb({y, x, pt.ff, pt.ss});
        }
        sort(ALL(lowerPoints), compBySlope);
        for (auto &pt : upperHalf) {
            int x = highPoint.ss - pt.ss, y = highPoint.ff - pt.ff; // slope: y / x
            upperPoints.pb({y, x, pt.ff, pt.ss});
        }
        sort(ALL(upperPoints), compBySlope);
        int id = 0;
        for (auto &pt : lowerPoints) points[id++] = {pt[2], pt[3]};
        for (auto &pt : upperPoints) points[id++] = {pt[2], pt[3]};
    }
    void buildConvexHull() {
        PII st = points[0], end = points.back(), cur = points[1], last;
        convexHull.pb(st);
        convexHull.pb(cur);
        points.pb(st);
        auto cmp = [&](PII& pt) -> bool {
            int sz = convexHull.size();
            PII x = convexHull[sz-1];
            PII y = convexHull[sz-2];
            if (isOnLine(x, y, pt)) {
                return 0;
            }
            x = {pt.ss - x.ss, pt.ff - x.ff};
            y = {pt.ss - y.ss, pt.ff - y.ff};
            swap(x, y);
            return (x.ff * y.ss < x.ss * y.ff);
        };
        for (int i = 2; i <= n; i++) {
            cur = points[i];
            while(cmp(cur)) 
                convexHull.pop_back();
            convexHull.pb(cur);
        }
        points.pop_back();
        convexHull.pop_back();
    }
public:
    VPI convexHull;
    ConvexHull(VPI& _points) {
        points = _points;
        n = points.size();
        findLowestAndHighestPoints();
        dividePoints();
        sortPoints();
        buildConvexHull();
    }
};
void go () {
    int n, a, b;
    // cin >> n;
    while(cin >> n) {
        if (n == 0) break;
        VPI pts;   
        for (int i = 0; i < n; i++) {
            cin >> a >> b;
            pts.pb({b, a});
        }
        ConvexHull myConvexHull(pts);
        cout << myConvexHull.convexHull.size() << '\n';
        for (auto [a, b] : myConvexHull.convexHull) {
            cout << b << ' ' << a << '\n';
        }
    }
}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
#endif
    _upgrade
    int T = 1;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/















