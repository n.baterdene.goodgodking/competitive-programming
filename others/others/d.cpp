#include <bits/stdc++.h>
// #pragma GCC optimize("O3, unroll-loops")
// #pragma GCC target("avx2")
// #define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define Q queue
#define ff first
#define ss second
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define ub upper_bound 
#define umap unordered_map
#define sz(x) (int)x.size()
#define chmin(a,b) a=min(a,b)
#define chmax(a,b) a=max(a,b)

using db = double;
using ld = long double;
using sint = short int;
using ll = long long;
using uint = unsigned int;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using PII = pair<int, int>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using PLL = pair<ll, ll>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}
/*

|                    _                                                     
|  __   _           | |                                                    
| |   \| | ___  ___ | |_                                                   
| | |\ | |/ _ \/ __/| __|                                                  
| | | \  |  __/\__ \| |_                                                   
| |_|  \_|\___ /___/ \__|                                                  
|                                                      _         _   _     
|                                                     (_)       | | | |    
|  _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_   
| | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|  
| | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_   
| | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|  
| | |               __/ |                                                  
| |_|              |___/                   _                               
|                                  _      (_)                              
|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                               
| | '_ ` _ \ / _` |_   / _` |/ _` | |/ _` | |                              
| | | | | | | (_|  /  / (_| | (_| | | (_| | |                              
| |_| |_| |_|\__,_|____\__,_|\__,_|_|\__,_|_|                              
*/

const db PI = acos(-1.0); //M_PI;
const ll INFF = 4e18;
const int INF = 1.07e9;
const int MOD = 1e9 + 7; // 100663319, 53, 97 good hash values
const int MOD1 = 998244353; //7*19*2^23 +1;
const int dx[] = {-1, 0, 1, 0, -1, -1, 1, 1};
const int dy[] = {0, 1, 0, -1, -1, 1, -1, 1};




const int N = 1e5 + 5;
const int M = 3e5 + 5;

int ans[M];
bool vis[N];

int n, m, k, q;
VI paths[N], paths1[N];
vector <PII> edges;

int par[N];
int find(int a) {
    return par[a] = (par[a] == a ? a : find(par[a]));
}
bool merge(int a, int b) {
    a = find(a);
    b = find(b);
    if (a == b) return 0;
    par[b] = a;
    return 1;
}
int lvl[N];
int up[N][20];
int ttime;
int oo[N], cc[N];
void dfs(int pos, int p = -1) {
    if (p == -1) p = pos;
    oo[pos] = ttime++;
    lvl[pos] = lvl[p]+1;
    vis[pos] = 1;
    up[pos][0] = p;
    for (auto& x : paths[pos]) {
        if (x == p) continue;
        dfs(x, pos);
    }
    cc[pos] = ttime-1;
}
int jump(int a, int b) {
    b = max(0, b);
    for (int j = 0; j < 20; j++) {
        if (b & (1<<j)) a = up[a][j];
    }
    return a;
}
int LCA(int a, int b) {
    if (lvl[a] < lvl[b]) swap(a, b);
    a = jump(a, lvl[a]-lvl[b]);
    if (a == b) return a;
    for (int j = 19; j >= 0; j--) {
        if (up[a][j] != up[b][j]) {
            a = up[a][j];
            b = up[b][j];
        }
    }
    return up[a][0];
}

VI ap, low, disc;
map <PII, bool> br;
map <PII, bool> ap1;
int curT;
int dfsAP(int u, int p) {
  int children = 0;
  low[u] = disc[u] = ++curT;
  for (int& v : paths1[u]) {
    if (v == p) continue;
    if (!disc[v]) {
      children++;
      dfsAP(v, u);
      if (disc[u] <= low[v]) ap1[{u, v}] = ap1[{v, u}] = 1;
      low[u] = min(low[u], low[v]);
    } else
      low[u] = min(low[u], disc[v]);
  }
  return children;
}
void AP() {
  ap = low = disc = vector<int>(n+1, 0);
  curT = 0;
  for (int u = 1; u <= n; u++)
    if (!disc[u])
      ap[u] = dfsAP(u, u) > 1;
}
void dfsBR(int u, int p) {
  low[u] = disc[u] = ++curT;
  for (int& v : paths1[u]) {
    if (v == p) continue;
    if (!disc[v]) { 
      dfsBR(v, u);
      if (disc[u] < low[v]) br[{u, v}] = br[{v, u}] = 1;
      low[u] = min(low[u], low[v]);
    } else
      low[u] = min(low[u], disc[v]);
  }
}
void BR() {
  low = disc = vector<int>(n+1);
  curT = 0;
  for (int u = 1; u <= n; u++)
    if (!disc[u])
      dfsBR(u, u);
}

string solve(int a, int b, int c, int d) {
    if (a == b) return "yes";
    if (find(a) != find(b)) return "no";
    if (oo[a] > oo[b]) swap(a, b);
    if (oo[c] > oo[d]) swap(c, d);
    if ((oo[c] <= oo[a] && oo[a] <= cc[d]) == (oo[c] <= oo[b] && oo[b] <= cc[d])) return "yes";
    if (br.count({c, d})) return "no";
    return "yes";
}
string solve1(int a, int b, int c) {
    if (a == b) return "yes";
    if (a == c || b == c) return "no";
    if (find(a) != find(b)) return "no";

    if (oo[a] > oo[b]) swap(a, b);
    if ((oo[c] <= oo[a] && oo[a] <= cc[c]) == (oo[c] <= oo[b] && oo[b] <= cc[c])) return "yes";
    if (lvl[a] < lvl[b]) swap(a, b);
    int upA = jump(a, lvl[a]-lvl[c]-1);
    if (ap1.count({c, upA})) return "no";
    return "yes";
}
void go () {
    cin >> n >> m;
    for (int i = 1; i <= n; i++) par[i] = i;
    // build tree
    for (int i = 1; i <= m; i++) {
        int a, b; cin >> a >> b;
        if (merge(a, b)) {
            paths[a].pb(b);
            paths[b].pb(a);
        } else {
            edges.pb({a, b});
        }
        paths1[a].pb(b);
        paths1[b].pb(a);
    }
    for (int i = 1; i <= n; i++) 
        if (!vis[i]) dfs(i);

    for (int j = 1; j < 20; j++)
    for (int i = 1; i <= n; i++) 
        up[i][j] = up[up[i][j-1]][j-1];
    
    AP();
    BR();
    cin >> q;
    for (int i = 1; i <= q; i++) {
        int a, b, c, d;
        cin >> a;
        if (a == 1) {
            cin >> a >> b >> c >> d;
            cout << solve(a, b, c, d) << '\n';
        } else {
            cin >> a >> b >> c;
            cout << solve1(a, b, c) << '\n';
        }
    }
}



signed main () {

    // freopen("0.in", "r", stdin);
    // freopen("0.out", "w", stdout);
    _upgrade
    int T = 1;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)
    
*/














