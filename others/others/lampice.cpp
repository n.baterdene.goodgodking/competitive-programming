#include <bits/stdc++.h>
// #pragma GCC optimize("O3, unroll-loops")
// #pragma GCC target("avx2")
#define int long long
using namespace std;

#define _upgrade                  \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0);
#define FOR(i, a, b) for (int i = a; i <= b; i++)
#define FORB(i, a, b) for (int i = a; i >= b; i--)
#define REP(i, a) for (int i = 0; i < a; i++)
#define REP1(i, a) for (int i = 1; i < a; i++)
#define REPB(i, a) for (int i = a - 1; i >= 0; i--)
#define TRAV(a,x) for (auto& a: x)
#define LINE "---------------------\n"
#define ALL(A) A.begin(), A.end()
#define LLA(A) A.rbegin(), A.rend()
#define ff first
#define ss second
#define ts to_string
#define pb push_back
#define mp make_pair
#define lb lower_bound 
#define ub upper_bound 
#define sz(x) (int)x.size()

using ll = long long;
using db = double;
using ld = long double;
using uint = unsigned;
// PQ going up <int, VI, greater<int> >
using VI = vector<int>;
using VS = vector<string>;
using VB = vector<bool>;
using VVB = vector<VB>;
using VVVB = vector<VVB>;
using VLL = vector<ll>;
using VVLL = vector<VLL>;
using VD = vector<db>;
using PII = pair<int, int>;
using PLL = pair<ll, ll>;
using VPI = vector<PII>;
using VVPI = vector<VPI>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
void eraseDups(VI& a) {
    a.erase(unique(a.begin(), a.end()), a.end());
}
int strToInt(string&a) {
    stringstream x(a);
    int b;
    x >> b;
    return b;
}
int bitCnt(int a) {
    bitset <64> b(a);
    return b.count();
}
int bitCnt(string a) {
    bitset <64> b(a);
    return b.count();
}

int gcd(int a, int b) {
   if (b == 0) return a;
   return gcd(b, a % b);
}

void print(VI& a) {
    for (auto el : a) {
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VPI& a) {
    for (auto el : a) {
        cout << el.ff << ',' << el.ss << ' ';
    }
    cout << '\n';
}

void print(VI& a, int n) {
    int cnt = 0;
    for (auto el : a) {
        if (cnt++ == n) break;
        cout << el << ' ';
    }
    cout << '\n';
}
void print(VVI& a) {
    for (auto el : a) {
        print(el);
    }
}

/*

|                    _                                                     
|  __   _           | |                                                    
| |   \| | ___  ___ | |_                                                   
| | |\ | |/ _ \/ __/| __|                                                  
| | | \  |  __/\__ \| |_                                                   
| |_|  \_|\___ /___/ \__|                                                  
|                                                      _         _   _     
|                                                     (_)       | | | |    
|  _ __  _ __ ___   __ _ _ __ __ _ _ __ ___  _ __ ___  _ ___ ___| |_| |_   
| | '_ \| '__/ _ \ / _` | '__/ _` | '_ ` _ \| '_ ` _ \| / __/ __| __| __|  
| | |_) | | | (_) | (_| | | | (_| | | | | | | | | | | | \__ \__ \ |_| |_   
| | .__/|_|  \___/ \__, |_|  \__,_|_| |_| |_|_| |_| |_|_|___/___/\__|\__|  
| | |               __/ |                                                  
| |_|              |___/                   _                               
|                                  _      (_)                              
|  _ __ ___   __ _ ____ __ _  __ _| | __ _ _                               
| | '_ ` _ \ / _` |_   / _` |/ _` | |/ _` | |                              
| | | | | | | (_|  /  / (_| | (_| | | (_| | |                              
| |_| |_| |_|\__,_|____\__,_|\__,_|_|\__,_|_|                              
*/

const db PI = acos(-1.0); //M_PI;
const ll INFF = 4e18;
const int INF = 1.07e9;
const int MOD = 1e9 + 7;
const int MOD1 = 998244353; //7*17*2^23 +1;
const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0, 1, 0, -1};





const int N = 5e4 + 5;
const int M = 16;
const ll P = 271;
const ll Q = 1e9+7;
int nums[N];

int n, m, k, q, lim, ans = 1;
string s;
vector <int> paths[N];
// centroid
bool removed[N], found;
int sizes[N], lvl[N];
int dfsCentroid(int pos, int par) {
    sizes[pos] = 1;
    for (auto& a : paths[pos]) {
        if (a == par || removed[a]) continue;
        sizes[pos] += dfsCentroid(a, pos);
    }
    return sizes[pos];
}
int getCentroid(int pos, int par, int curSz) {
    for (auto& a : paths[pos]) {
        if (a == par || removed[a]) continue;
        if (sizes[a]*2 > curSz) return getCentroid(a, pos, curSz);
    }
    return pos;
}
// hash
set <pair <ll, ll> > vals;
set <pair <ll, ll> > :: iterator it;
int jump[N][M];
ll down[N], up[N], pows[N]; // down: increasing, up: decreasing

void dfsPrint(int pos, int par = -1) {
    // cout << pos << ' ' << up[pos] << ',' << down[pos] << '\n' ;
    for (auto& el : paths[pos]) {
        if (el == par || removed[el]) continue;
        dfsPrint(el, pos);
    }
}

void fix(int& a) {
    a = (a % Q + Q) % Q;
}
void dfsHash(int pos, int par = 0, int d = 0, int branch = -1) {
    if (found || d+1 > lim) return;
    up[pos] = (up[par] * P + s[pos]) % Q; 
    down[pos] = (down[par] + s[pos] * pows[d]) % Q;
    fix(up[pos]);
    fix(down[pos]);
    vals.insert({up[pos], branch});

    lvl[pos] = d;
    jump[pos][0] = par;
    for (int i = 1; i < M; i++)
        jump[pos][i] = jump[jump[pos][i-1]][i-1];

    // cout << pos << ": " << up[pos] << ',' << down[pos] << '\n';
    if (down[pos] == up[pos] && d+1 == lim) found = 1;
    for (auto& el : paths[pos]) {
        if (el == par || removed[el]) continue;
        dfsHash(el, pos, d+1, (branch == -1 ? el : branch));
    }
}
void solve(int pos, int par, int branch = -1) {
    if (found || lvl[pos] >= lim) return;
    int lvlA = lvl[pos], A = pos;
    int lvlB = lim - 1 - lvlA;
    if (lvlA >= lvlB && lvlB > 0) {
        int C = pos, lvlC = lvlA - lvlB;
        for (int i = 0; i < M; i++) 
            if (lvlB & (1<<i)) C = jump[C][i];

        if (down[C] == up[C]) {
            /*
                (up[A] - pows[lvlB] * up[jump[x][0]] + pows[lvlB] * s[centroid]) % MOD
            */
            ll target = (up[A] - up[jump[C][0]] * pows[lvlA - lvlC + 1]);
            fix(target);
            if (!found) {
                it = vals.lb({target, 0});
                if (it->ff == target && it->ss != branch) {
                    found = 1;
                }
            }
            if (!found) {
                it = --vals.lb({target+1, 0});
                if (it->ff == target && it->ss != branch) {
                    found = 1;
                }
            }
        }
    }
    // cout << "Solve: " << pos  << ',' << par << ' ' << branch << '\n';
    for (auto& el : paths[pos]) {
        if (removed[el] || el == par) continue;
        solve(el, pos, branch == -1 ? el : branch);
    }
}
/* comment
         R
       /   \
      C     B
     /
    A
    lvl_A >= lvl_B && lvl_A + lvl_b + 1 == lim == distance;
    we fix A -> we have lvl_A
        we can know the lvl_B which is: 
            => lvl_B = lim - 1 - lvl_A;
        we can know the lvl_C and C
            => lvl_A - lvl_C = lvl_B - lvl_R => lvl_C = lvl_A - lvl_B
            => lvl_C = lvl_A - lvl_B => lvl_A.up(lvl_A - lvl_C) = lvl_A.up(lvl_B)
        we have A and C, we can find target
            => target = (up[A] - up[par(C)] * pow[lvl_A - lvl_B]) = up[B];
    look for down_B ( target ) but it has to be in a different branch
comment */
void centroidDecomposition(int pos) {
    if (found) return;
    pos = getCentroid(pos, 0, dfsCentroid(pos, 0));
    removed[pos] = 1;
    vals.clear();
    vals.insert({-INFF, 0}); // left, right border
    vals.insert({INFF, 0}); 
    dfsHash(pos);
    solve(pos, -1);
    // cout << pos << ": "; dfsPrint(pos); cout << "\n";
    for (auto& el : paths[pos]) {
        if (removed[el]) continue;
        centroidDecomposition(el);
    }
}
bool check(int _lim) {
    lim = _lim;
    if (lim > n) return 0;
    found = 0;
    for (int i = 1; i <= n; i++) removed[i] = 0;
    // cout << "CHECK FOR : " << lim << "\n";
    centroidDecomposition(1);
    if (found) ans = max(ans, lim);
    return found;
}
void go () {
    cin >> n >> s;
    s = "."+s;
    pows[0] = 1;
    for (int i = 1; i <= n; i++) 
        pows[i] = pows[i-1] * P % Q;
    for (int i = 1; i < n; i++) {
        int a, b; cin >> a >> b;
        paths[a].pb(b);
        paths[b].pb(a);
    }
    { // odd
        int l = 1, r = n / 2 + 2;
        while (l <= r) {
            int mid = (l+r)>>1;
            int cur = mid * 2 + 1;
            if (check(cur)) {
                l = mid+1;
            } else {
                r = mid-1;
            }
        }
    }
    { // even
        int l = 1, r = n / 2 + 2;
        while (l <= r) {
            int mid = (l+r)>>1;
            int cur = mid * 2;
            if (check(cur)) {
                l = mid+1;
            } else {
                r = mid-1;
            }
        }
    }
    // check(2);
    // for (int i = 1; i <= n; i++) check(i);
    cout << ans << '\n';

}



signed main () {

#ifndef ONLINE_JUDGE
    freopen("0.in", "r", stdin);
    freopen("0.out", "w", stdout);
#endif
    _upgrade
    int T = 1;
    while(T--) go();
    return (0-0); //<3
}

/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?)

*/














