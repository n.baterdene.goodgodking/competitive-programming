#include <cmath>
#include <iostream>
#include <set>
#include <climits>
#include <cstdio>
#include <algorithm>
#include <cassert>
#include <string>
#include <vector>
#include <iomanip>
#include <unordered_map>
#include <type_traits>
#include <string>
#include <queue>
#include <map>
// #include <ext/pb_ds/assoc_container.hpp>
 
#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")
 
 
using namespace std;
using ll = long long;
const ll MOD = 998244353;
const int BASE = 293;
const ll inv = 705244304;
 
 
class Tree {
public:
    vector<int> sizes, lvl;
    vector<ll> up, down;
    vector<bool> hasVisited;
    vector<int> paths[(int)5e4];
    vector<ll> powr, ipowr;
    int jump[(int)5e4][17];
    string s;
    int sz;
    int dfsCentroid (int pos, int par) {
        sizes[pos] = 1;
        for (int i: paths[pos]) 
            if (!hasVisited[i] && i != par) sizes[pos] += dfsCentroid(i, pos);
        return (sz = sizes[pos]);
    }
    int getCentroid (int pos, int par) {
        for (int i : paths[pos]) 
            if (!hasVisited[i] && i != par && sizes[i] > sz/2) return getCentroid(i, pos);
        return pos;
    }
    int max_len; int fine = 0;
    void fill (int pos, int par, int d, ll val1, ll val2) {
        up[pos] = val1 = (BASE * val1 + s[pos]) % MOD;
        down[pos] = val2 = (powr[d] * s[pos] + val2) % MOD;
        cout << pos << ": " << up[pos] << ' ' << down[pos] << '\n';
        fine += (up[pos] == down[pos] && d + 1 == max_len);
        lvl[pos] = d;
        jump[pos][0] = par;
        for (int i = 1; i < 17; i++) 
            jump[pos][i] = jump[jump[pos][i - 1]][i - 1];
        
        for (int i: paths[pos]) {
            if (!hasVisited[i] && i != par) {
                fill(i, pos, d + 1, val1, val2);
            }
        }
    }
 
    ll go_up (int l, int d) {
        while (d) {
            l = jump[l][(int)log2(d & -d)];
            d -= (d & -d);
        }
        return l;
    }
 
    int centroid;
    map <int, bool> mp;
    __gnu_pbds::gp_hash_table<int, bool> mp;

    vector<int> to_do;
    void dfs (int pos, int par) {
        if (lvl[pos] + 1 >= max_len) {
            return;
        }
        to_do.push_back(up[pos]);
        for (int i: paths[pos]) {
            if (i != par && !hasVisited[i]) {
                dfs (i, pos);
            }
        }
        if (2 * lvl[pos] + 1 >= max_len) {
            int x = go_up(pos, max_len - lvl[pos] - 2);
            if (up[jump[x][0]] == down[jump[x][0]]) {
                if (mp.find(
                    (
                        (
                            up[pos] - 
                            (powr[max_len - lvl[pos] - 1] * up[jump[x][0]]) 
                            % MOD + MOD
                        ) % MOD + 
                        powr[max_len - lvl[pos] - 1] * 
                        s[centroid]) % MOD) != mp.end()
                    ) {
                    fine ++;
                    return;
                }
            }
        }
    }
 
    bool solve (int pos) {
        dfsCentroid(pos, pos);
        centroid = getCentroid(pos, pos);
        hasVisited[centroid] = true;
        lvl[centroid] = 0;
        for (int i = 0; i < 17; i++) jump[centroid][i] = centroid;
        up[centroid] = s[centroid], down[centroid] = s[centroid];
        fine += (max_len == 1);
        for (int i: paths[centroid]) {
            if (!hasVisited[i]) {
                fill(i, centroid, 1, s[centroid], s[centroid]);
            }
        }
        mp.clear();
        for (int i: paths[centroid]) {
            if (!hasVisited[i]) {
                dfs (i, centroid);
                for (int j: to_do) mp[j] = 1;
                to_do.clear();
            }
        }
        if (fine) return true;
        reverse(paths[centroid].begin(), paths[centroid].end());
        mp.clear();
        for (int i: paths[centroid]) {
            if (!hasVisited[i]) {
                dfs (i, centroid);
                for (int j: to_do) mp[j] = 1;
                to_do.clear();
            }
        }
        if (fine) return true;
        for (int i: paths[centroid]) {
            if (!hasVisited[i]) {
                if (solve(i)) {
                    return true;
                }
            }
        }
        return false;
    }
    Tree (int n) {
        sizes.resize(n);
        jump.resize(n), lvl.resize(n), up.resize(n), down.resize(n);
        hasVisited.assign(n, false); 
        powr.push_back(1); 
        for (int i = 0; i <= n + 5; i++) powr.push_back(powr.back() * BASE%MOD);
        ipowr.push_back(1); 
        for (int i = 0; i <= n + 5; i++) ipowr.push_back(ipowr.back() * inv%MOD);
    }
};
 
int main() {
#ifndef ONLINE_JUDGE
    freopen("0.in", "r", stdin);
    freopen("1.out", "w", stdout);
#endif
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n; cin >> n;
    string s; cin >> s;
    Tree myTree(n);
    for (int i = 0; i < n - 1; i++) {
        int u, v;
        cin >> u >> v;
        u--, v--;
        myTree.paths[u].push_back(v), myTree.paths[v].push_back(u);
    }
    myTree.s = s;
    int myMax = 0;
    int l = 0;
    int r = s.length()/2;
    while (l != r) {
        int m = (l + r + 1)/2;
        myTree.max_len = 2 * m; myTree.fine = 0; myTree.hasVisited.assign(n, false);
        myTree.solve(0);
        if (myTree.fine) {
            l = m;
        } else {
            r = m - 1;
        }
    }
    myMax = max(myMax, 2 * l); l = 0;
    r = s.length()/2;
    while (l < r) {
        int m = (l + r + 1)/2;
        myTree.max_len = 2 * m + 1; myTree.fine = 0; myTree.hasVisited.assign(n, false);
        myTree.solve(0);
        if (myTree.fine) {
            l = m;
        } else {
            r = m - 1;
        }
    }
    myMax = max(myMax, 2 * l + 1);
    cout << myMax;
}