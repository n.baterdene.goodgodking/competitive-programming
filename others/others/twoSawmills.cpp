#include <bits/stdc++.h>
#define ff first
#define ss second
using namespace std;
using ll = long long;
using PII = pair <ll, ll>;
const int N = 2e4 + 5;

int n, m;
ll w[N], x[N], remain[N], p[N], s[N], q[N], ans;
ll getVal(PII line, int pos) {
	return pos * line.ff + line.ss;
}
long double intersectX(PII a, PII b) {
	return 1.0 * (b.ss - a.ss) / (a.ff - b.ff);
}
/*
	ax + b = px + q;
	x  = (q - b) / (a - p)
*/
signed main() {
	// freopen("0.in", "r", stdin);
	// freopen("0.out", "w", stdout);
	ios::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	cin >> n;
	for (int i = 1; i <= n; i++) {
		cin >> w[i] >> x[i];
	}
	for (int i = n, d = 0; i > 0; i--) {
		d += x[i];
		// cout << d << ',' << w[i] << " \n"[i==1];
		remain[i] = remain[i+1] + d * w[i];
	}
	q[0] = w[0];
	for (int i = 1, d = 0; i <= n; i++) {
		p[i] = p[i-1] + x[i-1];
		q[i] = q[i-1] + w[i];
		s[i] = s[i-1] + q[i-1] * x[i-1];
	}
	/*
		dp[i] = Min[j < i]( S[i] - q[j+1] * (p[i] - p[j+1]) )
		dp[i] = Min[j < i] S[i] + ( - q[j+1] * p[i] +  q[j+1] * p[j+1] )
		slope: -q[j+1], const: q[j+1] * p[j+1];
	*/
	deque <PII> dq;
	dq.push_back({0, 0});
	PII last;
	ans = remain[1];
	for (int i = 1; i <= n; i++) {
		while (dq.size() > 1 && getVal(dq[0], p[i]) >= getVal(dq[1], p[i])) dq.pop_front();
		ll cur = getVal(dq[0], p[i]) + s[i];
		if (i < 3) cur = 0;
		ans = min(ans, cur + remain[i+1]);
		// cout << i << ": " << cur << ',' << remain[i+1] << " -> " << cur + remain[i+1] << "\n";
		while(dq.size() > 1 && intersectX(dq.back(), dq[dq.size()-2]) >= intersectX(dq.back(), last) ) dq.pop_back();
		dq.push_back(last);
		last = {-q[i+1], q[i+1]*p[i+1]};
	}
	cout << ans << '\n';
}