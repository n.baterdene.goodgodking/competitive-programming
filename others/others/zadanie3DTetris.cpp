#include <bits/stdc++.h>
#define LINE "------------------------\n"
using namespace std;
template <typename T> bool chkmin(T &x,T y){return x>y?x=y,1:0;}
template <typename T> bool chkmax(T &x,T y){return x<y?x=y,1:0;}
const int N = 1000;
const int M = 2048;
int n, m, k, q;
struct SegTree1D {
    int node[M], lazy[M], L, R, val;
    void push(int head) {
        if (!lazy[head]) return;
        int x = lazy[head]; lazy[head] = 0;
        chkmax(node[head*2+1], x);
        chkmax(lazy[head*2+1], x);
        chkmax(node[head*2+2], x);
        chkmax(lazy[head*2+2], x);
    }
    void update(int l, int r, int val, int head) {
        if (l > R || L > r) return;
        if (L <= l && r <= R) {
            chkmax(node[head], val);
            chkmax(lazy[head], val);
            return;
        }
        push(head);
        int mid = (l+r)>>1;
        update(l, mid, val, head*2+1);
        update(mid+1, r, val, head*2+2);
        node[head] = max({node[head], node[head*2+1], node[head*2+2]});
    }
    int query(int l, int r, int head) {
        if (l > R || L > r) return 0;
        if (L <= l && r <= R) return node[head];
        push(head);
        int mid = (l+r)>>1;
        return max(
            query(l, mid, head*2+1),
            query(mid+1, r, head*2+2)
        );
    }
    void update(int l, int r, int _val) {
        L = l, R = r, val = _val;
        return update(0, N, val, 0);
    }
    int query(int l, int r) {
        L = l, R = r;
        return query(0, N, 0);
    }
};
struct SegTree2D {
    SegTree1D node[M], node1[M]; // node outside, node1 inside
    int l1, r1, L, R, val;
    void update(int l, int r, int head) {
        if (l > R || L > r) return;
        if (L <= l && r <= R) {
            node[head].update(l1, r1, val);
            node1[head].update(l1, r1, val);
            return;
        }
        int mid = (l+r)>>1;
        update(l, mid, head*2+1);
        update(mid+1, r, head*2+2);
        node1[head].update(l1, r1, val);
    }
    int query(int l, int r, int head) {
        if (l > R || L > r) return 0;
        if (L <= l && r <= R) return node1[head].query(l1, r1);
        int mid = (l+r)>>1;
        return max({
            node[head].query(l1, r1),
            query(l, mid, head*2+1),
            query(mid+1, r, head*2+2)
        });
    }
    void update(int l, int r, int _l1, int _r1, int _val) {
        L = l, R = r, l1 = _l1, r1 = _r1, val = _val;
        return update(0, N, 0);
    }
    int query(int l, int r, int _l1, int _r1) {
        L = l, R = r, l1 = _l1, r1 = _r1;
        return query(0, N, 0);
    }
} segTree;
signed main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    // freopen("0.in", "r", stdin);
    // freopen("0.out", "w", stdout);
    cin >> n >> m >> q;
    for (int t = 1; t <= q; t++) {
        int x, y, z, a, b;
        cin >> x >> y >> z >> a >> b;
        x += a-1;
        y += b-1;
        segTree.update(a, x, b, y, segTree.query(a, x, b, y)+z);    
    }
    cout << segTree.query(0, N, 0, N) << '\n';
}